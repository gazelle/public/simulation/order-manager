package net.ihe.gazelle.ordermanager.test;

import java.io.File;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.dcm4che.data.Attributes;
import org.dcm4che.data.Tag;
import org.dcm4che.data.UID;
import org.dcm4che.io.DicomInputStream;
import org.dcm4che.io.DicomOutputStream;
import org.dcm4che.net.pdu.AAssociateRQ;
import org.dcm4che.net.pdu.PresentationContext;
import org.dcm4che.net.ApplicationEntity;
import org.dcm4che.net.Association;
import org.dcm4che.net.Connection;
import org.dcm4che.net.Device;
import org.dcm4che.net.DimseRSPHandler;
import org.dcm4che.net.IncompatibleConnectionException;
import org.dcm4che.net.Status;

public class TestMPPSScu {
	
	private static final class MppsWithIUID {
        String iuid;
        Attributes mpps;
        MppsWithIUID(String iuid, Attributes mpps) {
            this.iuid = iuid;
            this.mpps = mpps;
        }
    }

	public static void main(String[] args) throws IOException, InterruptedException, IncompatibleConnectionException,
			GeneralSecurityException {
		ApplicationEntity ae = new ApplicationEntity("MAKI");
		AAssociateRQ rq = new AAssociateRQ();
		Device device = new Device("mppsscu");
		Connection remote = new Connection();
		Connection connection = new Connection();
		device.addConnection(connection);
		device.addApplicationEntity(ae);
		ae.addConnection(connection);
		
		// configure connection
		rq.setCalledAET("CENTRAL_ARCHIVE");
		remote.setHostname("192.168.20.52");
		remote.setPort(11112);
		
		// configure bind
		connection.setHostname("127.0.0.1");
		connection.setPort(13456);
		
		// configure transfer syntaxes
		
		rq.addPresentationContext(new PresentationContext(1, UID.VerificationSOPClass,
				UID.ImplicitVRLittleEndian));
		rq.addPresentationContext(new PresentationContext(3, UID.ModalityPerformedProcedureStepSOPClass,
				UID.ImplicitVRLittleEndian, UID.ExplicitVRBigEndian));
		rq.addPresentationContext(new PresentationContext(2, UID.ModalityWorklistInformationModelFIND,
				UID.ImplicitVRLittleEndian, UID.ExplicitVRBigEndian));
		ExecutorService executorService = Executors.newSingleThreadExecutor();
		device.setExecutor(executorService);
		Association association = null;
		try {
			association = ae.connect(remote, rq);
			association.cecho().next();
			DimseRSPHandler rspHandler = new DimseRSPHandler(association.nextMessageID()) {
				
	            @Override
	            public void onDimseRSP(Association as, Attributes cmd, Attributes data) {
	            	String iuid = "1";
	                switch(cmd.getInt(Tag.Status, -1)) {
	                    case Status.Success:
	                    case Status.AttributeListError:
	                    case Status.AttributeValueOutOfRange:
	                        System.out.println("AffectedSOPInstanceUID: " + cmd.getString(
	                                Tag.AffectedSOPInstanceUID, "MINE"));
	                }
	                System.out.println("Status: " + cmd.getInt(Tag.Status, -1));
	                System.out.println("AffectedSOPInstanceUID: " + cmd.getString(
                            Tag.AffectedSOPInstanceUID, "MINE"));
	                super.onDimseRSP(as, cmd, data);
	            }
	        };
//	        DicomInputStream dis = new DicomInputStream(new File("/home/aberge/Téléchargements/createMPPS.dcm"));
	        DicomInputStream dis = new DicomInputStream(new File("/opt/mpps/test.dcm"));
//	        Attributes mpps = dis.readFileMetaInformation();
	        dis.readFileMetaInformation();
	        Attributes mpps = dis.readDataset(dis.available(), -1);
	        dis.close();
			association.nset(UID.ModalityPerformedProcedureStepSOPClass, "1", mpps, UID.ImplicitVRLittleEndian, rspHandler);
//	        association.ncreate(UID.ModalityPerformedProcedureStepSOPClass, "1", mpps, UID.ImplicitVRLittleEndian, rspHandler);
		} catch (Throwable t) {
			t.printStackTrace();
		} finally {
			if (association != null) {
				association.waitForOutstandingRSP();
				association.release();
				association.waitForSocketClose();
			}
		}

	}
}