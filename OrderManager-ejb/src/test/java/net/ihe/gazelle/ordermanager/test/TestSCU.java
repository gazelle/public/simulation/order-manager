package net.ihe.gazelle.ordermanager.test;

import java.io.IOException;

import net.ihe.gazelle.ordermanager.mwl.scu.MyIdentifierHandler;

import com.pixelmed.dicom.Attribute;
import com.pixelmed.dicom.AttributeList;
import com.pixelmed.dicom.AttributeTag;
import com.pixelmed.dicom.DicomException;
import com.pixelmed.dicom.PersonNameAttribute;
import com.pixelmed.dicom.ShortStringAttribute;
import com.pixelmed.dicom.TagFromName;
import com.pixelmed.network.FindSOPClassSCU;

public class TestSCU {

	/**
	 * @param args
	 * @throws IOException
	 * @throws DicomException
	 */
	public static void main(String[] args) throws DicomException, IOException {
		String host = "127.0.0.1";
		int port = 10002;
		String callingAETitle = "JAVA_TEST";
		String calledAETitle = "RAD_OF";
		String modalityWorklistFindSopClass = "1.2.840.10008.5.1.4.31";
		AttributeList attributes = new AttributeList();
		int debugLevel = 0;
		
		try {
			AttributeTag patientName = TagFromName.PatientName;
			Attribute patientNameValue = new PersonNameAttribute(patientName);
			patientNameValue.addValue("MOD_HMC_MR^Simple2");
			attributes.put(patientName, patientNameValue);

			AttributeTag accessionNumber = TagFromName.AccessionNumber;
			attributes.put(accessionNumber, new ShortStringAttribute(accessionNumber));
			
			new FindSOPClassSCU(host, port, calledAETitle, callingAETitle, modalityWorklistFindSopClass, attributes,
					new MyIdentifierHandler(), debugLevel);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
}
