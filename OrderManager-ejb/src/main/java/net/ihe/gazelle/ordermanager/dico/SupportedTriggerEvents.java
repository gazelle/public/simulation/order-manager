package net.ihe.gazelle.ordermanager.dico;

/**
 * <b>Class Description : </b>SupportedTriggerEvents<br>
 * <br>
 *
 * @author Anne-Gaelle Berge / IHE-Europe development Project
 * @version 1.0 - 12/10/16
 */
public final class SupportedTriggerEvents {

    /** Constant <code>O33="O33"</code> */
    public static final String O33 = "O33";

    private SupportedTriggerEvents(){

    }

    /** Constant <code>R22="R22"</code> */
    public static final String R22 = "R22";
}
