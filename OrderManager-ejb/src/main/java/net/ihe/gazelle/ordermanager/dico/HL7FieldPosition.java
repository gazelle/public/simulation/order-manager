package net.ihe.gazelle.ordermanager.dico;

/**
 * <b>Class Description : </b>HL7FieldPosition<br>
 * <br>
 *
 * @author Anne-Gaelle Berge / IHE-Europe development Project
 * @version 1.0 - 16/09/16
 *
 *
 *
 */
public final class HL7FieldPosition {

    private HL7FieldPosition(){

    }

    public final static int FIRST_REP = 0;
    public final static int PATIENT_NAME = 5;
    public final static int LAST_NAME = 1;
    public final static int ALT_NAME_REP = 1;
    public final static int FIRST_NAME = 2;
    public final static int VALUE = 1;
    public final static int MOTHER_MAIDEN_NAME = 6;
    public final static int DATE_OF_BIRTH = 7;
    public final static int TS_DATE_TIME = 1;
    public final static int GENDER_CODE = 8;
    public static final int FIRST_COMP = 1;
    public static final int CX_ASSIGNING_AUTH = 4;
    public static final int HD_NAME = 1;
    public static final int HD_ID = 2;
    public static final int HD_ID_TYPE = 3;
    public static final int CX_ID_TYPE = 5;

}
