package net.ihe.gazelle.ordermanager.hl7.initiators;

import net.ihe.gazelle.ordermanager.filter.AbstractOrderFilter;
import net.ihe.gazelle.ordermanager.model.AbstractOrder;
import net.ihe.gazelle.ordermanager.model.Encounter;
import net.ihe.gazelle.ordermanager.model.Patient;

import javax.faces.model.SelectItem;
import java.util.List;

public abstract class AbstractOrderSender<T extends AbstractOrder> extends AbstractSender {

	public static final String EYE_KEYWORD = "EYE";
	public static final String OP_KEYWORD = "OP";
	public static final String OF_KEYWORD = "OF";
	public static final String ANALYZER_MGR_KEYWORD = "ANALYZER_MGR";
	public static final String ANALYZER = "ANALYZER";
	public static final String AM_KEYWORD = "AM";
	public static final String NEW = "gazelle.ordermanager.createNewOrder";
	public static final String CANCEL = "gazelle.ordermanager.cancelOrder";
	public static final String UPDATE = "gazelle.ordermanager.updateOrder";
	public static final String DISCONTINUE = "gazelle.ordermanager.discontinueOrder";
	public static final String REPLACE = "gazelle.ordermanager.replaceOrder";
	public static final String TRO_FULFILLER = "TRO_FULFILLER";
	public static final String TRO_CREATOR = "TRO_CREATOR";
	public static final String TRO_FORWARDER = "TRO_FORWARDER";

	protected String orderControlCode;
	protected String newOrderStatus;
	protected String newResultStatus;
	protected boolean workOrder;
    protected AbstractOrderFilter<T> orderFilter;
    protected T selectedOrder;
	/**
	 * This method creates a new order for the selected encounter/patient
	 */
	public abstract void createANewOrder();

	/**
	 * this method is called when the user hit the "filter" button, you are expected here to set the appropriate DataModel
	 */

	@Override
	public void initializePage() {
		super.initializePage();
		// get parameters from the URL
		if (urlParams != null) {
			// lab order and work order are simular, we will use the same page but we need to know in which case we are
			if (!urlParams.containsKey("workOrder") || urlParams.get("workOrder").equalsIgnoreCase("false")) {
				workOrder = false;
			} else {
				workOrder = true;
			}
			// get the simulated actor keyword and set the sut actor
			if (simulatedActor.getKeyword().equals(OP_KEYWORD)) {
				sutActorKeyword = OF_KEYWORD;
			} else if (workOrder) {
				sutActorKeyword = AM_KEYWORD;
			} else if (simulatedActor.getKeyword().equals(ANALYZER_MGR_KEYWORD)) {
				sutActorKeyword = ANALYZER;
			} else if (simulatedActor.getKeyword().equals(TRO_CREATOR)){
				sutActorKeyword = TRO_FULFILLER;
			} else if (simulatedActor.getKeyword().equals(TRO_FULFILLER)){
				sutActorKeyword = TRO_CREATOR;
			} else {
				sutActorKeyword = OP_KEYWORD;
			}
		}
		orderControlCode = NEW;
		onChangeActionEvent();
	}

	@Override
	public void selectEncounter(Encounter inEncounter) {
		if (inEncounter != null) {
			super.selectEncounter(inEncounter);
		} else {
			super.setSelectedEncounter(null);
		}
		createANewOrder();
	}

	@Override
	public void skipPatientSelection() {
		selectEncounter(null);
		selectPatient = false;
	}

	/**
	 * 
	 * @return list of actions
	 */
	@Override
	public abstract List<SelectItem> listAvailableActions();

	/**
	 * create new patient, new encounter and instanciate new order
	 */
	@Override
	public void createNewPatientAndEncounter() {
		super.createNewPatientAndEncounter();
		createANewOrder();
	}

	/**
	 * 
	 * @param inPatient
	 */
	@Override
	public void createANewEncounterForPatient(Patient inPatient) {
		super.createANewEncounterForPatient(inPatient);
		createANewOrder();
	}

	public abstract List<SelectItem> getAvailableOrderStatuses();

	public String getEyeKeyword() {
		return EYE_KEYWORD;
	}

	public String getOpKeyword() {
		return OP_KEYWORD;
	}

	public String getOfKeyword() {
		return OF_KEYWORD;
	}

	public String getNew() {
		return NEW;
	}

	public String getCancel() {
		return CANCEL;
	}

	public String getUpdate() {
		return UPDATE;
	}

	public String getDiscontinue() {
		return DISCONTINUE;
	}

	public String getReplace() {
		return REPLACE;
	}

	public String getOrderControlCode() {
		return orderControlCode;
	}

	public void setOrderControlCode(String orderControlCode) {
		this.orderControlCode = orderControlCode;
	}

	public void setNewOrderStatus(String newOrderStatus) {
		this.newOrderStatus = newOrderStatus;
	}

	public String getNewOrderStatus() {
		return newOrderStatus;
	}

	public boolean isWorkOrder() {
		return workOrder;
	}

	public void setWorkOrder(boolean workOrder) {
		this.workOrder = workOrder;
	}

	public String getNewResultStatus() {
		return newResultStatus;
	}

	public void setNewResultStatus(String newResultStatus) {
		this.newResultStatus = newResultStatus;
	}

    public AbstractOrderFilter<T> getOrderFilter() {
        return orderFilter;
    }

    public T getSelectedOrder() {
        return selectedOrder;
    }

    public void setSelectedOrder(T selectedOrder) {
        if (selectedOrder == null) {
            this.newOrderStatus = null;
        }
        this.selectedOrder = selectedOrder;
    }
}
