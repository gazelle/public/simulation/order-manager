package net.ihe.gazelle.ordermanager.datamodel;

import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.restrictions.HQLRestrictionLikeMatchMode;
import net.ihe.gazelle.hql.restrictions.HQLRestrictions;
import net.ihe.gazelle.ordermanager.model.Encounter;
import net.ihe.gazelle.ordermanager.model.TeleRadiologyOrder;
import net.ihe.gazelle.ordermanager.model.UserPreferences;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.common.tf.model.Domain;
import org.jboss.seam.Component;
import org.jboss.seam.security.Identity;

import java.util.Date;

/**
 * Class description : <b>OrderDataModel</b>
 * 
 * This class is a data model for properly and quickly displaying the order objects in the GUI
 * 
 *
 *
 * @author Anne-Gaëlle Bergé - INRIA Rennes/Bretagne Atlantique
 *
 * @version 1.0 - 2011, November 17th
 * 
 */
public class TeleRadiologyOrderDataModel extends FilterDataModel<TeleRadiologyOrder> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5565609163400800733L;
	private String creator;
	private Date startDate;
	private Date endDate;
	private String placerNumber;
	private String fillerNumber;
	private Domain domain;
	private Actor simulatedActor;
	private String newOrderControlCode;
	private Encounter encounter;
	private String searchedVisitNumber;
	private String searchedPatientId;

	public TeleRadiologyOrderDataModel() {
		super(new TeleRadiologyOrderFilter());
	}

	public TeleRadiologyOrderDataModel(Date startDate, Date endDate, String placerNumber, String fillerNumber,
			Domain domain, Actor simulatedActor, String patientId, String visitNumber, String newOrderControlCode) {
		super(new TeleRadiologyOrderFilter());
		this.creator = null;
		if (Identity.instance().isLoggedIn()) {
			UserPreferences preferences = (UserPreferences) Component.getInstance("userPreferences");
			if (preferences.isViewOnlyMyObjects()) {
				this.creator = preferences.getUsername();
			}
		}
		setStartDate(startDate);
		setEndDate(endDate);
		this.placerNumber = placerNumber;
		this.fillerNumber = fillerNumber;
		this.domain = domain;
		this.simulatedActor = simulatedActor;
		this.searchedVisitNumber = visitNumber;
		this.searchedPatientId = patientId;
		this.newOrderControlCode = newOrderControlCode;
	}

	public TeleRadiologyOrderDataModel(Encounter encounter) {
		super(new TeleRadiologyOrderFilter());
		this.encounter = encounter;
	}

	@Override
	public void appendFiltersFields(HQLQueryBuilder<TeleRadiologyOrder> hqlBuilder) {
		if ((creator != null) && !creator.isEmpty()) {
			hqlBuilder.addEq("creator", creator);
		}
		if (startDate != null) {
			hqlBuilder.addRestriction(HQLRestrictions.gt("lastChanged", startDate));
		}
		if (endDate != null) {
			hqlBuilder.addRestriction(HQLRestrictions.lt("lastChanged", endDate));
		}
		if ((placerNumber != null) && !placerNumber.isEmpty()) {
			hqlBuilder.addRestriction(HQLRestrictions.like("placerOrderNumber", placerNumber,
					HQLRestrictionLikeMatchMode.ANYWHERE));
		}
		if ((fillerNumber != null) && !fillerNumber.isEmpty()) {
			hqlBuilder.addRestriction(HQLRestrictions.like("fillerOrderNumber", fillerNumber,
					HQLRestrictionLikeMatchMode.ANYWHERE));
		}
		if (domain != null) {
			hqlBuilder.addEq("domain", domain);
		}
		if (simulatedActor != null) {
			hqlBuilder.addEq("simulatedActor", simulatedActor);
		}
		if ((newOrderControlCode != null) && !newOrderControlCode.isEmpty()) {
			// creator cancels order
			if (newOrderControlCode.equals("OC")) {
				hqlBuilder.addEq("orderControlCode", "NW");
			}
			// order filler updates order status
			else if (newOrderControlCode.equals("SC")) {
				hqlBuilder.addRestriction(HQLRestrictions.or(HQLRestrictions.eq("orderControlCode", "NW"),
						HQLRestrictions.eq("orderStatus", "SC")));
			}
		}
		if (encounter != null) {
			hqlBuilder.addEq("encounter", encounter);
		}
		if ((searchedVisitNumber != null) && !searchedVisitNumber.isEmpty()) {
			hqlBuilder.addRestriction(HQLRestrictions.like("encounter.visitNumber", searchedVisitNumber,
					HQLRestrictionLikeMatchMode.ANYWHERE));
		}
		if ((searchedPatientId != null) && !searchedPatientId.isEmpty()) {
			hqlBuilder.addRestriction(HQLRestrictions.like("encounter.patient.patientIdentifiers.identifier",
					searchedPatientId, HQLRestrictionLikeMatchMode.ANYWHERE));
		}
	}

	public String getCreator() {
		return creator;
	}

	public void setCreator(String creator) {
		this.creator = creator;
	}

    public Date getStartDate() {
        if (this.startDate != null) {
            return (Date) startDate.clone();
        } else {
            return null;
        }
    }

    public final void setStartDate(Date startDate) {
        if (startDate != null) {
            this.startDate = (Date) startDate.clone();
        }else {
            this.startDate = null;
        }
    }

    public Date getEndDate() {
        if (this.endDate != null) {
            return (Date) endDate.clone();
        } else {
            return null;
        }
    }

    public final void setEndDate(Date endDate) {
        if (endDate != null) {
            this.endDate = (Date) endDate.clone();
        } else {
            this.endDate = null;
        }
    }

	public String getPlacerNumber() {
		return placerNumber;
	}

	public void setPlacerNumber(String placerNumber) {
		this.placerNumber = placerNumber;
	}

	public String getFillerNumber() {
		return fillerNumber;
	}

	public void setFillerNumber(String fillerNumber) {
		this.fillerNumber = fillerNumber;
	}

	public Domain getDomain() {
		return domain;
	}

	public void setDomain(Domain domain) {
		this.domain = domain;
	}

	public Actor getSimulatedActor() {
		return simulatedActor;
	}

	public void setSimulatedActor(Actor simulatedActor) {
		this.simulatedActor = simulatedActor;
	}

	/**
	 * @param orderControlCode
	 *            the orderControlCode to set
	 */
	public void setNewOrderControlCode(String orderControlCode) {
		this.newOrderControlCode = orderControlCode;
	}

	/**
	 * @return the orderControlCode
	 */
	public String getNewOrderControlCode() {
		return newOrderControlCode;
	}

	/**
	 * @param encounter
	 *            the encounter to set
	 */
	public void setEncounter(Encounter encounter) {
		this.encounter = encounter;
	}

	/**
	 * @return the encounter
	 */
	public Encounter getEncounter() {
		return encounter;
	}

	/**
	 * @param searchedVisitNumber
	 *            the searchedVisitNumber to set
	 */
	public void setSearchedVisitNumber(String searchedVisitNumber) {
		this.searchedVisitNumber = searchedVisitNumber;
	}

	/**
	 * @return the searchedVisitNumber
	 */
	public String getSearchedVisitNumber() {
		return searchedVisitNumber;
	}

	public void setSearchedPatientId(String searchedPatientId) {
		this.searchedPatientId = searchedPatientId;
	}

	public String getSearchedPatientId() {
		return searchedPatientId;
	}
@Override
        protected Object getId(TeleRadiologyOrder t) {
        // TODO Auto-generated method stub
        return t.getId();
        }
}
