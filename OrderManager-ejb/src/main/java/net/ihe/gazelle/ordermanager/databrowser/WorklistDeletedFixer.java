package net.ihe.gazelle.ordermanager.databrowser;

import net.ihe.gazelle.hql.criterion.ValueProvider;
import net.ihe.gazelle.simulator.common.action.ApplicationConfigurationManagerLocal;
import org.jboss.seam.Component;

public class WorklistDeletedFixer implements ValueProvider {

	public static final WorklistDeletedFixer INSTANCE = new WorklistDeletedFixer();

	@Override
	public Object getValue() {
		ApplicationConfigurationManagerLocal manager = (ApplicationConfigurationManagerLocal) Component
				.getInstance("applicationConfigurationManager");
		if (manager.isUserAllowedAsAdmin()) {
			return null;
		} else {
			return false;
		}
	}

}
