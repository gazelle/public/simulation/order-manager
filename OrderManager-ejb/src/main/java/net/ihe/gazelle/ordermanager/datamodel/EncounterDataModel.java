package net.ihe.gazelle.ordermanager.datamodel;

import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.restrictions.HQLRestrictionLikeMatchMode;
import net.ihe.gazelle.hql.restrictions.HQLRestrictions;
import net.ihe.gazelle.ordermanager.model.Encounter;
import net.ihe.gazelle.ordermanager.model.Patient;
import net.ihe.gazelle.ordermanager.model.UserPreferences;
import org.jboss.seam.Component;
import org.jboss.seam.security.Identity;

import java.util.Date;

/**
 * Class description : <b>EncounterDataModel</b>
 * 
 * This class is a data model for properly and quickly displaying the encounter objects in the GUI
 * 
 *
 *
 * @author Anne-Gaëlle Bergé - INRIA Rennes/Bretagne Atlantique
 *
 * @version 1.0 - 2011, November 17th
 * 
 */
public class EncounterDataModel extends FilterDataModel<Encounter> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 932007061812399231L;
	private String visitNumber;
	private String creator;
	private Date startDate;
	private Date endDate;
	private String patientId;
	private Patient patient;

	public EncounterDataModel() {
		super(new EncounterFilter());
	}

	public EncounterDataModel(String visitNumber, Date startDate, Date endDate, String patientId) {
		super(new EncounterFilter());
		this.creator = null;
		if (Identity.instance().isLoggedIn()) {
			UserPreferences preferences = (UserPreferences) Component.getInstance("userPreferences");
			if (preferences.isViewOnlyMyObjects()) {
				this.creator = preferences.getUsername();
			}
		}
		setStartDate(startDate);
		setEndDate(endDate);
		this.visitNumber = visitNumber;
		this.patientId = patientId;
		this.patient = null;
	}

	public EncounterDataModel(Patient patient) {
		super(new EncounterFilter());
		this.patient = patient;
		this.startDate = null;
		this.endDate = null;
		this.patientId = null;
		this.visitNumber = null;
	}

	@Override
	public void appendFiltersFields(HQLQueryBuilder<Encounter> hqlBuilder) {
		if ((visitNumber != null) && !visitNumber.isEmpty()) {
			hqlBuilder.addRestriction(HQLRestrictions.like("visitNumber", visitNumber,
					HQLRestrictionLikeMatchMode.ANYWHERE));
		}
		if ((creator != null) && !creator.isEmpty()) {
			hqlBuilder.addEq("creator", creator);
		}
		if (startDate != null) {
			hqlBuilder.addRestriction(HQLRestrictions.gt("creationDate", startDate));
		}
		if (endDate != null) {
			hqlBuilder.addRestriction(HQLRestrictions.lt("creationDate", endDate));
		}
		if ((patientId != null) && !patientId.isEmpty()) {
			hqlBuilder.addRestriction(HQLRestrictions.like("patient.patientIdentifiers.identifier", patientId,
					HQLRestrictionLikeMatchMode.ANYWHERE));
		}
		if (patient != null) {
			hqlBuilder.addEq("patient", patient);
		}
	}

	public String getVisitNumber() {
		return visitNumber;
	}

	public void setVisitNumber(String visitNumber) {
		this.visitNumber = visitNumber;
	}

	public String getCreator() {
		return creator;
	}

	public void setCreator(String creator) {
		this.creator = creator;
	}

    public Date getStartDate() {
        if (this.startDate != null) {
            return (Date) startDate.clone();
        } else {
            return null;
        }
    }

    public final void setStartDate(Date startDate) {
        if (startDate != null) {
            this.startDate = (Date) startDate.clone();
        }else {
            this.startDate = null;
        }
    }

    public Date getEndDate() {
        if (this.endDate != null) {
            return (Date) endDate.clone();
        } else {
            return null;
        }
    }

    public final void setEndDate(Date endDate) {
        if (endDate != null) {
            this.endDate = (Date) endDate.clone();
        } else {
            this.endDate = null;
        }
    }


    /**
	 * @param patientId
	 *            the patientId to set
	 */
	public void setPatientId(String patientId) {
		this.patientId = patientId;
	}

	/**
	 * @return the patientId
	 */
	public String getPatientId() {
		return patientId;
	}

	/**
	 * @param patient
	 *            the patient to set
	 */
	public void setPatient(Patient patient) {
		this.patient = patient;
	}

	/**
	 * @return the patient
	 */
	public Patient getPatient() {
		return patient;
	}

@Override
        protected Object getId(Encounter t) {
        // TODO Auto-generated method stub
        return t.getId();
        }
}
