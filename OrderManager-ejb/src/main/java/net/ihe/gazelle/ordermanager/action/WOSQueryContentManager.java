package net.ihe.gazelle.ordermanager.action;

import ca.uhn.hl7v2.model.Message;
import ca.uhn.hl7v2.parser.PipeParser;
import net.ihe.gazelle.HL7Common.initiator.Initiator;
import net.ihe.gazelle.HL7Common.model.HL7V2ResponderSUTConfiguration;
import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import net.ihe.gazelle.ordermanager.model.LabOrder;
import net.ihe.gazelle.ordermanager.model.WOSQueryContent;
import net.ihe.gazelle.ordermanager.model.WOSQueryContentQuery;
import net.ihe.gazelle.ordermanager.utils.LabMessageBuilder;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;
import net.ihe.gazelle.simulator.message.model.TransactionInstance;
import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;

/**
 * <p>WOSQueryContentManager class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
@Name("wosQueryContentManager")
@Scope(ScopeType.PAGE)
public class WOSQueryContentManager implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3474693534758890526L;

	private static Logger log = LoggerFactory.getLogger(WOSQueryContentManager.class);

	private Filter<WOSQueryContent> filter;
	private FilterDataModel<WOSQueryContent> queries;
	private WOSQueryContent selectedQuery;
	private boolean displayListOfSut = false;
	private HL7V2ResponderSUTConfiguration sut;

	/**
	 * <p>Getter for the field <code>filter</code>.</p>
	 *
	 * @return a {@link net.ihe.gazelle.common.filter.Filter} object.
	 */
	public Filter<WOSQueryContent> getFilter() {
		if (this.filter == null) {
			this.filter = new Filter<WOSQueryContent>(getHQLCriterionsForFilter());
		}
		return this.filter;
	}

	private HQLCriterionsForFilter<WOSQueryContent> getHQLCriterionsForFilter() {
		WOSQueryContentQuery query = new WOSQueryContentQuery();
		HQLCriterionsForFilter<WOSQueryContent> criterions = query.getHQLCriterionsForFilter();
		criterions.addPath("timestamp", query.timestamp());
		criterions.addPath("requester", query.requester());
		criterions.addPath("queryType", query.queryType());
		return criterions;
	}

	/**
	 * <p>reset.</p>
	 */
	public void reset() {
		this.filter.clear();
		this.queries.getFilter().clear();
	}

	/**
	 * <p>Getter for the field <code>selectedQuery</code>.</p>
	 *
	 * @return a {@link net.ihe.gazelle.ordermanager.model.WOSQueryContent} object.
	 */
	public WOSQueryContent getSelectedQuery() {
		return selectedQuery;
	}

	/**
	 * <p>displayAWOS.</p>
	 *
	 * @param inOrder a {@link net.ihe.gazelle.ordermanager.model.LabOrder} object.
	 * @return a {@link java.lang.String} object.
	 */
	public String displayAWOS(LabOrder inOrder) {
		return "/browsing/labOrder.seam?domain=LAB&id=" + inOrder.getId();
	}

	/**
	 * <p>Setter for the field <code>selectedQuery</code>.</p>
	 *
	 * @param selectedQuery a {@link net.ihe.gazelle.ordermanager.model.WOSQueryContent} object.
	 */
	public void setSelectedQuery(WOSQueryContent selectedQuery) {
		this.selectedQuery = selectedQuery;
	}

	/**
	 * <p>Getter for the field <code>queries</code>.</p>
	 *
	 * @return a {@link net.ihe.gazelle.common.filter.FilterDataModel} object.
	 */
	public FilterDataModel<WOSQueryContent> getQueries() {
		if (this.queries == null) {
			queries = new FilterDataModel<WOSQueryContent>(getFilter()){
                    @Override
        protected Object getId(WOSQueryContent t) {
        // TODO Auto-generated method stub
        return t.getId();
        }
        };
		}
		return this.queries;
	}

	/**
	 * <p>updateMatchingOrders.</p>
	 *
	 * @param inQueryContent a {@link net.ihe.gazelle.ordermanager.model.WOSQueryContent} object.
	 */
	public void updateMatchingOrders(WOSQueryContent inQueryContent) {
		inQueryContent.updateMatchingOrders();
		selectedQuery = inQueryContent;
	}

	/**
	 * <p>createAWOSForQuery.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String createAWOSForQuery() {
		if ((selectedQuery != null) && !selectedQuery.getQueryType().equals("WOS_ALL")) {
			return "/hl7Initiators/awosSender.seam?workOrder=false&actor=ANALYZER_MGR&domain=LAB&wosQuery="
					+ selectedQuery.getId();
		} else {
			return null;
		}
	}

	/**
	 * <p>sendNegativeQueryResponse.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String sendNegativeQueryResponse() {
		if (sut != null) {
			try {
				Message omlMessage = LabMessageBuilder.buildNegativeQueryResponse(sut, selectedQuery);
				PipeParser parser = PipeParser.getInstanceWithNoValidation();
				String messageStringToSend = parser.encode(omlMessage);
				Initiator hl7Initiator = new Initiator(sut, "IHE", "OM_LAB_ANALYZER_MGR",
						Actor.findActorWithKeyword("ANALYZER_MGR"), Transaction.GetTransactionByKeyword("LAB-28"),
						messageStringToSend, "OML^O33^OML_O33 (DC)", "LAB", Actor.findActorWithKeyword("ANALYZER"));
				TransactionInstance hl7Message = hl7Initiator.sendMessageAndGetTheHL7Message();
				return "/messages/messageDisplay.seam?id=" + hl7Message.getId();
			} catch (Exception e) {
				log.error(e.getMessage());
				FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Unable to build message");
				return null;
			}
		} else {
			log.error("No SUT Selected");
			return null;
		}
	}


	/**
	 * <p>isDisplayListOfSut.</p>
	 *
	 * @return a boolean.
	 */
	public boolean isDisplayListOfSut() {
		return displayListOfSut;
	}

	/**
	 * <p>Setter for the field <code>displayListOfSut</code>.</p>
	 *
	 * @param displayListOfSut a boolean.
	 */
	public void setDisplayListOfSut(boolean displayListOfSut) {
		if (displayListOfSut) {
			sut = (HL7V2ResponderSUTConfiguration) Component.getInstance("selectedSUT");
		}
		this.displayListOfSut = displayListOfSut;
	}

	/**
	 * <p>Getter for the field <code>sut</code>.</p>
	 *
	 * @return a {@link net.ihe.gazelle.HL7Common.model.HL7V2ResponderSUTConfiguration} object.
	 */
	public HL7V2ResponderSUTConfiguration getSut() {
		return sut;
	}

	/**
	 * <p>Setter for the field <code>sut</code>.</p>
	 *
	 * @param sut a {@link net.ihe.gazelle.HL7Common.model.HL7V2ResponderSUTConfiguration} object.
	 */
	public void setSut(HL7V2ResponderSUTConfiguration sut) {
		if (sut != null) {
			this.sut = sut;
		} else {
			log.info("given value is null, do not use it !");
		}
	}
}
