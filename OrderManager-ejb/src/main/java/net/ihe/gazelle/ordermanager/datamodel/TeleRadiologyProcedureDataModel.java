package net.ihe.gazelle.ordermanager.datamodel;

import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.ordermanager.model.TeleRadiologyOrder;
import net.ihe.gazelle.ordermanager.model.TeleRadiologyProcedure;

/**
 * Class description : <b>RequestedProcedureDataModel</b>
 * 
 * This class is a data model for properly and quickly displaying the RequestedProcedure objects in the GUI
 * 
 *
 *
 * @author Anne-Gaëlle Bergé - IHE Europe
 *
 * @version 1.0 - 2012, April 12th
 * 
 */
public class TeleRadiologyProcedureDataModel extends FilterDataModel<TeleRadiologyProcedure> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2315089591630132424L;
	private TeleRadiologyOrder relativeOrder;

	public TeleRadiologyProcedureDataModel() {
		super(new TeleRadiologyProcedureFilter());
	}

	public TeleRadiologyProcedureDataModel(TeleRadiologyOrder relativeOrder) {
		super(new TeleRadiologyProcedureFilter());
		this.relativeOrder = relativeOrder;
	}

	@Override
	public void appendFiltersFields(HQLQueryBuilder<TeleRadiologyProcedure> hqlBuilder) {
		if (relativeOrder != null) {
			hqlBuilder.addEq("teleRadiologyOrder.id", relativeOrder.getId());
		}
	}
@Override
        protected Object getId(TeleRadiologyProcedure t) {
        // TODO Auto-generated method stub
        return t.getId();
        }
}
