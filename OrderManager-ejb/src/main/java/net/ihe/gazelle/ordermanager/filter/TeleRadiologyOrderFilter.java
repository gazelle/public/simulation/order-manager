package net.ihe.gazelle.ordermanager.filter;

import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import net.ihe.gazelle.hql.restrictions.HQLRestrictions;
import net.ihe.gazelle.ordermanager.model.TeleRadiologyOrder;
import net.ihe.gazelle.ordermanager.model.TeleRadiologyOrderQuery;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.common.tf.model.Domain;

import java.util.Map;

/**
 * <b>Class Description : </b>OrderFilter<br>
 * <br>
 *
 * @author Anne-Gaelle Berge / IHE-Europe development Project
 * @version 1.0 - 30/06/16
 *
 *
 *
 */
public class TeleRadiologyOrderFilter extends AbstractOrderFilter<TeleRadiologyOrder> {

    public TeleRadiologyOrderFilter(Domain domain, Actor simulatedActor, String newOrderControlCode) {
        super(domain, simulatedActor, newOrderControlCode, null);
    }

    @Override
    protected HQLCriterionsForFilter<TeleRadiologyOrder> getHQLCriterionsForFilter() {
        TeleRadiologyOrderQuery query = new TeleRadiologyOrderQuery();
        HQLCriterionsForFilter<TeleRadiologyOrder> criteria = query.getHQLCriterionsForFilter();
        criteria.addPath("firstname", query.encounter().patient().firstName());
        criteria.addPath("lastname", query.encounter().patient().lastName());
        criteria.addPath("actor", query.simulatedActor(), getActor());
        criteria.addPath("domain", query.domain(), getDomain());
        criteria.addPath("creator", query.creator());
        criteria.addPath("creationDate", query.lastChanged());
        criteria.addQueryModifier(this);
        return criteria;
    }

    @Override
    protected Integer getObjectId(TeleRadiologyOrder order) {
        return order.getId();
    }

    @Override
    public void modifyQuery(HQLQueryBuilder<TeleRadiologyOrder> hqlQueryBuilder, Map<String, Object> map) {
        TeleRadiologyOrderQuery query = new TeleRadiologyOrderQuery();
        if (getPlacerOrderNumber() != null && !getPlacerOrderNumber().isEmpty()) {
            hqlQueryBuilder.addRestriction(query.placerOrderNumber().likeRestriction(getPlacerOrderNumber()));
        }
        if ((getFillerOrderNumber() != null) && !getFillerOrderNumber().isEmpty()) {
            hqlQueryBuilder.addRestriction(query.fillerOrderNumber().likeRestriction(getFillerOrderNumber()));
        }
        if (getPatientid() != null && !getPatientid().isEmpty()){
            hqlQueryBuilder.addRestriction(query.encounter().patient().patientIdentifiers().identifier().likeRestriction(getPatientid()));
        }
        if (getVisitnumber() != null && !getVisitnumber().isEmpty()){
            hqlQueryBuilder.addRestriction(query.encounter().visitNumber().likeRestriction(getVisitnumber()));
        }
        if (getScheduled() != null) {
            if (getScheduled()) {
                hqlQueryBuilder.addRestriction(query.teleRadiologyProcedures().isNotEmptyRestriction());
            } else {
                hqlQueryBuilder.addRestriction(query.teleRadiologyProcedures().isEmptyRestriction());
            }
        }
        if ((getNewOrderControlCode() != null) && !getNewOrderControlCode().isEmpty()) {
            // order placer discontinues order
            if (getNewOrderControlCode().equals("DC")) {
                hqlQueryBuilder.addRestriction(query.orderStatus().eqRestriction("IP"));
                hqlQueryBuilder.addRestriction(query.orderControlCode().neqRestriction("DC"));
            }
            // order placer cancels order
            else if (getNewOrderControlCode().equals("CA")) {
                hqlQueryBuilder.addRestriction(query.orderStatus().isNullRestriction());
                hqlQueryBuilder.addRestriction(query.orderControlCode().neqRestriction("CA"));
            }
            // order filler cancels order
            else if (getNewOrderControlCode().equals("OC")) {
                hqlQueryBuilder.addRestriction(query.orderStatus().isNullRestriction());
            }
            // order filler updates order status
            else if (getNewOrderControlCode().equals("SC")) {
                hqlQueryBuilder.addRestriction(HQLRestrictions.or(
                        query.orderStatus().isNullRestriction(),
                        query.orderStatus().eqRestriction(""), query.orderStatus().eqRestriction("IP")));
            }
        }
    }
}
