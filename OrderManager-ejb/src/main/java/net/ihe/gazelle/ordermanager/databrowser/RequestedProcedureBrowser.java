package net.ihe.gazelle.ordermanager.databrowser;

import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import net.ihe.gazelle.ordermanager.model.RequestedProcedure;
import net.ihe.gazelle.ordermanager.model.RequestedProcedureQuery;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

import javax.faces.context.FacesContext;
import java.util.Map;

/**
 * <p>RequestedProcedureBrowser class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
@Name("requestedProcedureBrowser")
@Scope(ScopeType.PAGE)
public class RequestedProcedureBrowser extends DataBrowser<RequestedProcedure> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3586684515848290768L;
	private String studyInstanceUID;

	/** {@inheritDoc} */
	@Override
	public void modifyQuery(HQLQueryBuilder<RequestedProcedure> queryBuilder, Map<String, Object> map) {
		if ((fillerOrderNumber != null) && !fillerOrderNumber.isEmpty()) {
			queryBuilder.addLike("order.fillerOrderNumber", fillerOrderNumber);
		}
		if ((placerOrderNumber != null) && !placerOrderNumber.isEmpty()) {
			queryBuilder.addLike("order.placerOrderNumber", placerOrderNumber);
		}
		if ((patientId != null) && !patientId.isEmpty()) {
			queryBuilder.addLike("order.encounter.patient.patientIdentifiers.identifier", patientId);
		}
		if ((visitNumber != null) && !visitNumber.isEmpty()) {
			queryBuilder.addLike("order.encounter.visitNumber", visitNumber);
		}
		if ((studyInstanceUID != null) && !studyInstanceUID.isEmpty()) {
			queryBuilder.addLike("studyInstanceUID", studyInstanceUID);
		}
	}

	/** {@inheritDoc} */
	@Override
	protected HQLCriterionsForFilter<RequestedProcedure> getHQLCriterionsForFilter() {
		RequestedProcedureQuery query = new RequestedProcedureQuery();
		HQLCriterionsForFilter<RequestedProcedure> criterions = query.getHQLCriterionsForFilter();
		criterions.addPath("lastChanged", query.lastChanged());
		criterions.addPath("actor", query.simulatedActor(), actor);
		criterions.addPath("domain", query.domain(), domain);
		criterions.addPath("creator", query.creator());
		criterions.addQueryModifier(this);
		return criterions;
	}

	/** {@inheritDoc} */
	@Override
	public String getPermanentLink(Integer objectId) {
		return "/browsing/procedure.seam?id=" + objectId;
	}

    /** {@inheritDoc} */
    @Override
    public FilterDataModel<RequestedProcedure> getDataModel() {
        return new FilterDataModel<RequestedProcedure>(getFilter()) {
            @Override
            protected Object getId(RequestedProcedure requestedProcedure) {
                return requestedProcedure.getId();
            }
        };
    }

    /** {@inheritDoc} */
    @Override
	public void reset() {
		studyInstanceUID = null;
		super.reset();
	}

    /** {@inheritDoc} */
    @Override
    public Filter<RequestedProcedure> getFilter() {
        if (filter == null){
            filter = new Filter<RequestedProcedure>(getHQLCriterionsForFilter(), FacesContext.getCurrentInstance().getExternalContext()
                    .getRequestParameterMap());
        }
        return filter;
    }

    /**
     * <p>Getter for the field <code>studyInstanceUID</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getStudyInstanceUID() {
		return studyInstanceUID;
	}

	/**
	 * <p>Setter for the field <code>studyInstanceUID</code>.</p>
	 *
	 * @param studyInstanceUID a {@link java.lang.String} object.
	 */
	public void setStudyInstanceUID(String studyInstanceUID) {
		this.studyInstanceUID = studyInstanceUID;
	}

}
