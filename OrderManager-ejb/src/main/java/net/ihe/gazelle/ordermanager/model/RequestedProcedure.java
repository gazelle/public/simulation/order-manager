package net.ihe.gazelle.ordermanager.model;

import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.common.tf.model.Domain;
import org.jboss.seam.annotations.Name;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

@Entity
@Name("requestedProcedure")
@Table(name = "om_requested_procedure", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = "id"))
@SequenceGenerator(name = "om_requested_procedure_sequence", sequenceName = "om_requested_procedure_id_seq", allocationSize = 1)
public class RequestedProcedure extends CommonColumns implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7434950677108584476L;

	@Id
	@NotNull
	@GeneratedValue(generator = "om_requested_procedure_sequence", strategy = GenerationType.SEQUENCE)
	@Column(name = "id", nullable = false, unique = true)
	private Integer id;

	/**
	 * ORC-1
	 */
	@Column(name = "order_control_code")
	private String orderControlCode;

	/**
	 * ORC-5
	 */
	@Column(name = "order_status")
	private String orderStatus;

	/**
	 * OBR-19 and (0040,1001)
	 */
	@Column(name = "requested_procedure_id")
	private String requestedProcedureID;

	/**
	 * (0032,1064)
	 */
	@Column(name = "code")
	private String code;

	/**
	 * (0032,1060)
	 */
	@Column(name = "description")
	private String description;

	/**
	 * 
	 */
	@Column(name = "code_meaning")
	private String codeMeaning;

	/**
	 * 
	 */
	@Column(name = "coding_scheme")
	private String codingScheme;

	/**
	 * ZDS-1
	 */
	@Column(name = "study_instance_uid")
	private String studyInstanceUID;

	@OneToMany(targetEntity = ScheduledProcedureStep.class, mappedBy = "requestedProcedure", cascade = CascadeType.MERGE)
	private List<ScheduledProcedureStep> scheduledProcedureSteps;

	@ManyToOne
	@JoinColumn(name = "order_id")
	private Order order;

	/**
	 *  (0008,1084) (0008,1080)
	 */
	@Column(name = "admitting_diagnostic_code")
	private String admittingDiagnosticCode;

	public RequestedProcedure() {
		super();
	}

	/**
	 * 
	 * @param simulatedActor
	 * @param creator
	 * @param domain
	 */
	public RequestedProcedure(Actor simulatedActor, String creator, Domain domain) {
		super(simulatedActor, creator, domain);
	}

	/**
	 * 
	 * @param entityManager
	 * @return the requested procedure
	 */
	public RequestedProcedure save(EntityManager entityManager) {
		if (entityManager == null) {
			entityManager = EntityManagerService.provideEntityManager();
		}
		RequestedProcedure procedure = entityManager.merge(this);
		entityManager.flush();
		return procedure;
	}

	/**
	 * 
	 * @param order
	 * @param requestedProcedureID
	 * @param studyInstanceUID
	 * @return the list of requested procedure
	 */
	public static List<RequestedProcedure> getRequestedProcedureFiltered(EntityManager entityManager, Order order,
			String requestedProcedureID, String studyInstanceUID, Actor simulatedActor) {
		HQLQueryBuilder<RequestedProcedure> builder = new HQLQueryBuilder<RequestedProcedure>(entityManager,
				RequestedProcedure.class);
		if (simulatedActor != null) {
			builder.addEq("simulatedActor", simulatedActor);
		}
		if ((order != null) && (order.getId() != null)) {
			builder.addEq("order.id", order.getId());
		}
		if ((requestedProcedureID != null) && !requestedProcedureID.isEmpty()) {
			builder.addEq("requestedProcedureID", requestedProcedureID);
		}
		if ((studyInstanceUID != null) && !studyInstanceUID.isEmpty()) {
			builder.addEq("studyInstanceUID", studyInstanceUID);
		}
		List<RequestedProcedure> procedures = builder.getList();
		if ((procedures != null) && !procedures.isEmpty()) {
			return procedures;
		} else {
			return null;
		}
	}

	public String getRequestedProcedureID() {
		return requestedProcedureID;
	}

	public void setRequestedProcedureID(String requestedProcedureID) {
		this.requestedProcedureID = requestedProcedureID;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String procedureCode) {
		this.code = procedureCode;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String procedureDescription) {
		this.description = procedureDescription;
	}

	public String getStudyInstanceUID() {
		return studyInstanceUID;
	}

	public void setStudyInstanceUID(String studyInstanceUID) {
		this.studyInstanceUID = studyInstanceUID;
	}

	public List<ScheduledProcedureStep> getScheduledProcedureSteps() {
		return scheduledProcedureSteps;
	}

	public void setScheduledProcedureSteps(List<ScheduledProcedureStep> scheduledProcedureSteps) {
		this.scheduledProcedureSteps = scheduledProcedureSteps;
	}

	public Order getOrder() {
		return order;
	}

	public void setOrder(Order order) {
		this.order = order;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getId() {
		return id;
	}

	public void setOrderControlCode(String orderControlCode) {
		this.orderControlCode = orderControlCode;
	}

	public String getOrderControlCode() {
		return orderControlCode;
	}

	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}

	public String getOrderStatus() {
		return orderStatus;
	}

	public void setCodeMeaning(String codeMeaning) {
		this.codeMeaning = codeMeaning;
	}

	public String getCodeMeaning() {
		return codeMeaning;
	}

	public void setCodingScheme(String codingScheme) {
		this.codingScheme = codingScheme;
	}

	public String getCodingScheme() {
		return codingScheme;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = (prime * result) + ((studyInstanceUID == null) ? 0 : studyInstanceUID.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		RequestedProcedure other = (RequestedProcedure) obj;
		if (studyInstanceUID == null) {
			if (other.studyInstanceUID != null) {
				return false;
			}
		} else if (!studyInstanceUID.equals(other.studyInstanceUID)) {
			return false;
		}
		return true;
	}

	public String getAdmittingDiagnosticCode() {
		return admittingDiagnosticCode;
	}

	public void setAdmittingDiagnosticCode(String admittingDiagnosticCode) {
		this.admittingDiagnosticCode = admittingDiagnosticCode;
	}
}
