package net.ihe.gazelle.ordermanager.model;

import net.ihe.gazelle.hql.providers.EntityManagerService;
import org.jboss.seam.annotations.Name;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import java.io.Serializable;

/**
 * Class description : <b>AccessionNumberGenerator</b>
 * 
 * This class is used to generate the Accession Number required by the worklists
 * 
 *
 *
 * @author Anne-Gaëlle Bergé - INRIA Rennes/Bretagne Atlantique
 *
 * @version 1.0 - 2011, November 17th
 * 
 */

@Entity
@Name("accessionNumberGenerator")
@Table(name = "accession_number", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = "id"))
public class AccessionNumberGenerator implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4706807814775941853L;

	@Id
	@GeneratedValue
	@Column(name = "id")
	private Integer id;

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getId() {
		return id;
	}

	public AccessionNumberGenerator() {

	}

	public static String generate() {
		AccessionNumberGenerator generator = new AccessionNumberGenerator();
		EntityManager em = EntityManagerService.provideEntityManager();
		generator = em.merge(generator);
		em.flush();
		String accessionNumber = generator.getId().toString();
		String padding = "000000";
		if (accessionNumber.length() < 6) {
			return padding.substring(0, 5 - accessionNumber.length()) + accessionNumber;
		} else if (accessionNumber.length() > 16) {
			return accessionNumber.substring(accessionNumber.length() - 16, accessionNumber.length() - 1);
		} else {
			return accessionNumber;
		}
	}
}
