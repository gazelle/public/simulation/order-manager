/**
 * Copyright 2012 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.ordermanager.action;

import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.model.Message;
import ca.uhn.hl7v2.model.v251.message.ORL_O34;
import ca.uhn.hl7v2.parser.PipeParser;
import net.ihe.gazelle.HL7Common.initiator.Initiator;
import net.ihe.gazelle.HL7Common.model.HL7V2ResponderSUTConfiguration;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.ordermanager.model.AbstractOrder;
import net.ihe.gazelle.ordermanager.model.Encounter;
import net.ihe.gazelle.ordermanager.model.LabOrder;
import net.ihe.gazelle.ordermanager.model.Order;
import net.ihe.gazelle.ordermanager.model.Patient;
import net.ihe.gazelle.ordermanager.model.Specimen;
import net.ihe.gazelle.ordermanager.model.TeleRadiologyOrder;
import net.ihe.gazelle.ordermanager.utils.LabMessageBuilder;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.common.tf.model.Domain;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;
import net.ihe.gazelle.simulator.message.model.TransactionInstance;
import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import java.io.Serializable;
import java.util.Map;

/**
 * Class description : <b>OrderManager</b>
 * 
 * This class is the backing bean for the page /browsing/order.xhtml
 * 
 *
 *
 * @author Anne-Gaëlle Bergé - INRIA Rennes/Bretagne Atlantique
 *
 * @version 1.0 - 2011, November 22nd
 * 
 */

@Name("orderManager")
@Scope(ScopeType.PAGE)
public class OrderManager implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private static Logger log = LoggerFactory.getLogger(OrderManager.class);

	private TeleRadiologyOrder teleRadOrder;
	private Order order;
	private LabOrder labOrder;
	private Patient patient;
	private Encounter encounter;

	@Create
	public void selectOrderById() {
		Map<String, String> urlParams = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		if ((urlParams != null) && urlParams.containsKey("id")) {
			try {
                Domain selectedDomain;
				Integer orderId = Integer.valueOf(urlParams.get("id"));
				EntityManager entityManager = EntityManagerService.provideEntityManager();
				if (urlParams.containsKey("domain")) {
					selectedDomain = Domain.getDomainByKeyword(urlParams.get("domain"));
				} else {
					FacesMessages.instance().add(StatusMessage.Severity.ERROR, "domain parameter is expected but missing in the URL");
                    return;
				}
				AbstractOrder aOrder = null;
				if (selectedDomain.getKeyword().equals("LAB") || selectedDomain.getKeyword().equals("PAT")) {
					order = null;
					labOrder = entityManager.find(LabOrder.class, orderId);
					teleRadOrder = null;
					aOrder = labOrder;
				} else if (selectedDomain.getKeyword().equals("SeHE")){
					order = null;
					labOrder = null;
					teleRadOrder = entityManager.find(TeleRadiologyOrder.class, orderId);
					aOrder = teleRadOrder;
				} else {
					order = entityManager.find(Order.class, orderId);
					labOrder = null;
					teleRadOrder = null;
					aOrder = order;
				}
				if (aOrder != null) {
					encounter = aOrder.getEncounter();
					if (encounter != null) {
						patient = aOrder.getEncounter().getPatient();
					} else {
						patient = null;
					}
				} else {
					encounter = null;
					patient = null;
				}
			} catch (NumberFormatException e) {
				log.error("invalid id for order");
				FacesMessages.instance().add(urlParams.get("id") + " is not a valid parameter");
				patient = null;
				encounter = null;
				order = null;
				labOrder = null;
			}
		}
	}

	/**
	 * If the LabOrder is a WOS owned by the Analyzer Mgr with ORC-1 = NW or UA, the user can send it to his/her SUT.
	 * 
	 * @return the link to the messages
	 */
	public String broadcastWOS() {
		HL7V2ResponderSUTConfiguration sut = (HL7V2ResponderSUTConfiguration) Component.getInstance("selectedSUT");
		if (sut == null) {
			FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Please, first select the system under test");
			return null;
		} else {
			EntityManager entityManager = EntityManagerService.provideEntityManager();
			Specimen specimen = entityManager.find(Specimen.class, labOrder.getSpecimens().get(0).getId());
			LabMessageBuilder messageBuilder = new LabMessageBuilder("LAB-28", "ANALYZER_MGR", encounter, null,
					specimen, sut, "OM_LAB_ANALYZER_MGR", "IHE", "OML^O33^OML_O33", null);
			try {
				String message = messageBuilder.generateMessage();
				Initiator initiator = new Initiator(sut, "IHE", "OML_LAB_ANALYZER_MGR",
						Actor.findActorWithKeyword("ANALYZER_MGR"), Transaction.GetTransactionByKeyword("LAB-28"),
						message, "OML^O33^OML_O33", "LAB", Actor.findActorWithKeyword("ANALYZER"));
				TransactionInstance hl7Message = initiator.sendMessageAndGetTheHL7Message();
				if (hl7Message != null) {
					// parse the message to get the returned order control code
					PipeParser parser = PipeParser.getInstanceWithNoValidation();
					try {
						Message ack = parser.parse(hl7Message.getReceivedMessageContent());
						if (ack instanceof ORL_O34) {
							ORL_O34 orlMessage = (ORL_O34) ack;
							labOrder.setOrderControlCode(orlMessage.getRESPONSE().getPATIENT().getSPECIMEN(0)
									.getORDER(0).getORC().getOrderControl().getValue());
							labOrder.save(entityManager);
						} else {
							FacesMessages
									.instance()
									.add("Received message is not of type ORL_O34, cannot parse it, Order Control Code cannot be updated");
						}
					} catch (HL7Exception e) {
						FacesMessages.instance().add(
								"Cannot parse the received message, Order Control Code cannot be updated");
					}
					// redirect user to the newly created message page
					return "/messages/messageDisplay.seam?id=" + hl7Message.getId();
				} else {
					FacesMessages.instance().add(StatusMessage.Severity.ERROR, "An error occurred when sending message, please try again");
					return null;
				}
			} catch (Exception e) {
				FacesMessages.instance().add(e.getMessage());
				return null;
			}
		}
	}

	public Order getOrder() {
		return order;
	}

	public void setOrder(Order order) {
		this.order = order;
	}

	/**
	 * @param patient
	 *            the patient to set
	 */
	public void setPatient(Patient patient) {
		this.patient = patient;
	}

	/**
	 * @return the patient
	 */
	public Patient getPatient() {
		return patient;
	}

	/**
	 * @param encounter
	 *            the encounter to set
	 */
	public void setEncounter(Encounter encounter) {
		this.encounter = encounter;
	}

	/**
	 * @return the encounter
	 */
	public Encounter getEncounter() {
		return encounter;
	}

	public void setLabOrder(LabOrder labOrder) {
		this.labOrder = labOrder;
	}

	public LabOrder getLabOrder() {
		return labOrder;
	}

	public TeleRadiologyOrder getTeleRadOrder() {
		return teleRadOrder;
	}
}
