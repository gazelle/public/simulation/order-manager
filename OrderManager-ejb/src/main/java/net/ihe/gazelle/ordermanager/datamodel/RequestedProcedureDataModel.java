package net.ihe.gazelle.ordermanager.datamodel;

import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.restrictions.HQLRestrictionLikeMatchMode;
import net.ihe.gazelle.hql.restrictions.HQLRestrictions;
import net.ihe.gazelle.ordermanager.model.Order;
import net.ihe.gazelle.ordermanager.model.RequestedProcedure;
import net.ihe.gazelle.ordermanager.model.UserPreferences;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.common.tf.model.Domain;
import org.jboss.seam.Component;
import org.jboss.seam.security.Identity;

import java.util.Date;

/**
 * Class description : <b>RequestedProcedureDataModel</b>
 * 
 * This class is a data model for properly and quickly displaying the RequestedProcedure objects in the GUI
 * 
 *
 *
 * @author Anne-Gaëlle Bergé - IHE Europe
 *
 * @version 1.0 - 2012, April 12th
 * 
 */
public class RequestedProcedureDataModel extends FilterDataModel<RequestedProcedure> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2315089591630132424L;
	private String creator;
	private Date startDate;
	private Date endDate;
	private String placerNumber;
	private String fillerNumber;
	private Domain domain;
	private Actor simulatedActor;
	private String newAction;
	private String searchedVisitNumber;
	private String searchedPatientId;
	private Order relativeOrder;

	public RequestedProcedureDataModel() {
		super(new RequestedProcedureFilter());
	}

	public RequestedProcedureDataModel(Date startDate, Date endDate, String placerNumber, String fillerNumber,
			Domain domain, Actor simulatedActor, String patientId, String visitNumber, String newAction,
			Order relativeOrder) {
		super(new RequestedProcedureFilter());
		this.creator = null;
		if (Identity.instance().isLoggedIn()) {
			UserPreferences preferences = (UserPreferences) Component.getInstance("userPreferences");
			if (preferences.isViewOnlyMyObjects()) {
				this.creator = preferences.getUsername();
			}
		}
		if (startDate != null) {
            this.startDate = (Date) startDate.clone();
        }
        if (endDate != null) {
            this.endDate = (Date) endDate.clone();
        }
		this.placerNumber = placerNumber;
		this.fillerNumber = fillerNumber;
		this.domain = domain;
		this.simulatedActor = simulatedActor;
		this.searchedVisitNumber = visitNumber;
		this.searchedPatientId = patientId;
		this.newAction = newAction;
		this.relativeOrder = relativeOrder;
	}

	@Override
	public void appendFiltersFields(HQLQueryBuilder<RequestedProcedure> hqlBuilder) {
		if ((creator != null) && !creator.isEmpty()) {
			hqlBuilder.addEq("creator", creator);
		}
		if (startDate != null) {
			hqlBuilder.addRestriction(HQLRestrictions.gt("lastChanged", startDate));
		}
		if (endDate != null) {
			hqlBuilder.addRestriction(HQLRestrictions.lt("lastChanged", endDate));
		}
		if ((placerNumber != null) && !placerNumber.isEmpty()) {
			hqlBuilder.addRestriction(HQLRestrictions.like("order.placerOrderNumber", placerNumber,
					HQLRestrictionLikeMatchMode.ANYWHERE));
		}
		if ((fillerNumber != null) && !fillerNumber.isEmpty()) {
			hqlBuilder.addRestriction(HQLRestrictions.like("order.fillerOrderNumber", fillerNumber,
					HQLRestrictionLikeMatchMode.ANYWHERE));
		}
		if (domain != null) {
			hqlBuilder.addEq("domain", domain);
		}
		if (simulatedActor != null) {
			hqlBuilder.addEq("simulatedActor", simulatedActor);
		}
		if ((newAction != null) && !newAction.isEmpty()) {
			if (newAction.equals("DC") || newAction.equals("CA") || newAction.equals("SC")) {
				hqlBuilder.addEq("orderStatus", "SC");
			}
			// order filler updates order status
			else if (newAction.equals("CM")) {
				hqlBuilder.addRestriction(HQLRestrictions.neq("orderStatus", "CA"));
			}
		}
		if ((searchedVisitNumber != null) && !searchedVisitNumber.isEmpty()) {
			hqlBuilder.addRestriction(HQLRestrictions.like("order.encounter.visitNumber", searchedVisitNumber,
					HQLRestrictionLikeMatchMode.ANYWHERE));
		}
		if ((searchedPatientId != null) && !searchedPatientId.isEmpty()) {
			hqlBuilder.addRestriction(HQLRestrictions.like("order.encounter.patient.patientIdentifiers.identifier",
					searchedPatientId, HQLRestrictionLikeMatchMode.ANYWHERE));
		}
		if (relativeOrder != null) {
			hqlBuilder.addEq("order.id", relativeOrder.getId());
		}
	}
@Override
        protected Object getId(RequestedProcedure t) {
        // TODO Auto-generated method stub
        return t.getId();
        }
}
