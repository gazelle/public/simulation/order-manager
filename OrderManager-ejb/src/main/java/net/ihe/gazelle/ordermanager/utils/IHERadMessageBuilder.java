package net.ihe.gazelle.ordermanager.utils;

import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.model.Message;
import ca.uhn.hl7v2.model.v231.message.ORM_O01;
import ca.uhn.hl7v2.model.v231.segment.OBR;
import ca.uhn.hl7v2.model.v251.message.OMG_O19;
import ca.uhn.hl7v2.model.v251.message.OMI_O23;
import ca.uhn.hl7v2.parser.PipeParser;
import net.ihe.gazelle.HL7Common.model.HL7V2ResponderSUTConfiguration;
import net.ihe.gazelle.ordermanager.model.Order;
import net.ihe.gazelle.ordermanager.model.ProtocolItem;
import net.ihe.gazelle.ordermanager.model.RequestedProcedure;
import net.ihe.gazelle.ordermanager.model.ScheduledProcedureStep;
import net.ihe.gazelle.ormwithzds.hl7v2.model.v231.segment.ZDS;

public class IHERadMessageBuilder extends RadMessageBuilder<Order> {

	public IHERadMessageBuilder(String inTransactionKeyword, String inSendingActorKeyword, Order inOrder,
                                HL7V2ResponderSUTConfiguration sut, String sendingApplication, String sendingFacility, boolean hl7v251Option) {
		super(inTransactionKeyword, inSendingActorKeyword, inOrder, sut, sendingApplication, sendingFacility);
		this.hl7v251 = hl7v251Option;
	}

	public IHERadMessageBuilder(String inTransactionKeyword, String inSendingActorKeyword,
			RequestedProcedure inRequestedProcedure, Order inOrder, HL7V2ResponderSUTConfiguration sut, String sendingApplication,
			String sendingFacility, boolean hl7v251Option) {
		super(inTransactionKeyword, inSendingActorKeyword, inOrder, sut, sendingApplication, sendingFacility);
		this.requestedProcedure = inRequestedProcedure;
		this.hl7v251 = hl7v251Option;
	}

	private RequestedProcedure requestedProcedure;

	private boolean hl7v251;

	/**
	 * Thanks to the transaction and actor, creates the appropriates message
	 * 
	 * @return the String version of the created message
	 * @throws HL7Exception
	 */
	public String generateMessage() throws HL7Exception {
		Message message = null;
		if (hl7v251) {
			if (transactionKeyword.equals("RAD-2")) {
				message = buildRAD2Messagev251();
			} else if (transactionKeyword.equals("RAD-3")) {
				message = buildRAD3Messagev251();
			} else if (transactionKeyword.equals("RAD-4") || transactionKeyword.equals("RAD-13")) {
				message = buildRAD4andRAD13Messagev251();
			}
		} else {
			if (transactionKeyword.equals("RAD-2")) {
				message = buildRAD2Message();
			} else if (transactionKeyword.equals("RAD-3")) {
				message = buildRAD3Message();
			} else if (transactionKeyword.equals("RAD-4") || transactionKeyword.equals("RAD-13")) {
				message = buildRAD4andRAD13Message();
			}
		}
		// message to string and returns
		if (message == null) {
			return null;
		} else {
			PipeParser pipeParser = new PipeParser();
			return pipeParser.encode(message);
		}
	}

	private Message buildRAD4andRAD13Message() throws HL7Exception {
		net.ihe.gazelle.ormwithzds.hl7v2.model.v231.message.ORM_O01 ormMessage = new net.ihe.gazelle.ormwithzds.hl7v2.model.v231.message.ORM_O01();
		populateMSHv231(ormMessage.getMSH(), "ORM^O01^ORM_O01");
		populatePIDv231(ormMessage.getPATIENT().getPID());
		populatePV1v231(ormMessage.getPATIENT().getPV1());
		if (requestedProcedure.getScheduledProcedureSteps() != null) {
			int orderGroupIndex = 0;
			for (ScheduledProcedureStep sps : requestedProcedure.getScheduledProcedureSteps()) {
				if (sps.getProtocolItems() != null) {
					for (ProtocolItem protocol : sps.getProtocolItems()) {
						populateORCv231(ormMessage.getORDER(orderGroupIndex).getORC(), sps);
						populateOBRv231(ormMessage.getORDER(orderGroupIndex).getOBR(), sps, protocol);
						orderGroupIndex++;
					}
				}
			}
		}
		populateZDSv231(ormMessage.getZDS());
		return ormMessage;
	}

	/**
	 * builds the ORM^O01^ORM_O01 sent by the Order Filler actor of the RAD-3 transaction
	 * 
	 * @return
	 * @throws HL7Exception
	 */
	private Message buildRAD3Message() throws HL7Exception {
		ORM_O01 ormMessage = new ORM_O01();
		populateMSHv231(ormMessage.getMSH(), "ORM^O01^ORM_O01");
		populatePIDv231(ormMessage.getPIDPD1NTEPV1PV2IN1IN2IN3GT1AL1().getPID());
		populatePV1v231(ormMessage.getPIDPD1NTEPV1PV2IN1IN2IN3GT1AL1().getPV1PV2().getPV1());
		if (order.getOrderControlCode().equals("SN")) {
			populateORCv231(ormMessage.getORCOBRRQDRQ1ODSODTRXONTEDG1RXRRXCNTEOBXNTECTIBLG().getORC(), true, false,
					true);
			populateOBRv231(ormMessage.getORCOBRRQDRQ1ODSODTRXONTEDG1RXRRXCNTEOBXNTECTIBLG()
					.getOBRRQDRQ1ODSODTRXONTEDG1RXRRXCNTEOBXNTE().getOBR());
		}
		// cancel/update status
		else {
			populateORCv231(ormMessage.getORCOBRRQDRQ1ODSODTRXONTEDG1RXRRXCNTEOBXNTECTIBLG().getORC(), false, true,
					true);
		}
		return ormMessage;
	}

	/**
	 * builds the ORM^O01^ORM_O01 message sent by the Order Placer actor in the context of the RAD-2 transaction
	 * 
	 * @return
	 * @throws HL7Exception
	 */
	private Message buildRAD2Message() throws HL7Exception {
		ORM_O01 ormMessage = new ORM_O01();
		populateMSHv231(ormMessage.getMSH(), "ORM^O01^ORM_O01");
		populatePIDv231(ormMessage.getPIDPD1NTEPV1PV2IN1IN2IN3GT1AL1().getPID());
		populatePV1v231(ormMessage.getPIDPD1NTEPV1PV2IN1IN2IN3GT1AL1().getPV1PV2().getPV1());
		if (order.getOrderControlCode().equals("NW")) {
			populateORCv231(ormMessage.getORCOBRRQDRQ1ODSODTRXONTEDG1RXRRXCNTEOBXNTECTIBLG().getORC(), true, true,
					false);
			populateOBRv231(ormMessage.getORCOBRRQDRQ1ODSODTRXONTEDG1RXRRXCNTEOBXNTECTIBLG()
					.getOBRRQDRQ1ODSODTRXONTEDG1RXRRXCNTEOBXNTE().getOBR());
		}
		// cancel/discontinue
		else {
			populateORCv231(ormMessage.getORCOBRRQDRQ1ODSODTRXONTEDG1RXRRXCNTEOBXNTECTIBLG().getORC(), false, true,
					true);
			// OBR segment is not present in this message
		}
		return ormMessage;
	}

	/**
	 * builds the OMI^O23^OMI_O23 message sent by the Order Filler in the context of the RAD-4/RAD-13 transactions (HL7v2.5.1 option)
	 * 
	 * @return
	 */
	private Message buildRAD4andRAD13Messagev251() throws HL7Exception {
		OMI_O23 omiMessage = new OMI_O23();
		populateMSHv251(omiMessage.getMSH(), "OMI^O23^OMI_O23");
		populatePIDv251(omiMessage.getPATIENT().getPID());
		populatePV1v251(omiMessage.getPATIENT().getPATIENT_VISIT().getPV1());
		if (requestedProcedure.getScheduledProcedureSteps() != null) {
			int orderGroupIndex = 0;
			for (ScheduledProcedureStep sps : requestedProcedure.getScheduledProcedureSteps()) {
				if (sps.getProtocolItems() != null) {
					for (ProtocolItem protocol : sps.getProtocolItems()) {
						populateORCv251(omiMessage.getORDER(orderGroupIndex).getORC(), sps);
						populateOBRv251(omiMessage.getORDER(orderGroupIndex).getOBR(), sps, protocol);
						populateIPC(omiMessage.getORDER(orderGroupIndex).getIPC(), sps, protocol);
						populateTQ1v251(omiMessage.getORDER().getTIMING().getTQ1(), order);
						orderGroupIndex++;
					}
				}
			}
		}
		return omiMessage;
	}

	/**
	 * builds the OMG^O19^OMG_O19 message sent by the Order Filler in the context of the RAD-3 transaction (HL7v2.5.1 option)
	 * 
	 * @return
	 */
	private Message buildRAD3Messagev251() throws HL7Exception {
		OMG_O19 omgMessage = new OMG_O19();
		populateMSHv251(omgMessage.getMSH(), "OMG^O19^OMG_O19");
		populatePIDv251(omgMessage.getPATIENT().getPID());
		populatePV1v251(omgMessage.getPATIENT().getPATIENT_VISIT().getPV1());
		if (order.getOrderControlCode().equals("SN")) {
			populateORCv251(omgMessage.getORDER().getORC(), true, false, true);
		}
		// cancel/update status
		else {
			populateORCv251(omgMessage.getORDER().getORC(), false, true, true);
		}
		populateOBRv251(omgMessage.getORDER().getOBR());
		populateTQ1v251(omgMessage.getORDER().getTIMING().getTQ1(), order);
		// TODO TQ1
		return omgMessage;
	}

	/**
	 * builds the OMG^O19^OMG_O19 message sent by the Order Placer in the context of the RAD-2 transaction (HL7v2.5.1 option)
	 * 
	 * @return
	 */
	private Message buildRAD2Messagev251() throws HL7Exception {
		OMG_O19 omgMessage = new OMG_O19();
		populateMSHv251(omgMessage.getMSH(), "OMG^O19^OMG_O19");
		populatePIDv251(omgMessage.getPATIENT().getPID());
		populatePV1v251(omgMessage.getPATIENT().getPATIENT_VISIT().getPV1());
		if (order.getOrderControlCode().equals("NW")) {
			populateORCv251(omgMessage.getORDER().getORC(), true, true, false);
			populateOBRv251(omgMessage.getORDER().getOBR());
		}
		// cancel/discontinue
		else {
			populateORCv251(omgMessage.getORDER().getORC(), false, true, true);
			// OBR segment is not present in this message
		}
		populateTQ1v251(omgMessage.getORDER().getTIMING().getTQ1(), order);
		return omgMessage;
	}

	@Override
	protected void populateZDSv231(ZDS zds) throws HL7Exception {
		super.populateZDSv231(zds);
		zds.getStudyInstanceUID().getPointer().setValue(requestedProcedure.getStudyInstanceUID());
	}

	@Override
	protected void populateOBRv231(net.ihe.gazelle.ormwithzds.hl7v2.model.v231.segment.OBR obr,
			ScheduledProcedureStep sps, ProtocolItem protocol) throws HL7Exception {
		super.populateOBRv231(obr, sps, protocol);
		if ((order.getSpecimenSource() != null) && !order.getSpecimenSource().equals("null")) {
			obr.getSpecimenSource().getSiteModifier().getCe1_Identifier().setValue(order.getSpecimenSource());
		}
		obr.getPlacerField1().setValue(order.getAccessionNumber());
		obr.getPlacerField2().setValue(requestedProcedure.getRequestedProcedureID());
		obr.getTransportationMode().setValue(order.getTransportationMode());
		obr.getProcedureCode().getCe1_Identifier().setValue(requestedProcedure.getCode());
		obr.getProcedureCode().getCe2_Text().setValue(requestedProcedure.getCodeMeaning());
		obr.getProcedureCode().getCe3_NameOfCodingSystem().setValue(requestedProcedure.getCodingScheme());
		obr.getProcedureCode().getCe5_AlternateText().setValue(requestedProcedure.getDescription());
	}

	@Override
	protected void populateOBRv251(ca.uhn.hl7v2.model.v251.segment.OBR obr, ScheduledProcedureStep sps,
			ProtocolItem protocol) throws HL7Exception {
		super.populateOBRv251(obr, sps, protocol);
		if ((order.getSpecimenSource() != null) && !order.getSpecimenSource().equals("null")) {
			obr.getSpecimenSource().getSiteModifier().getIdentifier().setValue(order.getSpecimenSource());
		}
		obr.getPlacerField1().setValue(order.getAccessionNumber());
		obr.getPlacerField2().setValue(requestedProcedure.getRequestedProcedureID());
		obr.getTransportationMode().setValue(order.getTransportationMode());
		obr.getProcedureCode().getCe1_Identifier().setValue(requestedProcedure.getCode());
		obr.getProcedureCode().getCe2_Text().setValue(requestedProcedure.getCodeMeaning());
		obr.getProcedureCode().getCe3_NameOfCodingSystem().setValue(requestedProcedure.getCodingScheme());
		obr.getProcedureCode().getCe5_AlternateText().setValue(requestedProcedure.getDescription());
	}

	@Override
	protected void populateORCv231(net.ihe.gazelle.ormwithzds.hl7v2.model.v231.segment.ORC orc,
			ScheduledProcedureStep sps) throws HL7Exception {
		super.populateORCv231(orc, sps);
		orc.getOrderControl().setValue(requestedProcedure.getOrderControlCode());
		orc.getOrderStatus().setValue(requestedProcedure.getOrderStatus());
	}

	@Override
	protected void populateORCv251(ca.uhn.hl7v2.model.v251.segment.ORC orc, ScheduledProcedureStep sps)
			throws HL7Exception {
		super.populateORCv251(orc, sps);
		orc.getOrderControl().setValue(requestedProcedure.getOrderControlCode());
		orc.getOrderStatus().setValue(requestedProcedure.getOrderStatus());
	}

	@Override
	protected void populateOBRv231(OBR obrSegment) throws HL7Exception {
		super.populateOBRv231(obrSegment);
		if (order.getSpecimenSource() != null) {
			obrSegment.getSpecimenSource().getSiteModifier().parse(order.getSpecimenSource());
		}
		if (order.getTransportationMode() != null) {
			obrSegment.getTransportationMode().setValue(order.getTransportationMode());
		}
		if (order.getTransportArranged() != null) {
			obrSegment.getTransportArranged().setValue(order.getTransportArranged());
		}
	}

	@Override
	protected void populateOBRv251(ca.uhn.hl7v2.model.v251.segment.OBR obrSegment) throws HL7Exception {
		super.populateOBRv251(obrSegment);
		if (order.getTransportationMode() != null) {
			obrSegment.getTransportationMode().setValue(order.getTransportationMode());
		}
		if (order.getTransportArranged() != null) {
			obrSegment.getTransportArranged().setValue(order.getTransportArranged());
		}
	}
}
