package net.ihe.gazelle.ordermanager.utils;

import javax.faces.model.SelectItem;

public enum LAWSpecimenRole {
	L("L", "Pooled patient specimen", "HL70369"),
	P("P", "Patient specimen", "HL70369"),
	Q("Q", "Control specimen", "HL70369"),
	U("U", "Unknown", "IHELAW");

	private static SelectItem[] specimenRoleList;


	static {
		specimenRoleList = new SelectItem[5];
		specimenRoleList[0] = new SelectItem(null, "Please select...");
		int index = 1;
		for (LAWSpecimenRole role : values()) {
			specimenRoleList[index] = new SelectItem(role.getCode(), role.getLabel());
			index++;
		}
	}
	String code;
	String label;
	String codingSystem;

	LAWSpecimenRole(String code, String label, String codingSystem) {
		this.code = code;
		this.label = label;
		this.codingSystem = codingSystem;
	}

	public String getCode() {
		return this.code;
	}

	public String getLabel() {
		return this.label;
	}

	public static SelectItem[] getSpecimenRoleList() {
		return specimenRoleList.clone();
	}

	public String getCodingSystem(){
		return this.codingSystem;
	}

}