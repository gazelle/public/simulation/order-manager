package net.ihe.gazelle.ordermanager.model;

import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.simulator.common.model.ApplicationConfiguration;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.common.tf.model.Domain;
import org.jboss.seam.annotations.Name;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import java.io.File;
import java.io.Serializable;
import java.util.Date;

@Entity
@Name("worklistEntry")
@Table(name = "om_worklist_entry", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = "id"))
@SequenceGenerator(name = "om_worklist_entry_sequence", sequenceName = "om_worklist_entry_id_seq", allocationSize = 1)
public class WorklistEntry extends CommonColumns implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 7147584711405967013L;

    @Id
    @NotNull
    @GeneratedValue(generator = "om_worklist_entry_sequence", strategy = GenerationType.SEQUENCE)
    @Column(name = "id", nullable = false, unique = true)
    private Integer id;

    @Column(name = "is_deleted")
    private boolean deleted;

    @Column(name = "file_path")
    private String filepath;

    /**
     * (0040,0001)
     */
    @Column(name = "station_ae_title")
    private String stationAETitle;

    @OneToOne(targetEntity = ScheduledProcedureStep.class)
    @JoinColumn(name = "scheduled_procedure_step_id")
    private ScheduledProcedureStep scheduledProcedureStep;

    public WorklistEntry() {
        super();
        this.deleted = false;
    }

    public WorklistEntry(ScheduledProcedureStep scheduledProcedureStep, Actor simulatedActor, Domain domain) {
        super(simulatedActor, null, domain);
        this.scheduledProcedureStep = scheduledProcedureStep;
        this.deleted = false;
    }

    public WorklistEntry(WorklistEntry worklist) {
        this.lastChanged = new Date();
        this.deleted = false;
        this.scheduledProcedureStep = worklist.getScheduledProcedureStep();
        this.stationAETitle = worklist.getStationAETitle();
        this.simulatedActor = worklist.getSimulatedActor();
        this.domain = worklist.getDomain();
    }

    /**
     * Restores the current object (puts deleted attribute to false and move the file in the right location)
     */
    public boolean restore() {
        String baseDir = ApplicationConfiguration.getValueOfVariable("worklists_basedir");
        StringBuilder currentPath = new StringBuilder();
        currentPath.append(baseDir);
        currentPath.append("/_DISABLED/");
        String idString = id.toString();
        currentPath.append(idString);
        currentPath.append(".wl");
        File currentFile = new File(currentPath.toString());
        boolean success = currentFile.renameTo(new File(this.getWLFilePath()));
        if (success) {
            this.deleted = false;
            saveWorklist();
        }

        return success;
    }

    /**
     * Deletes the current object (puts deleted attribute to true and move the file in the _DISABLED directory)
     */
    public boolean delete() {
        StringBuilder newDirPath = new StringBuilder();
        newDirPath.append(ApplicationConfiguration.getValueOfVariable("worklists_basedir"));
        newDirPath.append("/_DISABLED");
        File file = new File(this.getWLFilePath());
        File newDir = new File(newDirPath.toString());
        boolean success = file.renameTo(new File(newDir, file.getName()));
        if (success) {
            this.deleted = true;
            saveWorklist();
        }
        return success;
    }

    /**
     * save the worklist object in the database
     *
     * @return worklist entry
     */
    public WorklistEntry saveWorklist() {
        EntityManager em = EntityManagerService.provideEntityManager();
        WorklistEntry worklist = em.merge(this);
        if (worklist.getFilepath() == null) {
            worklist.setFilepath(worklist.buildFilePath());
            worklist = em.merge(worklist);
        }
        em.flush();
        return worklist;
    }

    public String buildFileDirectory() {
        StringBuilder dirPath = new StringBuilder();
        dirPath.append(ApplicationConfiguration.getValueOfVariable("worklists_basedir"));
        dirPath.append('/');
        if (this.getDomain() != null) {
            dirPath.append(this.getDomain().getKeyword());
        }
        dirPath.append('_');
        if (this.simulatedActor != null) {
            dirPath.append(this.getSimulatedActor().getKeyword());
        }
        return dirPath.toString();
    }

    public String buildFilePath() {
        if (this.id != null) {
            StringBuilder path = new StringBuilder();
            path.append(buildFileDirectory());
            path.append('/');
            String idString = id.toString();
            path.append(idString);
            return path.toString();
        } else {
            return null;
        }
    }

    public String getWLFilePath() {
        if (this.filepath != null) {
            return this.filepath.concat(".wl");
        } else {
            return null;
        }
    }

    public String getXMLFilePath() {
        if (this.filepath != null) {
            return this.filepath.concat(".xml");
        } else {
            return null;
        }
    }

    public String getDumpFilePath() {
        if (this.filepath != null) {
            return this.filepath.concat(".dump");
        } else {
            return null;
        }
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public String getFilepath() {
        return filepath;
    }

    public void setFilepath(String filepath) {
        this.filepath = filepath;
    }

    @Override
    public String getCreator() {
        return creator;
    }

    @Override
    public void setCreator(String creator) {
        this.creator = creator;
    }

    public String getStationAETitle() {
        return stationAETitle;
    }

    public void setStationAETitle(String stationAETitle) {
        this.stationAETitle = stationAETitle;
    }

    public void setScheduledProcedureStep(ScheduledProcedureStep scheduledProcedureStep) {
        this.scheduledProcedureStep = scheduledProcedureStep;
    }

    public ScheduledProcedureStep getScheduledProcedureStep() {
        return scheduledProcedureStep;
    }

    @Override
    public boolean equals(Object o) {
        if (o == null) return false;
        if (this == o) return true;
        if (!super.equals(o)) return false;
        if (getClass() != o.getClass()) return false;


        WorklistEntry that = (WorklistEntry) o;

        if (deleted != that.deleted) return false;
        if (filepath != null ? !filepath.equals(that.filepath) : that.filepath != null) return false;
        if (stationAETitle != null ? !stationAETitle.equals(that.stationAETitle) : that.stationAETitle != null)
            return false;
        return scheduledProcedureStep != null ? scheduledProcedureStep.equals(that.scheduledProcedureStep) : that.scheduledProcedureStep == null;

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (deleted ? 1 : 0);
        result = 31 * result + (filepath != null ? filepath.hashCode() : 0);
        result = 31 * result + (stationAETitle != null ? stationAETitle.hashCode() : 0);
        result = 31 * result + (scheduledProcedureStep != null ? scheduledProcedureStep.hashCode() : 0);
        return result;
    }
}
