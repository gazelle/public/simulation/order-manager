package net.ihe.gazelle.ordermanager.model;

import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import org.jboss.seam.annotations.Name;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

/**
 * Class description : <b>NumberGenerator</b>
 * 
 * This class describes a number generator. For each actor, an OID is set and methods defined in this class are used to generate placer/filler order numbers
 * 
 * This class defines the following attributes
 * <ul>
 * <li><b>id</b> unique identifier in the database</li>
 * <li><b>namespaceId</b> namespaceId of the assigning authority</li>
 * <li><b>universalId</b> OID of the assigning authority</li>
 * <li><b>newEntityIdentifier</b> the ID to use for the next number</li>
 * <li><b>universalIdType</b> type of the universal id (usually is ISO)</li>
 * <li><b>actor</b> actor using this assigning authority</li>
 * <ul>
 * 
 *
 *
 * @author Anne-Gaëlle Bergé - INRIA Rennes/Bretagne Atlantique
 *
 * @version 1.0 - 2011, November 17th
 * 
 */
@Entity
@Name("numberGenerator")
@Table(name = "om_number_generator", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = "id"))
@SequenceGenerator(name = "om_number_generator_sequence", sequenceName = "om_number_generator_id_seq", allocationSize = 1)
public class NumberGenerator implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@NotNull
	@GeneratedValue(generator = "om_number_generator_sequence")
	@Column(name = "id", nullable = false, unique = true)
	private Integer id;

	@Column(name = "namespace_id")
	private String namespaceId;

	@Column(name = "universal_id")
	private String universalId;

	@Column(name = "next_entity_identifier")
	private Integer nextEntityIdentifier;

	@Column(name = "universal_id_type")
	private String universalIdType;

	@ManyToOne
	@JoinColumn(name = "actor_id")
	private Actor actor;

	public NumberGenerator() {

	}

	/**
	 * 
	 * @param actor
	 * @param entityManager
	 * @return the universal id
	 */
	public static String getUniversalIdForActor(Actor actor, EntityManager entityManager) {
		HQLQueryBuilder<NumberGenerator> builder = new HQLQueryBuilder<NumberGenerator>(entityManager,
				NumberGenerator.class);
		builder.addEq("actor", actor);
		List<NumberGenerator> generators = builder.getList();
		if ((generators != null) && !generators.isEmpty()) {
			return generators.get(0).getUniversalId();
		} else {
			return null;
		}
	}

	/**
	 * common separator is ^
	 * 
	 * @param actor
	 * @param entityManager
	 * @return the order
	 */
	public static String getOrderNumberForActor(Actor actor, EntityManager entityManager) {
		return getOrderNumberForActor(actor, entityManager, "^");
	}

	public static String getOrderNumberForActor(Actor actor, EntityManager entityManager, String separateCharacter) {
		NumberGeneratorQuery query = new NumberGeneratorQuery();
        query.actor().eq(actor);
        NumberGenerator generator = query.getUniqueResult();
		if (generator != null) {
			StringBuilder buffer = new StringBuilder();
            synchronized (NumberGenerator.class) {
                buffer.append(generator.getNextEntityIdentifier());
                if (generator.isNamespacePopulated() || generator.isUniversalIdPopulated() || generator.isUniversalIdTypePopulated()) {
                    buffer.append(separateCharacter);
                    if (generator.isNamespacePopulated()) {
                        buffer.append(generator.getNamespaceId());
                    }
                    if (generator.isUniversalIdPopulated() || generator.isUniversalIdTypePopulated()) {
                        buffer.append(separateCharacter);
                        if (generator.isUniversalIdPopulated()) {
                            buffer.append(generator.getUniversalId());
                        }
                        if (generator.isUniversalIdTypePopulated()) {
                            buffer.append(separateCharacter);
                            buffer.append(generator.getUniversalIdType());
                        }
                    }
                }
            }
			generator.setNextEntityIdentifier(generator.getNextEntityIdentifier() + 1);
			entityManager.merge(generator);
			entityManager.flush();
			return buffer.toString();
		} else {
			return null;
		}
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNamespaceId() {
		return namespaceId;
	}

	public void setNamespaceId(String namespaceId) {
		this.namespaceId = namespaceId;
	}

	public String getUniversalId() {
		return universalId;
	}

	public void setUniversalId(String universalId) {
		this.universalId = universalId;
	}

	public Integer getNextEntityIdentifier() {
		return nextEntityIdentifier;
	}

	public void setNextEntityIdentifier(Integer nextEntityIdentifier) {
		this.nextEntityIdentifier = nextEntityIdentifier;
	}

	public Actor getActor() {
		return actor;
	}

	public void setActor(Actor actor) {
		this.actor = actor;
	}

	/**
	 * @param universalIdType
	 *            the universalIdType to set
	 */
	public void setUniversalIdType(String universalIdType) {
		this.universalIdType = universalIdType;
	}

	/**
	 * @return the universalIdType
	 */
	public String getUniversalIdType() {
		return universalIdType;
	}

	public boolean isNamespacePopulated(){
        return (namespaceId != null && !namespaceId.isEmpty());
    }

    public boolean isUniversalIdPopulated(){
        return (universalId != null && !universalId.isEmpty());
    }

    public boolean isUniversalIdTypePopulated(){
        return (universalIdType != null && !universalIdType.isEmpty());
    }
}
