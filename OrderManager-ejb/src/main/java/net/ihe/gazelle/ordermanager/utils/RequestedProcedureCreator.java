package net.ihe.gazelle.ordermanager.utils;

import net.ihe.gazelle.ordermanager.model.ProtocolItem;
import net.ihe.gazelle.ordermanager.model.RequestedProcedure;
import net.ihe.gazelle.ordermanager.model.ScheduledProcedureStep;
import net.ihe.gazelle.ordermanager.model.UIDGenerator;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.common.tf.model.Domain;
import org.jboss.seam.log.Log;
import org.jboss.seam.log.Logging;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;

/**
 * Use to create requested procedure for a given order
 *
 * @author aberge
 *
 */
public class RequestedProcedureCreator {

	private static Logger log = LoggerFactory.getLogger(RequestedProcedureCreator.class);

	private String universalServiceId;
	private Actor simulatedActor;
	private Domain domain;
	private String creator;

	public RequestedProcedureCreator(String universalServiceId, Actor simulatedActor, Domain domain, String creator) {
		this.universalServiceId = universalServiceId;
		this.simulatedActor = simulatedActor;
		this.domain = domain;
		this.creator = creator;
	}

	public RequestedProcedure createProcedure(String orderHierarchyLocation) throws ParserConfigurationException, SAXException, IOException {
		SAXParserFactory saxFactory = SAXParserFactory.newInstance();
		SAXParser saxParser = saxFactory.newSAXParser();
		OrderHierarchyHandler handler = new OrderHierarchyHandler(universalServiceId);
		InputStream inputStream = null;
		// open XML file
		if (orderHierarchyLocation == null) {
			log.error("order hierarchy location is null");
			return null;
		} else if (orderHierarchyLocation.startsWith("http")) {
			URL fileUrl = new URL(orderHierarchyLocation);
			inputStream = fileUrl.openStream();
		} else {
			File xmlFile = new File(orderHierarchyLocation);
			inputStream = new FileInputStream(xmlFile);
		}
		Reader reader = new InputStreamReader(inputStream, Charset.forName("UTF-8"));
		InputSource source = new InputSource(reader);
		source.setEncoding("UTF-8");
		// parse file
		saxParser.parse(source, handler);
		RequestedProcedure procedure = handler.getRequestedProcedure();
		return procedure;
	}

	protected class OrderHierarchyHandler extends DefaultHandler {
		private Log log = Logging.getLog(OrderHierarchyHandler.class);

		private String universalServiceId;
		private RequestedProcedure requestedProcedure;
		private boolean orderSelected;
		private ScheduledProcedureStep sps;
		private ProtocolItem protocol;

		public OrderHierarchyHandler(String universalServiceId) {
			this.universalServiceId = universalServiceId;
			this.orderSelected = false;
		}

		@Override
		public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
			if (qName.equalsIgnoreCase("Order")) {
				if (attributes.getValue("universalServiceID").equalsIgnoreCase(this.universalServiceId)) {
					log.info("Order selected");
					orderSelected = true;
					requestedProcedure = new RequestedProcedure(simulatedActor, creator, domain);
					requestedProcedure.setOrderControlCode("NW");
					requestedProcedure.setOrderStatus("SC");
				} else {
					orderSelected = false;
				}
			} else if (orderSelected && qName.equalsIgnoreCase("RequestedProcedure")) {
				requestedProcedure.setCode(attributes.getValue("code"));
				requestedProcedure.setDescription(attributes.getValue("description"));
				requestedProcedure.setCodeMeaning(attributes.getValue("codeMeaning"));
				requestedProcedure.setCodingScheme(attributes.getValue("codingSchemeDesignator"));
				requestedProcedure.setScheduledProcedureSteps(new ArrayList<ScheduledProcedureStep>());
			} else if (orderSelected && qName.equalsIgnoreCase("ScheduledProcedureStep")) {
				sps = new ScheduledProcedureStep(simulatedActor, creator, domain);
				sps.setSpsDescription(attributes.getValue("description"));
				sps.setModality(attributes.getValue("modality"));
				sps.setProtocolItems(new ArrayList<ProtocolItem>());
			} else if (orderSelected && qName.equalsIgnoreCase("ProtocolItem")) {
				protocol = new ProtocolItem(simulatedActor, creator, domain);
				protocol.setProtocolCode(attributes.getValue("code"));
				protocol.setProtocolCodeMeaning(attributes.getValue("codeMeaning"));
				protocol.setCodingScheme(attributes.getValue("codingSchemeDesignator"));
			}
		}

		@Override
		public void endElement(String uri, String localName, String qName) throws SAXException {
			if (orderSelected && qName.equalsIgnoreCase("RequestedProcedure")) {
				// generate studyInstanceUID and requestedProcedureID
				requestedProcedure.setStudyInstanceUID(UIDGenerator.getNextUIDForTag("(0020,000d)"));
				requestedProcedure.setRequestedProcedureID(UIDGenerator.getNextUIDForTag("(0040,1001)"));
			} else if (orderSelected && qName.equalsIgnoreCase("ScheduledProcedureStep")) {
				log.info("adding SPS to procedure");
				// generate SPS ID
				sps.setScheduledProcedureID(UIDGenerator.getNextUIDForTag("(0040,0009)"));
				sps.setRequestedProcedure(requestedProcedure);
				requestedProcedure.getScheduledProcedureSteps().add(sps);
			} else if (orderSelected && qName.equalsIgnoreCase("ProtocolItem")) {
				log.info("adding Protocol Item to SPS");
				protocol.setScheduledProcedureStep(sps);
				sps.getProtocolItems().add(protocol);
			} else if (qName.equalsIgnoreCase("Order")) {
				orderSelected = false;
			}
		}

		public RequestedProcedure getRequestedProcedure() {
			return requestedProcedure;
		}
	}

	public String getUniversalServiceId() {
		return universalServiceId;
	}

	public void setUniversalServiceId(String universalServiceId) {
		this.universalServiceId = universalServiceId;
	}

	public Actor getSimulatedActor() {
		return simulatedActor;
	}

	public void setSimulatedActor(Actor simulatedActor) {
		this.simulatedActor = simulatedActor;
	}

	public Domain getDomain() {
		return domain;
	}

	public void setDomain(Domain domain) {
		this.domain = domain;
	}

	public String getCreator() {
		return creator;
	}

	public void setCreator(String creator) {
		this.creator = creator;
	}
}
