package net.ihe.gazelle.ordermanager.dicom.proxy;

import net.ihe.gazelle.ordermanager.model.Connection;
import net.ihe.gazelle.ordermanager.model.DicomMessage;
import net.ihe.gazelle.proxy.netty.ProxyEventListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;

public abstract class GazelleProxyEventListener<REQU, RESP> implements ProxyEventListener<REQU, RESP> {

    private static Logger log = LoggerFactory.getLogger(GazelleProxyEventListener.class);

    public GazelleProxyEventListener() {
        super();
    }

    public void processMessage(EntityManager entityManager, DicomMessage abstractMessage, Integer requestChannelId,
                               Integer responseChannelId) {
        if (abstractMessage.getConnection() == null) {
            Connection connection = ConnectionManager
                    .retrieveConnection(entityManager, requestChannelId, responseChannelId);
            abstractMessage.setConnection(connection);
        }
        if (abstractMessage.getId() == null) {
            entityManager.persist(abstractMessage);
            // Without this flush, the response message would not exist in the database when TEE queries for it.
            entityManager.flush();
        } else {
            entityManager.merge(abstractMessage);
        }
    }

}
