package net.ihe.gazelle.ordermanager.datamodel;

import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.hql.criterion.HQLCriterion;
import net.ihe.gazelle.ordermanager.model.DicomMessage;

import java.util.ArrayList;
import java.util.List;

/**
 * Class description : <b>DicomMessageFilter</b>
 * 
 *
 *
 * @author Anne-Gaëlle Bergé - INRIA Rennes/Bretagne Atlantique
 *
 * @version 1.0 - 2011, November 25th
 * 
 */
public class DicomMessageFilter extends Filter<DicomMessage> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1221817111522472417L;
	private static List<HQLCriterion<DicomMessage, ?>> defaultCriterions;

	static {
		defaultCriterions = new ArrayList<HQLCriterion<DicomMessage, ?>>();
	}

	public DicomMessageFilter() {
		super(DicomMessage.class, defaultCriterions);
	}
}
