package net.ihe.gazelle.ordermanager.model;

import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.common.tf.model.Domain;
import org.jboss.seam.annotations.Name;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Name("container")
@Table(name = "om_container", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = "id"))
@SequenceGenerator(name = "om_container_sequence", sequenceName = "om_container_id_seq", allocationSize = 1)
public class Container extends CommonColumns implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 490189776588821943L;
	public static final String regex_EI = "([a-zA-Z0-9_\\.]*(\\^)?){1,3}[a-zA-Z0-9_\\.]*";
	public static final String regex_NA = "([0-9]{0,16}(\\^)?){1,3}[0-9]{0,16}";
	public static final String regex_CE = "([a-zA-Z0-9_\\.]*(\\^)?){1,5}[a-zA-Z0-9_\\.]*";

	@Id
	@Column(name = "id", nullable = false, unique = true)
	@GeneratedValue(generator = "om_container_sequence", strategy = GenerationType.SEQUENCE)
	@NotNull
	private Integer id;

	/**
	 * SAC-3
	 */
	@Column(name = "container_identifier")
	private String containerIdentifier;

	/**
	 * related specimen
	 */
	@ManyToOne
	@JoinColumn(name = "specimen_id")
	private Specimen specimen;

	/**
	 * null if message is not container centric
	 */
	@OneToMany(mappedBy = "container", cascade = CascadeType.MERGE)
	private List<LabOrder> orders;

	/**
	 * true if the message in which the container has been created is OML_O35 false otherwise
	 */
	@Column(name = "is_main_entity")
	private boolean mainEntity;

	/**
	 * indicates whether the container is linked to a lab order or a work order
	 */
	@Column(name = "work_order")
	private boolean workOrder;

	// Added by Nicolas. The attributes below are Conditional in the LAW Profile.

	/**
	 * SAC-4
	 */
	@Column(name = "primary_container_identifier")
	private String primaryContainerIdentifier;

	/**
	 * SAC-10
	 */
	@Column(name = "carrier_identifier")
	private String carrierIdentifier;

	/**
	 * SAC-11
	 */
	@Column(name = "position_in_carrier")
	private String positionInCarrier;

	/**
	 * SAC-13
	 */
	@Column(name = "tray_identifier")
	private String trayIdentifier;

	/**
	 * SAC-14
	 */
	@Column(name = "position_in_tray")
	private String positionInTray;

	/**
	 * SAC-15
	 */
	@Column(name = "location")
	private String location;

    /**
     * SAC-26
     */
    @Column(name = "cap_type")
    private String capType;

    /**
	 * Constructors
	 * 
	 * @param isPartOfWorkOrder
	 *            TODO
	 */
	public Container(boolean isPartOfWorkOrder) {
		this.workOrder = isPartOfWorkOrder;
	}

	/**
	 * default constructor
	 **/
	public Container() {
	}

	public Container(boolean isMainEntity, Domain domain, Actor simulatedActor) {
		this.mainEntity = isMainEntity;
		if (!isMainEntity) {
			orders = null;
		} else {
			orders = new ArrayList<LabOrder>();
		}
		this.domain = domain;
		this.simulatedActor = simulatedActor;
		this.lastChanged = new Date();
		this.containerIdentifier = NumberGenerator.getOrderNumberForActor(Actor.findActorWithKeyword("OF"),
				EntityManagerService.provideEntityManager());
	}

	public Container(boolean isMainEntity, Container container) {
		this.mainEntity = isMainEntity;
		this.workOrder = true;
		this.lastChanged = new Date();
		this.domain = Domain.getDomainByKeyword("LAB");
		this.simulatedActor = Actor.findActorWithKeyword("OF");
		this.containerIdentifier = container.getContainerIdentifier();
	}

	/**
	 * returns a container object based on its containerIdentifer, simulatedActor and domain
	 * 
	 * @param containerIdentifier
	 * @param simulatedActor
	 * @param domain
	 * @param entityManager
	 * @param isPartOfWorkOrder
	 * @return the container
	 */
	public static Container getContainerByIdentifierByActorByDomain(String containerIdentifier, Actor simulatedActor,
			Domain domain, EntityManager entityManager, boolean isPartOfWorkOrder) {
		ContainerQuery query = new ContainerQuery(new HQLQueryBuilder<Container>(entityManager, Container.class));
		if (containerIdentifier != null) {
			query.containerIdentifier().eq(containerIdentifier);
		}
		if (simulatedActor != null) {
			query.simulatedActor().eq(simulatedActor);
		}
		if (domain != null) {
			query.domain().eq(domain);
		}
		query.workOrder().eq(isPartOfWorkOrder);
		List<Container> containers = query.getList();
		if (containers.isEmpty()) {
			return null;
		} else {
			return containers.get(0);
		}
	}

	public static Container getContainerBasedOnDescription(Container inContainer, EntityManager entityManager) {
		ContainerQuery query = new ContainerQuery(new HQLQueryBuilder<Container>(entityManager, Container.class));
		query.simulatedActor().eq(inContainer.getSimulatedActor());
		query.domain().eq(inContainer.getDomain());
		if ((inContainer.getContainerIdentifier() != null) && !inContainer.getContainerIdentifier().isEmpty()) {
			query.containerIdentifier().eq(inContainer.getContainerIdentifier());
		}
		if ((inContainer.getPrimaryContainerIdentifier() != null)
				&& !inContainer.getPrimaryContainerIdentifier().isEmpty()) {
			query.primaryContainerIdentifier().eq(inContainer.getPrimaryContainerIdentifier());
		}
		if ((inContainer.getCarrierIdentifier() != null) && !inContainer.getCarrierIdentifier().isEmpty()) {
			query.carrierIdentifier().eq(inContainer.getCarrierIdentifier());
		}
		if ((inContainer.getPositionInCarrier() != null) && !inContainer.getPositionInCarrier().isEmpty()) {
			query.positionInCarrier().eq(inContainer.getPositionInCarrier());
		}
		if ((inContainer.getTrayIdentifier() != null) && !inContainer.getTrayIdentifier().isEmpty()) {
			query.trayIdentifier().eq(inContainer.getTrayIdentifier());
		}
		if ((inContainer.getPositionInTray() != null) && !inContainer.getPositionInTray().isEmpty()) {
			query.positionInTray().eq(inContainer.getPositionInTray());
		}
		return query.getUniqueResult();
	}

    public String getCapType() {
        return capType;
    }

    public void setCapType(String capType) {
        this.capType = capType;
    }

    public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Specimen getSpecimen() {
		return specimen;
	}

	public void setSpecimen(Specimen specimen) {
		this.specimen = specimen;
	}

	public List<LabOrder> getOrders() {
		return orders;
	}

	public void setOrders(List<LabOrder> orders) {
		this.orders = orders;
	}

	public boolean isMainEntity() {
		return mainEntity;
	}

	public void setMainEntity(boolean mainEntity) {
		this.mainEntity = mainEntity;
	}

	public void setContainerIdentifier(String containerIdentifier) {
		this.containerIdentifier = containerIdentifier;
	}

	public String getContainerIdentifier() {
		return containerIdentifier;
	}

	public void setWorkOrder(boolean workOrder) {
		this.workOrder = workOrder;
	}

	public boolean isWorkOrder() {
		return workOrder;
	}

	public String getPrimaryContainerIdentifier() {
		return primaryContainerIdentifier;
	}

	public void setPrimaryContainerIdentifier(String primaryContainerIdentifier) {
		this.primaryContainerIdentifier = primaryContainerIdentifier;
	}

	public String getCarrierIdentifier() {
		return carrierIdentifier;
	}

	public void setCarrierIdentifier(String carrierIdentifier) {
		this.carrierIdentifier = carrierIdentifier;
	}

	public String getPositionInCarrier() {
		return positionInCarrier;
	}

	public void setPositionInCarrier(String positionInCarrier) {
		this.positionInCarrier = positionInCarrier;
	}

	public String getTrayIdentifier() {
		return trayIdentifier;
	}

	public void setTrayIdentifier(String trayIdentifier) {
		this.trayIdentifier = trayIdentifier;
	}

	public String getPositionInTray() {
		return positionInTray;
	}

	public void setPositionInTray(String positionInTray) {
		this.positionInTray = positionInTray;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = (prime * result) + ((containerIdentifier == null) ? 0 : containerIdentifier.hashCode());
		result = (prime * result) + ((primaryContainerIdentifier == null) ? 0 : primaryContainerIdentifier.hashCode());
		result = (prime * result) + (workOrder ? 1231 : 1237);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Container other = (Container) obj;
		if (containerIdentifier == null) {
			if (other.containerIdentifier != null) {
				return false;
			}
		} else if (!containerIdentifier.equals(other.containerIdentifier)) {
			return false;
		}
		if (primaryContainerIdentifier == null) {
			if (other.primaryContainerIdentifier != null) {
				return false;
			}
		} else if (!primaryContainerIdentifier.equals(other.primaryContainerIdentifier)) {
			return false;
		}
		if (workOrder != other.workOrder) {
			return false;
		}
		return true;
	}

	public Container save(EntityManager entityManager) {
		Container container = entityManager.merge(this);
		entityManager.flush();
		return container;
	}

}
