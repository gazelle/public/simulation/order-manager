package net.ihe.gazelle.ordermanager.mwl.scu;

import com.pixelmed.dicom.AttributeList;
import com.pixelmed.dicom.DicomException;
import com.pixelmed.network.IdentifierHandler;

public class MyIdentifierHandler extends IdentifierHandler {

	@Override
	public void doSomethingWithIdentifier(AttributeList identifier) throws DicomException {
		// do nothing by now
	}
}
