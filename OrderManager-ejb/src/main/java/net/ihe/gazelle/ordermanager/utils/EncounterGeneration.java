package net.ihe.gazelle.ordermanager.utils;

import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.ordermanager.model.Encounter;
import net.ihe.gazelle.ordermanager.model.Patient;
import net.ihe.gazelle.ordermanager.model.PatientIdentifier;
import net.ihe.gazelle.patient.AbstractPatient;
import net.ihe.gazelle.patient.PatientAddress;
import net.ihe.gazelle.patient.PatientGenerator;
import net.ihe.gazelle.simulator.common.model.ApplicationConfiguration;
import net.ihe.gazelle.simulator.common.utils.XmlUtil;
import org.jboss.resteasy.client.ClientRequest;
import org.jboss.resteasy.client.ClientResponse;
import org.jboss.seam.security.Identity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.persistence.EntityManager;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * <b>Class Description : </b>EncounterGeneration<br>
 * <br>
 * This abstract class is the REST client to access the patient and encounter generation service offered by the PAM Simulator.
 *
 * @author Anne-Gaelle Berge / INRIA Rennes IHE development Project
 * @version 1.0 - 2011, November 17th
 */

public abstract class EncounterGeneration extends PatientGenerator {

    private Encounter selectedEncounter;
    private Patient selectedPatient;


    private static Logger log = LoggerFactory.getLogger(EncounterGeneration.class);

    public void generateAnEncounter() {
        EntityManager entityManager = EntityManagerService.provideEntityManager();
        if (selectedPatient == null) {
            AbstractPatient abstractPatient = generatePatient();
            selectedPatient = new Patient(abstractPatient);
            selectedPatient = selectedPatient.save(entityManager);
            List<PatientIdentifier> identifiers = new ArrayList<PatientIdentifier>();
            PatientIdentifier ddsIdentifier = new PatientIdentifier(abstractPatient.getDdsIdentifier(), selectedPatient, PatientIdentifier
					.DEFAULT_IDENTIFIER_TYPE_CODE);
            ddsIdentifier = ddsIdentifier.save(entityManager);
            identifiers.add(ddsIdentifier);
            if (abstractPatient.getNationalPatientIdentifier() != null && !abstractPatient.getNationalPatientIdentifier().isEmpty()) {
                PatientIdentifier nationalIdentifier = new PatientIdentifier(abstractPatient.getNationalPatientIdentifier(), selectedPatient,
						PatientIdentifier.NATIONAL_IDENTIFIER_TYPE_CODE);
                nationalIdentifier = nationalIdentifier.save(entityManager);
                identifiers.add(nationalIdentifier);
            }
            selectedPatient.setPatientIdentifiers(identifiers);
            selectedPatient = selectedPatient.save(entityManager);
        }
        String pamRestServiceUrl = ApplicationConfiguration.getValueOfVariable("pam_encounter_generation_url");
        ClientRequest request = new ClientRequest(pamRestServiceUrl);
        request.queryParameter("getPatient", false);
        try {
            ClientResponse<String> response = request.get(String.class);
            if (response.getStatus() == 200) {
                String xmlContent = response.getEntity();
                Document document = XmlUtil.parse(xmlContent);
                NodeList encounters = document.getElementsByTagName("Encounter");
                if (encounters.getLength() > 0) {
                    Node encounter = encounters.item(0);
                    selectedEncounter = new Encounter();
                    selectedEncounter.setCreationDate(new Date());
                    String creator;
                    if (Identity.instance().isLoggedIn()) {
                        creator = Identity.instance().getCredentials().getUsername();
                    } else {
                        creator = null;
                    }
                    selectedEncounter.setCreator(creator);
                    NamedNodeMap encounterAttributes = encounter.getAttributes();
                    selectedEncounter.setPatientClassCode(encounterAttributes.getNamedItem("patientClassCode")
                            .getTextContent());
                    if (selectedEncounter.getPatientClassCode().isEmpty()) {
                        selectedEncounter.setPatientClassCode("I");
                    }
                    selectedEncounter.setReferringDoctorCode(encounterAttributes.getNamedItem("referingDoctorCode")
                            .getTextContent());
                    selectedEncounter.setVisitNumber(encounterAttributes.getNamedItem("visitNumber").getTextContent());

                    NodeList locations = document.getElementsByTagName("AssignedPatientLocation");
                    if (locations.getLength() > 0) {
                        Node location = locations.item(0);
                        selectedEncounter.setAssignedPatientLocation(location.getTextContent());
                    }
                    selectedEncounter.setPatient(selectedPatient);
                    selectedEncounter = selectedEncounter.save(entityManager);
                }
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            selectedEncounter = null;
        }
    }

    public Encounter getSelectedEncounter() {
        return selectedEncounter;
    }

    public void setSelectedEncounter(Encounter generatedEncounter) {
        this.selectedEncounter = generatedEncounter;
    }

    public Patient getSelectedPatient() {
        return selectedPatient;
    }

    public void setSelectedPatient(Patient patient) {
        this.selectedPatient = patient;
    }

}
