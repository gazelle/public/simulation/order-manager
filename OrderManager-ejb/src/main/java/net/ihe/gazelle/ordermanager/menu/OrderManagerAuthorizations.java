package net.ihe.gazelle.ordermanager.menu;

import net.ihe.gazelle.common.pages.Authorization;
import net.ihe.gazelle.preferences.PreferenceService;
import org.jboss.seam.security.Identity;

/**
 * <p>OrderManagerAuthorizations class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
public enum OrderManagerAuthorizations implements
        Authorization {

    ALL,

    LOGGED,

    ADMIN,

    RADIOLOGY,

    EYECARE,

    LABORATORY,

    SEHE,

    NON_ADMIN;

    /** {@inheritDoc} */
    @Override
    public boolean isGranted(Object... context) {
        boolean radiology = Boolean.parseBoolean(PreferenceService.getString("radiology_enabled"));
        boolean laboratory = Boolean.parseBoolean(PreferenceService.getString("laboratory_enabled"));
        boolean eyecare = Boolean.parseBoolean(PreferenceService.getString("eyecare_enabled"));
        boolean sehe = Boolean.parseBoolean(PreferenceService.getString("SeHE_mode_enabled"));
        switch (this) {
            case ALL:
                return true;
            case NON_ADMIN:
                return !(Identity.instance().isLoggedIn() && Identity.instance().hasRole("admin_role"));
            case LOGGED:
                return Identity.instance().isLoggedIn();
            case ADMIN:
                return Identity.instance().isLoggedIn() && Identity.instance().hasRole("admin_role");
            case RADIOLOGY:
                return radiology;
            case EYECARE:
                return eyecare;
            case LABORATORY:
                return laboratory;
            case SEHE:
                return sehe;
            default:
                return false;
        }
    }

}
