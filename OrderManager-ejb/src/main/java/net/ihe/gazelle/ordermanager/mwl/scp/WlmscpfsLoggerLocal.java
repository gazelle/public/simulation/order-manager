package net.ihe.gazelle.ordermanager.mwl.scp;

import javax.ejb.Local;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

/**
 * <b>Class Description : </b>WlmscpfsLoggerLocal<br>
 * <br>
 *
 * @author Anne-Gaelle Berge / IHE-Europe development Project
 * @version 1.0 - 03/10/16
 *
 *
 *
 */

@Local
@Path("/worklist")
public interface WlmscpfsLoggerLocal {

    @GET
    @Path("/logs")
    @Produces("application/json")
    String getLog(@Context HttpServletRequest req);

    @GET
    @Path("/Hello")
    @Produces("text/plain")
    Response hello(@Context HttpServletRequest req);
}
