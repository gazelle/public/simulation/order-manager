package net.ihe.gazelle.ordermanager.datamodel;

import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.restrictions.HQLRestrictionLikeMatchMode;
import net.ihe.gazelle.hql.restrictions.HQLRestrictions;
import net.ihe.gazelle.ordermanager.model.UserPreferences;
import net.ihe.gazelle.ordermanager.model.WorklistEntry;
import net.ihe.gazelle.simulator.common.action.ApplicationConfigurationManagerLocal;
import net.ihe.gazelle.simulator.common.tf.model.Domain;
import org.jboss.seam.Component;
import org.jboss.seam.security.Identity;

import java.util.Date;

/**
 * Class description : <b>WorklistDataModel</b>
 * 
 * This class is a data model for properly and quickly displaying the worklist objects in the GUI
 * 
 *
 *
 * @author Anne-Gaëlle Bergé - INRIA Rennes/Bretagne Atlantique
 *
 * @version 1.0 - 2011, November 17th
 * 
 */
public class WorklistEntryDataModel extends FilterDataModel<WorklistEntry> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3087693340656182833L;
	private String creator;
	private Date startDate;
	private Date endDate;
	private String visitNumber;
	private String patientId;
	private String fillerNumber;
	private String placerNumber;
	private boolean showIfDeleted;
	private Domain domain;

	public WorklistEntryDataModel() {
		super(new WorklistEntryFilter());
	}

	public WorklistEntryDataModel(Domain simulatedDomain, Date startDate, Date endDate, String visitNumber,
			String patientId, String fillerNumber, String placerNumber) {
		super(new WorklistEntryFilter());
		this.creator = null;
		showIfDeleted = false;
		if (Identity.instance().isLoggedIn()) {
			UserPreferences preferences = (UserPreferences) Component.getInstance("userPreferences");
			if (preferences.isViewOnlyMyObjects()) {
				this.creator = preferences.getUsername();
			}
			ApplicationConfigurationManagerLocal manager = (ApplicationConfigurationManagerLocal) Component
					.getInstance("applicationConfigurationManager");
			if (manager.isUserAllowedAsAdmin()) {
				showIfDeleted = true;
			}
		}
		setStartDate(startDate);
		setEndDate(endDate);
		this.visitNumber = visitNumber;
		this.patientId = patientId;
		this.fillerNumber = fillerNumber;
		this.placerNumber = placerNumber;
		this.domain = simulatedDomain;
	}

	@Override
	public void appendFiltersFields(HQLQueryBuilder<WorklistEntry> hqlBuilder) {
		if ((creator != null) && !creator.isEmpty()) {
			hqlBuilder.addEq("creator", creator);
		}
		if (startDate != null) {
			hqlBuilder.addRestriction(HQLRestrictions.gt("creationTime", startDate));
		}
		if (endDate != null) {
			hqlBuilder.addRestriction(HQLRestrictions.lt("creationTime", endDate));
		}
		if ((patientId != null) && !patientId.isEmpty()) {
			hqlBuilder.addRestriction(HQLRestrictions.like(
					"scheduledProcedureStep.requestedProcedure.order.encounter.patient.patientIdentifiers", patientId,
					HQLRestrictionLikeMatchMode.ANYWHERE));
		}
		if ((visitNumber != null) && !visitNumber.isEmpty()) {
			hqlBuilder.addRestriction(HQLRestrictions.like(
					"scheduledProcedureStep.requestedProcedure.order.encounter.visitNumber", visitNumber,
					HQLRestrictionLikeMatchMode.ANYWHERE));
		}
		if ((fillerNumber != null) && !fillerNumber.isEmpty()) {
			hqlBuilder.addRestriction(HQLRestrictions.like(
					"scheduledProcedureStep.requestedProcedure.order.fillerOrderNumber", fillerNumber,
					HQLRestrictionLikeMatchMode.ANYWHERE));
		}
		if ((placerNumber != null) && !placerNumber.isEmpty()) {
			hqlBuilder.addRestriction(HQLRestrictions.like(
					"scheduledProcedureStep.requestedProcedure.order.placerOrderNumber", placerNumber,
					HQLRestrictionLikeMatchMode.ANYWHERE));
		}
		if (!showIfDeleted) {
			hqlBuilder.addEq("deleted", false);
		}
		if (domain != null) {
			hqlBuilder.addEq("domain", domain);
		}
	}

	public String getCreator() {
		return creator;
	}

	public void setCreator(String creator) {
		this.creator = creator;
	}

    public Date getStartDate() {
        if (this.startDate != null) {
            return (Date) startDate.clone();
        } else {
            return null;
        }
    }

    public final void setStartDate(Date startDate) {
        if (startDate != null) {
            this.startDate = (Date) startDate.clone();
        }else {
            this.startDate = null;
        }
    }

    public Date getEndDate() {
        if (this.endDate != null) {
            return (Date) endDate.clone();
        } else {
            return null;
        }
    }

    public final void setEndDate(Date endDate) {
        if (endDate != null) {
            this.endDate = (Date) endDate.clone();
        } else {
            this.endDate = null;
        }
    }

@Override
        protected Object getId(WorklistEntry t) {
        // TODO Auto-generated method stub
        return t.getId();
        }
}
