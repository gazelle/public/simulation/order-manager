package net.ihe.gazelle.ordermanager.filter;

import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import net.ihe.gazelle.hql.criterion.QueryModifier;
import net.ihe.gazelle.ordermanager.model.RequestedProcedure;
import net.ihe.gazelle.ordermanager.model.RequestedProcedureQuery;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.common.tf.model.Domain;

import java.util.Map;

/**
 * <b>Class Description : </b>AbstractOrderFilter<br>
 * <br>
 *
 * @author Anne-Gaelle Berge / IHE-Europe development Project
 * @version 1.0 - 30/06/16
 *
 *
 *
 */
public class ProcedureFilter implements QueryModifier<RequestedProcedure> {

    private String fillerOrderNumber;
    private String placerOrderNumber;
    private Actor actor;
    private Domain domain;
    private String visitnumber;
    private String patientid;
    private String newOrderControlCode;
    private Filter<RequestedProcedure> filter;

    public ProcedureFilter(Domain domain, Actor simulatedActor, String newOrderControlCode) {
        this.domain = domain;
        this.actor = simulatedActor;
        this.newOrderControlCode = newOrderControlCode;
    }

    public Filter<RequestedProcedure> getFilter() {
        if (filter == null) {
            filter = new Filter<RequestedProcedure>(getHQLCriterionsForFilter());
        }
        return filter;
    }

    protected HQLCriterionsForFilter<RequestedProcedure> getHQLCriterionsForFilter() {
        RequestedProcedureQuery query = new RequestedProcedureQuery();
        HQLCriterionsForFilter<RequestedProcedure> criteria = query.getHQLCriterionsForFilter();
        criteria.addPath("firstname", query.order().encounter().patient().firstName());
        criteria.addPath("lastname", query.order().encounter().patient().lastName());
        criteria.addPath("actor", query.simulatedActor(), getActor());
        criteria.addPath("domain", query.domain(), getDomain());
        criteria.addPath("creator", query.creator());
        criteria.addPath("creationDate", query.lastChanged());
        criteria.addQueryModifier(this);
        return criteria;
    }

    public FilterDataModel<RequestedProcedure> getProcedures() {
        return new FilterDataModel<RequestedProcedure>(getFilter()) {
            @Override
            protected Object getId(RequestedProcedure t) {
                return t.getId();
            }
        };
    }

    @Override
    public void modifyQuery(HQLQueryBuilder<RequestedProcedure> hqlQueryBuilder, Map<String, Object> map) {
        RequestedProcedureQuery query = new RequestedProcedureQuery();
        if (getPlacerOrderNumber() != null && !getPlacerOrderNumber().isEmpty()) {
            hqlQueryBuilder.addRestriction(query.order().placerOrderNumber().likeRestriction(getPlacerOrderNumber()));
        }
        if ((getFillerOrderNumber() != null) && !getFillerOrderNumber().isEmpty()) {
            hqlQueryBuilder.addRestriction(query.order().fillerOrderNumber().likeRestriction(getFillerOrderNumber()));
        }
        if (getPatientid() != null && !getPatientid().isEmpty()){
            hqlQueryBuilder.addRestriction(query.order().encounter().patient().patientIdentifiers().identifier().likeRestriction(getPatientid()));
        }
        if (getVisitnumber() != null && !getVisitnumber().isEmpty()){
            hqlQueryBuilder.addRestriction(query.order().encounter().visitNumber().likeRestriction(getVisitnumber()));
        }
        if ((newOrderControlCode != null) && !newOrderControlCode.isEmpty()) {
            if (newOrderControlCode.equals("DC") || newOrderControlCode.equals("CA") || newOrderControlCode.equals("SC")) {
                hqlQueryBuilder.addRestriction(query.orderStatus().eqRestriction("SC"));
            }
            // order filler updates order status
            else if (newOrderControlCode.equals("CM")) {
                hqlQueryBuilder.addRestriction(query.orderStatus().neqRestriction("CA"));
            }
        }
    }

    public void reset() {
        fillerOrderNumber = null;
        placerOrderNumber = null;
        visitnumber = null;
        patientid = null;
        getFilter().clear();
    }

    public String getFillerOrderNumber() {
        return fillerOrderNumber;
    }

    public void setFillerOrderNumber(String fillerOrderNumber) {
        this.fillerOrderNumber = fillerOrderNumber;
    }

    public String getPlacerOrderNumber() {
        return placerOrderNumber;
    }

    public void setPlacerOrderNumber(String placerOrderNumber) {
        this.placerOrderNumber = placerOrderNumber;
    }

    public String getVisitnumber() {
        return visitnumber;
    }

    public void setVisitnumber(String visitnumber) {
        this.visitnumber = visitnumber;
    }

    public String getPatientid() {
        return patientid;
    }

    public void setPatientid(String patientid) {
        this.patientid = patientid;
    }

    public Actor getActor() {
        return actor;
    }

    public Domain getDomain() {
        return domain;
    }

    public String getNewOrderControlCode() {
        return newOrderControlCode;
    }
}
