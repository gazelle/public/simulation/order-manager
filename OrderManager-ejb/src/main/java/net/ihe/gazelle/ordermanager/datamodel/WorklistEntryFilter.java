package net.ihe.gazelle.ordermanager.datamodel;

import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.hql.criterion.HQLCriterion;
import net.ihe.gazelle.ordermanager.model.WorklistEntry;

import java.util.ArrayList;
import java.util.List;

/**
 * Class description : <b>WorklistFilter</b>
 * 
 *
 *
 * @author Anne-Gaëlle Bergé - INRIA Rennes/Bretagne Atlantique
 *
 * @version 1.0 - 2011, November 17th
 * 
 */
public class WorklistEntryFilter extends Filter<WorklistEntry> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6943023175775676445L;
	private static List<HQLCriterion<WorklistEntry, ?>> defaultCriterions;

	static {
		defaultCriterions = new ArrayList<HQLCriterion<WorklistEntry, ?>>();
	}

	public WorklistEntryFilter() {
		super(WorklistEntry.class, defaultCriterions);
	}
}
