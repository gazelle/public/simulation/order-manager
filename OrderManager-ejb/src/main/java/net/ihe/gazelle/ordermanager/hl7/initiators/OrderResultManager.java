package net.ihe.gazelle.ordermanager.hl7.initiators;

import ca.uhn.hl7v2.HL7Exception;
import net.ihe.gazelle.HL7Common.initiator.Initiator;
import net.ihe.gazelle.HL7Common.model.HL7Protocol;
import net.ihe.gazelle.HL7Common.model.HL7V2ResponderSUTConfiguration;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.ordermanager.datamodel.ContainerDataModel;
import net.ihe.gazelle.ordermanager.datamodel.LabOrderDataModel;
import net.ihe.gazelle.ordermanager.datamodel.SpecimenDataModel;
import net.ihe.gazelle.ordermanager.filter.ContainerFilter;
import net.ihe.gazelle.ordermanager.filter.LabOrderFilter;
import net.ihe.gazelle.ordermanager.filter.SpecimenFilter;
import net.ihe.gazelle.ordermanager.model.Container;
import net.ihe.gazelle.ordermanager.model.Encounter;
import net.ihe.gazelle.ordermanager.model.LabOrder;
import net.ihe.gazelle.ordermanager.model.Observation;
import net.ihe.gazelle.ordermanager.model.Specimen;
import net.ihe.gazelle.ordermanager.utils.LAWOptions;
import net.ihe.gazelle.ordermanager.utils.LAWSpecimenRole;
import net.ihe.gazelle.ordermanager.utils.LabMessageBuilder;
import net.ihe.gazelle.simulator.common.model.ApplicationConfiguration;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.message.model.TransactionInstance;
import net.ihe.gazelle.ssov7.gum.client.application.UserAttributeCommon;
import net.ihe.gazelle.ssov7.gum.client.application.service.UserService;
import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.core.ResourceBundle;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.model.SelectItem;
import javax.persistence.EntityManager;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;


/**
 * <b>Class description</b> OrderResultManager This class is the managed-bean for hl7Initiators/orderResultSender.xhtml dedicated to the sending of order result notifications by the Order Filler actor
 * to the ORT and to the sending of test result notifications by the Automation Manager actor to the OF.
 *
 * @author Anne-Gaëlle Bergé / IHE-Europe
 * @version 1.0 - February, 28th 2012
 */
@Name("orderResultManager")
@Scope(ScopeType.PAGE)
public class OrderResultManager extends AbstractResultManager<LabOrder> implements Serializable, UserAttributeCommon {

    private static final long serialVersionUID = 7353304477926725865L;

    /** Constant <code>ANALYZER="ANALYZER"</code> */
    protected static final String ANALYZER = "ANALYZER";

    private static Logger log = LoggerFactory.getLogger(OrderResultManager.class);

    private boolean workOrder;
    private static final SelectItem[] LIST_OF_ROLES = LAWSpecimenRole.getSpecimenRoleList();
    private List<LAWOptions> availableOptionsForLAW = null;
    private List<LAWOptions> selectedOptions = null;

    @In(value="gumUserService")
    private UserService userService;

    /** {@inheritDoc} */
    @Override
    public List<HL7V2ResponderSUTConfiguration> getAvailableSuts() {
        return HL7V2ResponderSUTConfiguration.getAllConfigurationsForSelection(transaction.getKeyword(), HL7Protocol.MLLP);
    }

    /** {@inheritDoc} */
    @Override
    @Create
    public void initializePage() {
        super.initializePage();
        if (simulatedActor.getKeyword().equals("AM")) {
            workOrder = true;
        } else {
            workOrder = false;
        }
        // check if an object is given in the URL
        if (urlParams.containsKey("labOrderId")) {
            LabOrder order = (EntityManagerService.provideEntityManager()).find(LabOrder.class,
                    Integer.decode(urlParams.get("labOrderId")));
            setSelectedOrder(order);
            messageStructure = BATTERYCENTRIC;
        } else if (urlParams.containsKey("specimenId")) {
            Specimen specimen = (EntityManagerService.provideEntityManager()).find(Specimen.class,
                    Integer.decode(urlParams.get("specimenId")));
            selectSpecimen(specimen);
            if ((selectedSpecimen != null) && (selectedSpecimen.getOrders() != null)
                    && !selectedSpecimen.getOrders().isEmpty()) {
                messageStructure = SPECIMENCENTRIC;
            } else {
                messageStructure = CONTAINERCENTRIC;
            }
        } else {
            onChangeActionEvent();
        }
        // for ANALYZER
        if (simulatedActor.getKeyword().equals(ANALYZER)) {
            availableOptionsForLAW = LAWOptions.getOptionsForActor(ANALYZER, "LAB-29");
            selectedOptions = new ArrayList<LAWOptions>();
        }

    }

    /** {@inheritDoc} */
    @Override
    public void fillOrderRandomly() {
        selectedOrder.fillOrderRandomly(selectedDomain, simulatedActor);
    }


    /** {@inheritDoc} */
    @Override
    public List<SelectItem> listAvailableActions() {
        List<SelectItem> items = new ArrayList<SelectItem>();
        items.add(new SelectItem(SPECIMENCENTRIC, ResourceBundle.instance().getString(
                "gazelle.ordermanager.SpecimenCenteredReporting")));
        if (sutActorKeyword.equals("ORT")) {
            // LAB-3
            items.add(new SelectItem(BATTERYCENTRIC, ResourceBundle.instance().getString(
                    "gazelle.ordermanager.OrderCenteredReporting")));
        } else if (sutActorKeyword.equals("OF")) {
            // LAB-5
            items.add(new SelectItem(CONTAINERCENTRIC, ResourceBundle.instance().getString(
                    "gazelle.ordermanager.ContainerCenterReporting")));
        }
        return items;
    }

    /** {@inheritDoc} */
    @Override
    public void sendMessage() {
        HL7V2ResponderSUTConfiguration sut = (HL7V2ResponderSUTConfiguration) Component.getInstance("selectedSUT");
        if (sut == null) {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "No SUT selected ! please select the system under test and retry");
            return;
        }
        LabMessageBuilder generator = new LabMessageBuilder(transaction.getKeyword(), simulatedActor.getKeyword(),
                getSelectedEncounter(), selectedOrder, selectedSpecimen, sut, sendingApplication, sendingFacility,
                messageStructure, selectedOptions);

        String messageToSend = null;
        try {
            messageToSend = generator.generateMessage();
        } catch (Exception e) {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "An error occurred during message generation");
            log.error(e.getMessage(), e);
            return;
        }

        if (messageToSend != null) {
            try {
                Initiator hl7Initiator = new Initiator(sut, sendingFacility, sendingApplication, simulatedActor,
                        transaction, messageToSend, generator.getMessageStructure(), "LAB", Actor.findActorWithKeyword(sutActorKeyword));

                TransactionInstance hl7Message = hl7Initiator.sendMessageAndGetTheHL7Message();
                hl7Messages = new ArrayList<TransactionInstance>();
                hl7Messages.add(hl7Message);
                setSelectedEncounter(null);
                setSelectedPatient(null);
                selectedOrder = null;
                selectedSpecimen = null;
            } catch (HL7Exception e) {
                FacesMessages.instance().add(
                        "The simulator has been unable to send message to your SUT for the following reason: "
                                + e.getMessage());
                log.error(e.getMessage(), e);
                return;
            }
        }
    }

    /** {@inheritDoc} */
    @Override
    public void addObservationToOrder() {
        if (selectedOrder.getObservations() == null) {
            selectedOrder.setObservations(new ArrayList<Observation>(Arrays.asList(selectedObservation)));
        } else {
            selectedOrder.getObservations().add(selectedObservation);
        }
        selectedOrder = selectedOrder.save(EntityManagerService.provideEntityManager());
        selectedObservation = null;
        // reset the selectedOrder to null if the main entity is the specimen
        if (!messageStructure.equals(BATTERYCENTRIC)) {
            selectedOrder = null;
        }
    }

    @Override
    public String getUserName(String userId) {
        return userService.getUserDisplayNameWithoutException(userId);
    }

    /**
     * <p>getSpecimensAsDataModelForSelectedOrder.</p>
     *
     * @return a {@link net.ihe.gazelle.ordermanager.datamodel.SpecimenDataModel} object.
     */
    public SpecimenDataModel getSpecimensAsDataModelForSelectedOrder(){
        return new SpecimenDataModel(selectedOrder);
    }

    /**
     * <p>getContainerDataModelForSelectedSpecimen.</p>
     *
     * @return a {@link net.ihe.gazelle.ordermanager.datamodel.ContainerDataModel} object.
     */
    public ContainerDataModel getContainerDataModelForSelectedSpecimen(){
        if(selectedSpecimen != null){
            return new ContainerDataModel(selectedSpecimen);
        } else {
            return null;
        }
    }

    /**
     * <p>getOrdersAsDataModel.</p>
     *
     * @param sac a {@link net.ihe.gazelle.ordermanager.model.Container} object.
     * @return a {@link net.ihe.gazelle.ordermanager.datamodel.LabOrderDataModel} object.
     */
    public LabOrderDataModel getOrdersAsDataModel(Container sac){
        return new LabOrderDataModel(sac);
    }

    /**
     * <p>getOrdersAsDataModelForSelectedSpecimen.</p>
     *
     * @return a {@link net.ihe.gazelle.ordermanager.datamodel.LabOrderDataModel} object.
     */
    public LabOrderDataModel getOrdersAsDataModelForSelectedSpecimen(){
        return new LabOrderDataModel(selectedSpecimen);
    }

    /** {@inheritDoc} */
    @Override
    public void createNewResult() {
        EntityManager entityManager = EntityManagerService.provideEntityManager();
        if (messageStructure.equals(BATTERYCENTRIC)) {
            selectedOrder = new LabOrder(selectedDomain, simulatedActor, getSelectedEncounter(), entityManager, true,
                    workOrder);
            selectedOrder.setCreator(username);
            selectedOrder.setContainer(null);
            selectedOrder.setOrderControlCode("SC");
            selectedSpecimen = null;
            selectedContainer = null;
            editOrder = true;
            editSpecimen = false;
        } else if (messageStructure.equals(SPECIMENCENTRIC)) {
            selectedOrder = null;
            selectedSpecimen = new Specimen(true, selectedDomain, simulatedActor, entityManager, workOrder);
            selectedSpecimen.setCreator(username);
            numberOfContainers = 1;
            editOrder = false;
            editSpecimen = true;
            if (simulatedActor.getKeyword().equals(ANALYZER)) {
                selectedContainer = new Container(false, selectedDomain, simulatedActor);
                selectedContainer.setSpecimen(selectedSpecimen);
                selectedSpecimen.setContainers(new ArrayList<Container>());
                selectedSpecimen.getContainers().add(selectedContainer);
            } else {
                selectedContainer = null;
            }
        } else {
            // CONTAINER-CENTRIC
            selectedSpecimen = new Specimen(true, selectedDomain, simulatedActor, entityManager, workOrder);
            selectedSpecimen.setCreator(username);
            editOrder = false;
            editSpecimen = true;
            selectedContainer = null;
            selectedOrder = null;
        }
    }

    /** {@inheritDoc} */
    @Override
    public void createNewObservationForOrder() {
        // sometimes, the List attributes of the inOrder object are not load properly,
        // reload the selectedOrder
        EntityManager entityManager = EntityManagerService.provideEntityManager();
        selectedOrder = entityManager.find(LabOrder.class, selectedOrder.getId());
        selectedObservation = new Observation(selectedDomain, simulatedActor, username);
        selectedObservation.setOrder(selectedOrder);
        if ((selectedOrder.getObservations() == null) || selectedOrder.getObservations().isEmpty()) {
            selectedObservation.setSetId(1);
        } else {
            selectedObservation.setSetId(selectedOrder.getObservations().size() + 1);
        }
        if (simulatedActor.getKeyword().equals(ANALYZER)) {
            String toolOid = ApplicationConfiguration.getValueOfVariable("analyzer_serial_number");
            selectedObservation.setEquipment("Analyzer^Gazelle~" + toolOid + "^Gazelle");
            selectedObservation.setRunId(1);
        }
        selectedObservation.setIncludeInMessage(true);
        observationParent = PARENTORDER;
    }

    /** {@inheritDoc} */
    @Override
    public void onChangeActionToBatteryCentric() {
        orderFilter = new LabOrderFilter(selectedDomain, simulatedActor, null, true, workOrder);
        setSelectionType(SelectionType.ORDER);
    }

    /** {@inheritDoc} */
    @Override
    public void onChangeActionToContainerCentric() {
        if (simulatedActor.getKeyword().equals(ANALYZER)) {
            containerFilter = new ContainerFilter(false, simulatedActor);
            setSelectionType(SelectionType.AWOS);
        } else {
            specimenFilter = new SpecimenFilter(true, workOrder, simulatedActor,
                    true);
            setSelectionType(SelectionType.SPECIMEN);
        }
    }

    /** {@inheritDoc} */
    @Override
    public void onChangeActionToSpecimenCentric() {
        specimenFilter = new SpecimenFilter(true, workOrder, simulatedActor, false);
        setSelectionType(SelectionType.SPECIMEN);
    }

    /** {@inheritDoc} */
    @Override
    public void selectSpecimen() {
        if (selectedSpecimen == null) {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "No specimen selected !");
        } else {
            if ((selectedSpecimen.getOrders() != null) && !selectedSpecimen.getOrders().isEmpty()) {
                setSelectedEncounter(selectedSpecimen.getOrders().get(0).getEncounter());
            } else if ((selectedSpecimen.getContainers() != null) && !selectedSpecimen.getContainers().isEmpty()
                    && (selectedSpecimen.getContainers().get(0).getOrders() != null)
                    && !selectedSpecimen.getContainers().get(0).getOrders().isEmpty()) {
                setSelectedEncounter(selectedSpecimen.getContainers().get(0).getOrders().get(0).getEncounter());
            } else {
                setSelectedEncounter(new Encounter());
            }
            if (getSelectedEncounter() != null) {
                setSelectedPatient(getSelectedEncounter().getPatient());
            } else {
                setSelectedPatient(null);
                selectPatient = false;
            }
        }
    }

    /** {@inheritDoc} */
    @Override
    public void addOrderToContainer() {
        selectedOrder.setContainer(selectedContainer);
        int currentContainerIndex = selectedSpecimen.getContainers().indexOf(selectedContainer);
        selectedContainer = selectedSpecimen.getContainers().get(currentContainerIndex);
        if (selectedContainer.getOrders() == null) {
            selectedContainer.setOrders(new ArrayList<LabOrder>());
        }
        selectedContainer.getOrders().add(selectedOrder);
        selectedSpecimen.getContainers().set(currentContainerIndex, selectedContainer);
        selectedOrder = null;
        expendPanel = true;
    }

    /** {@inheritDoc} */
    @Override
    public void addOrderToSpecimen() {
        if (selectedSpecimen.getOrders() == null) {
            selectedSpecimen.setOrders(new ArrayList<LabOrder>());
        }
        selectedSpecimen.getOrders().add(selectedOrder);
        createOrderForSpecimen();
        expendPanel = true;
        editOrder = false;
        selectedOrder = null;
    }

    /** {@inheritDoc} */
    @Override
    public void addSpecimenToOrder() {
        if (selectedOrder.getSpecimens() == null) {
            selectedOrder.setSpecimens(new ArrayList<Specimen>());
        }
        createContainersForSpecimen();
        selectedOrder.getSpecimens().add(selectedSpecimen);
        createSpecimenForOrder();
        expendPanel = true;
        selectedSpecimen = null;
        editSpecimen = false;
    }

    private void createContainersForSpecimen() {
        if ((numberOfContainers != null) && (numberOfContainers > 0)) {
            selectedSpecimen.setContainers(new ArrayList<Container>());
            for (int index = 1; index <= numberOfContainers; index++) {
                Container container = new Container(false, selectedDomain, simulatedActor);
                container.setSpecimen(selectedSpecimen);
                container.setWorkOrder(workOrder);
                selectedSpecimen.getContainers().add(container);
            }
        }
    }

    /** {@inheritDoc} */
    @Override
    public void createContainerForSpecimen() {
        if (selectedSpecimen.getContainers() == null) {
            selectedSpecimen.setContainers(new ArrayList<Container>());
        }
        Container container = new Container(true, selectedDomain, simulatedActor);
        container.setWorkOrder(workOrder);
        container.setSpecimen(selectedSpecimen);
        selectedSpecimen.getContainers().add(container);
        expendPanel = true;
        editSpecimen = false;
    }

    /** {@inheritDoc} */
    @Override
    public void createOrderForContainer(Container inContainer) {
        selectedContainer = inContainer;
        selectedOrder = new LabOrder(selectedDomain, simulatedActor, getSelectedEncounter(),
                EntityManagerService.provideEntityManager(), false, workOrder);
        selectedOrder.setOrderControlCode("SC");
        selectedOrder.setCreator(username);
        expendPanel = false;
    }

    /** {@inheritDoc} */
    @Override
    public void createOrderForSpecimen() {
        editSpecimen = false;
        createContainersForSpecimen();
        selectedOrder = new LabOrder(selectedDomain, simulatedActor, getSelectedEncounter(),
                EntityManagerService.provideEntityManager(), false, workOrder);
        selectedOrder.setCreator(username);
        selectedOrder.setSpecimens(new ArrayList<Specimen>());
        selectedOrder.getSpecimens().add(selectedSpecimen);
        if (!simulatedActor.getKeyword().equals(ANALYZER)) {
            selectedOrder.setOrderControlCode("SC");
        }
        expendPanel = false;
        editOrder = true;
    }

    /** {@inheritDoc} */
    @Override
    public void createSpecimenForOrder() {
        editOrder = false;
        selectedSpecimen = new Specimen(false, selectedDomain, simulatedActor,
                EntityManagerService.provideEntityManager(), workOrder);
        selectedSpecimen.setCreator(username);
        selectedSpecimen.setOrders(new ArrayList<LabOrder>());
        selectedSpecimen.getOrders().add(selectedOrder);
        numberOfContainers = 1;
        expendPanel = false;
        editSpecimen = true;
    }

    /** {@inheritDoc} */
    @Override
    public void removeContainerFromSpecimen(Container inContainer) {
        selectedSpecimen.getContainers().remove(inContainer);
    }

    /** {@inheritDoc} */
    @Override
    public void removeOrderFromContainer(Container currentContainer, LabOrder currentOrder) {
        int currentContainerIndex = selectedSpecimen.getContainers().indexOf(currentContainer);
        selectedContainer = selectedSpecimen.getContainers().get(currentContainerIndex);
        selectedContainer.getOrders().remove(currentOrder);
        selectedSpecimen.getContainers().set(currentContainerIndex, selectedContainer);
    }

    /** {@inheritDoc} */
    @Override
    public void removeOrderFromSpecimen(LabOrder inOrder) {
        selectedSpecimen.getOrders().remove(inOrder);
    }

    /** {@inheritDoc} */
    @Override
    public void removeSpecimenFromOrder(Specimen inSpecimen) {
        selectedOrder.getSpecimens().remove(inSpecimen);
    }

    /** {@inheritDoc} */
    @Override
    public void saveObjectAndGetItReadyForObservations() {
        EntityManager entityManager = EntityManagerService.provideEntityManager();
        if (messageStructure.equals(BATTERYCENTRIC)) {
            selectedOrder = selectedOrder.save(entityManager);
        } else {
            selectedSpecimen = selectedSpecimen.save(entityManager);
        }
        newObject = false;
    }

    /** {@inheritDoc} */
    @Override
    public void updateOrder(LabOrder inOrder) {
        inOrder.save(EntityManagerService.provideEntityManager());
        FacesMessages.instance().add(StatusMessage.Severity.INFO, "Order updated");
    }

    /** {@inheritDoc} */
    @Override
    public void selectObservationForEdit(Observation inObservation) {
        EntityManager entityManager = EntityManagerService.provideEntityManager();
        selectedObservation = entityManager.find(Observation.class, inObservation.getId());
        if (selectedObservation.getSpecimen() != null) {
            observationParent = PARENTSPECIMEN;
            selectedSpecimen = selectedObservation.getSpecimen();
        } else {
            observationParent = PARENTORDER;
            selectedOrder = selectedObservation.getOrder();
        }
    }

    /** {@inheritDoc} */
    @Override
    public void updateObservation() {
        if (selectedObservation != null) {
            selectedObservation.setLastChanged(new Date());
            EntityManager entityManager = EntityManagerService.provideEntityManager();
            entityManager.merge(selectedObservation);
            entityManager.flush();
            selectedObservation = null;
            if (messageStructure.equals(BATTERYCENTRIC)) {
                selectedOrder = entityManager.find(LabOrder.class, selectedOrder.getId());
            } else {
                selectedSpecimen = entityManager.find(Specimen.class, selectedSpecimen.getId());
            }
        }
    }

    /**
     * <p>selectObservationForRerun.</p>
     *
     * @param inObservation a {@link net.ihe.gazelle.ordermanager.model.Observation} object.
     */
    public void selectObservationForRerun(Observation inObservation) {
        Observation reRun = new Observation(inObservation.getDomain(), simulatedActor, username);
        reRun.setOrder(inObservation.getOrder());
        if (inObservation.getRunId() == null){
            reRun.setRunId(1);
        } else {
            reRun.setRunId(inObservation.getRunId() + 1);
        }
        if (inObservation.getOrder().getObservations() == null){
            reRun.setSetId(1);
        } else {
            reRun.setSetId(inObservation.getOrder().getObservations().size() + 1);
        }
        reRun.setIdentifier(inObservation.getIdentifier());
        reRun.setValueType(inObservation.getValueType());
        reRun.setUnits(inObservation.getUnits());
        reRun.setIncludeInMessage(true);
        reRun.setEquipment(inObservation.getEquipment());
        reRun.setReferencesRange(inObservation.getReferencesRange());
        reRun = reRun.save(EntityManagerService.provideEntityManager());
        selectObservationForEdit(reRun);
    }

    /**
     * <p>reflexOrder.</p>
     *
     * @param inOrder a {@link net.ihe.gazelle.ordermanager.model.LabOrder} object.
     */
    public void reflexOrder(LabOrder inOrder) {
        selectedOrder = new LabOrder(selectedDomain, simulatedActor, inOrder.getEncounter(),
                EntityManagerService.provideEntityManager(), false, false);
        selectedOrder.setParent(new ArrayList<String>());
        selectedOrder.getParent().add(inOrder.getPlacerOrderNumber());
        selectedOrder.setPlacerOrderNumber("\"\"");
        selectedOrder.setOrderControlCode("OK");
        selectedOrder.setOrderStatus("SC");
        editOrder = true;
        newObject = true;
    }

    /**
     * <p>isOptionSupported.</p>
     *
     * @param optionKeyword a {@link java.lang.String} object.
     * @return a boolean.
     */
    public boolean isOptionSupported(String optionKeyword) {
        LAWOptions option = LAWOptions.getOptionByKeyword(optionKeyword);
        if (option == null || selectedOptions.isEmpty()) {
            return false;
        } else{
            return selectedOptions.contains(option);
        }
    }

    /**
     *************************************
     * GETTERS and SETTERS
     **************************************
     *
     * @return a boolean.
     */
    public boolean isWorkOrder() {
        return workOrder;
    }

    /**
     * <p>Setter for the field <code>workOrder</code>.</p>
     *
     * @param workOrder a boolean.
     */
    public void setWorkOrder(boolean workOrder) {
        this.workOrder = workOrder;
    }

    /**
     * <p>getListOfRoles.</p>
     *
     * @return an array of {@link javax.faces.model.SelectItem} objects.
     */
    public SelectItem[] getListOfRoles() {
        return LIST_OF_ROLES.clone();
    }

    /**
     * <p>Getter for the field <code>selectedOptions</code>.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<LAWOptions> getSelectedOptions() {
        return selectedOptions;
    }

    /**
     * <p>Setter for the field <code>selectedOptions</code>.</p>
     *
     * @param selectedOptions a {@link java.util.List} object.
     */
    public void setSelectedOptions(List<LAWOptions> selectedOptions) {
        if (selectedOptions != null) {
            this.selectedOptions = selectedOptions;
        } else {
            this.selectedOptions = null;
        }
    }

    /**
     * <p>Getter for the field <code>availableOptionsForLAW</code>.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<LAWOptions> getAvailableOptionsForLAW() {
        return availableOptionsForLAW;
    }
}
