package net.ihe.gazelle.ordermanager.action;

import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.ordermanager.model.Container;
import net.ihe.gazelle.ordermanager.model.Specimen;
import net.ihe.gazelle.ssov7.gum.client.application.UserAttributeCommon;
import net.ihe.gazelle.ssov7.gum.client.application.service.UserService;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import java.io.Serializable;
import java.util.Map;

@Name("specimenManager")
@Scope(ScopeType.PAGE)
public class SpecimenManager implements Serializable, UserAttributeCommon {

	private static final long serialVersionUID = 1L;

    private static Logger log = LoggerFactory.getLogger(SpecimenManager.class);
    private Specimen selectedSpecimen;
	private Integer selectedContainerId;
	private Container selectedContainer;

	@In(value="gumUserService")
	private UserService userService;

    @Create
	public void selectSpecimenById() {
		Map<String, String> urlParams = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		if ((urlParams != null) && urlParams.containsKey("id")) {
			try {
				Integer specimenId = Integer.valueOf(urlParams.get("id"));
				EntityManager entityManager = EntityManagerService.provideEntityManager();
				selectedSpecimen = entityManager.find(Specimen.class, specimenId);
				selectedContainerId = null;
			} catch (NumberFormatException e) {
				log.error("invalid id for order");
				FacesMessages.instance().add(urlParams.get("id") + " is not a valid parameter");
				selectedSpecimen = null;
				selectedContainerId = null;
			}
		}
	}

	public void setSelectedSpecimen(Specimen selectedSpecimen) {
		this.selectedSpecimen = selectedSpecimen;
	}

	public Specimen getSelectedSpecimen() {
		return selectedSpecimen;
	}

	public void setSelectedContainerId(Integer selectedContainerId) {
		this.selectedContainerId = selectedContainerId;
		if (selectedContainerId != null) {
			selectedContainer = (EntityManagerService.provideEntityManager()).find(Container.class,
					selectedContainerId);
		} else {
			selectedContainer = null;
		}
	}

	public Integer getSelectedContainerId() {
		return selectedContainerId;
	}

	public void setSelectedContainer(Container selectedContainer) {
		this.selectedContainer = selectedContainer;
	}

	public Container getSelectedContainer() {
		return selectedContainer;
	}

	@Override
	public String getUserName(String userId) {
		return userService.getUserDisplayNameWithoutException(userId);
	}
}
