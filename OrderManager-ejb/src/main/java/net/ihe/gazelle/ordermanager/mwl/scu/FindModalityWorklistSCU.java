package net.ihe.gazelle.ordermanager.mwl.scu;

import com.pixelmed.dicom.AttributeList;
import com.pixelmed.dicom.DicomException;
import com.pixelmed.network.DicomNetworkException;
import com.pixelmed.network.FindSOPClassSCU;
import net.ihe.gazelle.ordermanager.dicom.proxy.DicomEventListener;
import net.ihe.gazelle.ordermanager.model.Connection;
import net.ihe.gazelle.ordermanager.model.SCPConfiguration;
import net.ihe.gazelle.proxy.netty.ChannelType;
import net.ihe.gazelle.proxy.netty.ConnectionConfig;
import net.ihe.gazelle.proxy.netty.ConnectionConfigSimple;
import net.ihe.gazelle.proxy.netty.protocols.dicom.DicomProxy;
import net.ihe.gazelle.simulator.common.model.ApplicationConfiguration;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.BindException;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.util.NoSuchElementException;

public class FindModalityWorklistSCU {

    private static final String modalityWorklistFindSopClass = "1.2.840.10008.5.1.4.31";
    public static final String LOCAL_AE_TITLE = "GAZELLE_OM";

    private static Logger log = LoggerFactory.getLogger(FindModalityWorklistSCU.class);
	/**
	 * The minimum number of server port number.
	 */
	public static final int MIN_PORT_NUMBER = 1;
    private static final int DEFAULT_BACKLOG = 50;
    private static final String LOCALHOST = "127.0.0.1";

	/**
	 * The maximum number of server port number.
	 */
	public static final int MAX_PORT_NUMBER = 49151;

	private SCPConfiguration scpConfiguration;
	private AttributeList attributes;

	public FindModalityWorklistSCU(SCPConfiguration sut, AttributeList inAttributes) {
		this.scpConfiguration = sut;
		this.attributes = inAttributes;
	}

	/**
	 * Executes the FindModalityWorklist query through the Dicom proxy and sends back the id of the connection
	 * 
	 * @return the connection id (may be used to retrieve the list of Dicom messages exchanged)
	 * @throws IOException
	 * @throws DicomException
	 * @throws DicomNetworkException
	 */
	public Connection send() throws DicomNetworkException, DicomException, IOException, NoSuchElementException {

		String dataSetDirectory = ApplicationConfiguration.getValueOfVariable("dcm_data_set_basedir");
		String proxyPortLowLimit = ApplicationConfiguration.getValueOfVariable("proxy_port_low_limit");
		int proxyPort;
		if (proxyPortLowLimit != null) {
			int fromPort = Integer.parseInt(proxyPortLowLimit);
			proxyPort = getNextAvailablePort(fromPort);
		} else {
			return null;
		}
		final DicomEventListener dicomProxyEvent = new DicomEventListener(Actor.findActorWithKeyword("MOD"),
                Transaction.GetTransactionByKeyword("RAD-5"));
		ConnectionConfig connectionConfig = new ConnectionConfigSimple(proxyPort, scpConfiguration.getHost(),
				scpConfiguration.getPort(), ChannelType.DICOM);
		DicomProxy dicomProxy = new DicomProxy(dicomProxyEvent, connectionConfig, dataSetDirectory);
		dicomProxy.start();
		new FindSOPClassSCU(LOCALHOST, proxyPort, scpConfiguration.getAeTitle(), LOCAL_AE_TITLE,
				modalityWorklistFindSopClass, attributes, new MyIdentifierHandler(), 0);
		dicomProxy.stop();
		return dicomProxyEvent.getCurrentConnection();
	}

	/**
	 * Gets the next available port starting at a port.
	 * 
	 * @param fromPort
	 *            the port to scan for availability
	 * @throws NoSuchElementException
	 *             if there are no ports available
	 */
	public static int getNextAvailablePort(int fromPort) {
		if ((fromPort < MIN_PORT_NUMBER) || (fromPort > MAX_PORT_NUMBER)) {
			throw new IllegalArgumentException("Invalid start port: " + fromPort);
		}
		for (int i = fromPort; i <= MAX_PORT_NUMBER; i++) {
			if (available(i)) {
				return i;
			}
		}
		throw new NoSuchElementException("Could not find an available port " + "above " + fromPort);
	}

	/**
	 * Checks to see if a specific port is available.
	 * 
	 * @param port
	 *            the port to check for availability
	 */
	public static boolean available(int port) {
		if ((port < MIN_PORT_NUMBER) || (port > MAX_PORT_NUMBER)) {
			throw new IllegalArgumentException("Invalid start port: " + port);
		}

		ServerSocket ss = null;
		DatagramSocket ds = null;
		try {
			ss = new ServerSocket(port, DEFAULT_BACKLOG, InetAddress.getLocalHost());
			ss.setReuseAddress(true);
			ds = new DatagramSocket(port);
			ds.setReuseAddress(true);
			return true;
		} catch (BindException e){
            log.info(e.getMessage());
        }catch (IOException e) {
			log.info(e.getMessage());
		} finally {
			if (ds != null) {
				ds.close();
			}
			if (ss != null) {
				try {
					ss.close();
				} catch (IOException e) {
					log.error(e.getMessage(), e);
				}
			}
		}
		return false;
	}

}
