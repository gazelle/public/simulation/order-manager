package net.ihe.gazelle.ordermanager.model;

import net.ihe.gazelle.simulator.sut.model.SystemConfiguration;
import org.jboss.seam.annotations.Name;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.io.Serializable;

@Entity
@Name("scpConfiguration")
@DiscriminatorValue("dicom_scp")
public class SCPConfiguration extends SystemConfiguration implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4992121851067646778L;

    @Column(name = "ae_title")
	private String aeTitle;

	@Column(name = "host")
	private String host;

	@Column(name = "dicom_port")
	private Integer port;

	public SCPConfiguration() {
		super();
	}

	public SCPConfiguration(SCPConfiguration configurationToCopy) {
		super(configurationToCopy);
		this.aeTitle = configurationToCopy.getAeTitle();
		this.host = configurationToCopy.getHost();
		this.port = configurationToCopy.getPort();
	}


	public String getAeTitle() {
		return aeTitle;
	}

	public void setAeTitle(String aETitle) {
		aeTitle = aETitle;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public Integer getPort() {
		return port;
	}

	public void setPort(Integer port) {
		this.port = port;
	}

    @Override
    public boolean equals(Object o) {
        if (o == null) return false;
        if (this == o) return true;
        if (!super.equals(o)) return false;
        if (getClass() != o.getClass()) return false;
        SCPConfiguration that = (SCPConfiguration) o;

        if (aeTitle != null ? !aeTitle.equals(that.aeTitle) : that.aeTitle != null) return false;
        if (host != null ? !host.equals(that.host) : that.host != null) return false;
        return port != null ? port.equals(that.port) : that.port == null;

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (aeTitle != null ? aeTitle.hashCode() : 0);
        result = 31 * result + (host != null ? host.hashCode() : 0);
        result = 31 * result + (port != null ? port.hashCode() : 0);
        return result;
    }

    @Override
    public String getEndpoint(){
        return host + ':' + port;
    }
}
