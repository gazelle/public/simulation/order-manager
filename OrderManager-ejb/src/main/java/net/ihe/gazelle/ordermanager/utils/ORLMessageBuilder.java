package net.ihe.gazelle.ordermanager.utils;

import ca.uhn.hl7v2.AcknowledgmentCode;
import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.model.DataTypeException;
import ca.uhn.hl7v2.model.Group;
import ca.uhn.hl7v2.model.Message;
import ca.uhn.hl7v2.model.Segment;
import ca.uhn.hl7v2.model.v25.group.ORL_O22_ORDER;
import ca.uhn.hl7v2.model.v25.group.ORL_O34_ORDER;
import ca.uhn.hl7v2.model.v25.group.ORL_O34_SPECIMEN;
import ca.uhn.hl7v2.model.v25.group.ORL_O36_ORDER;
import ca.uhn.hl7v2.model.v25.message.OML_O21;
import ca.uhn.hl7v2.model.v25.message.OML_O33;
import ca.uhn.hl7v2.model.v25.message.OML_O35;
import ca.uhn.hl7v2.model.v25.message.ORL_O22;
import ca.uhn.hl7v2.model.v25.message.ORL_O34;
import ca.uhn.hl7v2.model.v25.message.ORL_O36;
import net.ihe.gazelle.HL7Common.messages.SegmentBuilder;
import net.ihe.gazelle.laboratory.law.hl7v2.model.v251.group.ORL_O42_ORDER;
import net.ihe.gazelle.laboratory.law.hl7v2.model.v251.group.ORL_O42_SPECIMEN;
import net.ihe.gazelle.laboratory.law.hl7v2.model.v251.message.ORL_O42;
import net.ihe.gazelle.laboratory.law.hl7v2.model.v251.segment.MSA;
import net.ihe.gazelle.laboratory.law.hl7v2.model.v251.segment.MSH;
import net.ihe.gazelle.oml.hl7v2.model.v25.group.OML_O21_ORDER;
import net.ihe.gazelle.oml.hl7v2.model.v25.group.OML_O33_ORDER;
import net.ihe.gazelle.oml.hl7v2.model.v25.group.OML_O33_SPECIMEN;
import net.ihe.gazelle.oml.hl7v2.model.v25.group.OML_O35_ORDER;
import net.ihe.gazelle.oml.hl7v2.model.v25.segment.SPM;
import net.ihe.gazelle.ordermanager.hl7.responders.Receiver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class ORLMessageBuilder extends SegmentBuilder {

    protected static final String ANALYZER = "ANALYZER";
    private static Logger log = LoggerFactory.getLogger(ORLMessageBuilder.class);

    private String sendingApplication;
    private String sendingFacility;
    private String receivingApplication;
    private String receivingFacility;
    private String receivedMessageControlId;
    private String hl7Charset;

    public ORLMessageBuilder(String sendingApplication, String sendingFacility, String receivingApplication,
            String receivingFacility, String messageControlId, String charset) {
        this.sendingApplication = sendingApplication;
        this.sendingFacility = sendingFacility;
        this.receivingFacility = receivingFacility;
        this.receivingApplication = receivingApplication;
        this.receivedMessageControlId = messageControlId;
        this.hl7Charset = charset;
    }

    /**
     * @param ackCode
     * @param pidSegment
     * @return the acknowledgement
     */
    public ORL_O22 createNewORL_O22(AcknowledgmentCode ackCode, Segment pidSegment) {
        ORL_O22 response = new ORL_O22();
        try {
            SegmentBuilder
                    .fillMSHSegment(response.getMSH(), receivingApplication, receivingFacility, sendingApplication,
                            sendingFacility, "ORL", "O22", "ORL_O22", "2.5", new SimpleDateFormat("yyyyMMddHHmmss"),
                            hl7Charset);
            SegmentBuilder.fillMSASegment(response.getMSA(), receivedMessageControlId, ackCode);
            if ((pidSegment != null) && !MessageDecoder.isEmpty(pidSegment)) {
                fillPIDSegment(response.getRESPONSE().getPATIENT().getPID(), pidSegment);
            }
        } catch (DataTypeException e1) {
            log.error(e1.getMessage(), e1);
        }
        return response;
    }

    /**
     * @param ackCode
     * @param pidSegment
     * @return the acknowledgement
     */
    public ORL_O34 createNewORL_O34(AcknowledgmentCode ackCode, Segment pidSegment) {
        ORL_O34 response = new ORL_O34();
        try {
            SegmentBuilder
                    .fillMSHSegment(response.getMSH(), receivingApplication, receivingFacility, sendingApplication,
                            sendingFacility, "ORL", "O34", "ORL_O34", "2.5", new SimpleDateFormat("yyyyMMddHHmmss"),
                            hl7Charset);
            response.getMSH().parse(response.getMSH().encode());
            SegmentBuilder.fillMSASegment(response.getMSA(), receivedMessageControlId, ackCode);
            if ((pidSegment != null) && !MessageDecoder.isEmpty(pidSegment)) {
                fillPIDSegment(response.getRESPONSE().getPATIENT().getPID(), pidSegment);
            }
        } catch (DataTypeException e1) {
            log.error(e1.getMessage());
        } catch (HL7Exception e) {
            log.error(e.getMessage());
        }
        return response;
    }

    public ORL_O42 createNewORL_O42(AcknowledgmentCode ackCode, Segment pidSegment) {
        ORL_O42 response = new ORL_O42();
        try {
            fillMSHSegmentForORLO42(response.getMSH(), receivingApplication, receivingFacility, sendingApplication,
                    sendingFacility, new SimpleDateFormat("yyyyMMddHHmmss"), hl7Charset);
            fillMSASegmentForORLO42(response.getMSA(), receivedMessageControlId, ackCode);
            if ((pidSegment != null) && !MessageDecoder.isEmpty(pidSegment)) {
                fillPIDSegment(response.getRESPONSE().getPATIENT().getPID(), pidSegment);
            }
        } catch (DataTypeException e1) {
            log.error(e1.getMessage());
        } catch (HL7Exception e) {
            log.error(e.getMessage());
        }
        return response;
    }

    private void fillMSASegmentForORLO42(MSA msaSegment,
                                         String receivedMessageControlId, AcknowledgmentCode ackCode) throws DataTypeException {
        try {
            msaSegment.getAcknowledgmentCode().setValue(ackCode.name());
            msaSegment.getMessageControlID().setValue(receivedMessageControlId);
        } catch (DataTypeException e) {
            throw new DataTypeException("An error occurred when filling MSA segment: " + e.getMessage(), e);
        }
    }

    private void fillMSHSegmentForORLO42(MSH mshSegment, String receivingApplication, String receivingFacility,
            String sendingApplication, String sendingFacility, SimpleDateFormat sdf, String hl7Charset)
            throws HL7Exception {
        try {
            mshSegment.getFieldSeparator().setValue("|");
            mshSegment.getEncodingCharacters().setValue("^~\\&");
            mshSegment.getSendingApplication().getNamespaceID().setValue(sendingApplication);
            mshSegment.getSendingFacility().getNamespaceID().setValue(sendingFacility);
            mshSegment.getReceivingApplication().getNamespaceID().setValue(receivingApplication);
            mshSegment.getReceivingFacility().getNamespaceID().setValue(receivingFacility);
            mshSegment.getDateTimeOfMessage().getTime().setValue(sdf.format(Calendar.getInstance().getTime()));
            mshSegment.getMessageType().getMessageCode().setValue("ORL");
            mshSegment.getMessageType().getTriggerEvent().setValue("O34");
            mshSegment.getMessageType().getMessageStructure().setValue("ORL_O42");
            mshSegment.getProcessingID().getProcessingID().setValue("P");
            mshSegment.getMessageControlID().setValue(sdf.format(Calendar.getInstance().getTime()));
            mshSegment.getVersionID().getVersionID().setValue("2.5.1");
            mshSegment.getMessageProfileIdentifier(0).parse("LAB-28^IHE");
            if (hl7Charset != null) {
                mshSegment.getCharacterSet().setValue(hl7Charset);
            }
        } catch (DataTypeException e) {
            throw new DataTypeException("An error occurred when filling MSH segment: " + e.getMessage(), e);
        }
    }

    /**
     * @param ackCode
     * @param pidSegment
     * @return the acknowledgement
     */
    public ORL_O36 createNewORL_O36(AcknowledgmentCode ackCode, Segment pidSegment) {
        ORL_O36 response = new ORL_O36();
        try {
            SegmentBuilder
                    .fillMSHSegment(response.getMSH(), receivingApplication, receivingFacility, sendingApplication,
                            sendingFacility, "ORL", "O36", "ORL_O36", "2.5", new SimpleDateFormat("yyyyMMddHHmmss"),
                            hl7Charset);
            SegmentBuilder.fillMSASegment(response.getMSA(), receivedMessageControlId, ackCode);
            if ((pidSegment != null) && !MessageDecoder.isEmpty(pidSegment)) {
                fillPIDSegment(response.getRESPONSE().getPATIENT().getPID(), pidSegment);
            }
        } catch (DataTypeException e1) {
            log.error(e1.getMessage(), e1);
        }
        return response;
    }

    /**
     * @param errSegment
     * @param hl7ErrorCode
     * @param severity
     * @param userMessage
     */
    public void fillERRSegment(Segment errSegment, String hl7ErrorCode, String severity, String userMessage) {
        try {
            errSegment.getField(3, 0).parse(hl7ErrorCode + "^^HL70357");
            errSegment.getField(4, 0).parse(severity);
            errSegment.getField(8, 0).parse(userMessage);
        } catch (HL7Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    /**
     * @param pidFromResponse
     * @param pidFromMessage
     */
    public void fillPIDSegment(Segment pidFromResponse, Segment pidFromMessage) {
        try {
            pidFromResponse.parse(pidFromMessage.encode());
        } catch (HL7Exception e) {
            log.error("Cannot insert PID segment", e);
        }
    }

    /**
     * If message structure is OML_*, creates the associates response If message structure is ORL*, clears the RESPONSE group of the message, adds a ERR segment and updates the acknoledgment code
     *
     * @param incomingMessage
     * @param ackCode
     * @param hl7ErrorCode
     * @param severity
     * @param userMessage
     * @param simulatedActorKeyword
     * @return the acknowledgement
     */
    public Message createNack(Message incomingMessage, AcknowledgmentCode ackCode, String hl7ErrorCode, String severity, String userMessage,
                              String simulatedActorKeyword) {
        Message response = null;
        // if the message is of structure OML_O*, creates the relative response
        if (incomingMessage instanceof OML_O21) {
            response = createNewORL_O22(ackCode, null);
        } else if (incomingMessage instanceof OML_O33 && !simulatedActorKeyword.equals(ANALYZER)) {
            response = createNewORL_O34(ackCode, null);
        } else if (incomingMessage instanceof OML_O33 && simulatedActorKeyword.equals(ANALYZER)){
            response = createNewORL_O42(ackCode, null);
        } else if (incomingMessage instanceof OML_O35) {
            response = createNewORL_O36(ackCode, null);
        }
        // builds the response
        if (response instanceof ORL_O22) {
            ORL_O22 o22Response = (ORL_O22) response;
            o22Response.getRESPONSE().clear();
            updateMSASegment(o22Response.getMSA(), ackCode);
            fillERRSegment(o22Response.getERR(), hl7ErrorCode, severity, userMessage);
            return o22Response;
        } else if (response instanceof ORL_O34) {
            ORL_O34 o34Response = (ORL_O34) response;
            o34Response.getRESPONSE().getPATIENT().clear();
            updateMSASegment(o34Response.getMSA(), ackCode);
            fillERRSegment(o34Response.getERR(), hl7ErrorCode, severity, userMessage);
            return o34Response;
        } else if (response instanceof ORL_O36) {
            ORL_O36 o36Response = (ORL_O36) response;
            o36Response.getRESPONSE().clear();
            updateMSASegment(o36Response.getMSA(), ackCode);
            fillERRSegment(o36Response.getERR(), hl7ErrorCode, severity, userMessage);
            return o36Response;
        } else if (response instanceof ORL_O42) {
            ORL_O42 o42Response = (ORL_O42) response;
            o42Response.getRESPONSE().clear();
            updateMSASegment(o42Response.getMSA(), ackCode);
            if (hl7ErrorCode.equals(Receiver.DUPLICATE_KEY_IDENTIFIER)){
                hl7ErrorCode = Receiver.INTERNAL_ERROR;
            }
            fillERRSegment(o42Response.getERR(), hl7ErrorCode, severity, userMessage);
            return o42Response;
        } else {
            return response;
        }
    }

    /**
     * @param message
     * @param simulatedActorKeyword
     * @return the acknowledgement
     * @throws DataTypeException
     */
    public Message createAck(Message message, String simulatedActorKeyword) {
        try {
            // if the message is of structure OML_O*, creates the relative response
            if (message instanceof OML_O21) {
                ORL_O22 ack = new ORL_O22();
                fillMSHSegment(ack.getMSH(), receivingApplication, receivingFacility, sendingApplication,
                        sendingFacility, "ORL", "O22", "ORL_O22", "2.5", new SimpleDateFormat("yyyyMMddHHmmss"),
                        hl7Charset);
                fillMSASegment(ack.getMSA(), receivedMessageControlId, AcknowledgmentCode.AA);
                return ack;
            } else if (message instanceof OML_O33 && !simulatedActorKeyword.equals(ANALYZER)) {
                ORL_O34 ack = new ORL_O34();
                fillMSHSegment(ack.getMSH(), receivingApplication, receivingFacility, sendingApplication,
                        sendingFacility, "ORL", "O34", "ORL_O34", "2.5", new SimpleDateFormat("yyyyMMddHHmmss"),
                        hl7Charset);
                fillMSASegment(ack.getMSA(), receivedMessageControlId, AcknowledgmentCode.AA);
                return ack;
            } else if (message instanceof OML_O33 && simulatedActorKeyword.equals(ANALYZER)) {
                ORL_O42 ack = new ORL_O42();
                fillMSASegmentForORLO42(ack.getMSA(), receivedMessageControlId, AcknowledgmentCode.AA);
                fillMSHSegmentForORLO42(ack.getMSH(), receivingApplication, receivingFacility, sendingApplication,
                        sendingFacility, new SimpleDateFormat("yyyyMMddHHmmss"), hl7Charset);
                return ack;
            } else if (message instanceof OML_O35) {
                ORL_O36 ack = new ORL_O36();
                fillMSHSegment(ack.getMSH(), receivingApplication, receivingFacility, sendingApplication,
                        sendingFacility, "ORL", "O36", "ORL_O36", "2.5", new SimpleDateFormat("yyyyMMddHHmmss"),
                        hl7Charset);
                fillMSASegment(ack.getMSA(), receivedMessageControlId, AcknowledgmentCode.AA);
                return ack;
            } else {
                return message.generateACK(AcknowledgmentCode.AE, new HL7Exception("Unsupported message"));
            }
        } catch (HL7Exception e) {
            try {
                return message.generateACK(AcknowledgmentCode.AE, e);
            } catch (Exception e1) {
                log.error("Cannot build NACK: " + e.getMessage());
                return null;
            }
        } catch (IOException e){
            return null;
        }
    }

    /**
     * @param msaSegment
     * @param newAckCode
     */
    public void updateMSASegment(Segment msaSegment, AcknowledgmentCode newAckCode) {
        try {
            msaSegment.getField(1, 0).parse(newAckCode.name());
        } catch (HL7Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    public String getSendingApplication() {
        return sendingApplication;
    }

    public void setSendingApplication(String sendingApplication) {
        this.sendingApplication = sendingApplication;
    }

    public String getSendingFacility() {
        return sendingFacility;
    }

    public void setSendingFacility(String sendingFacility) {
        this.sendingFacility = sendingFacility;
    }

    public String getReceivingApplication() {
        return receivingApplication;
    }

    public void setReceivingApplication(String receivingApplication) {
        this.receivingApplication = receivingApplication;
    }

    public String getReceivingFacility() {
        return receivingFacility;
    }

    public void setReceivingFacility(String receivingFacility) {
        this.receivingFacility = receivingFacility;
    }

    public String getReceivedMessageControlId() {
        return receivedMessageControlId;
    }

    public void setReceivedMessageControlId(String receivedMessageControlId) {
        this.receivedMessageControlId = receivedMessageControlId;
    }

    /**
     * populates ORDER group of the ORL message
     *
     * @param responseGroup
     * @param orderGroup
     * @param newOrderControlCode
     * @param placerNumber
     * @param fillerNumber
     * @param groupNumber         TODO
     * @param updateOrderStatus   TODO
     */
    public void addOrderGroupAndUpdateControlCode(Group responseGroup, Group orderGroup, String newOrderControlCode,
            String placerNumber, String fillerNumber, String groupNumber, boolean updateOrderStatus) {
        if (responseGroup instanceof ORL_O22_ORDER) {
            OML_O21_ORDER omlGroup = (OML_O21_ORDER) orderGroup;
            try {
                ((ORL_O22_ORDER) responseGroup).getORC().parse(omlGroup.getORC().encode());
                ((ORL_O22_ORDER) responseGroup).getORC().getOrderControl().setValue(newOrderControlCode);
                ((ORL_O22_ORDER) responseGroup).getOBSERVATION_REQUEST().getOBR()
                        .parse(omlGroup.getOBSERVATION_REQUEST().getOBR().encode());
                if (placerNumber != null) {
                    ((ORL_O22_ORDER) responseGroup).getORC().getPlacerOrderNumber().parse(placerNumber);
                    ((ORL_O22_ORDER) responseGroup).getOBSERVATION_REQUEST().getOBR().getPlacerOrderNumber()
                            .parse(placerNumber);
                }
                if (fillerNumber != null) {
                    ((ORL_O22_ORDER) responseGroup).getORC().getFillerOrderNumber().parse(fillerNumber);
                    ((ORL_O22_ORDER) responseGroup).getOBSERVATION_REQUEST().getOBR().getFillerOrderNumber()
                            .parse(fillerNumber);
                }
                if (groupNumber != null) {
                    ((ORL_O22_ORDER) responseGroup).getORC().getPlacerGroupNumber().parse(groupNumber);
                }
                if (updateOrderStatus) {
                    ((ORL_O22_ORDER) responseGroup).getORC().getOrderStatus().setValue("SC");
                }
            } catch (HL7Exception e) {
                log.error("Unable to fill ORDER group", e);
            }
        } else if (responseGroup instanceof ORL_O34_ORDER) {
            OML_O33_ORDER omlGroup = (OML_O33_ORDER) orderGroup;
            try {
                ((ORL_O34_ORDER) responseGroup).getORC().parse(omlGroup.getORC().encode());
                ((ORL_O34_ORDER) responseGroup).getORC().getOrderControl().setValue(newOrderControlCode);
                ((ORL_O34_ORDER) responseGroup).getOBSERVATION_REQUEST().getOBR()
                        .parse(omlGroup.getOBSERVATION_REQUEST().getOBR().encode());
                if (placerNumber != null) {
                    ((ORL_O34_ORDER) responseGroup).getORC().getPlacerOrderNumber().parse(placerNumber);
                    ((ORL_O34_ORDER) responseGroup).getOBSERVATION_REQUEST().getOBR().getPlacerOrderNumber()
                            .parse(placerNumber);
                }
                if (fillerNumber != null) {
                    ((ORL_O34_ORDER) responseGroup).getORC().getFillerOrderNumber().parse(fillerNumber);
                    ((ORL_O34_ORDER) responseGroup).getOBSERVATION_REQUEST().getOBR().getFillerOrderNumber()
                            .parse(fillerNumber);
                }
                if (groupNumber != null) {
                    ((ORL_O34_ORDER) responseGroup).getORC().getPlacerGroupNumber().parse(groupNumber);
                }
                if (updateOrderStatus) {
                    ((ORL_O34_ORDER) responseGroup).getORC().getOrderStatus().setValue("SC");
                }
            } catch (HL7Exception e) {
                log.error("Unable to fill ORDER group", e);
            }
        } else if (responseGroup instanceof ORL_O36_ORDER) {
            OML_O35_ORDER omlGroup = (OML_O35_ORDER) orderGroup;
            try {
                ((ORL_O36_ORDER) responseGroup).getORC().parse(omlGroup.getORC().encode());
                ((ORL_O36_ORDER) responseGroup).getORC().getOrderControl().setValue(newOrderControlCode);
                ((ORL_O36_ORDER) responseGroup).getOBSERVATION_REQUEST().getOBR()
                        .parse(omlGroup.getOBSERVATION_REQUEST().getOBR().encode());
                if (placerNumber != null) {
                    ((ORL_O36_ORDER) responseGroup).getORC().getPlacerOrderNumber().parse(placerNumber);
                    ((ORL_O36_ORDER) responseGroup).getOBSERVATION_REQUEST().getOBR().getPlacerOrderNumber()
                            .parse(placerNumber);
                }
                if (fillerNumber != null) {
                    ((ORL_O36_ORDER) responseGroup).getORC().getFillerOrderNumber().parse(fillerNumber);
                    ((ORL_O36_ORDER) responseGroup).getOBSERVATION_REQUEST().getOBR().getFillerOrderNumber()
                            .parse(fillerNumber);
                }
                if (groupNumber != null) {
                    ((ORL_O36_ORDER) responseGroup).getORC().getPlacerGroupNumber().parse(groupNumber);
                }
                if (updateOrderStatus) {
                    ((ORL_O36_ORDER) responseGroup).getORC().getOrderStatus().setValue("SC");
                }
            } catch (HL7Exception e) {
                log.error("Unable to fill ORDER group", e);
            }
        } else if (responseGroup instanceof ORL_O42_ORDER) {
            OML_O33_ORDER omlGroup = (OML_O33_ORDER) orderGroup;
            try {
                ((ORL_O42_ORDER) responseGroup).getORC().parse(omlGroup.getORC().encode());
                ((ORL_O42_ORDER) responseGroup).getORC().getOrderControl().setValue(newOrderControlCode);
                ((ORL_O42_ORDER) responseGroup).getOBSERVATION_REQUEST().getOBR()
                        .parse(omlGroup.getOBSERVATION_REQUEST().getOBR().encode());
                if (placerNumber != null) {
                    ((ORL_O42_ORDER) responseGroup).getORC().getPlacerOrderNumber(0).parse(placerNumber);
                    ((ORL_O42_ORDER) responseGroup).getOBSERVATION_REQUEST().getOBR().getPlacerOrderNumber()
                            .parse(placerNumber);
                }
                if (fillerNumber != null) {
                    ((ORL_O42_ORDER) responseGroup).getORC().getFillerOrderNumber().parse(fillerNumber);
                    ((ORL_O42_ORDER) responseGroup).getOBSERVATION_REQUEST().getOBR().getFillerOrderNumber()
                            .parse(fillerNumber);
                }
                if (groupNumber != null) {
                    ((ORL_O42_ORDER) responseGroup).getORC().getPlacerGroupNumber().parse(groupNumber);
                }
                if (updateOrderStatus) {
                    if (newOrderControlCode.equals("CR")){
                        ((ORL_O42_ORDER) responseGroup).getORC().getOrderStatus().setValue("CA");
                    } else {
                        ((ORL_O42_ORDER) responseGroup).getORC().getOrderStatus().setValue("SC");
                    }
                }
                // empty fields which shall not be sent by the analyzer
                ((ORL_O42_ORDER) responseGroup).getORC().getOrc9_DateTimeOfTransaction(0).clear();
                ((ORL_O42_ORDER) responseGroup).getOBSERVATION_REQUEST().getOBR().getObr16_OrderingProvider(0).clear();
            } catch (HL7Exception e) {
                log.error("Unable to fill ORDER group", e);
            }
        }
    }

    public void addSpecimenAndContainers(Group responseGroup, Group specimenGroup, String placerNumber,
            String fillerNumber) {
        if (responseGroup instanceof ORL_O34_SPECIMEN) {
            OML_O33_SPECIMEN omlGroup = (OML_O33_SPECIMEN) specimenGroup;
            try {
                // if & is not replace by ^, only the first sub component is copied into the message
                if (placerNumber != null) {
                    omlGroup.getSPM().getSpecimenID().getEip1_PlacerAssignedIdentifier()
                            .parse(placerNumber.replace("&", "^"));
                }
                if (fillerNumber != null) {
                    omlGroup.getSPM().getSpecimenID().getEip2_FillerAssignedIdentifier()
                            .parse(fillerNumber.replace("&", "^"));
                }
                ((ORL_O34_SPECIMEN) responseGroup).getSPM().parse(omlGroup.getSPM().encode());
                for (int index = 0; index < omlGroup.getSACReps(); index++) {
                    ((ORL_O34_SPECIMEN) responseGroup).getSAC(index).parse(omlGroup.getSAC(index).encode());
                }
            } catch (HL7Exception e) {
                log.error("Unable to fill SPECIMEN group", e);
            }
        } else if (responseGroup instanceof ORL_O42_SPECIMEN) {
            OML_O33_SPECIMEN omlGroup = (OML_O33_SPECIMEN) specimenGroup;
            try {
                ((ORL_O42_SPECIMEN) responseGroup).getSPM().parse(omlGroup.getSPM().encode());
                for (int index = 0; index < omlGroup.getSACReps(); index++) {
                    ((ORL_O42_SPECIMEN) responseGroup).getSAC().parse(omlGroup.getSAC(index).encode());
                }
                // empty fields which shall not be sent by the analyzer
                ((ORL_O42_SPECIMEN) responseGroup).getSPM().getSpm7_SpecimenCollectionMethod(0).clear();
                ((ORL_O42_SPECIMEN) responseGroup).getSPM().getSpm8_SpecimenSourceSite(0).clear();
                ((ORL_O42_SPECIMEN) responseGroup).getSPM().getSpm9_SpecimenSourceSiteModifier(0).clear();
                ((ORL_O42_SPECIMEN) responseGroup).getSPM().getSpm16_SpecimenRiskCode(0).clear();
                ((ORL_O42_SPECIMEN) responseGroup).getSPM().getSpm18_SpecimenReceivedDateTime(0).clear();
                ((ORL_O42_SPECIMEN) responseGroup).getSPM().getSpm17_SpecimenCollectionDateTime(0).clear();
                ((ORL_O42_SPECIMEN) responseGroup).getSPM().getSpm27_ContainerType(0).clear();
                ((ORL_O42_SPECIMEN) responseGroup).getSAC().getSac9_CarrierType(0).clear();
                ((ORL_O42_SPECIMEN) responseGroup).getSAC().getSac21_ContainerVolume(0).clear();
                ((ORL_O42_SPECIMEN) responseGroup).getSAC().getSac22_AvailableSpecimenVolume(0).clear();
                ((ORL_O42_SPECIMEN) responseGroup).getSAC().getSac24_VolumeUnits(0).clear();
                ((ORL_O42_SPECIMEN) responseGroup).getSAC().getSac29_DilutionFactor(0).clear();
            } catch (HL7Exception e) {
                log.error("Unable to fill SPECIMEN group", e);
            }
        }
    }

    public void addSpecimen(Segment responseSPM, SPM requestSPM, String placerNumber, String fillerNumber) {
        try {
            if (placerNumber != null) {
                requestSPM.getSpecimenID().getEip1_PlacerAssignedIdentifier().parse(placerNumber.replace("&", "^"));
            }
            if (fillerNumber != null) {
                requestSPM.getSpecimenID().getEip2_FillerAssignedIdentifier().parse(fillerNumber.replace("&", "^"));
            }
            responseSPM.parse(requestSPM.encode());
        } catch (HL7Exception e) {
            log.error("Unable to fill SPM segment", e);
        }
    }

    public void addContainer(Segment responseSAC, net.ihe.gazelle.oml.hl7v2.model.v25.segment.SAC requestSAC) {
        try {
            responseSAC.parse(requestSAC.encode());
        } catch (HL7Exception e) {
            log.error("Unable to fill SAC segment", e);
        }
    }

}
