package net.ihe.gazelle.ordermanager.hl7.responders;

import ca.uhn.hl7v2.AcknowledgmentCode;
import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.model.Message;
import ca.uhn.hl7v2.parser.PipeParser;
import ca.uhn.hl7v2.protocol.ReceivingApplicationException;
import net.ihe.gazelle.HL7Common.messages.HL7MessageDecoder;
import net.ihe.gazelle.HL7Common.responder.IHEDefaultHandler;
import net.ihe.gazelle.HL7Common.utils.TransactionInstanceUtil;
import net.ihe.gazelle.laboratory.lbl.hl7v2.model.v25.message.QBP_Q11;
import net.ihe.gazelle.ordermanager.model.Encounter;
import net.ihe.gazelle.ordermanager.model.LabOrder;
import net.ihe.gazelle.ordermanager.model.Specimen;
import net.ihe.gazelle.ordermanager.utils.LabMessageBuilder;
import net.ihe.gazelle.ordermanager.utils.QBPSLIHandler;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.common.tf.model.Domain;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;
import net.ihe.gazelle.simulator.message.model.TransactionInstance;
import org.jboss.seam.log.Log;
import org.jboss.seam.log.Logging;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * Class description : <b>LabelInformationProvider</b>
 * <p/>
 * This class is the handler for messages received by the Label Information Provider in the context of the LAB-62 and LAB-63 transactions
 *
 * @author Anne-Gaëlle Bergé - KEREVAL
 * @version 1.0 - 2016, August 18th
 */
public class LabelInformationProvider extends OrderReceiver {

    private static Log log = Logging.getLog(LabelInformationProvider.class);


    /** {@inheritDoc} */
    @Override
    public IHEDefaultHandler newInstance() {
        return new LabelInformationProvider();
    }

    /** {@inheritDoc} */
    @Override
    public Message processMessage(Message incomingMessage) throws ReceivingApplicationException, HL7Exception {
        domain = Domain.getDomainByKeyword("LAB", entityManager);
        Message response;
        try {
            // check if we must accept the message
            response = Receiver.generateNack(incomingMessage, serverApplication, serverFacility, Arrays.asList("OML", "QBP"),
                    Arrays.asList("SLI", "O33"), hl7Charset);

            // if no NACK is generated, process the message
            if (response == null) {
                if (messageType == null) {
                    log.error("Message type is empty");
                } else if (messageType.contains("OML")) {
                    simulatedTransaction = Transaction.GetTransactionByKeyword("LAB-63");
                    response = processLAB63Transaction(incomingMessage);
                } else if (messageType.contains("QBP")) {
                    simulatedTransaction = Transaction.GetTransactionByKeyword("LAB-62");
                    response = processLAB62Transaction(incomingMessage);
                } else {
                    log.error("This kind of message is not supported, we should not reach this line !");
                }
            }
            setSutActor(Actor.findActorWithKeyword("LB", entityManager));
            setResponseMessageType(HL7MessageDecoder.getMessageType(response));
            return response;
        } catch (HL7Exception e) {
            throw new HL7Exception(e);
        } catch (Exception t) {
            throw new HL7Exception(t.getMessage(), t);
        }
    }

    private Message processLAB62Transaction(Message incomingMessage) {
        Message response = null;
        if (incomingMessage instanceof ca.uhn.hl7v2.model.v25.message.QBP_Q11) {
            QBP_Q11 queryMsg;
            try {
                String queryString = incomingMessage.encode();
                PipeParser parser = PipeParser.getInstanceWithNoValidation();
                queryMsg = (QBP_Q11) parser.parseForSpecificPackage(queryString, "net.ihe.gazelle.laboratory.lbl.hl7v2.model.v25.message");
            } catch (HL7Exception e) {
                log.error("Cannot parse received query");
                return null;
            }
            LabMessageBuilder builder;
            try {
                List<Specimen> specimens = QBPSLIHandler.findSpecimensForParameters(queryMsg.getQPD());
                // check encounter is the same through all specimens
                if (specimens != null && !specimens.isEmpty()) {
                    Encounter encounter = specimens.get(0).getOrders().get(0).getEncounter();
                    for (Specimen specimen : specimens) {
                        if (specimen.getOrders() != null) {
                            LabOrder order = specimen.getOrders().get(0);
                            if (!order.getEncounter().getId().equals(encounter.getId())) {
                                response = Receiver.buildNack(serverApplication, serverFacility, sendingApplication, sendingFacility,
                                        "SLI", AcknowledgmentCode.AE, "Cannot find consistent data", null, Receiver.INTERNAL_ERROR,
                                        null, messageControlId, hl7Charset, hl7Version);
                                break;
                            } else {
                                continue;
                            }
                        }
                    }
                    if (response == null) {
                        builder = new LabMessageBuilder("LAB-62", "LIP", encounter, receivingApplication, receivingFacility,
                                "RSP^SLI^RSP_K11", entityManager);
                        response = builder.buildLAB62Response(specimens, queryMsg.getQPD(), queryMsg.getMSH());
                    }
                } else {
                    builder = new LabMessageBuilder("LAB-62", "LIP", null, receivingApplication, receivingFacility,
                            "RSP^SLI^RSP_K11", entityManager);
                    response = builder.buildLAB62Response(null, queryMsg.getQPD(), queryMsg.getMSH());
                }
            } catch (HL7Exception e) {
                log.error(e.getMessage());
                builder = new LabMessageBuilder("LAB-62", "LIP", null, receivingApplication, receivingFacility,
                        "RSP^SLI^RSP_K11", entityManager);
                try {
                    response = builder.buildLAB62Response(null, queryMsg.getQPD(), queryMsg.getMSH());
                } catch (Exception e2) {
                    log.error(e2.getMessage());
                }
            } catch (Exception e) {
                log.error(e.getMessage());
            }
        }
        if (response == null) {
            response = Receiver.buildNack(serverApplication, serverFacility, sendingApplication, sendingFacility,
                    "SLI", AcknowledgmentCode.AE, "An error occurred when building response", null,
                    Receiver.INTERNAL_ERROR, null, messageControlId, hl7Charset, hl7Version);
        }
        return response;
    }

    private Message processLAB63Transaction(Message incomingMessage) {
        return processOMLMessage(incomingMessage, null, "SC", null, null);
    }
}
