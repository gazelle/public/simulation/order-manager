package net.ihe.gazelle.ordermanager.dico;

/**
 * <b>Class Description : </b>HL7Constant<br>
 * <br>
 *
 * @author Anne-Gaelle Berge / IHE-Europe development Project
 * @version 1.0 - 12/10/16
 *
 *
 *
 */
public final class HL7Constant {

    public static final String IP = "IP";
    public static final String CM = "CM";
    public static final String AE = "AE";
    public static final String NW = "NW";
    public static final String CA = "CA";
    public static final String DC = "DC";
    public static final String OK = "OK";
    public static final String SC = "SC";
    public static final String UA = "UA";
    public static final String CR = "CR";
    public static final String UC = "UC";
    public static final String AA = "AA";

    private HL7Constant(){

    }


}
