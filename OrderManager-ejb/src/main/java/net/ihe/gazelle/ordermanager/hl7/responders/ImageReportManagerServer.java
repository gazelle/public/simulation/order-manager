package net.ihe.gazelle.ordermanager.hl7.responders;

import net.ihe.gazelle.HL7Common.responder.AbstractServer;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.Startup;
import org.jboss.seam.annotations.Transactional;

import javax.ejb.DependsOn;

@Startup(depends = {"entityManager"})
@DependsOn({"entityManager"})
@Name("imageReportManagerServer")
@Scope(ScopeType.APPLICATION)
public class ImageReportManagerServer extends AbstractServer<ImageReportManager> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4490092412449676110L;

    @Create
    @Override
    @Transactional
	public void startServers() {
		Actor simulatedActor = Actor.findActorWithKeyword("IM", entityManager);
		handler = new ImageReportManager();
		startServers(simulatedActor, null, null);
	}

}
