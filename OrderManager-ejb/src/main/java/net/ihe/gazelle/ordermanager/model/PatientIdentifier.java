package net.ihe.gazelle.ordermanager.model;

import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import org.jboss.seam.annotations.Name;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

/**
 * <b>Class Description : </b>PatientIdentifier<br>
 * <br>
 * 
 * This class represents the identifier of a patient
 * 
 * PatientIdentifier possesses the following attributes :
 * <ul>
 * <li><b>id</b> : id in the database</li>
 * <li><b>identifier</b> : full identifier (CX-1 to CX-4)</li>
 * <li><b>identifierTypeCode</b>: CX-5</li>
 * <li><b>patient</b> patient which owns this identifier</li>
 * </ul>
 * 
 *
 *
 * @author Anne-Gaelle Berge / INRIA Rennes IHE development Project
 *
 * @version 1.0 - 2011, March 21st
 */
@Entity
@Name("patientIdentifier")
@Table(name = "om_patient_identifier", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = "id"))
@SequenceGenerator(name = "om_patient_identifier_sequence", sequenceName = "om_patient_identifier_id_seq", allocationSize = 1)
public class PatientIdentifier implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8137024430555256281L;
	public static final String DEFAULT_IDENTIFIER_TYPE_CODE = "PI";
	public static final String NATIONAL_IDENTIFIER_TYPE_CODE = "NI";

	@Id
	@NotNull
	@GeneratedValue(generator = "om_patient_identifier_sequence", strategy = GenerationType.SEQUENCE)
	@Column(name = "id", nullable = false, unique = true)
	private Integer id;

	@Column(name = "identifier")
	private String identifier;

	@Column(name = "identifier_type_code")
	private String identifierTypeCode;

	@ManyToOne
	@JoinColumn(name = "patient_id")
	private Patient patient;

	public PatientIdentifier() {

	}

	public PatientIdentifier(String inIdentifier, Patient inPatient, String inTypeCode) {
		this.identifier = inIdentifier;
		this.patient = inPatient;
		this.identifierTypeCode = inTypeCode;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getIdentifier() {
		return identifier;
	}

	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}

	public String getIdentifierTypeCode() {
		return identifierTypeCode;
	}

	public void setIdentifierTypeCode(String identifierTypeCode) {
		this.identifierTypeCode = identifierTypeCode;
	}

	/**
	 * @param patient
	 *            the patient to set
	 */
	public void setPatient(Patient patient) {
		this.patient = patient;
	}

	/**
	 * @return the patient
	 */
	public Patient getPatient() {
		return patient;
	}

	/**
	 * if the patient identifier exists in database, returns the retrieved object, otherwise merges the new identifier and returns the merged object
	 * 
	 * @param entityManager
	 * @return patient identifier
	 */
	public PatientIdentifier save(EntityManager entityManager) {
		HQLQueryBuilder<PatientIdentifier> queryBuilder = new HQLQueryBuilder<PatientIdentifier>(entityManager,
				PatientIdentifier.class);
		queryBuilder.addEq("identifier", this.identifier);
		if (this.identifierTypeCode != null) {
			queryBuilder.addEq("identifierTypeCode", this.identifierTypeCode);
		}
		List<PatientIdentifier> identifiers = queryBuilder.getList();
		PatientIdentifier returnedIdentifier;
		if ((identifiers != null) && !identifiers.isEmpty()) {
			returnedIdentifier = identifiers.get(0);
		} else {
			returnedIdentifier = entityManager.merge(this);
			entityManager.flush();
		}
		return returnedIdentifier;
	}

	public PatientIdentifier saveLinkWithPatient(EntityManager entityManager) {
		PatientIdentifier pid = entityManager.merge(this);
		entityManager.flush();
		return pid;
	}

	public PatientIdentifier getStoredPatientIdentifier(EntityManager entityManager) {
		PatientIdentifierQuery query = new PatientIdentifierQuery(new HQLQueryBuilder<PatientIdentifier>(entityManager,
				PatientIdentifier.class));
		query.identifier().eq(this.identifier);
		if (this.identifierTypeCode != null) {
			query.identifierTypeCode().eq(this.identifierTypeCode);
		}
		return query.getUniqueResult();
	}

	/**
	 * 
	 * @param inPatient
	 * @return patient identifiers
	 */
	public static List<PatientIdentifier> getIdentifiersForPatient(Patient inPatient) {
		EntityManager entityManager = EntityManagerService.provideEntityManager();
		HQLQueryBuilder<PatientIdentifier> builder = new HQLQueryBuilder<PatientIdentifier>(entityManager,
				PatientIdentifier.class);
		builder.addEq("patient", inPatient);
		List<PatientIdentifier> identifiers = builder.getList();
		if ((identifiers != null) && !identifiers.isEmpty()) {
			return identifiers;
		} else {
			return null;
		}
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = (prime * result) + ((identifier == null) ? 0 : identifier.hashCode());
		result = (prime * result) + ((identifierTypeCode == null) ? 0 : identifierTypeCode.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		PatientIdentifier other = (PatientIdentifier) obj;
		if (identifier == null) {
			if (other.identifier != null) {
				return false;
			}
		} else if (!identifier.equals(other.identifier)) {
			return false;
		}
		if (identifierTypeCode == null) {
			if (other.identifierTypeCode != null) {
				return false;
			}
		} else if (!identifierTypeCode.equals(other.identifierTypeCode)) {
			return false;
		}
		return true;
	}

}
