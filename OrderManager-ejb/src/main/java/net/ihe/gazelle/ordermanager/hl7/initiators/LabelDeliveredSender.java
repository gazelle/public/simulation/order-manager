package net.ihe.gazelle.ordermanager.hl7.initiators;

import ca.uhn.hl7v2.HL7Exception;
import net.ihe.gazelle.HL7Common.initiator.Initiator;
import net.ihe.gazelle.HL7Common.model.HL7V2ResponderSUTConfiguration;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.ordermanager.admin.UserPreferencesManager;
import net.ihe.gazelle.ordermanager.filter.SpecimenFilter;
import net.ihe.gazelle.ordermanager.model.Encounter;
import net.ihe.gazelle.ordermanager.model.LabOrder;
import net.ihe.gazelle.ordermanager.model.Specimen;
import net.ihe.gazelle.ordermanager.model.SpecimenQuery;
import net.ihe.gazelle.ordermanager.utils.LabMessageBuilder;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.common.tf.model.Domain;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;
import net.ihe.gazelle.simulator.message.model.TransactionInstance;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * <b>Class Description : </b>LabelDeliveredSender<br>
 * <br>
 *
 * @author Anne-Gaelle Berge / IHE-Europe development Project
 * @version 1.0 - 18/08/16
 *
 *
 *
 */
@Name("labelDeliveredSender")
@Scope(ScopeType.PAGE)
public class LabelDeliveredSender implements Serializable {


    private static Logger log = LoggerFactory.getLogger(LabelDeliveredSender.class);
    private SpecimenFilter specimenFilter;
    private static Transaction currentTransaction = null;
    private static Actor simulatedActor = null;
    private static Domain selectedDomain = null;
    private static Actor sutActor = null;
    private List<TransactionInstance> hl7Messages = null;
    private List<Integer> specimensToSend;

    static {
        try {
            currentTransaction = Transaction.GetTransactionByKeyword("LAB-63");
            simulatedActor = Actor.findActorWithKeyword("LB");
            selectedDomain = Domain.getDomainByKeyword("LAB");
            sutActor = Actor.findActorWithKeyword("LIP");
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    @Create
    public void init(){
        specimensToSend = new ArrayList<Integer>();
        hl7Messages = null;
    }

    public void addSpecimenToList(Specimen specimen){
        specimensToSend.add(specimen.getId());
    }

    public void removeSpecimenFromList(Specimen specimen){
        specimensToSend.remove(specimen.getId());
    }

    public boolean isSpecimenSelected(Specimen specimen){
        return specimensToSend.contains(specimen.getId());
    }

    public Integer getLabelCount(){
        return specimensToSend.size();
    }

    public void sendMessageForList(){
        // reset the list of messages
        SpecimenQuery query = new SpecimenQuery();
        query.id().in(specimensToSend);
        List<Specimen> specimens = query.getList();
        if (specimens != null){
            for (Specimen specimen: specimens){
                sendMessage(specimen);
            }
            if (hl7Messages != null && !hl7Messages.isEmpty()) {
                FacesMessages.instance().add(StatusMessage.Severity.INFO, hl7Messages.size() + " messages have been sent");
            } else {
                FacesMessages.instance().add(StatusMessage.Severity.WARN, "No message has been sent");
            }
        } else {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "No specimen to send !");
        }
    }

    public void sendMessage(Specimen selectedSpecimen) {
        HL7V2ResponderSUTConfiguration sut = UserPreferencesManager.instance().getSelectedSUT();
        if (sut == null) {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Please first select a SUT and retry");
        } else {
            EntityManager entityManager = EntityManagerService.provideEntityManager();
            Encounter selectedEncounter = null;
            for (LabOrder order : selectedSpecimen.getOrders()) {
                order.setOrderControlCode("SC");
                order.setOrderResultStatus("S");
                order.save(entityManager);
                if (selectedEncounter == null){
                    selectedEncounter = order.getEncounter();
                }
            }
            LabMessageBuilder builder = new LabMessageBuilder(currentTransaction.getKeyword(), simulatedActor.getKeyword(),
                    selectedEncounter, null, selectedSpecimen, sut, "OM_LAB_LB", "IHE", "OML^O33^OML_O33", null);
            String messageToSend = null;
            try{
                messageToSend = builder.generateMessage();
            }catch (Exception e){
                FacesMessages.instance().add(StatusMessage.Severity.ERROR, "The simulator was not able to build the message");
            }
            if (messageToSend != null){
                Initiator initiator = new Initiator(sut, "IHE", "OM_LAB_LB", simulatedActor, currentTransaction,
                        messageToSend, "OML^O33^OML_O33", selectedDomain.getKeyword(), sutActor);
                try{
                    if (hl7Messages == null){
                        hl7Messages = new ArrayList<TransactionInstance>();
                    }
                    TransactionInstance instance = initiator.sendMessageAndGetTheHL7Message();
                    hl7Messages.add(instance);
                }catch (HL7Exception e){
                    FacesMessages.instance().add(StatusMessage.Severity.ERROR, e.getMessage());
                }
            }
        }
    }


    public SpecimenFilter getSpecimenFilter() {
        if (specimenFilter == null) {
            specimenFilter = new SpecimenFilter(true, false, simulatedActor, false);
        }
        return specimenFilter;
    }


    public static Transaction getCurrentTransaction() {
        return currentTransaction;
    }

    public static Actor getSimulatedActor() {
        return simulatedActor;
    }

    public static Domain getSelectedDomain() {
        return selectedDomain;
    }

    public static Actor getSutActor() {
        return sutActor;
    }

    public List<TransactionInstance> getHl7Messages() {
        return hl7Messages;
    }

    public void setHl7Messages(List<TransactionInstance> hl7Messages) {
        this.hl7Messages = hl7Messages;
    }
}
