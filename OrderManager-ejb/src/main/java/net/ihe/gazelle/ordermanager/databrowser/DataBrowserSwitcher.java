package net.ihe.gazelle.ordermanager.databrowser;

import net.ihe.gazelle.common.interfacegenerator.GenerateInterface;
import net.ihe.gazelle.preferences.PreferenceService;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.faces.model.SelectItem;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

@Stateful
@Name("dataBrowserSwitcher")
@Scope(ScopeType.SESSION)
@GenerateInterface("DataBrowserSwitcherLocal")
public class DataBrowserSwitcher implements Serializable, DataBrowserSwitcherLocal {

    /**
     *
     */
    private static final long serialVersionUID = 2892258391823550112L;

    private ObjectType objectType;
    private String actorKeyword;
    private String domainKeyword;
    private Boolean lab;
    private Boolean eye;
    private Boolean rad;
    private Boolean sehe;

    @Create
    public void initialize() {
        updateCurrent(ObjectType.PATIENT, "OF", "RAD");
        lab = PreferenceService.getBoolean("laboratory_enabled");
        eye = PreferenceService.getBoolean("eyecare_enabled");
        rad = PreferenceService.getBoolean("radiology_enabled");
        sehe = PreferenceService.getBoolean("SeHE_mode_enabled");
    }

    public List<SelectItem> getAvailableDomains() {
        List<SelectItem> items = new ArrayList<SelectItem>();
        if (lab) {
            items.add(new SelectItem("LAB", "Laboratory"));
        }
        if (eye) {
            items.add(new SelectItem("EYE", "Eyecare"));
        }
        if (rad) {
            items.add(new SelectItem("RAD", "Radiology"));
        }
        if (sehe) {
            items.add(new SelectItem("SeHE", "Saudi eHealth Exchange"));
        }
        return items;
    }

    public List<SelectItem> getAvailableActors() {
        List<SelectItem> items = new ArrayList<SelectItem>();
        if (domainKeyword.equals("SeHE")) {
            items.add(new SelectItem("TRO_CREATOR", "TRO Creator"));
            items.add(new SelectItem("TRO_FORWARDER", "TRO Forwarder"));
            items.add(new SelectItem("TRO_FULFILLER", "TRO Fulfiller"));
            return items;
        } else if (lab || eye || rad) {
            items.add(new SelectItem("OF", "Order Filler"));
            items.add(new SelectItem("OP", "Order Placer"));
        }
        if (domainKeyword.equals("LAB")) {
            items.add(new SelectItem("ORT", "Order Result Tracker"));
            items.add(new SelectItem("AM", "Automation Manager"));
            items.add(new SelectItem("ANALYZER", "Analyzer"));
            items.add(new SelectItem("ANALYZER_MGR", "Analyzer Manager"));
            items.add(new SelectItem("LB", "Label Broker"));
            items.add(new SelectItem("LIP", "Label Information Provider"));
        }
        if (domainKeyword.equals("RAD") || domainKeyword.equals("EYE")) {
            items.add(new SelectItem("IM", "Image/Report Manager"));
        }
        Collections.sort(items, getComparator());
        if (!checkChoiceIsValid(items)){
            actorKeyword = (String) items.get(0).getValue();
        }
        return items;
    }

    private boolean checkChoiceIsValid(List<SelectItem> items) {
        for (SelectItem item: items){
            if (actorKeyword.equals((String) item.getValue())){
                return true;
            } else {
                continue;
            }
        }
        return false;
    }

    private Comparator<SelectItem> getComparator() {
        Comparator<SelectItem> comparator = new Comparator<SelectItem>() {
            @Override
            public int compare(SelectItem selectItem, SelectItem t1) {
                return selectItem.getLabel().compareTo(t1.getLabel());
            }
        };
        return comparator;
    }

    @Override
    public String goTo(ObjectType inType, String inActor, String inDomain) {
        updateCurrent(inType, inActor, inDomain);
        StringBuilder url = new StringBuilder(inType.getUrl());
        url.append("?actorKey=");
        url.append(actorKeyword);
        url.append("&domainKey=");
        url.append(domainKeyword);
        url.append("&objectType=");
        url.append(objectType.getKeyword());
        return url.toString();
    }


    private void updateCurrent(ObjectType objectType, String actorKeyword, String domainKeyword) {
        this.actorKeyword = actorKeyword;
        this.domainKeyword = domainKeyword;
        this.objectType = objectType;
    }

    @Override
    public String goTo(ObjectType objectType) {
        this.objectType = objectType;
        if (objectType.equals(ObjectType.DICOM) || objectType.equals(ObjectType.LOG)) {
            return objectType.getUrl();
        } else {
            return goTo(objectType, actorKeyword, domainKeyword);
        }
    }

    @Override
    public String goTo(String actorKeyword, String domainKeyword) {
        this.actorKeyword = actorKeyword;
        this.domainKeyword = domainKeyword;

        if ((objectType == null) || (objectType.getDomainActor() == null)) {
            objectType = ObjectType.PATIENT;
        } else {
            String pattern = actorKeyword + "/" + domainKeyword;
            if (!objectType.getDomainActor().contains(pattern)) {
                objectType = ObjectType.PATIENT;
            }
        }
        return goTo(objectType, this.actorKeyword, this.domainKeyword);
    }

    @Override
    public String getStyleClass(ObjectType currentObjectType) {
        if (currentObjectType.equals(objectType)) {
            return "gzl-btn gzl-btn-blue";
        } else {
            return "gzl-btn";
        }
    }

    public String redirect() {
        return goTo(objectType);
    }

    @Override
    @Remove
    @Destroy
    public void destroy() {

    }

    @Override
    public String getActorKeyword() {
        return actorKeyword;
    }

    @Override
    public String getDomainKeyword() {
        return domainKeyword;
    }

    @Override
    public ObjectType getObjectType() {
        return objectType;
    }

    public void setActorKeyword(String actorKeyword) {
        this.actorKeyword = actorKeyword;
    }

    public void setDomainKeyword(String domainKeyword) {
        this.domainKeyword = domainKeyword;
    }
}
