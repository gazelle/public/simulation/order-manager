package net.ihe.gazelle.ordermanager.model;

import net.ihe.gazelle.hql.providers.EntityManagerService;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.log.Log;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import java.io.Serializable;

/**
 * <b>Class Description : </b>UIDGenerator<br>
 * <br>
 *
 * The UIDGenerator class is used to store the patient for which an order is created
 *
 * UIDGenerator possesses the following attributes :
 * <ul>
 * <li><b>id</b> : id in the database</li>
 * <li><b>root</b> : Root OID for this UID</li>
 * <li><b>lastIndex</b> : last used index</li>
 * <li><b>tag</b> : tag requiring this UID generator</li>
 * </ul>
 *
 * @author Anne-Gaelle Berge / INRIA Rennes IHE development Project
 * @version 1.0 - 2011, March 21st
 */
@Entity
@Name("uidGenerator")
@Table(name = "uid_generator", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = "id"))
@SequenceGenerator(name = "uid_generator_sequence", sequenceName = "uid_generator_id_seq", allocationSize = 1)
public class UIDGenerator implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4196804885945560965L;

	@Logger
	private static Log log;

	@Id
	@GeneratedValue(generator = "uid_generator_sequence", strategy = GenerationType.SEQUENCE)
	@Column(name = "id")
	private Integer id;

	@Column(name = "root")
	private String root;

	@Column(name = "last_index")
	private Integer lastIndex;

	@Column(name = "tag")
	private String tag;

	@Column(name = "tag_keyword")
	private String tagKeyword;

	/**
	 * <p>Constructor for UIDGenerator.</p>
	 */
	public UIDGenerator() {

	}

	/**
	 * <p>getNextUIDForTag.</p>
	 *
	 * @param inTag a {@link java.lang.String} object.
	 * @return a {@link java.lang.String} object.
	 */
	public static String getNextUIDForTag(String inTag) {
		if ((inTag != null) && !inTag.isEmpty()) {
			EntityManager em = EntityManagerService.provideEntityManager();
			Session session = (Session) em.getDelegate();
			Criteria c = session.createCriteria(UIDGenerator.class);
			c.add(Restrictions.eq("tag", inTag));
			try {
				UIDGenerator generator = (UIDGenerator) c.uniqueResult();
				if (generator != null) {
					Integer nextIndex = generator.getLastIndex() + 1;
					String uid = generator.getRoot().concat(nextIndex.toString());
					generator.setLastIndex(nextIndex);
					em.merge(generator);
					return uid;
				} else {
					log.error("No UID Generator defined for tag " + inTag);
					return null;
				}
			} catch (HibernateException e) {
				log.error("No UID Generator defined for tag " + inTag);
				return null;
			}
		} else {
			return null;
		}
	}

	/**
	 * <p>Getter for the field <code>id</code>.</p>
	 *
	 * @return a {@link java.lang.Integer} object.
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * <p>Setter for the field <code>id</code>.</p>
	 *
	 * @param id a {@link java.lang.Integer} object.
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * <p>Getter for the field <code>root</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getRoot() {
		return root;
	}

	/**
	 * <p>Setter for the field <code>root</code>.</p>
	 *
	 * @param root a {@link java.lang.String} object.
	 */
	public void setRoot(String root) {
		this.root = root;
	}

	/**
	 * <p>Getter for the field <code>lastIndex</code>.</p>
	 *
	 * @return a {@link java.lang.Integer} object.
	 */
	public Integer getLastIndex() {
		return lastIndex;
	}

	/**
	 * <p>Setter for the field <code>lastIndex</code>.</p>
	 *
	 * @param lastIndex a {@link java.lang.Integer} object.
	 */
	public void setLastIndex(Integer lastIndex) {
		this.lastIndex = lastIndex;
	}

	/**
	 * <p>Getter for the field <code>tag</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getTag() {
		return tag;
	}

	/**
	 * <p>Setter for the field <code>tag</code>.</p>
	 *
	 * @param tag a {@link java.lang.String} object.
	 */
	public void setTag(String tag) {
		this.tag = tag;
	}

	/**
	 * <p>Getter for the field <code>tagKeyword</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getTagKeyword() {
		return tagKeyword;
	}

	/**
	 * <p>Setter for the field <code>tagKeyword</code>.</p>
	 *
	 * @param tagKeyword a {@link java.lang.String} object.
	 */
	public void setTagKeyword(String tagKeyword) {
		this.tagKeyword = tagKeyword;
	}

	/** {@inheritDoc} */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = (prime * result) + ((tag == null) ? 0 : tag.hashCode());
		return result;
	}

	/** {@inheritDoc} */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		UIDGenerator other = (UIDGenerator) obj;
		if (tag == null) {
			if (other.tag != null) {
				return false;
			}
		} else if (!tag.equals(other.tag)) {
			return false;
		}
		return true;
	}

}
