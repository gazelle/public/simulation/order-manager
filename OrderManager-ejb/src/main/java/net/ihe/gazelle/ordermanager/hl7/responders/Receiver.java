package net.ihe.gazelle.ordermanager.hl7.responders;

import ca.uhn.hl7v2.AcknowledgmentCode;
import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.model.Message;
import ca.uhn.hl7v2.util.Terser;
import net.ihe.gazelle.HL7Common.model.Charset;
import net.ihe.gazelle.HL7Common.model.HL7V2ResponderSUTConfiguration;
import net.ihe.gazelle.ordermanager.utils.IHERadMessageBuilder;

import java.util.List;

/**
 * Class description : <b>Receiver</b>
 * 
 * This class is used to build ACK messages
 * 
 *
 *
 * @author Anne-Gaëlle Bergé - INRIA Rennes/Bretagne Atlantique
 *
 * @version 1.0 - 2011, November 17th
 * 
 */
public final class Receiver {

	private Receiver() {
		super();
	}

	private static final String UNKNOWN_MESSAGE_TYPE = "1";
	private static final String UNKNOWN_TRIGGER_EVENT = "2";
	private static final String UNSUPPORTED_MESSAGE_TYPE_HL7_CODE = "200";
	private static final String UNSUPPORTED_TRIGGER_EVENT_HL7_CODE = "201";
	public static final String DUPLICATE_KEY_IDENTIFIER = "205";
	public static final String UNKNOWN_KEY_IDENTIFIER = "204";
	public static final String APPLICATION_RECORD_LOCK = "206";
	public static final String INTERNAL_ERROR = "207";
	public static final String REQUIRED_FIELD_MISSING = "101";
	public static final String SEGMENT_SEQUENCE_ERROR = "100";
	public static final String SEVERITY_ERROR = "E";
	public static final String SEVERITY_WARNING = "W";
	public static final String SEVERITY_INFORMATION = "I";
	private static final AcknowledgmentCode APPLICATION_REJECT = AcknowledgmentCode.AR;
	public static final String TABLE_VALUE_NOT_FOUND = "103";

	/**
	 * Check the MSH segment values and creates an NACK if necessary
	 * 
	 * @param receivedMessage
	 * @param expectedReceivingApplication
	 * @param expectedReceivingFacility
	 * @param acceptedMessageTypes
	 * @param acceptedTriggerEvents
	 * @param simulatorCharset
	 * @return null if the message can be processed
	 * @throws HL7Exception
	 */
	public static Message generateNack(Message receivedMessage, String expectedReceivingApplication,
			String expectedReceivingFacility, List<String> acceptedMessageTypes, List<String> acceptedTriggerEvents,
			String simulatorCharset) throws HL7Exception {
		Terser t = new Terser(receivedMessage);

		String sendingApplication = t.get("/.MSH-3-1");
		String sendingFacility = t.get("/.MSH-4-1");
		String receivingApplication = t.get("/.MSH-5-1");
		String receivingFacility = t.get("/.MSH-6-1");
		String messageType = t.get("/.MSH-9-1");
		String event = t.get("/.MSH-9-2");
		String hl7Version = t.get("MSH-12-1");
		String messageControlId = t.get("/.MSH-10");

		// check receivingApplication and receivingFacility are those expected
		if ((receivingApplication != null) && !receivingApplication.equals(expectedReceivingApplication)) {
			return buildNack(expectedReceivingApplication, expectedReceivingFacility, sendingApplication,
					sendingFacility, event, APPLICATION_REJECT, "Receiving Application unavailable: "
							+ receivingApplication, null, TABLE_VALUE_NOT_FOUND, SEVERITY_ERROR, messageControlId,
					simulatorCharset, hl7Version);
		}
		if ((receivingFacility != null) && !receivingFacility.equals(expectedReceivingFacility)) {
			return buildNack(expectedReceivingApplication, expectedReceivingFacility, sendingApplication,
					sendingFacility, event, APPLICATION_REJECT, "Receiving Facility unavailable: " + receivingFacility,
					null, TABLE_VALUE_NOT_FOUND, SEVERITY_ERROR, messageControlId, simulatorCharset, hl7Version);
		}

		// check message type and trigger event are accepted

		if ((messageType != null) && !acceptedMessageTypes.contains(messageType)) {
			return buildNack(expectedReceivingApplication, expectedReceivingFacility, sendingApplication,
					sendingFacility, event, APPLICATION_REJECT, "This message type is not supported",
					UNKNOWN_MESSAGE_TYPE, UNSUPPORTED_MESSAGE_TYPE_HL7_CODE, SEVERITY_ERROR, messageControlId,
					simulatorCharset, hl7Version);
		}

		if ((event != null) && !acceptedTriggerEvents.contains(event)) {
			return buildNack(expectedReceivingApplication, expectedReceivingFacility, sendingApplication,
					sendingFacility, event, APPLICATION_REJECT, "This trigger event is not supported",
					UNKNOWN_TRIGGER_EVENT, UNSUPPORTED_TRIGGER_EVENT_HL7_CODE, SEVERITY_ERROR, messageControlId,
					simulatorCharset, hl7Version);
		}
		return null;
	}

	/**
	 * 
	 * @param sendingApplication
	 * @param sendingFacility
	 * @param receivingApplication
	 * @param receivingFacility
	 * @param ackCode
	 * @param userMessage
	 * @param errorType
	 * @param hl7ErrorCode
	 * @param severity
	 * @param messageControlId
	 * @param simulatorCharset
	 * @param hl7Version
	 * @return the acknowledgement
	 */
	public static Message buildNack(String sendingApplication, String sendingFacility, String receivingApplication,
									String receivingFacility, String triggerEvent, AcknowledgmentCode ackCode, String userMessage, String errorType,
									String hl7ErrorCode, String severity, String messageControlId, String simulatorCharset, String hl7Version) {
		HL7V2ResponderSUTConfiguration sut = new HL7V2ResponderSUTConfiguration();
		sut.setApplication(receivingApplication);
		sut.setFacility(receivingFacility);
		Charset charset = new Charset();
		charset.setHl7Code(simulatorCharset);
		sut.setCharset(charset);
		IHERadMessageBuilder generator = new IHERadMessageBuilder(null, null, null, sut, sendingApplication, sendingFacility, false);
		return generator.buildACK(triggerEvent, ackCode, userMessage, errorType, hl7ErrorCode, severity,
				messageControlId, hl7Version, true);
	}

	/**
	 * 
	 * @param sendingApplication
	 * @param sendingFacility
	 * @param receivingApplication
	 * @param receivingFacility
	 * @param triggerEvent
	 * @param ackCode
	 * @param messageControlId
	 * @param simulatorCharset
	 * @param hl7Version
	 * @return the acknowledgement
	 */
	public static Message buildAck(String sendingApplication, String sendingFacility, String receivingApplication,
								   String receivingFacility, String triggerEvent, AcknowledgmentCode ackCode, String messageControlId,
								   String simulatorCharset, String hl7Version) {
		HL7V2ResponderSUTConfiguration sut = new HL7V2ResponderSUTConfiguration();
		sut.setApplication(receivingApplication);
		sut.setFacility(receivingFacility);
		Charset charset = new Charset();
		charset.setHl7Code(simulatorCharset);
		sut.setCharset(charset);
		IHERadMessageBuilder generator = new IHERadMessageBuilder(null, null, null, sut, sendingApplication, sendingFacility, false);
		return generator.buildACK(triggerEvent, ackCode, null, null, null, null, messageControlId, hl7Version, false);
	}
}
