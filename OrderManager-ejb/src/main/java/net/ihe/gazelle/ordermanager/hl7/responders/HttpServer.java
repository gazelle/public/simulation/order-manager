package net.ihe.gazelle.ordermanager.hl7.responders;

import ca.uhn.hl7v2.hoh.hapi.server.HohServlet;
import net.ihe.gazelle.HL7Common.model.HL7Protocol;
import net.ihe.gazelle.HL7Common.model.SimulatorResponderConfiguration;
import net.ihe.gazelle.HL7Common.model.SimulatorResponderConfigurationQuery;
import net.ihe.gazelle.HL7Common.responder.IHEDefaultHandler;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.common.tf.model.Domain;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;

public abstract class HttpServer extends HohServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	private static Logger log = LoggerFactory.getLogger(HttpServer.class);

	@Override
    public void init(ServletConfig theConfig) throws ServletException {
			EntityManager entityManager = EntityManagerService.provideEntityManager();
			SimulatorResponderConfigurationQuery query = new SimulatorResponderConfigurationQuery(entityManager);
			query.simulatedActor().keyword().eq(getActorKeyword());
			query.transaction().keyword().eq(getTransactionKeyword());
			query.domain().keyword().eq(getDomainKeyword());
			query.hl7Protocol().eq(HL7Protocol.HTTP);
			SimulatorResponderConfiguration config = query.getUniqueResult();
			if (config == null){
				log.error("No SimulatorResponderConfiguration found for " + getDomainKeyword() + ", " + getActorKeyword() + ", " + getTransactionKeyword());
			}else{
				IHEDefaultHandler defaultHandler = getHandler();
				defaultHandler.setServerApplication(config.getReceivingApplication());
				defaultHandler.setServerFacility(config.getReceivingFacility());
				defaultHandler.setSimulatedActor(Actor.findActorWithKeyword(getActorKeyword(), entityManager));
				defaultHandler.setSimulatedTransaction(Transaction.GetTransactionByKeyword(getTransactionKeyword(), entityManager));
				defaultHandler.setDomain(Domain.getDomainByKeyword(getDomainKeyword()));
				setApplication(defaultHandler);
			}
    }
	
	protected abstract IHEDefaultHandler getHandler();
	
	protected abstract String getActorKeyword();
	
	protected abstract String getTransactionKeyword();
	
	protected abstract String getDomainKeyword();
	
}
