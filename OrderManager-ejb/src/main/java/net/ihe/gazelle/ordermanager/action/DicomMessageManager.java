package net.ihe.gazelle.ordermanager.action;

import net.ihe.gazelle.evsclient.connector.api.EVSClientResults;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.ordermanager.datamodel.DicomMessageDataModel;
import net.ihe.gazelle.ordermanager.dicom.utils.DICOMDumpUtil;
import net.ihe.gazelle.ordermanager.model.DicomMessage;
import net.ihe.gazelle.ordermanager.model.DicomValidationResult;
import net.ihe.gazelle.ordermanager.model.DicomValidationResultQuery;
import net.ihe.gazelle.preferences.PreferenceService;
import org.apache.commons.io.IOUtils;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import java.io.*;
import java.nio.charset.Charset;
import java.util.List;
import java.util.Map;

/**
 * @author aberge
 */
@Name("dicomMessageManager")
@Scope(ScopeType.PAGE)
public class DicomMessageManager implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    public static final String UTF_8 = "UTF-8";


    private static final Logger LOG = LoggerFactory.getLogger(DicomMessageManager.class);
    public static final String KEY_OID = "oid";

    private DicomMessage selectedMessage;
    private DicomMessageDataModel relatedMessages;
    private String messageContent;
    private String errorMessage;
    private String validationResult;

    @Create
    public void initializePage() {
        Map<String, String> urlParams = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        if ((urlParams != null) && urlParams.containsKey("id")) {
            Integer id = Integer.decode(urlParams.get("id"));
            EntityManager entityManager = EntityManagerService.provideEntityManager();
            try {
                selectedMessage = entityManager.find(DicomMessage.class, id);
                if (selectedMessage != null) {
                    relatedMessages = new DicomMessageDataModel(selectedMessage.getConnection(),
                            selectedMessage.getId());
                    if (selectedMessage.getFilePath() != null) {
                        String dumpFilePath = selectedMessage.getDumpFilePath();
                        File dumpFile = new File(dumpFilePath);
                        if (!dumpFile.exists()) {
                            convertDataSet();
                        }
                        messageContent = extractMessageContent(dumpFile);
                        if (messageContent == null){
                            errorMessage = "The tool has not been able to dump the DICOM file";
                        }
                    } else {
                        errorMessage = "This message does not contain any payload to display";
                    }
                } else {
                    FacesMessages.instance().add(StatusMessage.Severity.ERROR, "The given id does not match any dicom message in the database");
                    relatedMessages = null;
                    messageContent = null;
                }
            } catch (NumberFormatException e) {
                LOG.debug(e.getMessage(), e);
                FacesMessages.instance().add(StatusMessage.Severity.ERROR, "The given parameter is not a valid Id");
            }
        }
            String validationOid = EVSClientResults.getResultOidFromUrl(urlParams);
         if (validationOid != null) {
             DicomValidationResult result = new DicomValidationResult(selectedMessage, validationOid);
             getResultDetails(result);
         }
    }

    private static void getResultDetails(final DicomValidationResult result) {
        String evsclientUrl = PreferenceService.getString("evs_client_url");
        String validationDate = EVSClientResults.getValidationDate(result.getOid(), evsclientUrl);
        result.setValidationDate(validationDate);
        String validationStatus = EVSClientResults.getValidationStatus(result.getOid(), evsclientUrl);
        result.setValidationStatus(validationStatus);
        String linkToResult = EVSClientResults.getValidationPermanentLink(result.getOid(), evsclientUrl);
        result.setPermanentLink(linkToResult);
        result.save();
    }


    private void convertDataSet() {
        try {
            DICOMDumpUtil.convertDataSetToText(selectedMessage.getFilePath());
        }catch (InterruptedException e){
            LOG.error(e.getMessage());
            Thread.currentThread().interrupt();
        }
    }

    private static String extractMessageContent(File file) {
        FileInputStream fis = null;
        byte[] content = null;
        try {
            fis = new FileInputStream(file);
            int length = fis.available();
            content = new byte[length];
            if (fis.read(content) < length){
                LOG.warn("Not all bytes were read");
            }
        } catch (FileNotFoundException e) {
            LOG.error("Cannot find file at " + file.getAbsolutePath());
        } catch (IOException e) {
            LOG.error("Cannot read file at " + file.getAbsolutePath());
        } finally {
            IOUtils.closeQuietly(fis);
        }
        if (content != null) {
            return new String(content, Charset.forName(UTF_8));
        } else {
            return null;
        }
    }

    public boolean displayResults(){
        DicomValidationResultQuery query = new DicomValidationResultQuery();
        query.dicomMessage().id().eq(selectedMessage.getId());
        return query.getCount() > 0;
    }

    public List<DicomValidationResult> getValidationResults(){
        DicomValidationResultQuery query = new DicomValidationResultQuery();
        query.dicomMessage().id().eq(selectedMessage.getId());
        query.oid().order(false);
        return query.getList();
    }

    public DicomMessage getSelectedMessage() {
        return selectedMessage;
    }

    public void setSelectedMessage(DicomMessage selectedMessage) {
        this.selectedMessage = selectedMessage;
    }

    public DicomMessageDataModel getRelatedMessages() {
        return relatedMessages;
    }

    public void setRelatedMessages(DicomMessageDataModel relatedMessages) {
        this.relatedMessages = relatedMessages;
    }

    public void setMessageContent(String messageContent) {
        this.messageContent = messageContent;
    }

    public String getMessageContent() {
        return messageContent;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setValidationResult(String validationResult) {
        this.validationResult = validationResult;
    }

    public String getValidationResult() {
        return validationResult;
    }

}
