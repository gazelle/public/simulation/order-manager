package net.ihe.gazelle.ordermanager.menu;

import net.ihe.gazelle.common.pages.Authorization;
import net.ihe.gazelle.common.pages.AuthorizationAnd;
import net.ihe.gazelle.common.pages.AuthorizationOr;
import net.ihe.gazelle.common.pages.Page;
import net.ihe.gazelle.ordermanager.action.OrderManager;

public enum OrderManagerPages implements
		Page {

	ADMIN_CONFIG("/administration/configure.xhtml", null, "Application configuration", OrderManagerAuthorizations.ADMIN),
	ADMIN_VALUE_SETS("/administration/valueSetManager.xhtml", null, "Value sets", OrderManagerAuthorizations.ADMIN),
    VALUE_SETS("/administration/valueSetManager.xhtml", null, "Value sets", OrderManagerAuthorizations.NON_ADMIN),
	ADMIN_WORKLIST("/administration/worklistManager.xhtml", null, "DICOM worklist", new AuthorizationAnd(OrderManagerAuthorizations.ADMIN,
            new AuthorizationOr(OrderManagerAuthorizations.RADIOLOGY, OrderManagerAuthorizations.EYECARE))),
	ADMIN_HL7_RESP("/administration/responderManager.xhtml", null, "HL7 responders", OrderManagerAuthorizations.ADMIN),
	ADMIN_ISSUERS("/administration/numberGenerators.xhtml", null, "Manage referenced objects", OrderManagerAuthorizations.ADMIN),
	ADMIN_USER_PREFERENCES("/administration/allUsers.xhtml", null, "User preferences", OrderManagerAuthorizations.ADMIN),
	ADMIN_VALIDATION_PARAMS("/configuration/validationParametersList.xhtml", null, "Validation parameters", OrderManagerAuthorizations.ADMIN),
    EYE_IM_CONF("/hl7Responders/configurationAndMessages.seam?actor=IM&domain=EYE", null, "Image/Report Manager", OrderManagerAuthorizations.EYECARE),
    OF("", null, "Order Filler", OrderManagerAuthorizations.ALL),
    OP("", null, "Order Placer", OrderManagerAuthorizations.ALL),
    ANALYZER("", null, "Analyzer", OrderManagerAuthorizations.ALL),
    MOD("", null, "Acquisition Modality", OrderManagerAuthorizations.ALL),
    AM("", null, "Automation Manager", OrderManagerAuthorizations.ALL),
    ANALYZER_MGR("", null, "Analyzer Manager", OrderManagerAuthorizations.ALL),
    SUT("", null, "SUT Configurations", OrderManagerAuthorizations.ALL),
    MESSAGES("", null, "Messages", OrderManagerAuthorizations.ALL),
    ADMINISTRATION("", null, "Administration", OrderManagerAuthorizations.ADMIN),
    EYECARE("", null, "Eyecare", OrderManagerAuthorizations.EYECARE),
    LAB("", null, "Laboratory", OrderManagerAuthorizations.LABORATORY),
    RADIOLOGY("", null, "Radiology", OrderManagerAuthorizations.RADIOLOGY),
    EYE_OF_RAD3("/hl7Initiators/orderSender.seam?actor=OF&domain=EYE", null, "[RAD-3] Create/Update/Cancel orders", OrderManagerAuthorizations.EYECARE),
    EYE_OF_RAD4("/hl7Initiators/procedureSender.seam?actor=OF&domain=EYE", null, "[RAD-4]/[RAD-13] Schedule/Update procedures", OrderManagerAuthorizations.EYECARE),
    EYE_OF_RAD48("/underConstruction.seam", null, "[RAD-48] Appointment notification", OrderManagerAuthorizations.ADMIN),
    EYE_OF_WORKLIST("/mwlscp/createWorklistEntry.seam?domain=EYE", null, "Create a DICOM worklist", OrderManagerAuthorizations.EYECARE),
    EYE_OF_CONF("/hl7Responders/configurationAndMessages.seam?actor=OF&domain=EYE", null, "Configuration", OrderManagerAuthorizations.EYECARE),
    EYE_OF_RAD5("/mwlscp/logs.seam", null, "[RAD-5] Modality Worklist's logs", OrderManagerAuthorizations.EYECARE),
    EYE_OP_RAD2("/hl7Initiators/orderSender.seam?actor=OP&domain=EYE", null, "[RAD-2] Create/Cancel orders", OrderManagerAuthorizations.EYECARE),
    EYE_OP_CONF("/hl7Responders/configurationAndMessages.seam?actor=OP&domain=EYE", null, "Configuration", OrderManagerAuthorizations.EYECARE),
    RAD_IM_CONF("/hl7Responders/configurationAndMessages.seam?actor=IM&domain=RAD", null, "Image/Report Manager", OrderManagerAuthorizations.RADIOLOGY),
    RAD_MOD_RAD5("/dicomSCU/findModalityWorklistSCU.xhtml", null, "[RAD-5] Query Modality Worklist", OrderManagerAuthorizations.RADIOLOGY),
    RAD_OF_RAD3("/hl7Initiators/orderSender.seam?actor=OF&domain=RAD", null, "[RAD-3] Create/Update/Cancel orders", OrderManagerAuthorizations.RADIOLOGY),
    RAD_OF_RAD4("/hl7Initiators/procedureSender.seam?actor=OF&domain=RAD", null, "[RAD-4]/[RAD-13] Schedule/Update procedures", OrderManagerAuthorizations.RADIOLOGY),
    RAD_OF_RAD48("/underConstruction.xhtml", null, "[RAD-48] Appointment notification", OrderManagerAuthorizations.RADIOLOGY),
    RAD_OF_WORKLIST("/mwlscp/createWorklistEntry.seam?domain=RAD", null, "Create a DICOM worklist", OrderManagerAuthorizations.RADIOLOGY),
    RAD_OF_CONF("/hl7Responders/configurationAndMessages.seam?actor=OF&domain=RAD", null, "Configuration", OrderManagerAuthorizations.RADIOLOGY),
    RAD_OF_RAD5("/mwlscp/logs.seam", null, "[RAD-5] Modality Worklist's logs", OrderManagerAuthorizations.RADIOLOGY),
    RAD_OP_RAD2("/hl7Initiators/orderSender.seam?actor=OP&domain=RAD", null, "[RAD-2] Create/Cancel orders", OrderManagerAuthorizations.RADIOLOGY),
    RAD_OP_CONF("/hl7Responders/configurationAndMessages.seam?actor=OP&domain=RAD", null, "Configuration", OrderManagerAuthorizations.RADIOLOGY),
    SUT_HL7_CONF("/sut/sutAsHL7v2Responder.xhtml", null, "HL7v2 responders", OrderManagerAuthorizations.ALL),
    SUT_DICOM_CONF("/sut/scpConfigurations.xhtml", null, "DICOM SCP", OrderManagerAuthorizations.RADIOLOGY),
    LAB_AN_LAB27("/hl7Initiators/queryForAWOSSender.seam?domain=LAB", null, "[LAB-27] Query for AWOS", OrderManagerAuthorizations.LABORATORY),
    LAB_AN_LAB29("/hl7Initiators/orderResultSender.seam?domain=LAB&actor=ANALYZER", null, "[LAB-29] AWOS Status Change", OrderManagerAuthorizations.LABORATORY),
    LAB_AN_CONF("/hl7Responders/configurationAndMessages.seam?actor=ANALYZER&domain=LAB", null, "Configuration", OrderManagerAuthorizations.LABORATORY),
    LAB_ANMGR_LAB27("/lab/wosQueryContent.xhtml", null, "[LAB-27] Received AWOS queries", OrderManagerAuthorizations.LABORATORY),
    LAB_ANMGR_LAB28("/hl7Initiators/awosSender.seam?workOrder=false&actor=ANALYZER_MGR&domain=LAB", null, "[LAB-28] AWOS Broadcast", OrderManagerAuthorizations.LABORATORY),
    LAB_ANMGR_CONF("/hl7Responders/configurationAndMessages.seam?actor=ANALYZER_MGR&domain=LAB", null, "Configuration", OrderManagerAuthorizations.LABORATORY),
    LAB_AM_LAB5("/hl7Initiators/orderResultSender.seam?domain=LAB&actor=AM", null, "[LAB-5] Test results management", OrderManagerAuthorizations.LABORATORY),
    LAB_AM_CONF("/hl7Responders/configurationAndMessages.seam?actor=AM&domain=LAB", null, "Configuration", OrderManagerAuthorizations.LABORATORY),
    LAB_OF_CONF("/hl7Responders/configurationAndMessages.seam?actor=OF&domain=LAB", null, "Configuration", OrderManagerAuthorizations.LABORATORY),
    LAB_OP_CONF("/hl7Responders/configurationAndMessages.seam?actor=OP&domain=LAB", null, "Configuration", OrderManagerAuthorizations.LABORATORY),
    LAB_ORT_CONF("/hl7Responders/configurationAndMessages.seam?actor=ORT&domain=LAB", null, "Order Result Tracker", OrderManagerAuthorizations.LABORATORY),
    LAB_LB_CONF("/hl7Responders/configurationAndMessages.seam?actor=LB&domain=LAB", null, "Configuration", OrderManagerAuthorizations.LABORATORY),
    LAB_LIP_CONF("/hl7Responders/configurationAndMessages.seam?actor=LIP&domain=LAB", null, "Configuration", OrderManagerAuthorizations.LABORATORY),
    LAB_OF_LAB1("/hl7Initiators/labOrderSender.seam?workOrder=false&actor=OF&domain=LAB", null, "[LAB-1]/[LAB-2] Create/Update/Cancel orders", OrderManagerAuthorizations.LABORATORY),
    LAB_OF_LAB3("/hl7Initiators/orderResultSender.seam?domain=LAB&actor=OF", null, "[LAB-3] Send test results to ORT", OrderManagerAuthorizations.LABORATORY),
    LAB_OF_LAB4("/hl7Initiators/labOrderSender.seam?workOrder=true&actor=OF&domain=LAB", null, "[LAB-4] Create/Cancel work orders", OrderManagerAuthorizations.LABORATORY),
    LAB_OP_LAB1("/hl7Initiators/labOrderSender.seam?workOrder=false&actor=OP&domain=LAB", null, "[LAB-1]/[LAB-2] Create/Cancel orders", OrderManagerAuthorizations.LABORATORY),
    LAB_LB_LAB62("/hl7Initiators/queryForLabelDelivery.seam?domain=LAB&actor=LB", null, "[LAB-62] Query for Label Delivery", OrderManagerAuthorizations.LABORATORY),
    LAB_LB_LAB63("/hl7Initiators/labelDeliveredSender.xhtml", null, "[LAB-63] Labels & Containers delivered", OrderManagerAuthorizations.LABORATORY),
    LAB_LIP_LAB61("/hl7Initiators/labelDeliveryRequest.seam?domain=LAB&actor=LIP", null, "[LAB-61] Label Delivery Request", OrderManagerAuthorizations.LABORATORY),
    HL7_MESSAGES("/messages/browser.xhtml", null, "HL7v2 messages", OrderManagerAuthorizations.ALL),
    DICOM_MESSAGES("/browsing/allDicomMessages.seam?objectType=dicomMessage", null, "DICOM messages", OrderManagerAuthorizations.RADIOLOGY, OrderManagerAuthorizations.EYECARE),
    DATA_BROWSER("/browsing/dataBrowser.xhtml", null, "Data Browser", OrderManagerAuthorizations.ALL),
    HOME("/home.xhtml", null, "Home", OrderManagerAuthorizations.ALL),
    SEHE("", null, "SeHE Tele-Radiology", OrderManagerAuthorizations.SEHE),
    SEHE_TRO_CREATOR("", null, "Tele-Radiology Order Creator", OrderManagerAuthorizations.ALL),
    SEHE_TRO_FORWARDER("/hl7Responders/configurationAndMessages.seam?actor=TRO_FORWARDER&afdomain=SeHE", null, "Tele-Radiology Order Forwarder", OrderManagerAuthorizations.ALL),
    SEHE_TRO_FULFILLER("", null, "Tele-Radiology Order Fulfiller", OrderManagerAuthorizations.ALL),
    SEHE_CREATOR_MANAGE_ORDER("/tro/manageOrder.seam?actor=TRO_CREATOR&domain=SeHE", null, "[KSA-TRO] Manage Order", OrderManagerAuthorizations.SEHE),
    SEHE_TRO_CREATOR_CONF("/hl7Responders/configurationAndMessages.seam?actor=TRO_CREATOR&afdomain=SeHE", null, "Configuration", OrderManagerAuthorizations.SEHE),
    SEHE_FULFILLER_MANAGE_ORDER("/tro/manageOrder.seam?actor=TRO_FULFILLER&domain=SeHE", null, "[KSA-TRO] Manage Order", OrderManagerAuthorizations.SEHE),
    SEHE_TRO_FULFILLER_CONF("/hl7Responders/configurationAndMessages.seam?actor=TRO_FULFILLER&afdomain=SeHE", null, "Configuration", OrderManagerAuthorizations.SEHE),
    LB("", null, "Label Broker", OrderManagerAuthorizations.LABORATORY),
    LIP("", null, "Label Information Provider", OrderManagerAuthorizations.LABORATORY),
    ALL_PATIENTS("/browsing/allPatients.seam", null, "All patients", OrderManagerAuthorizations.ALL),
    HL7MESSAGES("/messages/messageDisplay.seam", null, "HL7 messages", OrderManagerAuthorizations.ALL);


    private String link;

	private Authorization[] authorizations;

	private String label;

	private String icon;

	OrderManagerPages(String link, String icon, String label, Authorization... authorizations) {
		this.link = link;
		this.authorizations = authorizations;
		this.label = label;
		this.icon = icon;
	}

	@Override
	public String getId() {
		return name();
	}

	@Override
	public String getIcon() {
		return icon;
	}

	@Override
	public Authorization[] getAuthorizations() {
        if (authorizations != null) {
            return authorizations.clone();
        } else {
            return null;
        }
	}

	@Override
	public String getLabel() {
		return label;
	}

    @Override
    public String getLink(){
        return link;
    }

    @Override
    public String getMenuLink(){
        return link.replace(".xhtml", ".seam");
    }
}
