package net.ihe.gazelle.ordermanager.utils;

import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.model.DataTypeException;
import net.ihe.gazelle.HL7Common.messages.SegmentBuilder;
import net.ihe.gazelle.laboratory.law.hl7v2.model.v251.message.QBP_Q11;
import net.ihe.gazelle.laboratory.law.hl7v2.model.v251.message.RSP_K11;
import net.ihe.gazelle.laboratory.law.hl7v2.model.v251.segment.ERR;
import net.ihe.gazelle.laboratory.law.hl7v2.model.v251.segment.MSA;
import net.ihe.gazelle.laboratory.law.hl7v2.model.v251.segment.MSH;
import net.ihe.gazelle.laboratory.law.hl7v2.model.v251.segment.QAK;
import net.ihe.gazelle.laboratory.law.hl7v2.model.v251.segment.QPD;
import net.ihe.gazelle.laboratory.law.hl7v2.model.v251.segment.RCP;
import net.ihe.gazelle.ordermanager.model.Container;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class LAWQueryMessagesGenerator extends SegmentBuilder {

	private static Logger log = LoggerFactory.getLogger(LAWQueryMessagesGenerator.class);

	public static QBP_Q11 generateQBP_Q11(String targetApplication, String targetFacility, String sendingApplication,
                                          String sendingFacility, String charset, Container container, QueryMode queryMode) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss", Locale.ENGLISH);

		QBP_Q11 message = new QBP_Q11();

		MSH mshSegment = message.getMSH();
		QPD qpdSegment = message.getQPD();
		RCP rcpSegment = message.getRCP();

		fillMSHSegmentv251(mshSegment, targetApplication, targetFacility, sdf, sendingApplication, sendingFacility,
				charset, "QBP", "Q11", "QBP_Q11", "LAB-27^IHE");

		fillQPDSegmentLAW(qpdSegment, sdf, container, queryMode);

		fillRCPSegmentv251(rcpSegment);

		return message;
	}


	public static RSP_K11 generateRSP_K11(QBP_Q11 messageReceived, String sendingApplication, String sendingFacility,
			String charset, boolean error, String hl7ErrorCode, String severity, String diagnosticInformation) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss", Locale.ENGLISH);

		RSP_K11 message = new RSP_K11();

		String msaAckCode = "AA";
		String queryResponseStatus = "OK";
		if (error) {
			msaAckCode = "AR";
			queryResponseStatus = "AR";
			ERR errSegment = message.getERR();
			fillERRSegment(errSegment, hl7ErrorCode, severity, diagnosticInformation);
		}

		try {
			fillMSHSegmentv251(message.getMSH(), messageReceived.getMSH().getSendingApplication().encode(), messageReceived
					.getMSH().getMsh4_SendingFacility().encode(), sdf, sendingApplication, sendingFacility, charset,
					"RSP", "K11", "RSP_K11", "LAB-27^IHE");

			fillMSASegment(message.getMSA(), msaAckCode, messageReceived.getMSH().getMessageControlID().encode());

			fillQAKSegment(message.getQAK(), messageReceived.getQPD().getQueryTag().encode(), queryResponseStatus,
					messageReceived.getQPD().getMessageQueryName().encode());

			message.getQPD().parse(messageReceived.getQPD().encode());

		} catch (HL7Exception e) {
			log.error(e.getMessage(), e);
		}

		return message;
	}

	public static void fillMSHSegmentv251(MSH mshSegment, String targetApplication, String targetFacility,
                                          SimpleDateFormat sdf, String sendingApplication, String sendingFacility, String charset,
                                          String messageCode, String triggerEvent, String messageStructure, String messageProfileIdentifier) {
		try {
			mshSegment.getFieldSeparator().setValue("|");
			mshSegment.getEncodingCharacters().setValue("^~\\&");
			mshSegment.getSendingApplication().getNamespaceID().setValue(sendingApplication);
			mshSegment.getSendingFacility().getNamespaceID().setValue(sendingFacility);
			mshSegment.getReceivingApplication().getNamespaceID().setValue(targetApplication);
			mshSegment.getReceivingFacility().getNamespaceID().setValue(targetFacility);
			mshSegment.getDateTimeOfMessage().getTime().setValue(sdf.format(Calendar.getInstance().getTime()));
			mshSegment.getMessageType().getMessageCode().setValue(messageCode);
			mshSegment.getMessageType().getTriggerEvent().setValue(triggerEvent);
			mshSegment.getMessageType().getMessageStructure().setValue(messageStructure);
			mshSegment.getProcessingID().getProcessingID().setValue("P");
			mshSegment.getMessageControlID().setValue(sdf.format(Calendar.getInstance().getTime()));
			mshSegment.getVersionID().getVersionID().setValue("2.5.1");
			mshSegment.getCharacterSet().setValue(charset);
			mshSegment.getMessageProfileIdentifier(0).parse(messageProfileIdentifier);
			if (messageCode.equals("QBP")) {
				mshSegment.getAcceptAcknowledgmentType().setValue("NE");
				mshSegment.getApplicationAcknowledgmentType().setValue("AL");
			}
		} catch (DataTypeException e) {
			log.error(e.getMessage(), e);
		} catch (HL7Exception e) {
			log.error(e.getMessage(), e);
		}
	}

	private static void fillQPDSegmentLAW(QPD qpdSegment, SimpleDateFormat sdf, Container container, QueryMode queryMode) {
		try {
			if (!queryMode.equals(QueryMode.WOS_ALL)) {
				try {
					if (container.getContainerIdentifier() != null) {
						qpdSegment.getQpd3_SAC3ContainerIdentifier().parse(container.getContainerIdentifier());
					}
					if (container.getCarrierIdentifier() != null) {
						qpdSegment.getQpd4_SAC10CarrierIdentification().parse(container.getCarrierIdentifier());
					}
					if (container.getPositionInCarrier() != null) {
						qpdSegment.getQpd5_SAC11PositionInCarrier().parse(container.getPositionInCarrier());
					}
					if (container.getTrayIdentifier() != null) {
						qpdSegment.getQpd6_SAC13TrayIdentifier().parse(container.getTrayIdentifier());
					}
					if (container.getPositionInTray() != null) {
						qpdSegment.getQpd7_SAC11PositionInTray().parse(container.getPositionInTray());
					}
					if (container.getLocation() != null) {
						qpdSegment.getQpd8_SAC15Location().parse(container.getLocation());
					}
					if (container.getPrimaryContainerIdentifier() != null){
                        qpdSegment.getQpd9_SAC4PrimaryParentContainerIdentifier().parse(container.getPrimaryContainerIdentifier());
					}
				} catch (HL7Exception e) {
					log.error(e.getMessage(), e);
				}
			}
            qpdSegment.getMessageQueryName().getCe1_Identifier().setValue(queryMode.getQueryNameId());
            qpdSegment.getMessageQueryName().getCe2_Text().setValue(queryMode.getQueryName());
			qpdSegment.getMessageQueryName().getCe3_NameOfCodingSystem().setValue(queryMode.CODING_SYSTEM);
			qpdSegment.getQueryTag().setValue(sdf.format(new Date()));
		} catch (DataTypeException e) {
			log.error(e.getMessage(), e);
		}
	}

	private static void fillRCPSegmentv251(RCP rcpSegment) {
		try {
			rcpSegment.getQueryPriority().setValue("I");
			rcpSegment.getResponseModality().getIdentifier().setValue("R");
		} catch (DataTypeException e) {
			log.error(e.getMessage(), e);
		}

	}

	public static void fillMSASegment(MSA msaSegment, String ack, String messageControlId) {
		try {
			msaSegment.getAcknowledgmentCode().parse(ack);
			msaSegment.getMessageControlID().parse(messageControlId);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
	}

	private static void fillQAKSegment(QAK qakSegment, String queryTag, String queryResponseStatus,
			String messageQueryName) {
		try {
			qakSegment.getQueryTag().parse(queryTag);
			qakSegment.getQueryResponseStatus().parse(queryResponseStatus);
			qakSegment.getMessageQueryName().parse(messageQueryName);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
	}

	private static void fillERRSegment(ERR errSegment, String hL7ErrorCode, String severity,
			String diagnosticInformation) {

		try {
			// ERR-3
			if ((hL7ErrorCode != null) && !hL7ErrorCode.isEmpty()) {
				errSegment.getHL7ErrorCode().parse(hL7ErrorCode);
			}

			// ERR-4
			if ((severity != null) && !severity.isEmpty()) {
				errSegment.getSeverity().parse(severity);
			}

			// ERR-7
			if ((diagnosticInformation != null) && !diagnosticInformation.isEmpty()) {
				errSegment.getDiagnosticInformation().parse(diagnosticInformation);
			}

		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
	}

}
