package net.ihe.gazelle.ordermanager.model;

import org.jboss.seam.annotations.Name;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

/**
 * Class description : <b>Appointment</b>
 * 
 * This class describes the appointment object sent by the order filler and received by the order placer in the context of the RAD-48 transaction
 * 
 * This class defines the following attributes
 * <ul>
 * <li><b>id</b> unique identifier in the database</li>
 * <li><b>order</b> associated Order object</li>
 * <li><b>simulatedActor</b> indicates which actors holds the appointment (OF/OP)</li>
 * <li><b>creator</b> user/SUT who/which has created the appointment</li>
 * <li><b>lastChanged</b> the date of the last modification of the object</li>
 * <li><b>fillerAppointmentId</b> value set in SCH-2 field</li>
 * <li><b>fillerContactPerson</b> value set in SCH-16 field</li>
 * <li><b>resources</b> the list of resources required by this appointment</li>
 * <ul>
 * 
 *
 *
 * @author Anne-Gaëlle Bergé - INRIA Rennes/Bretagne Atlantique
 *
 * @version 1.0 - 2011, November 17th
 * 
 */

@Entity
@Name("appointment")
@Table(name = "om_appointment", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = "id"))
@SequenceGenerator(name = "om_appointment_sequence", sequenceName = "om_appointment_id_seq", allocationSize = 1)
public class Appointment extends CommonColumns implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@NotNull
	@GeneratedValue(generator = "om_appointment_sequence", strategy = GenerationType.SEQUENCE)
	@Column(name = "id", nullable = false, unique = true)
	private Integer id;

	@ManyToOne
	@JoinColumn(name = "order_id")
	private Order order;

	/**
	 * SCH-2
	 */
	@Column(name = "filler_appointment_id")
	private String fillerAppointmentId;

	/**
	 * SCH-16
	 */
	@Column(name = "filler_contact_person")
	private String fillerContactPerson;

	@OneToMany(mappedBy = "appointment")
	private List<AppointmentResource> resources;

	public Appointment() {

	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Order getOrder() {
		return order;
	}

	public void setOrder(Order order) {
		this.order = order;
	}

	public String getFillerAppointmentId() {
		return fillerAppointmentId;
	}

	public void setFillerAppointmentId(String fillerAppointmentId) {
		this.fillerAppointmentId = fillerAppointmentId;
	}

	public String getFillerContactPerson() {
		return fillerContactPerson;
	}

	public void setFillerContactPerson(String fillerContactPerson) {
		this.fillerContactPerson = fillerContactPerson;
	}

	public List<AppointmentResource> getResources() {
		return resources;
	}

	public void setResources(List<AppointmentResource> resources) {
		this.resources = resources;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = (prime * result) + ((fillerAppointmentId == null) ? 0 : fillerAppointmentId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Appointment other = (Appointment) obj;
		if (fillerAppointmentId == null) {
			if (other.fillerAppointmentId != null) {
				return false;
			}
		} else if (!fillerAppointmentId.equals(other.fillerAppointmentId)) {
			return false;
		}
		return true;
	}
}
