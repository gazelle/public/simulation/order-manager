package net.ihe.gazelle.ordermanager.hl7.responders;

import ca.uhn.hl7v2.AcknowledgmentCode;
import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.model.Message;
import ca.uhn.hl7v2.model.Segment;
import ca.uhn.hl7v2.model.v25.group.OUL_R22_CONTAINER;
import ca.uhn.hl7v2.model.v25.group.OUL_R22_ORDER;
import ca.uhn.hl7v2.model.v25.group.OUL_R22_RESULT;
import ca.uhn.hl7v2.model.v25.group.OUL_R22_SPECIMEN;
import ca.uhn.hl7v2.model.v25.message.OUL_R22;
import ca.uhn.hl7v2.model.v251.message.ACK;
import ca.uhn.hl7v2.parser.PipeParser;
import ca.uhn.hl7v2.protocol.ReceivingApplicationException;
import net.ihe.gazelle.HL7Common.messages.HL7MessageDecoder;
import net.ihe.gazelle.HL7Common.responder.IHEDefaultHandler;
import net.ihe.gazelle.HL7Common.utils.TransactionInstanceUtil;
import net.ihe.gazelle.laboratory.law.hl7v2.model.v251.message.QBP_Q11;
import net.ihe.gazelle.ordermanager.dico.HL7Constant;
import net.ihe.gazelle.ordermanager.dico.SupportedTriggerEvents;
import net.ihe.gazelle.ordermanager.model.AbstractOrder;
import net.ihe.gazelle.ordermanager.model.Container;
import net.ihe.gazelle.ordermanager.model.Encounter;
import net.ihe.gazelle.ordermanager.model.LabOrder;
import net.ihe.gazelle.ordermanager.model.Observation;
import net.ihe.gazelle.ordermanager.model.Patient;
import net.ihe.gazelle.ordermanager.model.PatientIdentifier;
import net.ihe.gazelle.ordermanager.model.Specimen;
import net.ihe.gazelle.ordermanager.model.WOSQueryContent;
import net.ihe.gazelle.ordermanager.utils.LAWQueryMessagesGenerator;
import net.ihe.gazelle.ordermanager.utils.MessageDecoder;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.common.tf.model.Domain;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;
import net.ihe.gazelle.simulator.message.model.TransactionInstance;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.FlushModeType;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * Class description : <b>AutomationManager</b>
 * This class is the handler for messages received by the Automation Manager in the context of the LAB-4 transaction
 *
 * @author Nicolas Lefebvre - INRIA Rennes/Bretagne Atlantique
 * @version 1.0 - 2012, February 20th
 */
public class AnalyzerManager extends OrderReceiver {

    private static final int NO_REPS = 0;
    private static final int ALLOWED_CONTAINER_COUNT = 1;
    private static Logger log = LoggerFactory.getLogger(AnalyzerManager.class);

    @Override
    public IHEDefaultHandler newInstance() {
        return new AnalyzerManager();
    }

    @Override
    public Message processMessage(Message incomingMessage) throws ReceivingApplicationException, HL7Exception {
        // start hibernate transaction
        orderControlCode = null;

        Message response;

        response = Receiver.generateNack(incomingMessage, serverApplication, serverFacility,
                Arrays.asList("QBP", "OUL"), Arrays.asList("Q11", SupportedTriggerEvents.R22), hl7Charset);

        simulatedTransaction = Transaction.GetTransactionByKeyword(terser.get("/.MSH-21-1"), entityManager);
        PipeParser pipeParser = PipeParser.getInstanceWithNoValidation();
        if (response == null) {
            String messageString = pipeParser.encode(incomingMessage);
            if (incomingMessage instanceof ca.uhn.hl7v2.model.v251.message.QBP_Q11) {
                simulatedTransaction = Transaction.GetTransactionByKeyword("LAB-27", entityManager);

                QBP_Q11 messageReceived = (QBP_Q11) pipeParser.parseForSpecificPackage(messageString,
                        "net.ihe.gazelle.laboratory.law.hl7v2.model.v251.message");

                if (!messageReceived.getMSH().getMessageType().getMessageStructure().encode().equals("QBP_Q11")) {
                    response = LAWQueryMessagesGenerator.generateRSP_K11(messageReceived, serverApplication,
                            serverFacility, hl7Charset, true, "200", "E", "The Message Type is not supported. ");
                } else {
                    WOSQueryContent queryContent = new WOSQueryContent(messageReceived.getQPD(), sendingFacility + " "
                            + sendingApplication);
                    queryContent.save(entityManager);
                    response = LAWQueryMessagesGenerator.generateRSP_K11(messageReceived, serverApplication,
                            serverFacility, hl7Charset, false, null, null, null);
                }
            } else if (incomingMessage instanceof ca.uhn.hl7v2.model.v251.message.OUL_R22) {
                simulatedTransaction = Transaction.GetTransactionByKeyword("LAB-29", entityManager);
                String fakeMessage = incomingMessage.encode();
                fakeMessage = fakeMessage.replaceFirst("\\|2.5.1", "|2.5");
                Message messageToParse = pipeParser.parse(fakeMessage);
                response = processOULR22Message((OUL_R22) messageToParse);

                if (response instanceof ca.uhn.hl7v2.model.v251.message.ACK) {
                    ACK responseACK = (ACK) response;
                    responseACK.getMSH().getMessageProfileIdentifier(0).parse("LAB-29^IHE");
                    response = responseACK;
                }
            } else {
                response = Receiver.buildNack(serverApplication, serverFacility, receivingApplication,
                        receivingFacility, terser.get("/.MSH-9-2"), AcknowledgmentCode.AE,
                        "The message type is not supported by the Analyzer Manager Simulator.", null, "200", "W",
                        terser.get("/.MSH-10"), terser.get("/.MSH-18"), hl7Version);

            }
        }
        if (simulatedTransaction == null) {
            simulatedTransaction = Transaction.GetTransactionByKeyword("LAB-27", entityManager);
        }

        setSutActor(Actor.findActorWithKeyword("ANALYZER", entityManager));
        setResponseMessageType(HL7MessageDecoder.getMessageType(response));
        return response;
    }

    private Message checkRequiredElementArePresent(OUL_R22 oulMessage) {
        boolean erroneous = false;
        String errorMessage = null;
        int nb0fSpecimens = oulMessage.getSPECIMENReps();
        try {
            if ((oulMessage.getPATIENT().getPID().isEmpty() && !oulMessage.getVISIT().getPV1().isEmpty())
                    || (!oulMessage.getPATIENT().getPID().isEmpty() && oulMessage.getVISIT().getPV1().isEmpty())) {
                erroneous = true;
                errorMessage = "Either patient demographics are not supported either both PID and PV1 segment shall be present";
            } else if (nb0fSpecimens == NO_REPS) {
                erroneous = true;
                errorMessage = "SPECIMEN group is required";
            } else {
                for (ca.uhn.hl7v2.model.v25.group.OUL_R22_SPECIMEN group : oulMessage.getSPECIMENAll()) {
                    errorMessage = checkRequiredElementsArePresent(group);
                    if (errorMessage != null) {
                        erroneous = true;
                        break;
                    }
                }
            }
        } catch (HL7Exception e) {
            erroneous = true;
            errorMessage = "Cannot parse message";
        }
        if (erroneous) {
            return Receiver.buildNack(sendingApplication, sendingFacility, serverApplication,
                    serverFacility, SupportedTriggerEvents.R22, AcknowledgmentCode.AE, errorMessage, null,
                    Receiver.SEGMENT_SEQUENCE_ERROR, Receiver.SEVERITY_ERROR, messageControlId,
                    hl7Charset, hl7Version);
        } else {
            return null;
        }

    }

    private String checkRequiredElementsArePresent(OUL_R22_SPECIMEN group) throws HL7Exception {
        String errorMessage = null;
        int sacGroupCount = group.getCONTAINERReps();
        if (group.getSPM().isEmpty()) {
            return "SPM segment is required";
        } else if (sacGroupCount != ALLOWED_CONTAINER_COUNT) {
            errorMessage = "Exactly one CONTAINER group is required";
        } else {
            for (OUL_R22_CONTAINER sacGroup : group.getCONTAINERAll()) {
                if (sacGroup.getSAC().isEmpty()) {
                    errorMessage = "SAC segment is required";
                    break;
                }
            }
            if (group.getORDERReps() == NO_REPS) {
                errorMessage = "ORDER group is required";
            } else {
                for (OUL_R22_ORDER orderGroup : group.getORDERAll()) {
                    errorMessage = checkRequiredElementsArePresent(orderGroup);
                    if (errorMessage != null) {
                        break;
                    } else {
                        continue;
                    }
                }
            }
        }
        return errorMessage;
    }

    private String checkRequiredElementsArePresent(OUL_R22_ORDER orderGroup) throws HL7Exception {
        if (orderGroup.getOBR().isEmpty()) {
            return "OBR segment is required";
        } else if (orderGroup.getORC().isEmpty()) {
            return "ORC segment is required";
        } else if (orderGroup.getORC().getOrderControl().isEmpty()) {
            return "ORC-1 field is required";
        } else {
            String occ = orderGroup.getORC().getOrderControl().getValue();
            if (occ.equals(HL7Constant.IP) || occ.equals(HL7Constant.CM)) {
                if (orderGroup.getRESULTReps() == NO_REPS) {
                    return "RESULT group is required";
                } else {
                    for (OUL_R22_RESULT result : orderGroup.getRESULTAll()) {
                        if (result.getOBX().isEmpty()) {
                            return "At least one OBX segment shall be present in RESULT";
                        }
                    }
                }
            }
            return null;
        }
    }

    @Override
    protected Message processOULR22Message(OUL_R22 oulMessage) {
        entityManager.setFlushMode(FlushModeType.COMMIT);
        Message nack = checkRequiredElementArePresent(oulMessage);
        if (nack != null) {
            return nack;
        }
        Patient selectedPatient = null;
        String creator = sendingApplication + "_" + sendingFacility;
        Domain domain = Domain.getDomainByKeyword("LAB", entityManager);
        Encounter selectedEncounter = null;
        // extract patient
        if (!MessageDecoder.isEmpty(oulMessage.getPATIENT().getPID())) {
            Patient pidPatient = MessageDecoder.extractPatientFromPID(oulMessage.getPATIENT().getPID());
            List<PatientIdentifier> pidPatientIdentifiers;
            pidPatientIdentifiers = MessageDecoder.extractPatientIdentifiersFromMessage25(oulMessage);
            List<PatientIdentifier> identifiersToAdd = new ArrayList<PatientIdentifier>();
            for (PatientIdentifier pid : pidPatientIdentifiers) {
                PatientIdentifier existingPID = pid.getStoredPatientIdentifier(entityManager);
                if (existingPID != null) {
                    if (selectedPatient == null) {
                        selectedPatient = existingPID.getPatient();
                    } else if (!selectedPatient.getId().equals(existingPID.getPatient().getId())) {
                        // error patients with same identifiers mismatch
                        // create a good ACK
                        return Receiver.buildNack(sendingApplication, sendingFacility, serverApplication,
                                serverFacility, SupportedTriggerEvents.R22, AcknowledgmentCode.AE, ERROR_INCONSISTENT_PID, null,
                                Receiver.DUPLICATE_KEY_IDENTIFIER, Receiver.SEVERITY_ERROR, messageControlId,
                                hl7Charset, hl7Version);
                    }
                } else {
                    pid = entityManager.merge(pid);
                    identifiersToAdd.add(pid);
                }
            }
            // if none of the identifiers is already known by the simulator, we save the patient contained in the message
            if (selectedPatient == null) {
                // save the patient
                pidPatient.setCreator(creator);
                selectedPatient = entityManager.merge(pidPatient);
            }
            for (PatientIdentifier pid : identifiersToAdd) {
                pid.setPatient(selectedPatient);
                entityManager.merge(pid);
            }
            // then, extract encounter
            Encounter pv1Encounter = null;
            pv1Encounter = MessageDecoder.extractEncounterFromPV1(oulMessage);
            if (pv1Encounter.getVisitNumber() != null) {
                // check the encounter exists
                selectedEncounter = Encounter.getEncounterByVisitNumber(pv1Encounter.getVisitNumber(),
                        entityManager);
                if ((selectedEncounter != null)
                        && (!selectedEncounter.getPatient().getId().equals(selectedPatient.getId()))) {
                    return Receiver.buildNack(sendingApplication, sendingFacility, serverApplication,
                            serverFacility, SupportedTriggerEvents.R22, AcknowledgmentCode.AE, "This visit number is already used for another patient",
                            null, Receiver.DUPLICATE_KEY_IDENTIFIER, Receiver.SEVERITY_ERROR, messageControlId,
                            hl7Charset, hl7Version);
                }
                // if not rattach the encounter to the selected patient
                else if (selectedEncounter == null) {
                    selectedEncounter = pv1Encounter;
                    selectedEncounter.setPatient(selectedPatient);
                    selectedEncounter.setCreator(creator);
                    selectedEncounter = entityManager.merge(selectedEncounter);
                }
                // else selectedEncounter is the one retrieved from the database
            } else {
                return Receiver.buildNack(sendingApplication, sendingFacility, serverApplication, serverFacility,
                        SupportedTriggerEvents.R22, AcknowledgmentCode.AE, "PV1-19 is required but empty", null, Receiver.REQUIRED_FIELD_MISSING,
                        Receiver.SEVERITY_ERROR, messageControlId, hl7Charset, hl7Version);
            }
        }
        // extract specimens
        int nbOfSpecimens = oulMessage.getSPECIMENReps();
        for (int specimenIndex = 0; specimenIndex < nbOfSpecimens; specimenIndex++) {
            // SPECIMEN begin
            Segment spm = oulMessage.getSPECIMEN(specimenIndex).getSPM();
            Specimen selectedSpecimen;
            Specimen messageSpecimen = MessageDecoder.extractSpecimenFromSegment(spm, creator, simulatedActor,
                    domain);
            messageSpecimen.setWorkOrder(false);
            messageSpecimen.setMainEntity(true);
            Container messageContainer = MessageDecoder.extractContainerFromSAC(
                    oulMessage.getSPECIMEN(specimenIndex).getCONTAINER().getSAC(), simulatedActor,
                    messageSpecimen, creator, domain);
            messageContainer.setMainEntity(false);
            messageContainer.setWorkOrder(false);
            Container selectedContainer = Container.getContainerByIdentifierByActorByDomain(
                    messageContainer.getContainerIdentifier(), simulatedActor, domain, entityManager, false);
            if (selectedContainer != null) {
                selectedSpecimen = selectedContainer.getSpecimen();
            } else {
                selectedSpecimen = entityManager.merge(messageSpecimen);
                selectedContainer = messageContainer;
                selectedContainer.setSpecimen(selectedSpecimen);
                entityManager.merge(selectedContainer);
            }

            // Specimen observations
            int nbOfObservations = oulMessage.getSPECIMEN(specimenIndex).getOBXReps();
            if (nbOfObservations > 0) {
                for (int obxIndex = 0; obxIndex < nbOfObservations; obxIndex++) {
                    // OBSERVATION begin
                    Segment obx = oulMessage.getSPECIMEN(specimenIndex).getOBX(obxIndex);
                    boolean obxIsEmpty = true;
                    try {
                        obxIsEmpty = obx.isEmpty();
                    } catch (HL7Exception e) {
                        log.info(e.getMessage(), e);
                    }
                    if (!obxIsEmpty) {
                        Observation messageObservation = MessageDecoder.extractObservationFromOBX(obx,
                                simulatedActor, domain, creator);
                        if (selectedSpecimen.getId() != null) {
                            List<Observation> observations = Observation.getObservationFiltered(entityManager,
                                    simulatedActor, domain, null, selectedSpecimen, null);
                            if (observations != null) {
                                selectedSpecimen.getObservations().remove(observations.get(0));
                                messageObservation.setId(observations.get(0).getId());
                            }
                        }
                        messageObservation.setSpecimen(selectedSpecimen);
                        if (selectedSpecimen.getObservations() == null) {
                            selectedSpecimen.setObservations(new ArrayList<Observation>());
                        }
                        selectedSpecimen.getObservations().add(messageObservation);
                        // OBSERVATION end
                    }
                }
                selectedSpecimen.save(entityManager);
            }

            // related orders
            int nbOfOrders = oulMessage.getSPECIMEN(specimenIndex).getORDERReps();
            for (int orderIndex = 0; orderIndex < nbOfOrders; orderIndex++) {
                // ORDER begin
                Segment obr = oulMessage.getSPECIMEN(specimenIndex).getORDER(orderIndex).getOBR();
                Segment orc = oulMessage.getSPECIMEN(specimenIndex).getORDER(orderIndex).getORC();
                Segment tq1 = oulMessage.getSPECIMEN(specimenIndex).getORDER(orderIndex).getTIMING_QTY()
                        .getTQ1();
                LabOrder messageOrder = MessageDecoder.extractLabOrderFromSegments(orc, obr, tq1,
                        simulatedActor, domain, creator, selectedEncounter);
                messageOrder.setMainEntity(false);
                messageOrder.setWorkOrder(false);
                LabOrder selectedOrder = AbstractOrder.getOrderByNumbersByActor(LabOrder.class,
                        messageOrder.getPlacerOrderNumber(), messageOrder.getFillerOrderNumber(), null,
                        simulatedActor, entityManager, false);
                if (selectedOrder == null) {
                    selectedOrder = messageOrder;
                } else {
                    // update order status and result status
                    selectedOrder.setOrderStatus(messageOrder.getOrderStatus());
                    selectedOrder.setOrderResultStatus(messageOrder.getOrderResultStatus());
                }
                // observations
                nbOfObservations = oulMessage.getSPECIMEN(specimenIndex).getORDER(orderIndex)
                        .getRESULTReps();
                if (nbOfObservations > 0) {
                    for (int obxIndex = 0; obxIndex < nbOfObservations; obxIndex++) {
                        // OBSERVATION begin
                        Segment obx = oulMessage.getSPECIMEN(specimenIndex).getORDER(orderIndex)
                                .getRESULT(obxIndex).getOBX();
                        Observation messageObservation = MessageDecoder.extractObservationFromOBX(obx,
                                simulatedActor, domain, creator);
                        if (selectedOrder.getId() != null) {
                            // this assumes that observations are always transmitted with the same setId
                            List<Observation> observations = Observation.getObservationFiltered(
                                    entityManager, simulatedActor, domain, selectedOrder, null,
                                    messageObservation.getSetId());
                            if (observations != null) {
                                messageObservation.setId(observations.get(0).getId());
                                selectedOrder.getObservations().remove(observations.get(0));
                            }
                        }
                        messageObservation.setOrder(selectedOrder);
                        if (selectedOrder.getObservations() == null) {
                            selectedOrder.setObservations(new ArrayList<Observation>());
                        }
                        selectedOrder.getObservations().add(messageObservation);
                        // OBSERVATION end
                    }
                }
                entityManager.flush();
                if ((selectedOrder.getId() != null) && (selectedSpecimen.getId() != null)
                        && (selectedSpecimen.getOrders() != null)) {
                    for (int i = 0; i < selectedSpecimen.getOrders().size(); i++) {
                        if (selectedSpecimen.getOrders().get(i).getId().equals(selectedOrder.getId())) {
                            LabOrder toAdd = selectedOrder.save(entityManager);
                            selectedSpecimen.getOrders().set(i, toAdd);
                            break;
                        } else {
                            continue;
                        }
                    }
                } else if (selectedSpecimen.getOrders() != null) {
                    selectedSpecimen.getOrders().add(selectedOrder);
                } else {
                    selectedSpecimen.setOrders(new ArrayList<LabOrder>());
                    selectedSpecimen.getOrders().add(selectedOrder);
                }
                // ORDER end
            }
            // SPECIMEN end
            selectedSpecimen.save(entityManager);
        }
        return Receiver.buildAck(sendingApplication, sendingFacility, serverApplication, serverFacility, SupportedTriggerEvents.R22, AcknowledgmentCode.AA,
                messageControlId, hl7Charset, hl7Version);
    }
}
