package net.ihe.gazelle.ordermanager.filter;

import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import net.ihe.gazelle.hql.restrictions.HQLRestrictions;
import net.ihe.gazelle.ordermanager.model.Order;
import net.ihe.gazelle.ordermanager.model.OrderQuery;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.common.tf.model.Domain;

import java.util.Arrays;
import java.util.Map;

/**
 * <b>Class Description : </b>OrderFilter<br>
 * <br>
 *
 * @author Anne-Gaelle Berge / IHE-Europe development Project
 * @version 1.0 - 30/06/16
 */
public class OrderFilter extends AbstractOrderFilter<Order> {

    /**
     * <p>Constructor for OrderFilter.</p>
     *
     * @param domain a {@link net.ihe.gazelle.simulator.common.tf.model.Domain} object.
     * @param simulatedActor a {@link net.ihe.gazelle.simulator.common.tf.model.Actor} object.
     * @param newOrderControlCode a {@link java.lang.String} object.
     * @param isOrderAlreadyScheduled a {@link java.lang.Boolean} object.
     */
    public OrderFilter(Domain domain, Actor simulatedActor, String newOrderControlCode, Boolean isOrderAlreadyScheduled) {
        super(domain, simulatedActor, newOrderControlCode, isOrderAlreadyScheduled);
    }

    /** {@inheritDoc} */
    @Override
    protected HQLCriterionsForFilter<Order> getHQLCriterionsForFilter() {
        OrderQuery query = new OrderQuery();
        HQLCriterionsForFilter<Order> criteria = query.getHQLCriterionsForFilter();
        criteria.addPath("firstname", query.encounter().patient().firstName());
        criteria.addPath("lastname", query.encounter().patient().lastName());
        criteria.addPath("actor", query.simulatedActor(), getActor());
        criteria.addPath("domain", query.domain(), getDomain());
        criteria.addPath("creator", query.creator());
        criteria.addPath("creationDate", query.lastChanged());
        criteria.addQueryModifier(this);
        return criteria;
    }

    /** {@inheritDoc} */
    @Override
    protected Integer getObjectId(Order order) {
        return order.getId();
    }

    /** {@inheritDoc} */
    @Override
    public void modifyQuery(HQLQueryBuilder<Order> hqlQueryBuilder, Map<String, Object> map) {
        OrderQuery query = new OrderQuery();
        if (getPlacerOrderNumber() != null && !getPlacerOrderNumber().isEmpty()) {
            hqlQueryBuilder.addRestriction(query.placerOrderNumber().likeRestriction(getPlacerOrderNumber()));
        }
        if ((getFillerOrderNumber() != null) && !getFillerOrderNumber().isEmpty()) {
            hqlQueryBuilder.addRestriction(query.fillerOrderNumber().likeRestriction(getFillerOrderNumber()));
        }
        if (getPatientid() != null && !getPatientid().isEmpty()){
            hqlQueryBuilder.addRestriction(query.encounter().patient().patientIdentifiers().identifier().likeRestriction(getPatientid()));
        }
        if (getVisitnumber() != null && !getVisitnumber().isEmpty()){
            hqlQueryBuilder.addRestriction(query.encounter().visitNumber().likeRestriction(getVisitnumber()));
        }
        if (getScheduled() != null) {
            if (getScheduled()) {
                hqlQueryBuilder.addRestriction(query.requestedProcedures().isNotEmptyRestriction());
            } else {
                hqlQueryBuilder.addRestriction(query.requestedProcedures().isEmptyRestriction());
            }
        }
        if ((getNewOrderControlCode() != null) && !getNewOrderControlCode().isEmpty()) {
            // order placer discontinues order
            if (getNewOrderControlCode().equals("DC")) {
                hqlQueryBuilder.addRestriction(query.orderStatus().eqRestriction("IP"));
                hqlQueryBuilder.addRestriction(query.orderControlCode().neqRestriction("DC"));
            }
            // order placer cancels order
            else if (getNewOrderControlCode().equals("CA")) {
                hqlQueryBuilder.addRestriction(query.orderStatus().isNullRestriction());
                hqlQueryBuilder.addRestriction(HQLRestrictions.and(query.orderControlCode().neqRestriction("CA")
                        , query.orderControlCode().neqRestriction("DC")));
            }
            // order filler cancels order
            else if (getNewOrderControlCode().equals("OC")) {
                String[] notAllowedOCC = {"CA", "DC", "OC"};
                hqlQueryBuilder.addRestriction(query.orderStatus().isNullRestriction());
                hqlQueryBuilder.addRestriction(query.orderControlCode().ninRestriction(Arrays.asList(notAllowedOCC)));
            }
            // order filler updates order status
            else if (getNewOrderControlCode().equals("SC")) {
                String[] notAllowedOCC = {"CA", "DC", "OC"};
                hqlQueryBuilder.addRestriction(query.orderControlCode().ninRestriction(Arrays.asList(notAllowedOCC)));
            }
        }
    }
}
