/*
 * Copyright 2011 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.ordermanager.action;

import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.ordermanager.model.Patient;
import net.ihe.gazelle.ordermanager.model.ScheduledProcedureStep;
import net.ihe.gazelle.ordermanager.model.WorklistEntry;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Map;

/**
 * Class description : <b>WorklistManager</b>
 * 
 * This class is the backing bean for the page /browsing/worklist.xhtml
 * 
 *
 *
 * @author Anne-Gaëlle Bergé - INRIA Rennes/Bretagne Atlantique
 *
 * @version 1.0 - 2011, November 22nd
 * 
 */

@Name("worklistEntryManager")
@Scope(ScopeType.PAGE)
public class WorklistEntryManager implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private static final String XML = ".xml";
	private static final String DICOM = ".wl";
	private static final String DUMP = ".dump";

	
	private static Logger log = LoggerFactory.getLogger(WorklistEntryManager.class);
	
	private WorklistEntry worklist;
	private ScheduledProcedureStep selectedProcedureStep;
	private Patient patient;

	/**
	 * 
	 */
    @Create
	public void selectWorklistById() {
		Map<String, String> urlParams = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		if ((urlParams != null) && urlParams.containsKey("id")) {
			try {
				Integer worklistId = Integer.valueOf(urlParams.get("id"));
				EntityManager entityManager = EntityManagerService.provideEntityManager();
				worklist = entityManager.find(WorklistEntry.class, worklistId);
				if (worklist != null) {
					selectedProcedureStep = worklist.getScheduledProcedureStep();
					patient = selectedProcedureStep.getRequestedProcedure().getOrder().getEncounter().getPatient();
				} else {
					patient = null;
					selectedProcedureStep = null;
				}
			} catch (NumberFormatException e) {
				log.error("invalid id for worklist");
				FacesMessages.instance().add(urlParams.get("id") + " is not a valid parameter");
				patient = null;
				selectedProcedureStep = null;
			}
		}
	}

	/**
	 * 
	 * @param formatType
	 * @param worklist
	 */
	public void downloadWorklist(String formatType, WorklistEntry worklist) {
		String filePath = worklist.getFilepath().concat(formatType);
		InputStream is = null;
		try {
			// read file
			File file = new File(filePath);
			int fileLength = (int) file.length();
			if (fileLength > 0) {
				byte[] content = new byte[fileLength];
				is = new FileInputStream(file);
				if (is.read(content) > 0){
                    // prepare HTTP response
                    FacesContext context = FacesContext.getCurrentInstance();
                    HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
                    if (formatType.equals(XML)) {
                        response.setContentType("application/xml");
                    } else if (formatType.equals(DICOM)) {
                        response.setContentType("application/octet-stream");
                    } else {
                        // dump case
                        response.setContentType("text/plain");
                    }
                    response.setHeader("Content-Disposition", "attachment;filename=worklist" + worklist.getId()
                            + formatType);
                    response.setContentLength(fileLength);
                    ServletOutputStream servletOutputStream;
                    servletOutputStream = response.getOutputStream();
                    servletOutputStream.write(content);
                    servletOutputStream.flush();
                    servletOutputStream.close();
                    context.responseComplete();
                } else {
                    FacesMessages.instance().add(StatusMessage.Severity.ERROR, "The file is empty");
                }
			} else {
				FacesMessages.instance().add(StatusMessage.Severity.ERROR, "The file is empty");
			}
		} catch (FileNotFoundException e) {
			log.error("Cannot find file " + filePath);
			FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Cannot find file " + filePath);
		} catch (IOException e) {
			log.error("Unable to build response for sending file " + filePath);
			FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Unable to build response for sending file " + filePath);
		} finally {
			if (is != null) {
				try {
					is.close();
				} catch (IOException e) {
					log.error(e.getMessage(), e);
				}
			}
		}
	}

	public void deleteWorklist(WorklistEntry worklist) {
		if (worklist.delete()) {
			FacesMessages.instance().add(StatusMessage.Severity.INFO, "The worklist has been successfully deleted");
		} else {
			FacesMessages.instance().add(StatusMessage.Severity.ERROR, "An error has prevented the server from deleting the worklist");
		}
	}

	public void restoreWorklist(WorklistEntry worklist) {
		if (worklist.restore()) {
			FacesMessages.instance().add(StatusMessage.Severity.INFO, "The worklist has been successfully restored");
		} else {
			FacesMessages.instance().add(StatusMessage.Severity.ERROR, "An error has prevented the server from restoring the worklist");
		}
	}

	public void copyWorklist(WorklistEntry worklist) {
		// TODO
	}

	public Patient getPatient() {
		return patient;
	}

	public void setPatient(Patient patient) {
		this.patient = patient;
	}

	public String getXML() {
		return XML;
	}

	public String getDICOM() {
		return DICOM;
	}

	public String getDUMP() {
		return DUMP;
	}

	public WorklistEntry getWorklist() {
		return worklist;
	}

	public void setWorklist(WorklistEntry worklist) {
		this.worklist = worklist;
	}

	public ScheduledProcedureStep getSelectedProcedureStep() {
		return selectedProcedureStep;
	}

	public void setSelectedProcedureStep(ScheduledProcedureStep procedureStep) {
		this.selectedProcedureStep = procedureStep;
	}

	public String displayWorklistAsXml(){
		String filePath = worklist.getFilepath().concat(XML);
		// read file
		File file = new File(filePath);
		int fileLength = (int) file.length();
		if (fileLength > 0) {
			try {
				byte[] content = Files.readAllBytes(Paths.get(filePath));
				return new String (content, StandardCharsets.UTF_8);
			} catch (IOException e) {
				FacesMessages.instance().add(StatusMessage.Severity.WARN, "Cannot display the XML dump of the worklist");
			}
		}
		return null;
	}

}
