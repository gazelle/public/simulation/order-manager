package net.ihe.gazelle.ordermanager.hl7.responders;

import ca.uhn.hl7v2.AcknowledgmentCode;
import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.model.Message;
import ca.uhn.hl7v2.model.Segment;
import ca.uhn.hl7v2.model.v251.message.ADT_A01;
import ca.uhn.hl7v2.model.v251.message.ADT_A39;
import ca.uhn.hl7v2.model.v251.message.OMI_O23;
import ca.uhn.hl7v2.model.v251.segment.IPC;
import ca.uhn.hl7v2.parser.PipeParser;
import ca.uhn.hl7v2.protocol.ReceivingApplicationException;
import ca.uhn.hl7v2.util.Terser;
import net.ihe.gazelle.HL7Common.messages.HL7MessageDecoder;
import net.ihe.gazelle.HL7Common.responder.IHEDefaultHandler;
import net.ihe.gazelle.ordermanager.model.Encounter;
import net.ihe.gazelle.ordermanager.model.Order;
import net.ihe.gazelle.ordermanager.model.Patient;
import net.ihe.gazelle.ordermanager.model.PatientIdentifier;
import net.ihe.gazelle.ordermanager.model.ProtocolItem;
import net.ihe.gazelle.ordermanager.model.RequestedProcedure;
import net.ihe.gazelle.ordermanager.model.ScheduledProcedureStep;
import net.ihe.gazelle.ordermanager.utils.MessageDecoder;
import net.ihe.gazelle.ormwithzds.hl7v2.model.v231.message.ORM_O01;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * <p>ImageReportManager class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
public class ImageReportManager extends IHEDefaultHandler {

	private static Logger log = LoggerFactory.getLogger(ImageReportManager.class);

	private String orderControlCode;
	private String creator;
	private RequestedProcedure requestedProcedure;
	private Message nack;
	private static final String A08 = "A08";
	private static final String A40 = "A40";
	private static final String O01 = "O01";
	private static final String O23 = "O23";

	/** {@inheritDoc} */
	@Override
	public IHEDefaultHandler newInstance() {
		return new ImageReportManager();
	}

	/** {@inheritDoc} */
	@Override
	public Message processMessage(Message incomingMessage) throws ReceivingApplicationException, HL7Exception {
		Message response ;
		orderControlCode = null;
		try {
			Actor sutActor = Actor.findActorWithKeyword("OF", entityManager);
			// check if we must accept the message
			response = Receiver.generateNack(incomingMessage, serverApplication, serverFacility,
					Arrays.asList("ORM", "ADT", "OMI"), Arrays.asList(O01, A08, A40, O23), hl7Charset);

			// if no NACK is generated, process the message
			if (response == null) {
				creator = sendingApplication + "_" + sendingFacility;
				if (messageType == null) {
					log.error("Message type is empty");
				} else if (messageType.contains("ORM")) {
					response = processORMMessage(incomingMessage);
					if ((orderControlCode != null) && orderControlCode.equals("NW")) {
						simulatedTransaction = Transaction.GetTransactionByKeyword("RAD-4", entityManager);
					} else {
						simulatedTransaction = Transaction.GetTransactionByKeyword("RAD-13", entityManager);
					}
				} else if (messageType.contains("ADT")) {
					response = processMessageFromADT(incomingMessage);
					sutActor = Actor.findActorWithKeyword("ADT", entityManager);
				} else if (messageType.contains("OMI")) {
					response = processOMIMessage((OMI_O23) incomingMessage);
					if ((orderControlCode != null) && orderControlCode.equals("NW")) {
						simulatedTransaction = Transaction.GetTransactionByKeyword("RAD-4", entityManager);
					} else {
						simulatedTransaction = Transaction.GetTransactionByKeyword("RAD-13", entityManager);
					}
				}
			}
			setSimulatedActor(sutActor);
			setResponseMessageType(HL7MessageDecoder.getMessageType(response));
		} catch (HL7Exception e) {
			throw new HL7Exception(e);
		} catch (Exception e) {
			throw new ReceivingApplicationException(e);
		}
		return response;
	}

	private Message processOMIMessage(OMI_O23 omiMessage) {
		// Process PID and PV1 segments
		Patient selectedPatient = null;
		Encounter selectedEncounter = null;
		// stored or retrieved the patient contained in the message
		if (MessageDecoder.isSegmentPresent(omiMessage, "PID")) {
			Patient pidPatient = MessageDecoder.extractPatientFromPID(omiMessage.getPATIENT().getPID());
			List<PatientIdentifier> pidPatientIdentifiers = MessageDecoder.extractPatientIdentifiersFromPID251(omiMessage);
			List<PatientIdentifier> identifiersToAdd = new ArrayList<PatientIdentifier>();
			for (PatientIdentifier pid : pidPatientIdentifiers) {
				PatientIdentifier existingPID = pid.getStoredPatientIdentifier(entityManager);
				if (existingPID != null) {
					if (selectedPatient == null) {
						selectedPatient = existingPID.getPatient();
					} else if (selectedPatient.getId().equals(existingPID.getPatient().getId())) {
						// OK: nothing to do
						continue;
					} else {
						// error patients with same identifiers mismatch
						return Receiver.buildNack(serverApplication, serverFacility, sendingApplication,
								sendingFacility, O23, AcknowledgmentCode.AE, "Patient identifiers mismatch", null,
								Receiver.DUPLICATE_KEY_IDENTIFIER, Receiver.SEVERITY_ERROR, messageControlId,
								hl7Charset, hl7Version);
					}
				} else {
					pid = pid.save(entityManager);
					identifiersToAdd.add(pid);
				}
			}
			// if none of the identifiers is already known by the simulator, we save the patient contained in the message
			if (selectedPatient == null) {
				// save the patient
				selectedPatient = pidPatient.save(entityManager);
				selectedPatient.setCreator(sendingApplication + "_" + sendingFacility);
			}
			for (PatientIdentifier pid : identifiersToAdd) {
				pid.setPatient(selectedPatient);
				pid.save(entityManager);
			}
			// check existance of PV1 segment

			if (MessageDecoder.isSegmentPresent(omiMessage, "PV1")) {
				// extract encounter
				Encounter pv1Encounter = MessageDecoder.extractEncounterFromPV1(omiMessage);
				if (pv1Encounter.getVisitNumber() != null) {
					// check the encounter exists
					selectedEncounter = Encounter.getEncounterByVisitNumber(pv1Encounter.getVisitNumber(),
							entityManager);
					if ((selectedEncounter != null)
							&& !(selectedEncounter.getPatient().getId().equals(selectedPatient.getId()))) {
						return Receiver.buildNack(serverApplication, serverFacility, sendingApplication,
								sendingFacility, O23, AcknowledgmentCode.AE,
								"Visit number is already used for an encounter for another patient", null,
								Receiver.DUPLICATE_KEY_IDENTIFIER, Receiver.SEVERITY_ERROR, messageControlId,
								hl7Charset, hl7Version);
					} else if (selectedEncounter == null) {
						selectedEncounter = pv1Encounter;
						selectedEncounter.setPatient(selectedPatient);
						selectedEncounter.setCreator(sendingApplication + "_" + sendingFacility);
						selectedEncounter = selectedEncounter.save(entityManager);
					}
					// else selectedEncounter is the one retrieved from the database
				} else {
					return Receiver.buildNack(serverApplication, serverFacility, sendingApplication, sendingFacility,
							O23, AcknowledgmentCode.AE, "Visit number (PV1-19) is required but missing", null,
							Receiver.DUPLICATE_KEY_IDENTIFIER, Receiver.SEVERITY_ERROR, messageControlId, hl7Charset,
							hl7Version);
				}
			} else {
				return Receiver.buildNack(serverApplication, serverFacility, sendingApplication, sendingFacility, O23,
						AcknowledgmentCode.AE, "PV1 segment is required but missing", null, Receiver.SEGMENT_SEQUENCE_ERROR,
						Receiver.SEVERITY_ERROR, messageControlId, hl7Charset, hl7Version);
			}
		} else {
			return Receiver.buildNack(serverApplication, serverFacility, sendingApplication, sendingFacility, O23,
					AcknowledgmentCode.AE, "PID segment is required but missing", null, Receiver.SEGMENT_SEQUENCE_ERROR,
					Receiver.SEVERITY_ERROR, messageControlId, hl7Charset, hl7Version);
		}
		// process ORDER groups
		int nbOfGroups = omiMessage.getORDERReps();
		if (nbOfGroups > 0) {
			requestedProcedure = null;
			for (int orderIndex = 0; orderIndex < nbOfGroups; orderIndex++) {
				Segment orc = omiMessage.getORDER(orderIndex).getORC();
				Segment obr = omiMessage.getORDER(orderIndex).getOBR();
				Segment tq1 = omiMessage.getORDER(orderIndex).getTIMING(0).getTQ1();
				List<IPC> ipc = null;
				try {
					ipc = omiMessage.getORDER(orderIndex).getIPCAll();
				} catch (HL7Exception e) {
					return Receiver.buildNack(serverApplication, serverFacility, sendingApplication, sendingFacility,
							O23, AcknowledgmentCode.AE, "IPC segment is required but missing in ORDER[" + orderIndex + "]", null,
							Receiver.SEGMENT_SEQUENCE_ERROR, Receiver.SEVERITY_ERROR, messageControlId, hl7Charset,
							hl7Version);
				}
				if (MessageDecoder.isEmpty(orc)) {
					return Receiver.buildNack(serverApplication, serverFacility, sendingApplication, sendingFacility,
							O23, AcknowledgmentCode.AE, "ORC segment is required but missing in ORDER[" + orderIndex + "]", null,
							Receiver.SEGMENT_SEQUENCE_ERROR, Receiver.SEVERITY_ERROR, messageControlId, hl7Charset,
							hl7Version);
				} else if (MessageDecoder.isEmpty(obr)) {
					return Receiver.buildNack(serverApplication, serverFacility, sendingApplication, sendingFacility,
							O23, AcknowledgmentCode.AE, "OBR segment is required but missing in ORDER[" + orderIndex + "]", null,
							Receiver.SEGMENT_SEQUENCE_ERROR, Receiver.SEVERITY_ERROR, messageControlId, hl7Charset,
							hl7Version);
				} else {
					nack = null;
					boolean mustContinue = processOrderGroup(orc, obr, tq1, ipc, selectedEncounter);
					if (nack != null) {
						return nack;
					} else if (mustContinue) {
						continue;
					} else {
						break;
					}
				}
			}
			if (requestedProcedure != null) {
				requestedProcedure.save(entityManager);
			} else {
				log.error("RequestedProcedure is null!!!!!");
			}
		} else {
			return Receiver.buildNack(serverApplication, serverFacility, sendingApplication, sendingFacility, O23,
					AcknowledgmentCode.AE, "At least one occurrence of ORDER group is required", null, Receiver.SEGMENT_SEQUENCE_ERROR,
					Receiver.SEVERITY_ERROR, messageControlId, hl7Charset, hl7Version);
		}

		return Receiver.buildAck(serverApplication, serverFacility, sendingApplication, sendingFacility, O23, AcknowledgmentCode.AA,
				messageControlId, hl7Charset, hl7Version);
	}

	/**
	 * 
	 * @param incomingMessage
	 * @return
	 */
	private Message processORMMessage(Message incomingMessage) {

		ORM_O01 ormMessage;
		if (incomingMessage instanceof ca.uhn.hl7v2.model.v231.message.ORM_O01) {
			try {
				PipeParser pipeParser = new PipeParser();
				ormMessage = (ORM_O01) pipeParser.parseForSpecificPackage(incomingMessage.encode(),
						"net.ihe.gazelle.ormwithzds.hl7v2.model.v231.message");
			} catch (HL7Exception e) {
				return Receiver.buildNack(serverApplication, serverFacility, sendingApplication, sendingFacility, O01,
						AcknowledgmentCode.AE, "Cannot properly encode the message for parsing", null, Receiver.INTERNAL_ERROR,
						Receiver.SEVERITY_ERROR, messageControlId, hl7Charset, hl7Version);
			}
		} else {
			return Receiver.buildNack(serverApplication, serverFacility, sendingApplication, sendingFacility, O01,
					AcknowledgmentCode.AE, "Event O01 required ORM_O01 structure", null, Receiver.INTERNAL_ERROR,
					Receiver.SEVERITY_ERROR, messageControlId, hl7Charset, hl7Version);
		}
		// Process PID and PV1 segments
		Patient selectedPatient = null;
		Encounter selectedEncounter = null;
		// stored or retrieved the patient contained in the message
		if (MessageDecoder.isSegmentPresent(ormMessage, "PID")) {
			Patient pidPatient = MessageDecoder.extractPatientFromPID(ormMessage.getPATIENT().getPID());
			List<PatientIdentifier> pidPatientIdentifiers = MessageDecoder
					.extractPatientIdentifiersFromPID251(ormMessage);
			List<PatientIdentifier> identifiersToAdd = new ArrayList<PatientIdentifier>();
			for (PatientIdentifier pid : pidPatientIdentifiers) {
				PatientIdentifier existingPID = pid.getStoredPatientIdentifier(entityManager);
				if (existingPID != null) {
					if (selectedPatient == null) {
						selectedPatient = existingPID.getPatient();
					} else if (selectedPatient.getId().equals(existingPID.getPatient().getId())) {
						// OK: nothing to do
						continue;
					} else {
						// error patients with same identifiers mismatch
						return Receiver.buildNack(serverApplication, serverFacility, sendingApplication,
								sendingFacility, O01, AcknowledgmentCode.AE, "Patient identifiers mismatch", null,
								Receiver.DUPLICATE_KEY_IDENTIFIER, Receiver.SEVERITY_ERROR, messageControlId,
								hl7Charset, hl7Version);
					}
				} else {
					pid = pid.save(entityManager);
					identifiersToAdd.add(pid);
				}
			}
			// if none of the identifiers is already known by the simulator, we save the patient contained in the message
			if (selectedPatient == null) {
				// save the patient
				selectedPatient = pidPatient.save(entityManager);
				selectedPatient.setCreator(sendingApplication + "_" + sendingFacility);
			}
			for (PatientIdentifier pid : identifiersToAdd) {
				pid.setPatient(selectedPatient);
				pid.save(entityManager);
			}
			// check existance of PV1 segment

			if (MessageDecoder.isSegmentPresent(ormMessage, "PV1")) {
				// extract encounter
				Encounter pv1Encounter = MessageDecoder.extractEncounterFromPV1(ormMessage);
				if (pv1Encounter.getVisitNumber() != null) {
					// check the encounter exists
					selectedEncounter = Encounter.getEncounterByVisitNumber(pv1Encounter.getVisitNumber(),
							entityManager);
					if ((selectedEncounter != null)
							&& !(selectedEncounter.getPatient().getId().equals(selectedPatient.getId()))) {
						return Receiver.buildNack(serverApplication, serverFacility, sendingApplication,
								sendingFacility, O01, AcknowledgmentCode.AE,
								"Visit number is already used for an encounter for another patient", null,
								Receiver.DUPLICATE_KEY_IDENTIFIER, Receiver.SEVERITY_ERROR, messageControlId,
								hl7Charset, hl7Version);
					} else if (selectedEncounter == null) {
						selectedEncounter = pv1Encounter;
						selectedEncounter.setPatient(selectedPatient);
						selectedEncounter.setCreator(sendingApplication + "_" + sendingFacility);
						selectedEncounter = selectedEncounter.save(entityManager);
					}
					// else selectedEncounter is the one retrieved from the database
				} else {
					return Receiver.buildNack(serverApplication, serverFacility, sendingApplication, sendingFacility,
							O01, AcknowledgmentCode.AE, "Visit number (PV1-19) is required but missing", null,
							Receiver.DUPLICATE_KEY_IDENTIFIER, Receiver.SEVERITY_ERROR, messageControlId, hl7Charset,
							hl7Version);
				}
			} else {
				return Receiver.buildNack(serverApplication, serverFacility, sendingApplication, sendingFacility, O01,
						AcknowledgmentCode.AE, "PV1 segment is required but missing", null, Receiver.SEGMENT_SEQUENCE_ERROR,
						Receiver.SEVERITY_ERROR, messageControlId, hl7Charset, hl7Version);
			}
		} else {
			return Receiver.buildNack(serverApplication, serverFacility, sendingApplication, sendingFacility, O01,
					AcknowledgmentCode.AE, "PID segment is required but missing", null, Receiver.SEGMENT_SEQUENCE_ERROR,
					Receiver.SEVERITY_ERROR, messageControlId, hl7Charset, hl7Version);
		}
		// check ZDS segment is present and extract the study instance UID
		Segment zds = ormMessage.getZDS();
		String studyInstanceUID = null;
		if (MessageDecoder.isEmpty(zds)) {
			return Receiver.buildNack(serverApplication, serverFacility, sendingApplication, sendingFacility, O01,
					AcknowledgmentCode.AE, "ZDS segment is required but missing", null, Receiver.SEGMENT_SEQUENCE_ERROR,
					Receiver.SEVERITY_ERROR, messageControlId, hl7Charset, hl7Version);
		} else {
			studyInstanceUID = ormMessage.getZDS().getStudyInstanceUID().getPointer().getValue();
		}
		// process ORDER groups
		int nbOfGroups = ormMessage.getORDERReps();
		if (nbOfGroups > 0) {
			requestedProcedure = null;
			for (int orderIndex = 0; orderIndex < nbOfGroups; orderIndex++) {
				Segment orc = ormMessage.getORDER(orderIndex).getORC();
				Segment obr = ormMessage.getORDER(orderIndex).getOBR();
				if (MessageDecoder.isEmpty(orc)) {
					return Receiver.buildNack(serverApplication, serverFacility, sendingApplication, sendingFacility,
							O01, AcknowledgmentCode.AE, "ORC segment is required but missing in ORDER[" + orderIndex + "]", null,
							Receiver.SEGMENT_SEQUENCE_ERROR, Receiver.SEVERITY_ERROR, messageControlId, hl7Charset,
							hl7Version);
				} else if (MessageDecoder.isEmpty(obr)) {
					return Receiver.buildNack(serverApplication, serverFacility, sendingApplication, sendingFacility,
							O01, AcknowledgmentCode.AE, "OBR segment is required but missing in ORDER[" + orderIndex + "]", null,
							Receiver.SEGMENT_SEQUENCE_ERROR, Receiver.SEVERITY_ERROR, messageControlId, hl7Charset,
							hl7Version);
				} else {
					nack = null;
					boolean mustContinue = processOrderGroup(orc, obr, selectedEncounter, studyInstanceUID);
					if (nack != null) {
						return nack;
					} else if (mustContinue) {
						continue;
					} else {
						break;
					}
				}
			}
			if (requestedProcedure != null) {
				requestedProcedure.save(entityManager);
			} else {
				log.error("RequestedProcedure is null!!!!!");
			}
		} else {
			return Receiver.buildNack(serverApplication, serverFacility, sendingApplication, sendingFacility, O01,
					AcknowledgmentCode.AE, "At least one occurrence of ORDER group is required", null, Receiver.SEGMENT_SEQUENCE_ERROR,
					Receiver.SEVERITY_ERROR, messageControlId, hl7Charset, hl7Version);
		}

		return Receiver.buildAck(serverApplication, serverFacility, sendingApplication, sendingFacility, O01, AcknowledgmentCode.AA,
				messageControlId, hl7Charset, hl7Version);
	}

	/**
	 * 
	 * @param orc
	 * @param obr
	 * @return
	 */
	private boolean processOrderGroup(Segment orc, Segment obr, Encounter selectedEncounter, String studyInstanceUID) {

		// extract order data
		Order order = null;
		if (requestedProcedure == null) {
			order = MessageDecoder.extractOrderFromORCAndOBR(orc, obr);
			Order dbOrder = Order.getOrderByNumbersByActor(order.getPlacerOrderNumber(), order.getFillerOrderNumber(),
					simulatedActor, entityManager);
			if (dbOrder != null) {
                order = copyOrderAttributes(dbOrder);
			} else {
                setOrderProperties(selectedEncounter, order);
			}
			order = order.save(entityManager);
		} else {
			order = requestedProcedure.getOrder();
		}
		// extract requested procedure data
		RequestedProcedure procedure = MessageDecoder.extractProcedureFromOrderGroup(orc, obr);
		orderControlCode = procedure.getOrderControlCode();
		procedure.setSimulatedActor(simulatedActor);
		procedure.setDomain(domain);
		procedure.setLastChanged(new Date());
		procedure.setCreator(creator);
		if (procedure.getOrderControlCode() == null) {
			nack = Receiver.buildNack(serverApplication, serverFacility, sendingApplication, sendingFacility, O01,
					AcknowledgmentCode.AE, "ORC-1 is required but missing", null, Receiver.REQUIRED_FIELD_MISSING,
					Receiver.SEVERITY_ERROR, messageControlId, hl7Charset, hl7Version);
			return false;
		} else if ((requestedProcedure != null) && (requestedProcedure.getRequestedProcedureID() != null)
				&& !requestedProcedure.getRequestedProcedureID().equals(procedure.getRequestedProcedureID())) {
			nack = Receiver.buildNack(serverApplication, serverFacility, sendingApplication, sendingFacility, O01,
					AcknowledgmentCode.AE, "Requested procedure ID mistmatch", null, Receiver.INTERNAL_ERROR, Receiver.SEVERITY_ERROR,
					messageControlId, hl7Charset, hl7Version);
			return false;
		}

		// if orderControlCode == NW, no procedure should be scheduled with this requestedProcedureID for the given order
		List<RequestedProcedure> procedures = RequestedProcedure.getRequestedProcedureFiltered(entityManager, order,
				null, studyInstanceUID, simulatedActor);
		if (procedure.getOrderControlCode().equals("NW")) {
			if (procedures != null) {
				nack = Receiver.buildNack(serverApplication, serverFacility, sendingApplication, sendingFacility, O01,
						AcknowledgmentCode.AE, "StudyInstanceUID is already used", null, Receiver.DUPLICATE_KEY_IDENTIFIER,
						Receiver.SEVERITY_ERROR, messageControlId, hl7Charset, hl7Version);
				return false;
			} else if (requestedProcedure == null) {
				requestedProcedure = procedure;
				requestedProcedure.setScheduledProcedureSteps(new ArrayList<ScheduledProcedureStep>());
				requestedProcedure.setStudyInstanceUID(studyInstanceUID);
				requestedProcedure.setOrder(order);
			}
			ScheduledProcedureStep sps = MessageDecoder.extractSPSFromOBRAndORC(obr, orc);
			sps.setSimulatedActor(simulatedActor);
			sps.setDomain(domain);
			sps.setCreator(creator);
			ProtocolItem item = MessageDecoder.extractProtocolFromOBR(obr);
			item.setSimulatedActor(simulatedActor);
			item.setDomain(domain);
			item.setCreator(creator);
			item.setLastChanged(new Date());
			item.setScheduledProcedureStep(sps);
			if (requestedProcedure.getScheduledProcedureSteps().contains(sps)) {
				int spsIndex = requestedProcedure.getScheduledProcedureSteps().indexOf(sps);
				sps = requestedProcedure.getScheduledProcedureSteps().get(spsIndex);
				if (sps.getProtocolItems() == null) {
					sps.setProtocolItems(new ArrayList<ProtocolItem>());
				}
				sps.getProtocolItems().add(item);
				sps.setRequestedProcedure(requestedProcedure);
				requestedProcedure.getScheduledProcedureSteps().set(spsIndex, sps);
			} else {
				sps.setProtocolItems(new ArrayList<ProtocolItem>());
				sps.getProtocolItems().add(item);
				sps.setLastChanged(new Date());
				sps.setRequestedProcedure(requestedProcedure);
				requestedProcedure.getScheduledProcedureSteps().add(sps);
			}
		} else if (procedures == null) {
			nack = Receiver.buildNack(serverApplication, serverFacility, sendingApplication, sendingFacility, O01,
					AcknowledgmentCode.AE, "StudyInstanceUID unknown, cannot perform update", null, Receiver.UNKNOWN_KEY_IDENTIFIER,
					Receiver.SEVERITY_ERROR, messageControlId, hl7Charset, hl7Version);
			return false;
		} else {
			if (requestedProcedure == null) {
				requestedProcedure = procedures.get(0);
				requestedProcedure.setOrderControlCode(procedure.getOrderControlCode());
				requestedProcedure.setOrderStatus(procedure.getOrderStatus());
				if (orderControlCode.equals("CA") || orderControlCode.equals("DC")) {
					return false;
				}
			}
			ScheduledProcedureStep sps = MessageDecoder.extractSPSFromOBRAndORC(obr, orc);
			sps.setSimulatedActor(simulatedActor);
			sps.setDomain(domain);
			ProtocolItem item = MessageDecoder.extractProtocolFromOBR(obr);
			item.setSimulatedActor(simulatedActor);
			item.setDomain(domain);
			if (requestedProcedure.getScheduledProcedureSteps().contains(sps)) {
				int spsIndex = requestedProcedure.getScheduledProcedureSteps().indexOf(sps);
				ScheduledProcedureStep oldSps = requestedProcedure.getScheduledProcedureSteps().get(spsIndex);
				sps.setLastChanged(new Date());
				sps.setCreator(oldSps.getCreator());
				sps.setId(oldSps.getId());
				// update protocol
				if (oldSps.getProtocolItems().contains(item)) {
					int itemIndex = oldSps.getProtocolItems().indexOf(item);
					ProtocolItem oldItem = oldSps.getProtocolItems().get(itemIndex);
					item.setLastChanged(new Date());
					item.setCreator(oldItem.getCreator());
					item.setId(oldItem.getId());
					item.setScheduledProcedureStep(sps);
					oldSps.getProtocolItems().set(itemIndex, item);
				}
				sps.setProtocolItems(new ArrayList<ProtocolItem>(oldSps.getProtocolItems()));
				// replace sps in the list
				sps.setRequestedProcedure(requestedProcedure);
				requestedProcedure.getScheduledProcedureSteps().set(spsIndex, sps);
			} else {
				nack = Receiver.buildNack(serverApplication, serverFacility, sendingApplication, sendingFacility, O01,
						AcknowledgmentCode.AE, "ScheduledProcedureStepID unknown, cannot perform update", null,
						Receiver.UNKNOWN_KEY_IDENTIFIER, Receiver.SEVERITY_ERROR, messageControlId, hl7Charset,
						hl7Version);
				return false;
			}
		}
		return true;
	}

    private void setOrderProperties(Encounter selectedEncounter, Order order) {
        order.setSimulatedActor(simulatedActor);
        order.setCreator(creator);
        order.setEncounter(selectedEncounter);
        order.setLastChanged(new Date());
        order.setDomain(domain);
    }

    /**
	 * 
	 * @param orc
	 * @param obr
	 * @return
	 */
	private boolean processOrderGroup(Segment orc, Segment obr, Segment tq1, List<IPC> ipcSegments, Encounter selectedEncounter) {

		// extract order data
		Order order = MessageDecoder.extractOrderFromORCAndOBR(orc, obr);
		Order dbOrder = Order.getOrderByNumbersByActor(order.getPlacerOrderNumber(), order.getFillerOrderNumber(),
				simulatedActor, entityManager);
		if (dbOrder != null) {
            order = copyOrderAttributes(dbOrder);
		} else {
            setOrderProperties(selectedEncounter, order);
		}
		order = order.save(entityManager);

		for (IPC ipc : ipcSegments){
			// extract requested procedure data
			RequestedProcedure procedure = MessageDecoder.extractProcedureFromOrderGroup(orc, obr, ipc);
			orderControlCode = procedure.getOrderControlCode();
			procedure.setSimulatedActor(simulatedActor);
			procedure.setDomain(domain);
			procedure.setLastChanged(new Date());
			procedure.setCreator(creator);
			if (procedure.getOrderControlCode() == null) {
				nack = Receiver.buildNack(serverApplication, serverFacility, sendingApplication, sendingFacility, O23,
						AcknowledgmentCode.AE, "ORC-1 is required but missing", null, Receiver.REQUIRED_FIELD_MISSING,
						Receiver.SEVERITY_ERROR, messageControlId, hl7Charset, hl7Version);
				return false;
			} else if ((requestedProcedure != null) && (requestedProcedure.getRequestedProcedureID() != null)
					&& !requestedProcedure.getRequestedProcedureID().equals(procedure.getRequestedProcedureID())) {
				nack = Receiver.buildNack(serverApplication, serverFacility, sendingApplication, sendingFacility, O23,
						AcknowledgmentCode.AE, "Requested procedure ID mistmatch", null, Receiver.INTERNAL_ERROR, Receiver.SEVERITY_ERROR,
						messageControlId, hl7Charset, hl7Version);
				return false;
			}
			// if orderControlCode == NW, no procedure should be scheduled with this requestedProcedureID for the given order
			List<RequestedProcedure> procedures = RequestedProcedure.getRequestedProcedureFiltered(entityManager, order,
					null, procedure.getStudyInstanceUID(), simulatedActor);
			if (procedure.getOrderControlCode().equals("NW")) {
				if (procedures != null) {
					nack = Receiver.buildNack(serverApplication, serverFacility, sendingApplication, sendingFacility, O23,
							AcknowledgmentCode.AE, "StudyInstanceUID is already used", null, Receiver.DUPLICATE_KEY_IDENTIFIER,
							Receiver.SEVERITY_ERROR, messageControlId, hl7Charset, hl7Version);
					return false;
				} else if (requestedProcedure == null) {
					requestedProcedure = procedure;
					requestedProcedure.setScheduledProcedureSteps(new ArrayList<ScheduledProcedureStep>());
					requestedProcedure.setOrder(order);
				}
				ScheduledProcedureStep sps = MessageDecoder.extractSPSFromOBRAndORC(tq1, ipc);
				sps.setSimulatedActor(simulatedActor);
				sps.setDomain(domain);
				sps.setCreator(creator);
				ProtocolItem item = MessageDecoder.extractProtocolFromOBR(ipc);
				item.setSimulatedActor(simulatedActor);
				item.setDomain(domain);
				item.setCreator(creator);
				item.setLastChanged(new Date());
				item.setScheduledProcedureStep(sps);
				if (requestedProcedure.getScheduledProcedureSteps().contains(sps)) {
					int spsIndex = requestedProcedure.getScheduledProcedureSteps().indexOf(sps);
					sps = requestedProcedure.getScheduledProcedureSteps().get(spsIndex);
					if (sps.getProtocolItems() == null) {
						sps.setProtocolItems(new ArrayList<ProtocolItem>());
					}
					sps.getProtocolItems().add(item);
					sps.setRequestedProcedure(requestedProcedure);
					requestedProcedure.getScheduledProcedureSteps().set(spsIndex, sps);
				} else {
					sps.setProtocolItems(new ArrayList<ProtocolItem>());
					sps.getProtocolItems().add(item);
					sps.setLastChanged(new Date());
					sps.setRequestedProcedure(requestedProcedure);
					requestedProcedure.getScheduledProcedureSteps().add(sps);
				}
			} else if (procedures == null) {
				nack = Receiver.buildNack(serverApplication, serverFacility, sendingApplication, sendingFacility, O23,
						AcknowledgmentCode.AE, "StudyInstanceUID unknown, cannot perform update", null, Receiver.UNKNOWN_KEY_IDENTIFIER,
						Receiver.SEVERITY_ERROR, messageControlId, hl7Charset, hl7Version);
				return false;
			} else {
				if (requestedProcedure == null) {
					requestedProcedure = procedures.get(0);
					requestedProcedure.setOrderControlCode(procedure.getOrderControlCode());
					requestedProcedure.setOrderStatus(procedure.getOrderStatus());
					if (orderControlCode.equals("CA") || orderControlCode.equals("DC")) {
						return false;
					}
				}
				ScheduledProcedureStep sps = MessageDecoder.extractSPSFromOBRAndORC(obr, orc);
				sps.setSimulatedActor(simulatedActor);
				sps.setDomain(domain);
				ProtocolItem item = MessageDecoder.extractProtocolFromOBR(obr);
				item.setSimulatedActor(simulatedActor);
				item.setDomain(domain);
				if (requestedProcedure.getScheduledProcedureSteps().contains(sps)) {
					int spsIndex = requestedProcedure.getScheduledProcedureSteps().indexOf(sps);
					ScheduledProcedureStep oldSps = requestedProcedure.getScheduledProcedureSteps().get(spsIndex);
					sps.setLastChanged(new Date());
					sps.setCreator(oldSps.getCreator());
					sps.setId(oldSps.getId());
					// update protocol
					if (oldSps.getProtocolItems().contains(item)) {
						int itemIndex = oldSps.getProtocolItems().indexOf(item);
						ProtocolItem oldItem = oldSps.getProtocolItems().get(itemIndex);
						item.setLastChanged(new Date());
						item.setCreator(oldItem.getCreator());
						item.setId(oldItem.getId());
						item.setScheduledProcedureStep(sps);
						oldSps.getProtocolItems().set(itemIndex, item);
					}
					sps.setProtocolItems(new ArrayList<ProtocolItem>(oldSps.getProtocolItems()));
					// replace sps in the list
					sps.setRequestedProcedure(requestedProcedure);
					requestedProcedure.getScheduledProcedureSteps().set(spsIndex, sps);
				} else {
					nack = Receiver.buildNack(serverApplication, serverFacility, sendingApplication, sendingFacility, O01,
							AcknowledgmentCode.AE, "ScheduledProcedureStepID unknown, cannot perform update", null,
							Receiver.UNKNOWN_KEY_IDENTIFIER, Receiver.SEVERITY_ERROR, messageControlId, hl7Charset,
							hl7Version);
					return false;
				}
			}
		}
		return true;
	}

    private Order copyOrderAttributes(Order dbOrder) {
        Order order;
        order = dbOrder;
        order.setLastChanged(new Date());
        if (dbOrder.getOrderControlCode().equals("XO")) {
            order.setOrderControlCode("SC");
        }
        return order;
    }

    /**
     * <p>processMessageFromADT.</p>
     *
     * @param incomingMessage a {@link ca.uhn.hl7v2.model.Message} object.
     * @return a {@link ca.uhn.hl7v2.model.Message} object.
     */
    protected Message processMessageFromADT(Message incomingMessage) {
		Terser terser = new Terser(incomingMessage);
		Message response = null;
		try {
			String event = terser.get("/.MSH-9-2");
			String transactionKeyword = null;
			if (event.equals(A08)) {
				transactionKeyword = "RAD-12";
				response = processA08Message(incomingMessage);
			} else if (event.equals(A40)) {
				transactionKeyword = "RAD-12";
				response = processA40Message(incomingMessage);
			}
			simulatedTransaction = Transaction.GetTransactionByKeyword(transactionKeyword, entityManager);
		} catch (HL7Exception e) {
			log.info(e.getMessage(), e);
		}
		return response;
	}

	/**
	 * <p>processA08Message.</p>
	 *
	 * @param incomingMessage a {@link ca.uhn.hl7v2.model.Message} object.
	 * @return a {@link ca.uhn.hl7v2.model.Message} object.
	 */
	protected Message processA08Message(Message incomingMessage) {
		if (incomingMessage instanceof ADT_A01) {
			ADT_A01 message = (ADT_A01) incomingMessage;
			Patient selectedPatient = null;
			creator = sendingApplication + "_" + sendingFacility;
			Patient pidPatient = MessageDecoder.extractPatientFromPID(message.getPID());
			List<PatientIdentifier> pidPatientIdentifiers;
			pidPatientIdentifiers = MessageDecoder.extractPatientIdentifiersFromPID251(message);
			for (PatientIdentifier pid : pidPatientIdentifiers) {
				PatientIdentifier existingPID = pid.getStoredPatientIdentifier(entityManager);
				if (existingPID != null) {
					if (selectedPatient == null) {
						selectedPatient = existingPID.getPatient();
					} else if (selectedPatient.getId().equals(existingPID.getPatient().getId())) {
						// OK: nothing to do
						continue;
					} else {
						// error patients with same identifiers mismatch
						// create a good ACK
						return Receiver.buildNack(sendingApplication, sendingFacility, serverApplication,
								serverFacility, A08, AcknowledgmentCode.AE, "The patient identifiers are inconsistent", null,
								Receiver.DUPLICATE_KEY_IDENTIFIER, Receiver.SEVERITY_ERROR, messageControlId,
								hl7Charset, hl7Version);
					}
				}
			}
			if (selectedPatient != null) {
				pidPatient.setId(selectedPatient.getId());
				pidPatient.setPatientIdentifiers(selectedPatient.getPatientIdentifiers());
				pidPatient.setEncounters(selectedPatient.getEncounters());
				pidPatient.setWeight(selectedPatient.getWeight());
				pidPatient.setCreator(creator);
				pidPatient.save(entityManager);
				return Receiver.buildAck(sendingApplication, sendingFacility, serverApplication, serverFacility, A08,
						AcknowledgmentCode.AA, messageControlId, hl7Charset, hl7Version);
			} else {
				return Receiver.buildNack(sendingApplication, sendingFacility, serverApplication, serverFacility, A08,
						AcknowledgmentCode.AE, "No patient found with such identifiers", null, Receiver.UNKNOWN_KEY_IDENTIFIER,
						Receiver.SEVERITY_ERROR, messageControlId, hl7Charset, hl7Version);
			}
		} else {
			return Receiver.buildNack(sendingApplication, sendingFacility, serverApplication, serverFacility, A08,
					AcknowledgmentCode.AR, "Message structure is not allowed for this event", null, Receiver.INTERNAL_ERROR,
					Receiver.SEVERITY_ERROR, messageControlId, hl7Charset, hl7Version);
		}

	}

	/**
	 * <p>processA40Message.</p>
	 *
	 * @param incomingMessage a {@link ca.uhn.hl7v2.model.Message} object.
	 * @return a {@link ca.uhn.hl7v2.model.Message} object.
	 */
	protected Message processA40Message(Message incomingMessage) {
		// check the message structure is the one expected by IHE
		if (incomingMessage instanceof ADT_A39) {
			ADT_A39 adtMessage = (ADT_A39) incomingMessage;
			creator = sendingApplication + "_" + sendingFacility;
			PatientIdentifier incorrectPatientIdentifier = MessageDecoder.buildPatientIdentifier(adtMessage
					.getPATIENT().getMRG().getPriorPatientIdentifierList()[0]);
			List<PatientIdentifier> correctPatientIdentifiers = MessageDecoder
					.extractPatientIdentifiersFromPID251(adtMessage);
			Patient incorrectPatient;
			Patient correctPatient;

			PatientIdentifier existingIncorrectPID = incorrectPatientIdentifier
					.getStoredPatientIdentifier(entityManager);
			if ((existingIncorrectPID == null) || (existingIncorrectPID.getPatient() == null)) {
				return Receiver.buildNack(sendingApplication, sendingFacility, serverApplication, serverFacility, A40,
						AcknowledgmentCode.AR, "Prior patient identifier is unknown", null, Receiver.UNKNOWN_KEY_IDENTIFIER,
						Receiver.SEVERITY_ERROR, messageControlId, hl7Charset, hl7Version);
			} else {
				incorrectPatient = existingIncorrectPID.getPatient();
			}
			PatientIdentifier existingCorrectPID = null;

			for (PatientIdentifier pid : correctPatientIdentifiers) {
				existingCorrectPID = pid.getStoredPatientIdentifier(entityManager);
				if (existingCorrectPID != null) {
					break;
				}
			}

			if ((existingCorrectPID == null) || (existingCorrectPID.getPatient() == null)) {
				correctPatient = MessageDecoder.extractPatientFromPID(adtMessage.getPATIENT().getPID());
				correctPatient.setPatientIdentifiers(correctPatientIdentifiers);
				correctPatient.setCreator(creator);
			} else {
				correctPatient = existingCorrectPID.getPatient();
			}
			correctPatient.save(entityManager);
			if ((incorrectPatient.getEncounters() != null) && (incorrectPatient.getEncounters().size() > 0)) {
				for (Encounter encounter : incorrectPatient.getEncounters()) {
					encounter.setPatient(correctPatient);
					encounter.save(entityManager);
				}
			}
			for (PatientIdentifier pid : incorrectPatient.getPatientIdentifiers()) {
				entityManager.remove(pid);
			}
			entityManager.flush();
			entityManager.remove(incorrectPatient);
			entityManager.flush();
			return Receiver.buildAck(sendingApplication, sendingFacility, serverApplication, serverFacility, A40, AcknowledgmentCode.AA,
					messageControlId, hl7Charset, hl7Version);
		} else {
			// message structure is not the one expected (ADT_A39)
			return Receiver.buildNack(serverApplication, serverFacility, sendingApplication, sendingFacility, A40,
					null, "Message structure is not the one expected, event A40 should use ADT_A39", null,
					Receiver.INTERNAL_ERROR, Receiver.SEVERITY_ERROR, messageControlId, hl7Charset, hl7Version);
		}
	}

}
