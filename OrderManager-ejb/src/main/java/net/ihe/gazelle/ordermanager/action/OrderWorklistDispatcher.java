package net.ihe.gazelle.ordermanager.action;

import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.ordermanager.model.Encounter;
import net.ihe.gazelle.ordermanager.model.Patient;
import net.ihe.gazelle.ordermanager.model.PatientIdentifier;
import net.ihe.gazelle.ordermanager.model.UserPreferences;
import net.ihe.gazelle.ordermanager.utils.EncounterGeneration;
import net.ihe.gazelle.patient.PatientAddress;
import net.ihe.gazelle.simulator.common.tf.model.Domain;
import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.core.ResourceBundle;
import org.jboss.seam.faces.FacesMessages;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.persistence.EntityManager;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Class description : <b>OrderWorklistDispatcher</b>
 * <p>
 * This class is the backing bean for page dispatcher.xhtml, it gathers all the required attributes and methods for dispatching an order/worklist creation request
 *
 * @author Anne-Gaëlle Bergé - INRIA Rennes/Bretagne Atlantique
 * @version 1.0 - 2011, November 17th
 *
 *
 *
 */

@Name("dispatcher")
@Scope(ScopeType.PAGE)
public class OrderWorklistDispatcher extends EncounterGeneration {

    private static final String SENDER_PARAM = "sender";
    private static final String PATIENT_MANAGER = "Patient Manager";
    private static final String TEST_MANAGEMENT = "Test Management";
    private static final String PID = "pid";
    private static final String PI = "PI";
    private static final String FIRST_NAME = "firstName";
    private static final String LAST_NAME = "lastName";
    private static final String MOTHER_MAIDEN_NAME = "motherMaidenName";
    private static final String ALTERNATE_MOTHERS_MAIDEN_NAME = "alternateMothersMaidenName";
    private static final String CITY = "city";
    private static final String STREET = "street";
    private static final String COUNTRY_CODE = "countryCode";
    private static final String ZIP_CODE = "zipCode";
    private static final String STATE = "state";
    private static final String GENDER_CODE = "genderCode";
    private static final String ALTERNATE_FIRST_NAME = "alternateFirstName";
    private static final String ALTERNATE_LAST_NAME = "alternateLastName";
    private static final String SECOND_NAME = "secondName";
    private static final String THIRD_NAME = "thirdName";
    private static final String ALTERNATE_SECOND_NAME = "alternateSecondName";
    private static final String ALTERNATE_THIRD_NAME = "alternateThirdName";
    private static final String DATETIME_PATTERN = "yyyyMMddhhmmss";
    private static final String DATE_OF_BIRTH = "dateOfBirth";
    private static final String VISIT_NUMBER = "visitNumber";
    private static final String LOCATION = "location";
    private static final String PATIENT_CLASS = "patientClass";
    private static final String DOCTOR = "doctor";
    private static Logger log = LoggerFactory.getLogger(OrderWorklistDispatcher.class);

    /**
     *
     */
    private static final long serialVersionUID = 7140822192208862098L;

    private static final String ORDER = "O";
    private static final String WORKLIST = "W";
    private static final String WORKORDER = "WO";
    private static final String TRO = "SeHE";
    private static final String PAM = "PAM";
    private static final String TM = "TM";

    private Domain selectedDomain;
    private String selectedSendingActor = null;
    private String objectType = null;
    private String sender;
    private boolean pageCanBeBuilt = true;
    private boolean displayOptions = false;
    // not used ini JSF

    @Create
    public void initializePage() {
        Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        if ((params != null) && !params.isEmpty()) {
            if (params.get(SENDER_PARAM) != null) {
                if (params.get(SENDER_PARAM).equals(PAM)) {
                    sender = PATIENT_MANAGER;
                } else if (params.get(SENDER_PARAM).equals(TM)) {
                    sender = TEST_MANAGEMENT;
                } else {
                    sender = params.get(SENDER_PARAM);
                }
            } else {
                sender = null;
            }
            UserPreferences userPreferences = (UserPreferences) Component.getInstance("currentUser");
            String username;
            if (userPreferences != null) {
                username = userPreferences.getUsername();
            } else {
                username = params.get(SENDER_PARAM);
            }
            EntityManager entityManager = EntityManagerService.provideEntityManager();
            if (params.containsKey(PID)) {
                PatientIdentifier pid = new PatientIdentifier();
                pid.setIdentifier(params.get(PID));
                pid.setIdentifierTypeCode(PI);
                pid = pid.save(entityManager);
                if (pid.getPatient() != null) {
                    setSelectedPatient(pid.getPatient());
                } else {
                    setSelectedPatient(new Patient());
                    getSelectedPatient().setFirstName(params.get(FIRST_NAME));
                    getSelectedPatient().setLastName(params.get(LAST_NAME));
                    getSelectedPatient().setMotherMaidenName(params.get(MOTHER_MAIDEN_NAME));
                    getSelectedPatient().setAlternateMothersMaidenName(params.get(ALTERNATE_MOTHERS_MAIDEN_NAME));
                    PatientAddress address = new PatientAddress(getSelectedPatient());
                    address.setCity(params.get(CITY));
                    address.setAddressLine(params.get(STREET));
                    getSelectedPatient().setCountryCode(params.get(COUNTRY_CODE));
                    address.setCountryCode(params.get(COUNTRY_CODE));
                    address.setZipCode(params.get(ZIP_CODE));
                    address.setState(params.get(STATE));
                    getSelectedPatient().addPatientAddress(address);
                    getSelectedPatient().setGenderCode(params.get(GENDER_CODE));
                    getSelectedPatient().setCreationDate(new Date());
                    getSelectedPatient().setCreator(username);
                    getSelectedPatient().setAlternateFirstName(params.get(ALTERNATE_FIRST_NAME));
                    getSelectedPatient().setAlternateLastName(params.get(ALTERNATE_LAST_NAME));
                    getSelectedPatient().setSecondName(params.get(SECOND_NAME));
                    getSelectedPatient().setThirdName(params.get(THIRD_NAME));
                    getSelectedPatient().setAlternateSecondName(params.get(ALTERNATE_SECOND_NAME));
                    getSelectedPatient().setAlternateThirdName(params.get(ALTERNATE_THIRD_NAME));
                    SimpleDateFormat sdf = new SimpleDateFormat(DATETIME_PATTERN);
                    if (params.containsKey(DATE_OF_BIRTH)) {
                        try {
                            getSelectedPatient().setDateOfBirth(sdf.parse(params.get(DATE_OF_BIRTH)));
                        } catch (ParseException e) {
                            log.error("dateOfBirth = " + params.get(DATE_OF_BIRTH), e);
                        }
                    }
                    setSelectedPatient(getSelectedPatient().save(entityManager));
                    pid.setPatient(getSelectedPatient());
                    pid = pid.saveLinkWithPatient(entityManager);
                    List<PatientIdentifier> idList = new ArrayList<PatientIdentifier>();
                    idList.add(pid);
                    getSelectedPatient().setPatientIdentifiers(idList);
                    setSelectedPatient(getSelectedPatient().save(entityManager));
                }
            } else {
                FacesMessages.instance().addFromResourceBundle("gazelle.ordermanager.patientIdentifierIsMissing");
                pageCanBeBuilt = false;
                return;
            }
            // if a visit with this visit number already exists, take this one, check the patient is the same
            if (params.containsKey(VISIT_NUMBER)) {
                setSelectedEncounter(new Encounter());
                getSelectedEncounter().setVisitNumber(params.get(VISIT_NUMBER));
                getSelectedEncounter().setAssignedPatientLocation(params.get(LOCATION));
                getSelectedEncounter().setCreationDate(new Date());
                getSelectedEncounter().setCreator(username);
                getSelectedEncounter().setPatientClassCode(params.get(PATIENT_CLASS));
                getSelectedEncounter().setReferringDoctorCode(params.get(DOCTOR));
                getSelectedEncounter().setPatient(getSelectedPatient());
                setSelectedEncounter(getSelectedEncounter().save(entityManager));
            } else {
                FacesMessages.instance().addFromResourceBundle("gazelle.ordermanager.noEncounterProvided");
                generateAnEncounter();
            }
        } else {
            FacesMessages.instance().addFromResourceBundle("gazelle.ordermanager.noParamGivenInUrl");
            pageCanBeBuilt = false;
            return;
        }
    }


    /**
     * @return the link to the page
     */
    public String goToPage() {
        StringBuilder url = new StringBuilder();
        if (objectType.equals(WORKLIST) || objectType.equals(WORKORDER)) {
            if (objectType.equals(WORKLIST)) {
                url = url.append("/mwlscp/createWorklistEntry.seam?");
            } else {
                url = url.append("/hl7Initiators/labOrderSender.seam?actor=OF&workOrder=true&");
            }
            url.append("encounterId=");
            url.append(getSelectedEncounter().getId());
            url.append("&domain=");
            url.append(selectedDomain.getKeyword());
        } else if (objectType.equals(TRO)) {
            url = url.append("/tro/manageOrder.seam?actor=TRO_CREATOR&domain=TRO&encounterId=");
            url.append(getSelectedEncounter().getId());
        } else {
            url = url.append("/hl7Initiators/orderSender.seam?encounterId=" + getSelectedEncounter().getId());
            url.append("&actor=");
            url.append(selectedSendingActor);
            url.append("&domain=");
            url.append(selectedDomain.getKeyword());
        }
        return url.toString();
    }

    /**
     * @return the link to the page
     */
    public List<SelectItem> getObjectTypes() {
        List<SelectItem> items = new ArrayList<SelectItem>();
        if (selectedDomain != null && !selectedDomain.getKeyword().equals(TRO)) {
            items.add(new SelectItem(ORDER, ResourceBundle.instance().getString("gazelle.ordermanager.createAnOrder")));
        }
        if ((selectedDomain != null) && !selectedDomain.getKeyword().equals("LAB")
                && !selectedDomain.getKeyword().equals(TRO)) {
            items.add(new SelectItem(WORKLIST, ResourceBundle.instance().getString(
                    "gazelle.ordermanager.createAWorklist")));
        }
        if ((selectedDomain != null) && selectedDomain.getKeyword().equals("LAB")) {
            items.add(new SelectItem(WORKORDER, ResourceBundle.instance().getString(
                    "gazelle.ordermanager.createAWorkOrder")));
        }
        if (selectedDomain != null && selectedDomain.getKeyword().equals(TRO)) {
            items.add(new SelectItem(TRO, "Tele-Radiology Orders"));
        }
        return items;
    }

    public Domain getSelectedDomain() {
        return selectedDomain;
    }

    public void setSelectedDomain(Domain selectedDomain) {
        this.selectedDomain = selectedDomain;
    }

    public String getObjectType() {
        return objectType;
    }

    public void setObjectType(String objectType) {
        this.objectType = objectType;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String infoSender) {
        this.sender = infoSender;
    }

    public String getSelectedSendingActor() {
        return selectedSendingActor;
    }

    public void setSelectedSendingActor(String selectedSendingActor) {
        this.selectedSendingActor = selectedSendingActor;
    }

    public String getOrder() {
        return ORDER;
    }

    public String getWorklist() {
        return WORKLIST;
    }

    public String getPam() {
        return PAM;
    }

    public String getTm() {
        return TM;
    }

    public String getWorkorder() {
        return WORKORDER;
    }

    public String getTro() {
        return TRO;
    }

    /**
     * @param pageCanBeBuilt the pageCanBeBuilt to set
     */
    public void setPageCanBeBuilt(boolean pageCanBeBuilt) {
        this.pageCanBeBuilt = pageCanBeBuilt;
    }

    /**
     * @return the pageCanBeBuilt
     */
    public boolean isPageCanBeBuilt() {
        return pageCanBeBuilt;
    }

    public void setDisplayOptions(boolean displayOptions) {
        this.displayOptions = displayOptions;
    }

    public boolean isDisplayOptions() {
        return displayOptions;
    }

}
