package net.ihe.gazelle.ordermanager.admin;

import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.ordermanager.model.NumberGenerator;
import net.ihe.gazelle.ordermanager.model.NumberGeneratorQuery;
import net.ihe.gazelle.ordermanager.model.UIDGenerator;
import net.ihe.gazelle.ordermanager.model.UIDGeneratorQuery;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

import javax.persistence.EntityManager;
import java.io.Serializable;
import java.util.List;

@Name("numberGeneratorManager")
@Scope(ScopeType.PAGE)
public class NumberGeneratorManager implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2649374748959958575L;

	private List<NumberGenerator> generators;
	private List<UIDGenerator> uidGenerators;

	@Create
	public void init() {
		NumberGeneratorQuery query = new NumberGeneratorQuery();
		query.actor().name().order(true);
		generators = query.getListNullIfEmpty();
		UIDGeneratorQuery uidGeneratorQuery = new UIDGeneratorQuery();
		uidGeneratorQuery.tag().order(true);
		uidGenerators = uidGeneratorQuery.getListNullIfEmpty();
	}

	public void saveGenerator(NumberGenerator selectedGenerator) {
		EntityManager entityManager = EntityManagerService.provideEntityManager();
		entityManager.merge(selectedGenerator);
		entityManager.flush();
	}

	public void saveGenerator(UIDGenerator selectedGenerator) {
		EntityManager entityManager = EntityManagerService.provideEntityManager();
		entityManager.merge(selectedGenerator);
		entityManager.flush();
	}

	public void resetIndex(NumberGenerator selectedGenerator) {
		selectedGenerator.setNextEntityIdentifier(1);
		saveGenerator(selectedGenerator);
	}

	public void resetIndex(UIDGenerator selectedGenerator) {
		selectedGenerator.setLastIndex(0);
		saveGenerator(selectedGenerator);
	}

	public List<NumberGenerator> getGenerators() {
		return generators;
	}

	public List<UIDGenerator> getUidGenerators() {
		return uidGenerators;
	}

}
