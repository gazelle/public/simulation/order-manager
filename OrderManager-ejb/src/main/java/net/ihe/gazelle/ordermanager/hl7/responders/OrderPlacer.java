package net.ihe.gazelle.ordermanager.hl7.responders;

import ca.uhn.hl7v2.AcknowledgmentCode;
import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.model.Message;
import ca.uhn.hl7v2.parser.PipeParser;
import ca.uhn.hl7v2.protocol.ReceivingApplicationException;
import net.ihe.gazelle.HL7Common.messages.HL7MessageDecoder;
import net.ihe.gazelle.HL7Common.responder.IHEDefaultHandler;
import net.ihe.gazelle.HL7Common.utils.TransactionInstanceUtil;
import net.ihe.gazelle.ordermanager.utils.MessageDecoder;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;
import org.jboss.seam.log.Log;
import org.jboss.seam.log.Logging;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.Date;

/**
 * Class description : <b>OrderPlacer</b>
 * This class is the handler for messages received by the OrderPlacer
 *
 * @author Anne-Gaëlle Bergé - INRIA Rennes/Bretagne Atlantique
 * @version 1.0 - 2011, November 17th
 */
public class OrderPlacer extends OrderReceiver {

    private static final Logger LOG = LoggerFactory.getLogger(OrderPlacer.class);

    @Override
    public IHEDefaultHandler newInstance() {
        return new OrderPlacer();
    }

    @Override
    public Message processMessage(Message incomingMessage) throws ReceivingApplicationException, HL7Exception {
        orderControlCode = null;
        Message response;

        try {
            Actor sutActor = Actor.findActorWithKeyword("OF", entityManager);
            // check if we must accept the message
            if (domain.getKeyword().contains("LAB")) {
                response = Receiver.generateNack(incomingMessage, serverApplication, serverFacility,
                        Arrays.asList("OML"), Arrays.asList("O21", "O33", "O35"), hl7Charset);
            } else {
                // OP supports ADT messages
                response = Receiver.generateNack(incomingMessage, serverApplication, serverFacility,
                        Arrays.asList("ORM", "ADT", "OMG"), Arrays.asList("O01", "A01", "A04", "A08", "A40", "O19"), hl7Charset);
            }

            // if no NACK is generated, process the message
            if (response == null) {
                if (messageType == null) {
                    LOG.error("Message type is empty");
                    // put something by default because sending_actor_id cannot be null in hl7_message table !!!!
                    sutActor = Actor.findActorWithKeyword("UNKNOWN", entityManager);
                } else if (messageType.contains("ADT")) {
                    response = processMessageFromADT(incomingMessage);
                    sutActor = Actor.findActorWithKeyword("ADT", entityManager);
                } else if (messageType.contains("ORM") || messageType.contains("OMG")) {
                    // RAD, CARD, EYE
                    // process message ORM^O01^ORM_O01
                    response = processRAD3Transaction(incomingMessage);
                } else if (messageType.contains("OML")) {
                    // LAB, PAT --> requires to determine the affinity domain of the OF
                    if (domain.getKeyword().equals("PAT")) {
                        // process message for LAB
                        response = processPAT2Transaction(incomingMessage);
                    } else {
                        // process message for PAT
                        response = processLAB2Transaction(incomingMessage);
                    }
                } else if (messageType.contains("SIU")) {
                    // process message OUL^R23^OUL_R23 or OUL^R22^OUL_R22
                    response = processRAD48Transaction(incomingMessage);
                } else {
                    LOG.error("This kind of message is not supported, we should not reach this line !");
                }
            }

            messageType = MessageDecoder.buildMessageType(incomingMessage);
            if (orderControlCode != null) {
                messageType = messageType.concat(" (" + orderControlCode + ")");
                setIncomingMessageType(messageType);
            }
            setSutActor(sutActor);
            setResponseMessageType(HL7MessageDecoder.getMessageType(response));
        } catch (HL7Exception e) {
            throw e;
        } catch (Exception e) {
            throw new ReceivingApplicationException(e);
        }
        return response;
    }

    private Message processRAD48Transaction(Message incomingMessage) {
        simulatedTransaction = Transaction.GetTransactionByKeyword("RAD-48", entityManager);
        return Receiver.buildAck(serverApplication, serverFacility, sendingApplication, sendingFacility, "S12", AcknowledgmentCode.AA,
                messageControlId, hl7Charset, hl7Version);
    }

    private Message processPAT2Transaction(Message incomingMessage) {
        simulatedTransaction = Transaction.GetTransactionByKeyword("PAT-2", entityManager);
        return Receiver.buildAck(serverApplication, serverFacility, sendingApplication, sendingFacility, "O22", AcknowledgmentCode.AA,
                messageControlId, hl7Charset, hl7Version);
    }

    private Message processLAB2Transaction(Message incomingMessage) {
        LOG.info("processing OML message");
        simulatedTransaction = Transaction.GetTransactionByKeyword("LAB-1", entityManager);
        return processOMLMessage(incomingMessage, "SN", "OC", "SC", null);
    }

    private Message processRAD3Transaction(Message incomingMessage) {
        simulatedTransaction = Transaction.GetTransactionByKeyword("RAD-3", entityManager);
        return processRadiologyOrderMessage(incomingMessage, "SN", "OC", "SC", null);
    }
}
