package net.ihe.gazelle.ordermanager.datamodel;

import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.hql.criterion.HQLCriterion;
import net.ihe.gazelle.ordermanager.model.LabOrder;

import java.util.ArrayList;
import java.util.List;

/**
 * Class description : <b>LabOrderFilter</b>
 * 
 *
 *
 * @author Anne-Gaëlle Bergé - INRIA Rennes/Bretagne Atlantique
 *
 * @version 1.0 - 2011, December 6th
 * 
 */
public class LabOrderFilter extends Filter<LabOrder> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4857700306380393540L;
	private static List<HQLCriterion<LabOrder, ?>> defaultCriterions;

	static {
		defaultCriterions = new ArrayList<HQLCriterion<LabOrder, ?>>();
	}

	public LabOrderFilter() {
		super(LabOrder.class, defaultCriterions);
	}
}
