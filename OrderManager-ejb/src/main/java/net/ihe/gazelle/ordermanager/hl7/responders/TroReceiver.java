package net.ihe.gazelle.ordermanager.hl7.responders;

import ca.uhn.hl7v2.AcknowledgmentCode;
import ca.uhn.hl7v2.ErrorCode;
import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.model.Message;
import ca.uhn.hl7v2.model.v251.datatype.EI;
import ca.uhn.hl7v2.model.v251.group.OMI_O23_ORDER;
import ca.uhn.hl7v2.model.v251.segment.IPC;
import ca.uhn.hl7v2.model.v251.segment.MSH;
import ca.uhn.hl7v2.model.v251.segment.NTE;
import ca.uhn.hl7v2.model.v251.segment.OBR;
import ca.uhn.hl7v2.model.v251.segment.ORC;
import ca.uhn.hl7v2.parser.Parser;
import ca.uhn.hl7v2.parser.PipeParser;
import ca.uhn.hl7v2.protocol.ReceivingApplicationException;
import net.ihe.gazelle.HL7Common.initiator.Initiator;
import net.ihe.gazelle.HL7Common.model.HL7Protocol;
import net.ihe.gazelle.HL7Common.model.HL7V2ResponderSUTConfiguration;
import net.ihe.gazelle.HL7Common.responder.IHEDefaultHandler;
import net.ihe.gazelle.hl7.validator.client.MessageEncoding;
import net.ihe.gazelle.ordermanager.model.Encounter;
import net.ihe.gazelle.ordermanager.model.Patient;
import net.ihe.gazelle.ordermanager.model.PatientIdentifier;
import net.ihe.gazelle.ordermanager.model.TeleRadiologyOrder;
import net.ihe.gazelle.ordermanager.model.TeleRadiologyProcedure;
import net.ihe.gazelle.ordermanager.utils.MessageDecoder;
import net.ihe.gazelle.ordermanager.utils.TROMessageBuilder;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.common.tf.model.Domain;
import net.ihe.gazelle.tro.hl7v2.model.v251.message.OMI_O23;
import net.ihe.gazelle.tro.hl7v2.model.v251.segment.ZPS;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class TroReceiver extends IHEDefaultHandler {

    private static Logger log = LoggerFactory.getLogger(TroReceiver.class);

    protected String orderControlCode = null;


    @Override
    public IHEDefaultHandler newInstance() {
        return new TroReceiver();
    }

    @Override
    public Message processMessage(Message incomingMessage) throws ReceivingApplicationException, HL7Exception {
        Message response;
        Parser parser = incomingMessage.getParser();
        String responseString = null;
        String messageString;
        String responseType = null;
        String messageType;
        messageString = parser.encode(incomingMessage);
        Actor sutActor = null;

        if (incomingMessage instanceof ca.uhn.hl7v2.model.v251.message.OMI_O23) {
            messageType = "OMI^O23^OMI_O23";
            ca.uhn.hl7v2.model.v251.message.OMI_O23 omiMessage = (ca.uhn.hl7v2.model.v251.message.OMI_O23) incomingMessage;
            orderControlCode = omiMessage.getORDER().getORC().getOrderControl().getValue();
            if (orderControlCode.equals("SC") && simulatedActor.getKeyword().equals("TRO_FORWARDER")) {
                sutActor = Actor.findActorWithKeyword("TRO_FULFILLER", entityManager);
                response = forwardMessage(messageString, Actor.findActorWithKeyword("TRO_CREATOR", entityManager));
            } else if (!orderControlCode.equals("SC") && simulatedActor.getKeyword().equals("TRO_FORWARDER")) {
                sutActor = Actor.findActorWithKeyword("TRO_CREATOR", entityManager);
                response = forwardMessage(messageString, Actor.findActorWithKeyword("TRO_FULFILLER", entityManager));
            } else {
                response = processTeleRadiologyOrder(incomingMessage);
                sutActor = Actor.findActorWithKeyword("TRO_FORWARDER", entityManager);
            }
            messageType = messageType.concat(" (" + orderControlCode + ")");
        } else {
            messageType = MessageDecoder.buildMessageType(incomingMessage);
            try {
                response = incomingMessage.generateACK(AcknowledgmentCode.AR, new HL7Exception(
                        "Unsupported message type", ErrorCode.UNSUPPORTED_MESSAGE_TYPE));
            } catch (IOException e) {
                throw new HL7Exception(e.getMessage(), e);
            }
        }
        if (response != null) {
            responseType = MessageDecoder.buildMessageType(response);
        } else {
            log.error("response is null");
        }
        setSutActor(sutActor);
        setIncomingMessageType(messageType);
        setResponseMessageType(responseType);
        return response;
    }

    private Message processTeleRadiologyOrder(Message incomingMessage) throws HL7Exception {
        PipeParser pipeParser = PipeParser.getInstanceWithNoValidation();
        incomingMessage.setParser(pipeParser);
        OMI_O23 omiMessage = (OMI_O23) pipeParser.parseForSpecificPackage(incomingMessage.encode(),
                "net.ihe.gazelle.tro.hl7v2.model.v251.message");
        TeleRadiologyOrder selectedOrder = null;
        Patient selectedPatient = null;
        Encounter selectedEncounter = null;
        if (omiMessage.getPATIENT().getPID().isEmpty()) {
            return buildNack(omiMessage.getMSH(), sendingApplication, sendingFacility, receivingApplication, receivingFacility,
                    "O23", AcknowledgmentCode.AE, "PID segment is required but missing", Receiver.SEGMENT_SEQUENCE_ERROR,
                    Receiver.SEGMENT_SEQUENCE_ERROR, Receiver.SEVERITY_ERROR, messageControlId, hl7Charset, hl7Version, "OMI_O23");
        } else if (orderControlCode.equals("NW")) {
            Patient pidPatient = MessageDecoder.extractPatientFromPID(omiMessage.getPATIENT().getPID());
            List<PatientIdentifier> pidPatientIdentifiers = MessageDecoder
                    .extractPatientIdentifiersFromPID251(omiMessage);
            List<PatientIdentifier> identifiersToAdd = new ArrayList<PatientIdentifier>();
            for (PatientIdentifier pid : pidPatientIdentifiers) {
                PatientIdentifier existingPID = pid.getStoredPatientIdentifier(entityManager);
                if (existingPID != null) {
                    if (selectedPatient == null) {
                        selectedPatient = existingPID.getPatient();
                    } else if (selectedPatient.getId().equals(existingPID.getPatient().getId())) {
                        // OK: nothing to do
                        continue;
                    } else {
                        // error patients with same identifiers mismatch
                        return buildNack(omiMessage.getMSH(), sendingApplication, sendingFacility,
                                receivingApplication, receivingFacility, "O23", AcknowledgmentCode.AE,
                                "Patient identifiers are inconsistent", Receiver.DUPLICATE_KEY_IDENTIFIER,
                                Receiver.DUPLICATE_KEY_IDENTIFIER, Receiver.SEVERITY_ERROR, messageControlId,
                                hl7Charset, hl7Version, "PID-3");
                    }
                } else {
                    pid = pid.save(entityManager);
                    identifiersToAdd.add(pid);
                }
            }
            // if none of the identifiers is already known by the simulator, we save the patient contained in the message
            if (selectedPatient == null) {
                // save the patient
                selectedPatient = pidPatient.save(entityManager);
                selectedPatient.setCreator(sendingApplication + "_" + sendingFacility);
            }
            for (PatientIdentifier pid : identifiersToAdd) {
                pid.setPatient(selectedPatient);
                pid.save(entityManager);
            }
        }
        // check presence of PV1
        if (omiMessage.getPATIENT().getPATIENT_VISIT().getPV1().isEmpty()) {
            return buildNack(omiMessage.getMSH(), sendingApplication, sendingFacility, receivingApplication,
                    receivingFacility, "O23", AcknowledgmentCode.AE, "PV1 segment is required but missing",
                    Receiver.SEGMENT_SEQUENCE_ERROR, Receiver.SEGMENT_SEQUENCE_ERROR, Receiver.SEVERITY_ERROR,
                    messageControlId, hl7Charset, hl7Version, "OMI_O23/PATIENT");
        } else if (orderControlCode.equals("NW")) {
            selectedEncounter = MessageDecoder.extractEncounterFromPV1(omiMessage);
            if (selectedEncounter.getPatientClassCode() == null) {
                return buildNack(omiMessage.getMSH(), sendingApplication, sendingFacility, receivingApplication,
                        receivingFacility, "O23", AcknowledgmentCode.AE, "Patient class is required but missing",
                        Receiver.REQUIRED_FIELD_MISSING, Receiver.REQUIRED_FIELD_MISSING, Receiver.SEVERITY_ERROR,
                        messageControlId, hl7Charset, hl7Version, "PV1");
            } else {
                selectedEncounter.setPatient(selectedPatient);
                selectedEncounter = selectedEncounter.save(entityManager);
            }
        }
        if (omiMessage.getORDERReps() == 0) {
            return buildNack(omiMessage.getMSH(), sendingApplication, sendingFacility, receivingApplication,
                    receivingFacility, "O23", AcknowledgmentCode.AE, "ORDER group is required but missing",
                    Receiver.SEGMENT_SEQUENCE_ERROR, Receiver.SEGMENT_SEQUENCE_ERROR, Receiver.SEVERITY_ERROR,
                    messageControlId, hl7Charset, hl7Version, "OMI_O23");
        } else {
            for (OMI_O23_ORDER orderGroup : omiMessage.getORDERAll()) {
                String controlCode = orderGroup.getORC().getOrderControl().getValue();
                String status = orderGroup.getORC().getOrderStatus().getValue();
                log.info("order filler number: " + orderGroup.getORC().getFillerOrderNumber().encode());
                selectedOrder = TeleRadiologyOrder.getOrderByOrderFillerNumber(orderGroup.getORC()
                        .getFillerOrderNumber().encode(), simulatedActor, entityManager);
                if (controlCode.equals("NW")) {
                    if (selectedOrder != null) {
                        return buildNack(omiMessage.getMSH(), sendingApplication, sendingFacility,
                                receivingApplication, receivingFacility, "O23", AcknowledgmentCode.AE,
                                "A tele-radiology order with filler order number "
                                        + orderGroup.getORC().getFillerOrderNumber().encode() + " already exist",
                                Receiver.DUPLICATE_KEY_IDENTIFIER, Receiver.DUPLICATE_KEY_IDENTIFIER,
                                Receiver.SEVERITY_ERROR, messageControlId, hl7Charset, hl7Version, "ORC-3");
                    } else {
                        Message response = extractTROFromGroup(omiMessage.getMSH(), orderGroup, omiMessage.getZPS(),
                                selectedEncounter);
                        if (response != null) {
                            return response;
                        } else {
                            continue;
                        }
                    }
                } else if (controlCode.equals("OC")) {
                    if (selectedOrder == null) {
                        return buildNack(omiMessage.getMSH(), sendingApplication, sendingFacility,
                                receivingApplication, receivingFacility, "O23", AcknowledgmentCode.AE,
                                "Tele-Radiology order with filler order number "
                                        + orderGroup.getORC().getFillerOrderNumber().encode() + " is unknown",
                                Receiver.UNKNOWN_KEY_IDENTIFIER, Receiver.UNKNOWN_KEY_IDENTIFIER,
                                Receiver.SEVERITY_ERROR, messageControlId, hl7Charset, hl7Version, "ORC-3");
                    } else if (selectedOrder.getOrderStatus().equals("IP")) {
                        selectedOrder.setOrderControlCode("OC");
                        selectedOrder.setOrderStatus(orderGroup.getORC().getOrderStatus().getValue());
                        selectedOrder.save(entityManager);
                        continue;
                    } else {
                        return buildNack(omiMessage.getMSH(), sendingApplication, sendingFacility,
                                receivingApplication, receivingFacility, "O23", AcknowledgmentCode.AE, "Forbidden state transition",
                                Receiver.INTERNAL_ERROR, Receiver.INTERNAL_ERROR, Receiver.SEVERITY_ERROR,
                                messageControlId, hl7Charset, hl7Version, "ORC-5");
                    }
                } else if (controlCode.equals("SC")) {
                    if (selectedOrder == null) {
                        return buildNack(omiMessage.getMSH(), sendingApplication, sendingFacility,
                                receivingApplication, receivingFacility, "O23", AcknowledgmentCode.AE,
                                "Tele-Radiology order with filler order number "
                                        + orderGroup.getORC().getFillerOrderNumber().encode() + " is unknown",
                                Receiver.UNKNOWN_KEY_IDENTIFIER, Receiver.UNKNOWN_KEY_IDENTIFIER,
                                Receiver.SEVERITY_ERROR, messageControlId, hl7Charset, hl7Version, "ORC-3");
                    } else if ((selectedOrder.getOrderStatus().equals("IP") && status.equals("SC"))
                            || (selectedOrder.getOrderStatus().equals("SC") && (status.equals("DC") || status
                            .equals("CM")))) {
                        selectedOrder.setOrderControlCode("SC");
                        selectedOrder.setOrderStatus(status);
                        selectedOrder.setAbortReason(orderGroup.getOBR().getFillerField1().getValue());
                        selectedOrder.save(entityManager);
                        continue;
                    } else {
                        return buildNack(omiMessage.getMSH(), sendingApplication, sendingFacility,
                                receivingApplication, receivingFacility, "O23", AcknowledgmentCode.AE, "Forbidden state transition",
                                Receiver.INTERNAL_ERROR, Receiver.INTERNAL_ERROR, Receiver.SEVERITY_ERROR,
                                messageControlId, hl7Charset, hl7Version, "ORC-5");
                    }
                } else {
                    return buildNack(omiMessage.getMSH(), sendingApplication, sendingFacility, receivingApplication,
                            receivingFacility, "O23", AcknowledgmentCode.AE, controlCode + " is not a valid order control code",
                            Receiver.INTERNAL_ERROR, Receiver.INTERNAL_ERROR, Receiver.SEVERITY_ERROR,
                            messageControlId, hl7Charset, hl7Version, "ORC-1");
                }
            }
            // if we reach this line that means that everything went OK, sent an ACK (AA)
            return buildAck(omiMessage.getMSH(), AcknowledgmentCode.AA, messageControlId, hl7Charset, hl7Version);
        }
    }

    private Message forwardMessage(String incomingMessage, Actor sutActor) throws HL7Exception {
        if (receivingApplication != null) {
            log.info("Forwarding message to " + receivingApplication);
            HL7V2ResponderSUTConfiguration sut = new HL7V2ResponderSUTConfiguration();
            sut.setApplication(receivingApplication);
            sut.setFacility(receivingFacility);
            sut.setUrl(receivingApplication);
            sut.setMessageEncoding(MessageEncoding.XML);
            sut.setHl7Protocol(HL7Protocol.HTTP);
            // TODO set the usage metadata or at least we might need the actor
            Initiator initiator = new Initiator(sut, sendingFacility, sendingApplication, simulatedActor,
                    simulatedTransaction, incomingMessage, "OMI^O23^OMI_O23 (" + orderControlCode + ")", "SeHE", sutActor);
            initiator.setEntityManager(entityManager);
            Message response = initiator.sendMessageAndGetResponse();
            return response;
        } else {
            throw new HL7Exception(receivingApplication + " is either not a correct URL or not reachable");
        }
    }

    /**
     * @param group
     * @param zps               TODO
     * @param selectedEncounter
     * @return the NACK if something went wrong, null otherwise
     * @throws HL7Exception
     */
    private Message extractTROFromGroup(MSH incomingMSH, OMI_O23_ORDER group, ZPS zps, Encounter selectedEncounter)
            throws HL7Exception {
        Domain domain = Domain.getDomainByKeyword("SeHE", entityManager);
        TeleRadiologyOrder order = new TeleRadiologyOrder();
        order.setSimulatedActor(simulatedActor);
        order.setCreator(sendingApplication + "_" + sendingFacility);
        order.setDomain(domain);
        order.setLastChanged(new Date());
        order.setEncounter(selectedEncounter);
        ORC orc = group.getORC();
        order.setOrderControlCode(orc.getOrderControl().getValue());
        order.setPlacerOrderNumber(orc.getPlacerOrderNumber().encode());
        order.setFillerOrderNumber(orc.getFillerOrderNumber().encode());
        order.setOrderStatus(orc.getOrderStatus().getValue());
        order.setEnteredBy(orc.getEnteredBy(0).getIDNumber().getValue());
        order.setOrderingProvider(orc.getOrderingProvider(0).getXcn1_IDNumber().getValue());
        order.setEnteringOrganization(orc.getEnteringOrganization().getIdentifier().getValue());
        OBR obr = group.getOBR();
        order.setUniversalServiceId(obr.getUniversalServiceIdentifier().getIdentifier().getValue());
        order.setDangerCode(obr.getDangerCode().getIdentifier().getValue());
        order.setRelevantClinicalInfo(obr.getRelevantClinicalInformation().getValue());
        order.setCallBackPhoneNumber(obr.getOrderCallbackPhoneNumber(0).encode());
        order.setViews(obr.getFillerField1().getValue());
        order.setReasonForStudy(obr.getReasonForStudy(0).encode());
        order.setImagingStudySource(obr.getPlacerField2().getValue());
        order.setProcedureCode(obr.getProcedureCode().getIdentifier().getValue());
        order.setProcedureDescription(obr.getProcedureCode().getAlternateText().getValue());
        if (zps != null && zps.getRelatedSudiesAccessIdentifierReps() > 0) {
            order.setRelatedStudies(new ArrayList<String>());
            for (EI number : zps.getRelatedSudiesAccessIdentifier()) {
                order.getRelatedStudies().add(number.encode());
            }
        }
        if (group.getNTEReps() > 0) {
            NTE nte = group.getNTE();
            order.setComment(nte.getComment(0).getValue());
        }

        order = order.save(entityManager);
        log.info("order id : " + order.getId());
        if (group.getIPCReps() > 0) {
            for (IPC ipc : group.getIPCAll()) {
                TeleRadiologyProcedure procedure = new TeleRadiologyProcedure(simulatedActor, sendingApplication + "_"
                        + sendingFacility, domain);
                procedure.setTeleRadiologyOrder(order);
                procedure.setAccessionIdentifier(ipc.getAccessionIdentifier().encode());
                procedure.setRequestedProcedureId(ipc.getRequestedProcedureID().encode());
                procedure.setStudyInstanceUid(ipc.getStudyInstanceUID().encode());
                procedure.setModality(ipc.getModality().getIdentifier().getValue());
                procedure.setProtocolCode(ipc.getProtocolCode(0).encode());
                procedure.save(entityManager);
            }
        } else {
            return buildNack(incomingMSH, sendingApplication, sendingFacility, receivingApplication, receivingFacility,
                    "O23", AcknowledgmentCode.AE, "IPC segment is required but missing", Receiver.SEGMENT_SEQUENCE_ERROR,
                    Receiver.SEGMENT_SEQUENCE_ERROR, Receiver.SEVERITY_ERROR, messageControlId, hl7Charset, hl7Version,
                    "OMI_O23/ORDER");
        }
        return null;
    }

    public static Message buildAck(MSH incomingMessageHeader, AcknowledgmentCode ackCode, String messageControlId,
                                   String simulatorCharset, String hl7Version) throws HL7Exception {
        TROMessageBuilder generator = new TROMessageBuilder();
        return generator.buildAcknowledgement(incomingMessageHeader, ackCode, null, null, null, null, messageControlId,
                hl7Version, false, null);
    }

    public static Message buildNack(MSH incomingMessageHeader, String sendingApplication, String sendingFacility,
                                    String receivingApplication, String receivingFacility, String triggerEvent, AcknowledgmentCode ackCode,
                                    String userMessage, String errorType, String hl7ErrorCode, String severity, String messageControlId,
                                    String simulatorCharset, String hl7Version, String errorLocation) throws HL7Exception {
        TROMessageBuilder generator = new TROMessageBuilder();
        return generator.buildAcknowledgement(incomingMessageHeader, ackCode, userMessage, errorType, hl7ErrorCode,
                severity, messageControlId, hl7Version, true, errorLocation);
    }
}
