package net.ihe.gazelle.ordermanager.filter;

import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import net.ihe.gazelle.hql.criterion.QueryModifier;
import net.ihe.gazelle.ordermanager.model.AbstractOrder;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.common.tf.model.Domain;

/**
 * <b>Class Description : </b>AbstractOrderFilter<br>
 * <br>
 *
 * @author Anne-Gaelle Berge / IHE-Europe development Project
 * @version 1.0 - 30/06/16
 *
 *
 *
 */
public abstract class AbstractOrderFilter<T extends AbstractOrder> implements QueryModifier<T> {

    private String fillerOrderNumber;
    private String placerOrderNumber;
    private Actor actor;
    private Domain domain;
    private String visitnumber;
    private String patientid;
    private String newOrderControlCode;
    private Boolean scheduled;
    private Filter<T> filter;

    public AbstractOrderFilter(Domain domain, Actor simulatedActor, String newOrderControlCode,
                               Boolean isOrderAlreadyScheduled) {
        this.domain = domain;
        this.actor = simulatedActor;
        this.newOrderControlCode = newOrderControlCode;
        this.scheduled = isOrderAlreadyScheduled;
    }

    public Filter<T> getFilter(){
        if (filter == null){
            filter = new Filter<T>(getHQLCriterionsForFilter());
        }
        return filter;
    }

    protected abstract HQLCriterionsForFilter<T> getHQLCriterionsForFilter();

    public FilterDataModel<T> getOrders(){
        return new FilterDataModel<T>(getFilter()) {
            @Override
            protected Object getId(T t) {
                return getObjectId(t);
            }
        };
    }

    protected abstract Integer getObjectId(T t);

    public void reset(){
        fillerOrderNumber = null;
        placerOrderNumber = null;
        visitnumber = null;
        patientid = null;
        getFilter().clear();
    }

    public String getFillerOrderNumber() {
        return fillerOrderNumber;
    }

    public void setFillerOrderNumber(String fillerOrderNumber) {
        this.fillerOrderNumber = fillerOrderNumber;
    }

    public String getPlacerOrderNumber() {
        return placerOrderNumber;
    }

    public void setPlacerOrderNumber(String placerOrderNumber) {
        this.placerOrderNumber = placerOrderNumber;
    }

    public String getVisitnumber() {
        return visitnumber;
    }

    public void setVisitnumber(String visitnumber) {
        this.visitnumber = visitnumber;
    }

    public String getPatientid() {
        return patientid;
    }

    public void setPatientid(String patientid) {
        this.patientid = patientid;
    }

    public Actor getActor() {
        return actor;
    }

    public Domain getDomain() {
        return domain;
    }

    public String getNewOrderControlCode() {
        return newOrderControlCode;
    }

    public Boolean getScheduled() {
        return scheduled;
    }
}
