package net.ihe.gazelle.ordermanager.hl7.responders;

import net.ihe.gazelle.HL7Common.responder.AbstractServer;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.Startup;
import org.jboss.seam.annotations.Transactional;

import javax.ejb.DependsOn;


@Scope(ScopeType.APPLICATION)
@Name("orderResultTrackerServer")
@Startup(depends = {"entityManager"})
@DependsOn({"entityManager"})
public class OrderResultTrackerServer extends AbstractServer<OrderResultTracker> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4146460687716413786L;

    @Create
    @Override
    @Transactional
	public void startServers() {
		Actor simulatedActor = Actor.findActorWithKeyword("ORT", entityManager);
		handler = new OrderResultTracker();
		startServers(simulatedActor, null, null);
	}

}
