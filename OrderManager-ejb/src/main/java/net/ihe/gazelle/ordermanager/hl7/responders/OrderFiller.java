package net.ihe.gazelle.ordermanager.hl7.responders;

import ca.uhn.hl7v2.AcknowledgmentCode;
import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.model.Message;
import ca.uhn.hl7v2.model.Segment;
import ca.uhn.hl7v2.model.v25.message.OUL_R22;
import ca.uhn.hl7v2.model.v25.message.OUL_R23;
import ca.uhn.hl7v2.protocol.ReceivingApplicationException;
import ca.uhn.hl7v2.util.Terser;
import net.ihe.gazelle.HL7Common.messages.HL7MessageDecoder;
import net.ihe.gazelle.HL7Common.responder.IHEDefaultHandler;
import net.ihe.gazelle.ordermanager.model.AbstractOrder;
import net.ihe.gazelle.ordermanager.model.Container;
import net.ihe.gazelle.ordermanager.model.Encounter;
import net.ihe.gazelle.ordermanager.model.LabOrder;
import net.ihe.gazelle.ordermanager.model.Observation;
import net.ihe.gazelle.ordermanager.model.Patient;
import net.ihe.gazelle.ordermanager.model.PatientIdentifier;
import net.ihe.gazelle.ordermanager.model.Specimen;
import net.ihe.gazelle.ordermanager.utils.MessageDecoder;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.common.tf.model.Domain;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;
import org.jboss.seam.log.Log;
import org.jboss.seam.log.Logging;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Class description : <b>OrderFiller</b>
 * 
 * This class is the handler for messages received by the Order Filler
 * 
 *
 *
 * @author Anne-Gaëlle Bergé - INRIA Rennes/Bretagne Atlantique
 *
 * @version 1.0 - 2011, November 17th
 * 
 */
public class OrderFiller extends OrderReceiver {

	private static Log log = Logging.getLog(OrderFiller.class);


	@Override
	public IHEDefaultHandler newInstance() {
		return new OrderFiller();
	}

	@Override
	public Message processMessage(Message incomingMessage) throws ReceivingApplicationException, HL7Exception {
		Message response;
		orderControlCode = null;
		try {
			// check if we must accept the message
			if (domain.getKeyword().contains("LAB")) {
				response = Receiver.generateNack(incomingMessage, serverApplication, serverFacility,
						Arrays.asList("OML", "OUL"), Arrays.asList("O21", "O33", "O35", "R22", "R23"), hl7Charset);
			} else { // ADT messages are supported
				response = Receiver.generateNack(incomingMessage, serverApplication, serverFacility,
						Arrays.asList("ORM", "ADT", "OMG"), Arrays.asList("O01", "A01", "A04", "A08", "A40", "O19"), hl7Charset);
			}
			Actor sutActor;
			// if no NACK is generated, process the message
			if (messageType == null) {
				log.error("Message type is empty");
				// put something by default because sending_actor_id cannot be null in hl7_message table !!!!
				sutActor = Actor.findActorWithKeyword("UNKNOWN", entityManager);
			} else if (messageType.contains("ADT")) {
				sutActor = Actor.findActorWithKeyword("ADT", entityManager);
				if (response == null) {
					response = processMessageFromADT(incomingMessage);
				}
			} else if (messageType.contains("ORM") || messageType.contains("OMG")) {
				// RAD, CARD, EYE
				// process message ORM^O01^ORM_O01 or OMG^O19^OMG_O19
				if (response == null) {
					response = processRAD2Transaction(incomingMessage);
				}
				sutActor = Actor.findActorWithKeyword("OP", entityManager);
			} else if (messageType.contains("OML")) {
				if (response == null) {
					// LAB, PAT --> requires to determine the affinity domain of the OF
					if (domain.getKeyword().equals("PAT")) {
						// process message for PAT
						response = processPAT1Transaction(incomingMessage);
					} else {
						// process message for LAB
						response = processLAB1Transaction(incomingMessage);
					}
				}
				sutActor = Actor.findActorWithKeyword("OP", entityManager);
			} else if (messageType.contains("OUL")) {
				// process message OUL^R23^OUL_R23 or OUL^R22^OUL_R22
				if (response == null) {
					response = processLAB5Transaction(incomingMessage);
				}
				sutActor = Actor.findActorWithKeyword("AM", entityManager);
			} else {
				log.error("This kind of message is not supported, we should not reach this line !");
				// put something by default because sending_actor_id cannot be null in hl7_message table !!!!
				sutActor = Actor.findActorWithKeyword("OP", entityManager);
			}
			// in some cases, we performed a rollback before saving the message in order to remove all the
			// created order if the message is refused
			String inMessageType = MessageDecoder.buildMessageType(incomingMessage);
			if (orderControlCode != null) {
				inMessageType = inMessageType.concat(" (" + orderControlCode + ")");
				setIncomingMessageType(inMessageType);
			}
            setSutActor(sutActor);
            setResponseMessageType(HL7MessageDecoder.getMessageType(response));
		} catch (HL7Exception e) {
			throw new HL7Exception(e);
		} catch (Exception e) {
			throw new ReceivingApplicationException(e);
		}
		return response;
	}

	/**
	 * 
	 * @param message
	 * @return
	 */
	private Message processRAD2Transaction(Message message) {
		simulatedTransaction = Transaction.GetTransactionByKeyword("RAD-2", entityManager);
		return processRadiologyOrderMessage(message, "NW", "CA", null, "DC");
	}

	/**
	 * For processing LAB-1, as 3 messages can be sent, we need to add a parameter which indicates which message has to be processed
	 * 
	 * @param message
	 * @return
	 */
	private Message processLAB1Transaction(Message message) {
		log.info("processing OML message");
		simulatedTransaction = Transaction.GetTransactionByKeyword("LAB-1", entityManager);
		return processOMLMessage(message, "NW", "CA", null, null);
	}

	/**
	 * 
	 * @param message
	 * @return
	 */
	private Message processPAT1Transaction(Message message) {
		simulatedTransaction = Transaction.GetTransactionByKeyword("PAT-1", entityManager);
		return Receiver.buildAck(serverApplication, serverFacility, sendingApplication, sendingFacility, "O22", AcknowledgmentCode.AA,
				messageControlId, hl7Charset, hl7Version);
	}

	/**
	 * 
	 * @param message
	 * @return
	 */
	private Message processLAB5Transaction(Message message) {
		simulatedTransaction = Transaction.GetTransactionByKeyword("LAB-5", entityManager);
		if (message instanceof OUL_R22) {
			return processOULR22Message((OUL_R22) message);
		} else if (message instanceof OUL_R23) {
			return processOULR23Message((OUL_R23) message);
		} else {
			Terser t = new Terser(message);
			String triggerEvent;
			try {
				triggerEvent = t.get("./MSH-9-2");
				return Receiver.buildNack(serverApplication, serverFacility, sendingApplication, sendingFacility,
						triggerEvent, AcknowledgmentCode.AE, "Unsupported message structure", null, Receiver.INTERNAL_ERROR,
						Receiver.SEVERITY_ERROR, messageControlId, hl7Charset, hl7Version);
			} catch (HL7Exception e) {
				log.error("Cannot understand received trigger event");
				return null;
			}

		}
	}

	private Message processOULR23Message(OUL_R23 oulMessage) {
		Patient selectedPatient = null;
		String creator = sendingApplication + "_" + sendingFacility;
		Domain domain = Domain.getDomainByKeyword("LAB", entityManager);
		// extract patient
		if (!MessageDecoder.isEmpty(oulMessage.getPATIENT().getPID())) {
			Patient pidPatient = MessageDecoder.extractPatientFromPID(oulMessage.getPATIENT().getPID());
			List<PatientIdentifier> pidPatientIdentifiers;
			pidPatientIdentifiers = MessageDecoder.extractPatientIdentifiersFromMessage25(oulMessage);
			List<PatientIdentifier> identifiersToAdd = new ArrayList<PatientIdentifier>();
			for (PatientIdentifier pid : pidPatientIdentifiers) {
				PatientIdentifier existingPID = pid.getStoredPatientIdentifier(entityManager);
				if (existingPID != null) {
					if (selectedPatient == null) {
						selectedPatient = existingPID.getPatient();
					} else if (selectedPatient.getId().equals(existingPID.getPatient().getId())) {
						// OK: nothing to do
						continue;
					} else {
						// error patients with same identifiers mismatch
						// TODO create a good ACK
						return Receiver.buildNack(serverApplication, serverFacility, sendingApplication,
								sendingFacility, "R23", AcknowledgmentCode.AE, "The patient identifiers are inconsistent", null,
								Receiver.DUPLICATE_KEY_IDENTIFIER, Receiver.SEVERITY_ERROR, messageControlId,
								hl7Charset, hl7Version);
					}
				} else {
					pid = pid.save(entityManager);
					identifiersToAdd.add(pid);
				}
			}
			// if none of the identifiers is already known by the simulator, we save the patient contained in the message
			if (selectedPatient == null) {
				// save the patient
				pidPatient.setCreator(creator);
				selectedPatient = pidPatient.save(entityManager);
			}
			for (PatientIdentifier pid : identifiersToAdd) {
				pid.setPatient(selectedPatient);
				pid.save(entityManager);
			}
		}
		// then, extract encounter
		Encounter selectedEncounter = null;
		if ((selectedPatient != null) && !MessageDecoder.isEmpty(oulMessage.getVISIT().getPV1())) {
			// check existance of PV1 segment
			Encounter pv1Encounter = null;
			pv1Encounter = MessageDecoder.extractEncounterFromPV1(oulMessage);
			if (pv1Encounter.getVisitNumber() != null) {
				// check the encounter exists
				selectedEncounter = Encounter.getEncounterByVisitNumber(pv1Encounter.getVisitNumber(), entityManager);
				if ((selectedEncounter != null)
						&& (!selectedEncounter.getPatient().getId().equals(selectedPatient.getId()))) {
					return Receiver.buildNack(serverApplication, serverFacility, sendingApplication, sendingFacility,
							"R23", AcknowledgmentCode.AE, "This visit number is already used for another patient", null,
							Receiver.DUPLICATE_KEY_IDENTIFIER, Receiver.SEVERITY_ERROR, messageControlId, hl7Charset,
							hl7Version);
				}
				// if not rattach the encounter to the selected patient
				else if (selectedEncounter == null) {
					selectedEncounter = pv1Encounter;
					selectedEncounter.setPatient(selectedPatient);
					selectedEncounter.setCreator(creator);
					selectedEncounter = selectedEncounter.save(entityManager);
				}
				// else selectedEncounter is the one retrieved from the database
			} else {
				return Receiver.buildNack(serverApplication, serverFacility, sendingApplication, sendingFacility,
						"R23", AcknowledgmentCode.AE, "PV1-19 is required but empty", null, Receiver.REQUIRED_FIELD_MISSING,
						Receiver.SEVERITY_ERROR, messageControlId, hl7Charset, hl7Version);
			}
		} else {
			selectedEncounter = new Encounter();
			selectedEncounter.setPatient(selectedPatient);
			selectedEncounter.setCreator(creator);
			selectedEncounter = selectedEncounter.save(entityManager);
		}
		// extract specimens
		int nbOfSpecimens = oulMessage.getSPMOBXSACINVOBRORCNTETQ1TQ2OBXTCDSIDNTECTIReps();
		if (nbOfSpecimens > 0) {
			for (int specimenIndex = 0; specimenIndex < nbOfSpecimens; specimenIndex++) {
				// SPECIMEN begin
				Segment spm = oulMessage.getSPMOBXSACINVOBRORCNTETQ1TQ2OBXTCDSIDNTECTI(specimenIndex).getSPM();
				if (MessageDecoder.isEmpty(spm)) {
					return Receiver.buildNack(serverApplication, serverFacility, sendingApplication, sendingFacility,
							"R23", AcknowledgmentCode.AE, "SPM segment is required but missing in rep " + specimenIndex, null,
							Receiver.SEGMENT_SEQUENCE_ERROR, Receiver.SEVERITY_ERROR, messageControlId, hl7Charset,
							hl7Version);
				}
				Specimen messageSpecimen = MessageDecoder.extractSpecimenFromSegment(spm, creator, simulatedActor,
						domain);
				messageSpecimen.setWorkOrder(true);
				messageSpecimen.setMainEntity(true);
				Specimen selectedSpecimen = Specimen.getSpecimenByAssignedIdentifiersByActorByDomain(
						messageSpecimen.getPlacerAssignedIdentifier(), messageSpecimen.getFillerAssignedIdentifier(),
						simulatedActor, domain, entityManager, true);
				if (selectedSpecimen == null) {
					selectedSpecimen = messageSpecimen;
				}
				// Specimen observations
				int nbOfObservations = oulMessage.getSPMOBXSACINVOBRORCNTETQ1TQ2OBXTCDSIDNTECTI(specimenIndex)
						.getOBXReps();
				if (nbOfObservations > 0) {
					for (int obxIndex = 0; obxIndex < nbOfObservations; obxIndex++) {
						// OBSERVATION begin
						Segment obx = oulMessage.getSPMOBXSACINVOBRORCNTETQ1TQ2OBXTCDSIDNTECTI(specimenIndex).getOBX(
								obxIndex);
						Observation messageObservation = MessageDecoder.extractObservationFromOBX(obx, simulatedActor,
								domain, creator);
						if (selectedSpecimen.getId() != null) {
							List<Observation> observations = Observation.getObservationFiltered(entityManager,
									simulatedActor, domain, null, selectedSpecimen, null);
							if (observations != null) {
								selectedSpecimen.getObservations().remove(observations.get(0));
								messageObservation.setId(observations.get(0).getId());
							}
						}
						messageObservation.setSpecimen(selectedSpecimen);
						if (selectedSpecimen.getObservations() == null) {
							selectedSpecimen.setObservations(new ArrayList<Observation>());
						}
						selectedSpecimen.getObservations().add(messageObservation);
						// OBSERVATION end
					}
				}
				// containers
				int nbOfContainers = oulMessage.getSPMOBXSACINVOBRORCNTETQ1TQ2OBXTCDSIDNTECTI(specimenIndex)
						.getCONTAINERReps();
				if (nbOfContainers > 0) {
					for (int sacIndex = 0; sacIndex < nbOfContainers; sacIndex++) {
						// CONTAINER begin
						Segment sac = oulMessage.getSPMOBXSACINVOBRORCNTETQ1TQ2OBXTCDSIDNTECTI(specimenIndex)
								.getCONTAINER(sacIndex).getSAC();
						Container messageContainer = MessageDecoder.extractContainerFromSAC(sac, simulatedActor,
								selectedSpecimen, creator, domain);
						messageContainer.setSpecimen(selectedSpecimen);
						messageContainer.setMainEntity(true);
						messageContainer.setWorkOrder(true);
						Container selectedContainer = null;
						if (selectedSpecimen.getId() != null) {
							selectedContainer = Container.getContainerByIdentifierByActorByDomain(
									messageContainer.getContainerIdentifier(), simulatedActor, domain, entityManager,
									true);
							if ((selectedContainer != null) && (selectedContainer.getSpecimen() != null)
									&& !selectedContainer.getSpecimen().equals(selectedSpecimen)) {
								return Receiver.buildNack(serverApplication, serverFacility, sendingApplication,
										sendingFacility, "R23", AcknowledgmentCode.AE,
										"This container identifier is already used with a different specimen", null,
										Receiver.INTERNAL_ERROR, Receiver.SEVERITY_ERROR, messageControlId, hl7Charset,
										hl7Version);
							}
						}
						if (selectedContainer == null) {
							selectedContainer = messageContainer;
						}
						// related orders
						int nbOfOrders = oulMessage.getSPMOBXSACINVOBRORCNTETQ1TQ2OBXTCDSIDNTECTI(specimenIndex)
								.getCONTAINER(sacIndex).getORDERReps();
						if (nbOfOrders > 0) {
							for (int orderIndex = 0; orderIndex < nbOfOrders; orderIndex++) {
								// ORDER begin
								Segment obr = oulMessage.getSPMOBXSACINVOBRORCNTETQ1TQ2OBXTCDSIDNTECTI(specimenIndex)
										.getCONTAINER(sacIndex).getORDER(orderIndex).getOBR();
								Segment orc = oulMessage.getSPMOBXSACINVOBRORCNTETQ1TQ2OBXTCDSIDNTECTI(specimenIndex)
										.getCONTAINER(sacIndex).getORDER(orderIndex).getORC();
								Segment tq1 = oulMessage.getSPMOBXSACINVOBRORCNTETQ1TQ2OBXTCDSIDNTECTI(specimenIndex)
										.getCONTAINER(sacIndex).getORDER(orderIndex).getTIMING_QTY().getTQ1();
								if (MessageDecoder.isEmpty(obr)) {
									return Receiver.buildNack(serverApplication, serverFacility, sendingApplication,
											sendingFacility, "R23", AcknowledgmentCode.AE,
											"OBR segment is required but missing in SPECIMEN[" + specimenIndex
													+ "]/CONTAINER[" + sacIndex + "]/ORDER[" + orderIndex + "]", null,
											Receiver.SEGMENT_SEQUENCE_ERROR, Receiver.SEVERITY_ERROR, messageControlId,
											hl7Charset, hl7Version);
								} else {
									LabOrder messageOrder = MessageDecoder.extractLabOrderFromSegments(orc, obr, tq1,
											simulatedActor, domain, creator, selectedEncounter);
									messageOrder.setMainEntity(false);
									messageOrder.setWorkOrder(true);
									messageOrder.setContainer(selectedContainer);
									LabOrder selectedOrder = AbstractOrder.getOrderByNumbersByActor(LabOrder.class,
											messageOrder.getPlacerOrderNumber(), messageOrder.getFillerOrderNumber(),
											null, simulatedActor, entityManager, true);
									if (selectedOrder == null) {
										selectedOrder = messageOrder;
									}
									// observations
									nbOfObservations = oulMessage
											.getSPMOBXSACINVOBRORCNTETQ1TQ2OBXTCDSIDNTECTI(specimenIndex)
											.getCONTAINER(sacIndex).getORDER(orderIndex).getRESULTReps();
									if (nbOfObservations > 0) {
										for (int obsIndex = 0; obsIndex < nbOfObservations; obsIndex++) {
											// OBSERVATION begin
											Segment obx = oulMessage
													.getSPMOBXSACINVOBRORCNTETQ1TQ2OBXTCDSIDNTECTI(specimenIndex)
													.getCONTAINER(sacIndex).getORDER(orderIndex).getRESULT(obsIndex)
													.getOBX();
											Observation messageObservation = MessageDecoder.extractObservationFromOBX(
													obx, simulatedActor, domain, creator);
											if (selectedOrder.getId() != null) {
												List<Observation> observations = Observation.getObservationFiltered(
														entityManager, simulatedActor, domain, selectedOrder, null,
														null);
												if (observations != null) {
													selectedOrder.getObservations().remove(observations.get(0));
													messageObservation.setId(observations.get(0).getId());
												}
											}
											messageObservation.setOrder(selectedOrder);
											if (selectedOrder.getObservations() == null) {
												selectedOrder.setObservations(new ArrayList<Observation>());
											}
											selectedOrder.getObservations().add(messageObservation);
											// OBSERVATION end
										}
									}
									if ((selectedOrder.getId() != null) && (selectedContainer.getId() != null)
											&& (selectedContainer.getOrders() != null)
											&& selectedContainer.getOrders().contains(selectedOrder)) {
										int currentOrderIndex = selectedContainer.getOrders().indexOf(selectedOrder);
										selectedContainer.getOrders().set(currentOrderIndex, selectedOrder);
									} else if (selectedContainer.getOrders() != null) {
										selectedContainer.getOrders().add(selectedOrder);
									} else {
										selectedContainer.setOrders(new ArrayList<LabOrder>());
										selectedContainer.getOrders().add(selectedOrder);
									}
								}
								// ORDER end
							}
							// CONTAINER end
							if (selectedSpecimen.getContainers() != null) {
								selectedSpecimen.getContainers().add(selectedContainer);
							} else {
								selectedSpecimen.setContainers(new ArrayList<Container>());
							}
						} else {
							return Receiver.buildNack(serverApplication, serverFacility, sendingApplication,
									sendingFacility, "R23", AcknowledgmentCode.AE, "ORDER group is required but missing in SPECIMEN["
											+ specimenIndex + "]/CONTAINER[" + sacIndex + "]", null,
									Receiver.SEGMENT_SEQUENCE_ERROR, Receiver.SEVERITY_ERROR, messageControlId,
									hl7Charset, hl7Version);
						}
					}
				} else {
					return Receiver.buildNack(serverApplication, serverFacility, sendingApplication, sendingFacility,
							"R23", AcknowledgmentCode.AE, "CONTAINER group is required but missing in rep " + specimenIndex
									+ " of SPECIMEN group", null, Receiver.SEGMENT_SEQUENCE_ERROR,
							Receiver.SEVERITY_ERROR, messageControlId, hl7Charset, hl7Version);
				}
				// SPECIMEN end
				selectedSpecimen.save(entityManager);
			}
		}
		return Receiver.buildAck(serverApplication, serverFacility, sendingApplication, sendingFacility, "R23", AcknowledgmentCode.AA,
				messageControlId, hl7Charset, hl7Version);
	}

}
