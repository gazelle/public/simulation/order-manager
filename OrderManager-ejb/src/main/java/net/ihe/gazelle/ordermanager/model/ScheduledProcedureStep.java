package net.ihe.gazelle.ordermanager.model;

import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.common.tf.model.Domain;
import org.jboss.seam.annotations.Name;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Entity
@Name("scheduledProcedureStep")
@Table(name = "om_scheduled_procedure_step", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = "id"))
@SequenceGenerator(name = "om_scheduled_procedure_step_sequence", sequenceName = "om_scheduled_procedure_step_id_seq", allocationSize = 1)
public class ScheduledProcedureStep extends CommonColumns implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4041197955440578025L;

	@Id
	@NotNull
	@GeneratedValue(generator = "om_scheduled_procedure_step_sequence", strategy = GenerationType.SEQUENCE)
	@Column(name = "id", nullable = false, unique = true)
	private Integer id;

	/**
	 * (0040,0002) and (0040,0003) and ORC-7-4
	 */
	@Column(name = "sps_start_date_time")
	@Temporal(TemporalType.TIMESTAMP)
	private Date spsStartDateTime;

	/**
	 * (0040,0007)
	 */
	@Column(name = "sps_description")
	private String spsDescription;

	/**
	 * OBR-24 and (0040,0001)
	 */
	@Column(name = "modality")
	private String modality;

	/**
	 * OBR-20
	 */
	@Column(name = "scheduled_procedure_id")
	private String scheduledProcedureID;

	@ManyToOne
	@JoinColumn(name = "requested_procedure_id")
	private RequestedProcedure requestedProcedure;

	@OneToMany(targetEntity = ProtocolItem.class, mappedBy = "scheduledProcedureStep", cascade = CascadeType.MERGE)
	private List<ProtocolItem> protocolItems;

	@OneToOne(targetEntity = WorklistEntry.class, mappedBy = "scheduledProcedureStep", cascade = CascadeType.MERGE)
	private WorklistEntry worklist;

	public ScheduledProcedureStep() {
		super();
	}

	public ScheduledProcedureStep(Actor simulatedActor, String creator, Domain domain) {
		super(simulatedActor, creator, domain);
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getSpsStartDateTime() {
        if (spsStartDateTime != null) {
            return (Date) spsStartDateTime.clone();
        } else {
            return null;
        }
	}

	public void setSpsStartDateTime(Date spsStartDateTime) {
		if (spsStartDateTime != null) {
			this.spsStartDateTime = (Date) spsStartDateTime.clone();
		} else {
            this.spsStartDateTime = null;
        }
	}

	public String getSpsDescription() {
		return spsDescription;
	}

	public void setSpsDescription(String spsDescription) {
		this.spsDescription = spsDescription;
	}

	public String getScheduledProcedureID() {
		return scheduledProcedureID;
	}

	public void setScheduledProcedureID(String scheduledProcedureID) {
		this.scheduledProcedureID = scheduledProcedureID;
	}

	public RequestedProcedure getRequestedProcedure() {
		return requestedProcedure;
	}

	public void setRequestedProcedure(RequestedProcedure requestedProcedure) {
		this.requestedProcedure = requestedProcedure;
	}

	public List<ProtocolItem> getProtocolItems() {
		return protocolItems;
	}

	public void setProtocolItems(List<ProtocolItem> protocolItems) {
		this.protocolItems = protocolItems;
	}

	public String getModality() {
		return modality;
	}

	public void setModality(String modality) {
		this.modality = modality;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = (prime * result) + ((scheduledProcedureID == null) ? 0 : scheduledProcedureID.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		ScheduledProcedureStep other = (ScheduledProcedureStep) obj;
		if (scheduledProcedureID == null) {
			if (other.scheduledProcedureID != null) {
				return false;
			}
		} else if (!scheduledProcedureID.equals(other.scheduledProcedureID)) {
			return false;
		}
		return true;
	}

	public void setWorklist(WorklistEntry worklist) {
		this.worklist = worklist;
	}

	public WorklistEntry getWorklist() {
		return worklist;
	}
}
