package net.ihe.gazelle.ordermanager.action;

import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.ordermanager.model.Encounter;
import net.ihe.gazelle.ordermanager.model.RequestedProcedure;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import java.io.Serializable;
import java.util.Map;

@Name("requestedProcedureManager")
@Scope(ScopeType.PAGE)
public class RequestedProcedureManager implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6062444855916641930L;

    private static Logger log = LoggerFactory.getLogger(RequestedProcedureManager.class);
    private RequestedProcedure selectedProcedure;
	private Encounter selectedEncounter;

    @Create
	public void selectRequestedProcedureById() {
		Map<String, String> urlParams = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		if ((urlParams != null) && urlParams.containsKey("id")) {
			try {
				Integer procedureId = Integer.valueOf(urlParams.get("id"));
				EntityManager entityManager = EntityManagerService.provideEntityManager();
				selectedProcedure = entityManager.find(RequestedProcedure.class, procedureId);
				selectedEncounter = entityManager.find(Encounter.class, selectedProcedure.getOrder().getEncounter()
						.getId());
			} catch (NumberFormatException e) {
				log.error("invalid id for procedure");
				FacesMessages.instance().add(urlParams.get("id") + " is not a valid parameter");
				selectedProcedure = null;
			} catch (NullPointerException e) {
				log.error("Some attributes are null");
				selectedEncounter = null;
			}
		}
	}

	public RequestedProcedure getSelectedProcedure() {
		return selectedProcedure;
	}

	public Encounter getSelectedEncounter() {
		return selectedEncounter;
	}

}
