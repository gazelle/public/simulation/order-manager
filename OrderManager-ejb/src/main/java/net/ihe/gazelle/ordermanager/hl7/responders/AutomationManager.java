package net.ihe.gazelle.ordermanager.hl7.responders;

import ca.uhn.hl7v2.AcknowledgmentCode;
import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.model.Message;
import ca.uhn.hl7v2.model.Segment;
import ca.uhn.hl7v2.parser.PipeParser;
import ca.uhn.hl7v2.protocol.ReceivingApplicationException;
import ca.uhn.hl7v2.util.Terser;
import net.ihe.gazelle.HL7Common.messages.HL7MessageDecoder;
import net.ihe.gazelle.HL7Common.responder.IHEDefaultHandler;
import net.ihe.gazelle.HL7Common.utils.TransactionInstanceUtil;
import net.ihe.gazelle.oml.hl7v2.model.v25.message.OML_O21;
import net.ihe.gazelle.oml.hl7v2.model.v25.message.OML_O33;
import net.ihe.gazelle.oml.hl7v2.model.v25.message.OML_O35;
import net.ihe.gazelle.ordermanager.model.AbstractOrder;
import net.ihe.gazelle.ordermanager.model.Container;
import net.ihe.gazelle.ordermanager.model.Encounter;
import net.ihe.gazelle.ordermanager.model.LabOrder;
import net.ihe.gazelle.ordermanager.model.Patient;
import net.ihe.gazelle.ordermanager.model.PatientIdentifier;
import net.ihe.gazelle.ordermanager.model.Specimen;
import net.ihe.gazelle.ordermanager.utils.MessageDecoder;
import net.ihe.gazelle.ordermanager.utils.ORLMessageBuilder;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.common.tf.model.Domain;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;
import net.ihe.gazelle.simulator.message.model.TransactionInstance;
import org.jboss.seam.log.Log;
import org.jboss.seam.log.Logging;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * Class description : <b>AutomationManager</b>
 * <p/>
 * This class is the handler for messages received by the Automation Manager in the context of the LAB-4 transaction
 *
 * @author Anne-Gaëlle Bergé - INRIA Rennes/Bretagne Atlantique
 * @version 1.0 - 2012, February 20th
 *
 *
 *
 */
public class AutomationManager extends IHEDefaultHandler {

    private static Log log = Logging.getLog(AutomationManager.class);

    @Override
    public IHEDefaultHandler newInstance() {
        return new AutomationManager();
    }

    @Override
    public Message processMessage(Message incomingMessage) throws ReceivingApplicationException, HL7Exception {
        simulatedTransaction = Transaction.GetTransactionByKeyword("LAB-4", entityManager);
        domain = Domain.getDomainByKeyword("LAB", entityManager);
        Message response;
        try {
            // check if we must accept the message
            response = Receiver.generateNack(incomingMessage, serverApplication, serverFacility, Arrays.asList("OML"),
                    Arrays.asList("O21", "O33", "O35"), hl7Charset);

            // if no NACK is generated, process the message
            if (response == null) {
                if (messageType == null) {
                    log.error("Message type is empty");
                } else if (messageType.contains("OML")) {
                    response = processLAB4Transaction(incomingMessage);
                } else {
                    log.error("This kind of message is not supported, we should not reach this line !");
                }
            }
            setSutActor(Actor.findActorWithKeyword("OF", entityManager));
            setResponseMessageType(HL7MessageDecoder.getMessageType(response));
            return response;
        } catch (Exception e) {
            throw new HL7Exception(e);
        }
    }

    /**
     * @param incomingMessage
     * @return
     */
    private Message processLAB4Transaction(Message incomingMessage) {
        // PATIENT group is optional for all three message structures, if present, extract patient and eventually encounter
        ORLMessageBuilder orlBuilder = new ORLMessageBuilder(serverApplication, serverFacility, sendingApplication,
                sendingFacility, messageControlId, hl7Charset);
        Encounter selectedEncounter = null;
        String creator = sendingApplication + "_" + sendingFacility;
        if (MessageDecoder.isSegmentPresent(incomingMessage, "PID")) {
            Patient pidPatient ;
            try {
                Terser terser = new Terser(incomingMessage);
                Segment pid = terser.getSegment("/.PID");
                pidPatient = MessageDecoder.extractPatientFromPID(pid);
            } catch (HL7Exception e) {
                log.error(e.getMessage(), e);
                return orlBuilder.createNack(incomingMessage, AcknowledgmentCode.AE, Receiver.SEGMENT_SEQUENCE_ERROR,
                        Receiver.SEVERITY_ERROR, "PID segment is missing", null);
            }
            Patient selectedPatient = null;
            List<PatientIdentifier> pidPatientIdentifiers;
            pidPatientIdentifiers = MessageDecoder.extractPatientIdentifiersFromMessage25(incomingMessage);
            List<PatientIdentifier> identifiersToAdd = new ArrayList<PatientIdentifier>();
            for (PatientIdentifier pid : pidPatientIdentifiers) {
                PatientIdentifier existingPID = pid.getStoredPatientIdentifier(entityManager);
                if (existingPID != null) {
                    if (selectedPatient == null) {
                        selectedPatient = existingPID.getPatient();
                    } else if (selectedPatient.getId().equals(existingPID.getPatient().getId())) {
                        // OK: nothing to do
                        continue;
                    } else {
                        // error patients with same identifiers mismatch
                        // TODO create a good ACK
                        return orlBuilder.createNack(incomingMessage, AcknowledgmentCode.AE, Receiver.DUPLICATE_KEY_IDENTIFIER,
                                Receiver.SEVERITY_ERROR, "The patient identifiers are inconsistent", null);
                    }
                } else {
                    pid = pid.save(entityManager);
                    identifiersToAdd.add(pid);
                }
            }
            // if none of the identifiers is already known by the simulator, we save the patient contained in the message
            if (selectedPatient == null) {
                // save the patient
                pidPatient.setCreator(creator);
                selectedPatient = pidPatient.save(entityManager);
            }
            for (PatientIdentifier pid : identifiersToAdd) {
                pid.setPatient(selectedPatient);
                pid.save(entityManager);
            }
            // check existance of PV1 segment
            Encounter pv1Encounter = null;
            if (MessageDecoder.isSegmentPresent(incomingMessage, "PV1")) {
                pv1Encounter = MessageDecoder.extractEncounterFromPV1(incomingMessage);
                if (pv1Encounter.getVisitNumber() != null) {
                    // check the encounter exists
                    selectedEncounter = Encounter.getEncounterByVisitNumber(pv1Encounter.getVisitNumber(),
                            entityManager);
                    if ((selectedEncounter != null)
                            && (!selectedEncounter.getPatient().getId().equals(selectedPatient.getId()))) {
                        entityManager.getTransaction().rollback();
                        return orlBuilder.createNack(incomingMessage, AcknowledgmentCode.AE, Receiver.DUPLICATE_KEY_IDENTIFIER,
                                Receiver.SEVERITY_ERROR, "This visit number is already used for another patient", null);
                    } else if (selectedEncounter == null) {
                        selectedEncounter = pv1Encounter;
                        selectedEncounter.setPatient(selectedPatient);
                        selectedEncounter.setCreator(creator);
                        selectedEncounter = selectedEncounter.save(entityManager);
                    }
                    // else selectedEncounter is the one retrieved from the database
                } else {
                    entityManager.getTransaction().rollback();
                    return orlBuilder.createNack(incomingMessage, AcknowledgmentCode.AE, Receiver.REQUIRED_FIELD_MISSING,
                            Receiver.SEVERITY_ERROR, "PV1-19 is required but missing", null);
                }
            } else {
                log.warn("No PV1 segment found !");
                selectedEncounter = new Encounter();
                // even if no encounter has been found, we need to create one in order to link the work order to the patient
                selectedEncounter.setPatient(selectedPatient);
                selectedEncounter.setCreator(creator);
                selectedEncounter = selectedEncounter.save(entityManager);
            }
        }
        if (incomingMessage instanceof ca.uhn.hl7v2.model.v25.message.OML_O21) {
            return parseOMLO21(incomingMessage, orlBuilder, creator, selectedEncounter);
        } else if (incomingMessage instanceof ca.uhn.hl7v2.model.v25.message.OML_O33) {
            return parseOMLO33(incomingMessage, orlBuilder, creator, selectedEncounter);
        } else if (incomingMessage instanceof ca.uhn.hl7v2.model.v25.message.OML_O35) {
            return parseOMLO35(incomingMessage, orlBuilder, creator, selectedEncounter);
        } else {
            return orlBuilder.createNack(incomingMessage, AcknowledgmentCode.AE, Receiver.INTERNAL_ERROR, Receiver.SEVERITY_ERROR,
                    "This message structure is not allowed", null);
        }
    }

    private Message parseOMLO35(Message incomingMessage, ORLMessageBuilder orlBuilder, String creator,
                                Encounter selectedEncounter) {
        PipeParser pipeParser = new PipeParser();
        // cast message usign the class generated using the IHE HL7 message profile
        OML_O35 omlMessage;
        try {
            omlMessage = (OML_O35) pipeParser.parseForSpecificPackage(incomingMessage.encode(),
                    "net.ihe.gazelle.oml.hl7v2.model.v25.message");
        } catch (HL7Exception e) {
            entityManager.getTransaction().rollback();
            return orlBuilder.createNack(incomingMessage, AcknowledgmentCode.AE, Receiver.INTERNAL_ERROR, Receiver.SEVERITY_ERROR,
                    "Cannot properly encode the message for parsing", null);
        }
        int nbOfSpecimens = omlMessage.getSPECIMENReps();
        if (nbOfSpecimens > 0) {
            for (int specimenIndex = 0; specimenIndex < nbOfSpecimens; specimenIndex++) {
                Segment spm = omlMessage.getSPECIMEN(specimenIndex).getSPM();
                if (MessageDecoder.isEmpty(spm)) {
                    entityManager.getTransaction().rollback();
                    return orlBuilder.createNack(incomingMessage, AcknowledgmentCode.AE, Receiver.SEGMENT_SEQUENCE_ERROR,
                            Receiver.SEVERITY_ERROR, "SPM segment is required but missing in SPECIMEN[" + specimenIndex
                                    + "]", null);
                } else {
                    Specimen messageSpecimen = MessageDecoder.extractSpecimenFromSegment(spm, creator, simulatedActor,
                            domain);
                    messageSpecimen.setMainEntity(true);
                    messageSpecimen.setWorkOrder(true);
                    Specimen selectedSpecimen = Specimen.getSpecimenByAssignedIdentifiersByActorByDomain(
                            messageSpecimen.getPlacerAssignedIdentifier(),
                            messageSpecimen.getFillerAssignedIdentifier(), simulatedActor, domain, entityManager, true);
                    if (selectedSpecimen != null) {
                        messageSpecimen = selectedSpecimen;
                    }
                    int nbOfContainers = omlMessage.getSPECIMEN(specimenIndex).getSPECIMEN_CONTAINERReps();
                    if (nbOfContainers > 0) {
                        List<Container> containers = new ArrayList<Container>();
                        for (int containerIndex = 0; containerIndex < nbOfContainers; containerIndex++) {
                            Segment sac = omlMessage.getSPECIMEN(specimenIndex).getSPECIMEN_CONTAINER(containerIndex)
                                    .getSAC();
                            if (!MessageDecoder.isEmpty(sac)) {
                                Container messageContainer = MessageDecoder.extractContainerFromSAC(sac,
                                        simulatedActor, messageSpecimen, creator, domain);
                                messageContainer.setMainEntity(true);
                                messageContainer.setWorkOrder(true);
                                // check if this container already exists in the database
                                Container selectedContainer = Container.getContainerByIdentifierByActorByDomain(
                                        messageContainer.getContainerIdentifier(), simulatedActor, domain,
                                        entityManager, true);
                                if (selectedContainer != null) {
                                    messageContainer = selectedContainer;
                                }
                                int nbOfOrders = omlMessage.getSPECIMEN(specimenIndex)
                                        .getSPECIMEN_CONTAINER(containerIndex).getORDERReps();
                                if (nbOfOrders > 0) {
                                    List<LabOrder> workOrders = new ArrayList<LabOrder>();
                                    for (int orderIndex = 0; orderIndex < nbOfOrders; orderIndex++) {
                                        Segment orc = omlMessage.getSPECIMEN(specimenIndex)
                                                .getSPECIMEN_CONTAINER(containerIndex).getORDER(orderIndex).getORC();
                                        if (MessageDecoder.isEmpty(orc)) {
                                            entityManager.getTransaction().rollback();
                                            return orlBuilder.createNack(incomingMessage, AcknowledgmentCode.AE,
                                                    Receiver.SEGMENT_SEQUENCE_ERROR, Receiver.SEVERITY_ERROR,
                                                    "ORC segment is required but missing in SPECIMEN[" + specimenIndex
                                                            + "]/ORDER[" + orderIndex + "]", null);
                                        } else {
                                            Segment tq1 = omlMessage.getSPECIMEN(specimenIndex)
                                                    .getSPECIMEN_CONTAINER(containerIndex).getORDER(orderIndex)
                                                    .getTIMING().getTQ1();
                                            Segment obr = omlMessage.getSPECIMEN(specimenIndex)
                                                    .getSPECIMEN_CONTAINER(containerIndex).getORDER(orderIndex)
                                                    .getOBSERVATION_REQUEST().getOBR();
                                            if (MessageDecoder.isEmpty(tq1)) {
                                                tq1 = null;
                                            }
                                            if (MessageDecoder.isEmpty(obr)) {
                                                obr = null;
                                            }
                                            LabOrder messageOrder = MessageDecoder.extractLabOrderFromSegments(orc,
                                                    obr, tq1, simulatedActor, domain, creator, selectedEncounter);
                                            messageOrder.setMainEntity(false);
                                            messageOrder.setWorkOrder(true);
                                            messageOrder.setContainer(messageContainer);
                                            LabOrder selectedOrder = AbstractOrder.getOrderByNumbersByActor(
                                                    LabOrder.class, messageOrder.getPlacerOrderNumber(),
                                                    messageOrder.getFillerOrderNumber(), null, simulatedActor,
                                                    entityManager, true);
                                            if (messageOrder.getOrderControlCode() == null) {
                                                entityManager.getTransaction().rollback();
                                                return orlBuilder.createNack(incomingMessage, AcknowledgmentCode.AE,
                                                        Receiver.REQUIRED_FIELD_MISSING, Receiver.SEVERITY_ERROR,
                                                        "ORC-1 is required but missing in SPECIMEN[" + specimenIndex
                                                                + "]/ORDER[" + orderIndex + "]/ORC", null);
                                            } else if (messageOrder.getOrderControlCode().equals("NW")) {
                                                if (selectedOrder != null) {
                                                    entityManager.getTransaction().rollback();
                                                    return orlBuilder.createNack(incomingMessage, AcknowledgmentCode.AE,
                                                            Receiver.DUPLICATE_KEY_IDENTIFIER, Receiver.SEVERITY_ERROR,
                                                            "A work order with those numbers is already registered",
                                                            null);
                                                } else {
                                                    workOrders.add(messageOrder);
                                                    continue;
                                                }
                                            } else if (messageOrder.getOrderControlCode().equals("CA")) {
                                                if (selectedOrder == null) {
                                                    entityManager.getTransaction().rollback();
                                                    return orlBuilder.createNack(incomingMessage, AcknowledgmentCode.AE,
                                                            Receiver.UNKNOWN_KEY_IDENTIFIER, Receiver.SEVERITY_ERROR,
                                                            "Unknown work order", null);
                                                } else {
                                                    selectedOrder.setOrderControlCode("CA");
                                                    selectedOrder.setLastChanged(new Date());
                                                    selectedOrder.save(entityManager);
                                                    continue;
                                                }
                                            } else {
                                                entityManager.getTransaction().rollback();
                                                return orlBuilder.createNack(incomingMessage, AcknowledgmentCode.AE,
                                                        Receiver.TABLE_VALUE_NOT_FOUND, Receiver.SEVERITY_ERROR,
                                                        "Invalid value of ORC-1 is required but missing in SPECIMEN["
                                                                + specimenIndex + "]/ORDER[" + orderIndex + "]/ORC-1",
                                                        null);
                                            }

                                        }
                                    }
                                    if (messageContainer.getOrders() == null) {
                                        messageContainer.setOrders(workOrders);
                                    } else {
                                        for (LabOrder wo : workOrders) {
                                            if (!messageContainer.getOrders().contains(wo)) {
                                                messageContainer.getOrders().add(wo);
                                            }
                                        }
                                    }
                                    containers.add(messageContainer);
                                }

                            }
                            if (messageSpecimen.getContainers() != null) {
                                for (Container container : containers) {
                                    if (!messageSpecimen.getContainers().contains(container)) {
                                        messageSpecimen.getContainers().add(container);
                                    }
                                }
                            } else {
                                messageSpecimen.setContainers(containers);
                            }
                        }
                    } else {
                        entityManager.getTransaction().rollback();
                        return orlBuilder.createNack(incomingMessage, AcknowledgmentCode.AE, Receiver.SEGMENT_SEQUENCE_ERROR,
                                Receiver.SEVERITY_ERROR, "CONTAINER must have at least one occurrence (see SPECIMEN["
                                        + specimenIndex + "]", null);
                    }

                    messageSpecimen.save(entityManager);
                }
            }
        } else {
            entityManager.getTransaction().rollback();
            return orlBuilder.createNack(incomingMessage, AcknowledgmentCode.AE, Receiver.INTERNAL_ERROR, Receiver.SEVERITY_ERROR,
                    "SPECIMEN group must have at least one occurrence", null);
        }
        return orlBuilder.createAck(incomingMessage, simulatedActor.getKeyword());
    }

    /**
     * @param incomingMessage
     * @param orlBuilder
     * @param creator
     * @param selectedEncounter
     * @return
     */
    private Message parseOMLO33(Message incomingMessage, ORLMessageBuilder orlBuilder, String creator,
                                Encounter selectedEncounter) {
        PipeParser pipeParser = new PipeParser();
        // cast message using the class generated using the IHE HL7 message profile
        OML_O33 omlMessage;
        try {
            omlMessage = (OML_O33) pipeParser.parseForSpecificPackage(incomingMessage.encode(),
                    "net.ihe.gazelle.oml.hl7v2.model.v25.message");
        } catch (HL7Exception e) {
            entityManager.getTransaction().rollback();
            return orlBuilder.createNack(incomingMessage, AcknowledgmentCode.AE, Receiver.INTERNAL_ERROR, Receiver.SEVERITY_ERROR,
                    "Cannot properly encode the message for parsing", null);
        }
        int nbOfSpecimens = omlMessage.getSPECIMENReps();
        if (nbOfSpecimens > 0) {
            for (int specimenIndex = 0; specimenIndex < nbOfSpecimens; specimenIndex++) {
                Segment spm = omlMessage.getSPECIMEN(specimenIndex).getSPM();
                if (MessageDecoder.isEmpty(spm)) {
                    entityManager.getTransaction().rollback();
                    return orlBuilder.createNack(incomingMessage, AcknowledgmentCode.AE, Receiver.INTERNAL_ERROR,
                            Receiver.SEVERITY_ERROR, "SPM segment is required but missing in SPECIMEN[" + specimenIndex
                                    + "]", null);
                } else {
                    Specimen messageSpecimen = MessageDecoder.extractSpecimenFromSegment(spm, creator, simulatedActor,
                            domain);
                    messageSpecimen.setMainEntity(true);
                    messageSpecimen.setWorkOrder(true);
                    Specimen selectedSpecimen = Specimen.getSpecimenByAssignedIdentifiersByActorByDomain(
                            messageSpecimen.getPlacerAssignedIdentifier(),
                            messageSpecimen.getFillerAssignedIdentifier(), simulatedActor, domain, entityManager, true);
                    if (selectedSpecimen != null) {
                        log.info("specimen found !");
                        messageSpecimen = selectedSpecimen;
                    }
                    int nbOfContainers = omlMessage.getSPECIMEN(specimenIndex).getSACReps();
                    if (nbOfContainers > 0) {
                        List<Container> containers = new ArrayList<Container>();
                        for (int containerIndex = 0; containerIndex < nbOfContainers; containerIndex++) {
                            Segment sac = omlMessage.getSPECIMEN(specimenIndex).getSAC(containerIndex);
                            if (!MessageDecoder.isEmpty(sac)) {
                                Container messageContainer = MessageDecoder.extractContainerFromSAC(sac,
                                        simulatedActor, messageSpecimen, creator, domain);
                                messageContainer.setMainEntity(false);
                                messageContainer.setWorkOrder(true);
                                Container selectedContainer = Container.getContainerByIdentifierByActorByDomain(
                                        messageContainer.getContainerIdentifier(), simulatedActor, domain,
                                        entityManager, true);
                                if (selectedContainer != null) {
                                    log.info("container found !!!!");
                                    messageContainer = selectedContainer;
                                }
                                containers.add(messageContainer);
                            }
                        }
                        if (messageSpecimen.getContainers() != null) {
                            for (Container container : containers) {
                                if (!messageSpecimen.getContainers().contains(container)) {
                                    log.info("Container is not linked to the specimen yet");
                                    messageSpecimen.getContainers().add(container);
                                }
                            }
                        } else {
                            messageSpecimen.setContainers(containers);
                        }
                    }
                    int nbOfOrders = omlMessage.getSPECIMEN(specimenIndex).getORDERReps();
                    if (nbOfOrders > 0) {
                        List<LabOrder> workOrders = new ArrayList<LabOrder>();
                        for (int orderIndex = 0; orderIndex < nbOfOrders; orderIndex++) {
                            Segment orc = omlMessage.getSPECIMEN(specimenIndex).getORDER(orderIndex).getORC();
                            if (MessageDecoder.isEmpty(orc)) {
                                entityManager.getTransaction().rollback();
                                return orlBuilder.createNack(incomingMessage, AcknowledgmentCode.AE, Receiver.SEGMENT_SEQUENCE_ERROR,
                                        Receiver.SEVERITY_ERROR, "ORC segment is required but missing in SPECIMEN["
                                                + specimenIndex + "]/ORDER[" + orderIndex + "]", null);
                            } else {
                                Segment tq1 = omlMessage.getSPECIMEN(specimenIndex).getORDER(orderIndex).getTIMING()
                                        .getTQ1();
                                Segment obr = omlMessage.getSPECIMEN(specimenIndex).getORDER(orderIndex)
                                        .getOBSERVATION_REQUEST().getOBR();
                                if (MessageDecoder.isEmpty(tq1)) {
                                    tq1 = null;
                                }
                                if (MessageDecoder.isEmpty(obr)) {
                                    obr = null;
                                }
                                LabOrder messageOrder = MessageDecoder.extractLabOrderFromSegments(orc, obr, tq1,
                                        simulatedActor, domain, creator, selectedEncounter);
                                messageOrder.setMainEntity(false);
                                messageOrder.setWorkOrder(true);
                                LabOrder selectedOrder = AbstractOrder.getOrderByNumbersByActor(LabOrder.class,
                                        messageOrder.getPlacerOrderNumber(), messageOrder.getFillerOrderNumber(), null,
                                        simulatedActor, entityManager, true);
                                if (messageOrder.getOrderControlCode() == null) {
                                    entityManager.getTransaction().rollback();
                                    return orlBuilder.createNack(incomingMessage, AcknowledgmentCode.AE,
                                            Receiver.REQUIRED_FIELD_MISSING, Receiver.SEVERITY_ERROR,
                                            "ORC-1 is required but missing in SPECIMEN[" + specimenIndex + "]/ORDER["
                                                    + orderIndex + "]/ORC", null);
                                } else if (messageOrder.getOrderControlCode().equals("NW")) {
                                    if (selectedOrder != null) {
                                        entityManager.getTransaction().rollback();
                                        return orlBuilder.createNack(incomingMessage, AcknowledgmentCode.AE,
                                                Receiver.DUPLICATE_KEY_IDENTIFIER, Receiver.SEVERITY_ERROR,
                                                "A work order with those numbers is already registered", null);
                                    } else {
                                        messageOrder.setSpecimens(Arrays.asList(messageSpecimen));
                                        workOrders.add(messageOrder);
                                        continue;
                                    }
                                } else if (messageOrder.getOrderControlCode().equals("CA")) {
                                    if (selectedOrder == null) {
                                        entityManager.getTransaction().rollback();
                                        return orlBuilder.createNack(incomingMessage, AcknowledgmentCode.AE,
                                                Receiver.UNKNOWN_KEY_IDENTIFIER, Receiver.SEVERITY_ERROR,
                                                "Unknown work order", null);
                                    } else {
                                        selectedOrder.setOrderControlCode("CA");
                                        selectedOrder.setLastChanged(new Date());
                                        if (selectedOrder.getSpecimens() != null) {
                                            selectedOrder.getSpecimens().add(messageSpecimen);
                                        } else {
                                            selectedOrder.setSpecimens(Arrays.asList(messageSpecimen));
                                        }
                                        workOrders.add(selectedOrder);
                                        continue;
                                    }
                                } else {
                                    entityManager.getTransaction().rollback();
                                    return orlBuilder.createNack(incomingMessage, AcknowledgmentCode.AE, Receiver.TABLE_VALUE_NOT_FOUND,
                                            Receiver.SEVERITY_ERROR, "invalid value of ORC-1 in SPECIMEN["
                                                    + specimenIndex + "]/ORDER[" + orderIndex + "]/ORC-1", null);
                                }

                            }
                        }
                        if (messageSpecimen.getOrders() != null) {
                            for (LabOrder order : workOrders) {
                                if (!messageSpecimen.getOrders().contains(order)) {
                                    messageSpecimen.getOrders().add(order);
                                }
                            }
                        } else {
                            messageSpecimen.setOrders(workOrders);
                        }
                        messageSpecimen.save(entityManager);
                    } else {
                        entityManager.getTransaction().rollback();
                        return orlBuilder.createNack(incomingMessage, AcknowledgmentCode.AE, Receiver.INTERNAL_ERROR,
                                Receiver.SEVERITY_ERROR, "ORDER group is required but missing within SPECIMEN["
                                        + specimenIndex + "]", null);
                    }
                }
            }
        } else {
            orlBuilder.createNack(incomingMessage, AcknowledgmentCode.AE, Receiver.SEGMENT_SEQUENCE_ERROR, Receiver.SEVERITY_ERROR,
                    "SPECIMEN group should have at least one occurrence", null);
        }
        return orlBuilder.createAck(incomingMessage, simulatedActor.getKeyword());
    }

    /**
     * @param incomingMessage
     * @param orlBuilder
     * @param creator
     * @param selectedEncounter
     * @return
     */
    private Message parseOMLO21(Message incomingMessage, ORLMessageBuilder orlBuilder, String creator,
                                Encounter selectedEncounter) {
        PipeParser pipeParser = new PipeParser();
        // cast message using the class generated using the IHE HL7 message profile
        OML_O21 omlMessage;
        try {
            omlMessage = (OML_O21) pipeParser.parseForSpecificPackage(incomingMessage.encode(),
                    "net.ihe.gazelle.oml.hl7v2.model.v25.message");
        } catch (HL7Exception e) {
            return orlBuilder.createNack(incomingMessage, AcknowledgmentCode.AE, Receiver.INTERNAL_ERROR, Receiver.SEVERITY_ERROR,
                    "Cannot properly encode the message for parsing", null);
        }
        int nbOfOrders = omlMessage.getORDERReps();
        if (nbOfOrders > 0) {
            for (int groupIndex = 0; groupIndex < nbOfOrders; groupIndex++) {
                // check ORC segment is present
                Segment orc = omlMessage.getORDER(groupIndex).getORC();
                if (MessageDecoder.isEmpty(orc)) {
                    entityManager.getTransaction().rollback();
                    return orlBuilder.createNack(incomingMessage, AcknowledgmentCode.AE, Receiver.SEGMENT_SEQUENCE_ERROR,
                            Receiver.SEVERITY_ERROR, "ORC segment is missing in ORDER[" + groupIndex + "]", null);
                } else {
                    // TQ1 and OBR are optional
                    Segment tq1 = omlMessage.getORDER(groupIndex).getTIMING().getTQ1();
                    if (MessageDecoder.isEmpty(tq1)) {
                        tq1 = null;
                    }
                    Segment obr = omlMessage.getORDER(groupIndex).getOBSERVATION_REQUEST().getOBR();
                    if (MessageDecoder.isEmpty(obr)) {
                        obr = null;
                    }
                    LabOrder messageOrder = MessageDecoder.extractLabOrderFromSegments(orc, obr, tq1, simulatedActor,
                            domain, creator, selectedEncounter);
                    messageOrder.setMainEntity(true);
                    messageOrder.setWorkOrder(true);
                    LabOrder selectedOrder = AbstractOrder.getOrderByNumbersByActor(LabOrder.class,
                            messageOrder.getPlacerOrderNumber(), messageOrder.getFillerOrderNumber(), null,
                            simulatedActor, entityManager, true);
                    if (messageOrder.getOrderControlCode() == null) {
                        entityManager.getTransaction().rollback();
                        return orlBuilder.createNack(incomingMessage, AcknowledgmentCode.AE, Receiver.REQUIRED_FIELD_MISSING,
                                Receiver.SEVERITY_ERROR, "ORC-1 is required but missing in ORDER[" + groupIndex
                                        + "]/ORC", null);
                    } else if (messageOrder.getOrderControlCode().equals("CA") && (selectedOrder != null)) {
                        cancelOrder(selectedOrder);
                        continue;
                    } else if (messageOrder.getOrderControlCode().equals("CA") && (selectedOrder == null)) {
                        entityManager.getTransaction().rollback();
                        return orlBuilder.createNack(incomingMessage, AcknowledgmentCode.AE, Receiver.UNKNOWN_KEY_IDENTIFIER,
                                Receiver.SEVERITY_ERROR, "Unknown work order", null);
                    } else if (messageOrder.getOrderControlCode().equals("NW") && (selectedOrder != null)) {
                        entityManager.getTransaction().rollback();
                        return orlBuilder.createNack(incomingMessage, AcknowledgmentCode.AE, Receiver.DUPLICATE_KEY_IDENTIFIER,
                                Receiver.SEVERITY_ERROR,
                                "An order with this identifiers already exists within the Automation Manager", null);
                    } else if (messageOrder.getOrderControlCode().equals("NW") && (selectedOrder == null)) {
                        // parse the message for specimens and containers
                        int nbOfSpecimens = omlMessage.getORDER(groupIndex).getOBSERVATION_REQUEST().getSPECIMENReps();
                        if (nbOfSpecimens > 0) {
                            List<Specimen> specimens = new ArrayList<Specimen>();
                            for (int specimenIndex = 0; specimenIndex < nbOfSpecimens; specimenIndex++) {
                                Segment spm = omlMessage.getORDER(groupIndex).getOBSERVATION_REQUEST()
                                        .getSPECIMEN(specimenIndex).getSPM();
                                Specimen messageSpecimen = MessageDecoder.extractSpecimenFromSegment(spm, creator,
                                        simulatedActor, domain);
                                messageSpecimen.setMainEntity(false);
                                messageSpecimen.setWorkOrder(true);
                                Specimen selectedSpecimen = Specimen.getSpecimenByAssignedIdentifiersByActorByDomain(
                                        messageSpecimen.getPlacerAssignedIdentifier(),
                                        messageSpecimen.getFillerAssignedIdentifier(), simulatedActor, domain,
                                        entityManager, true);
                                if (selectedSpecimen != null) {
                                    messageSpecimen = selectedSpecimen;
                                }
                                if (messageSpecimen.getOrders() == null) {
                                    messageSpecimen.setOrders(Arrays.asList(messageOrder));
                                } else {
                                    messageSpecimen.getOrders().add(messageOrder);
                                }
                                int nbOfContainers = omlMessage.getORDER(groupIndex).getOBSERVATION_REQUEST()
                                        .getSPECIMEN(specimenIndex).getCONTAINERReps();
                                if (nbOfContainers > 0) {
                                    List<Container> containers = new ArrayList<Container>();
                                    for (int containerIndex = 0; containerIndex < nbOfContainers; containerIndex++) {
                                        Segment sac = omlMessage.getORDER(groupIndex).getOBSERVATION_REQUEST()
                                                .getSPECIMEN(specimenIndex).getCONTAINER(containerIndex).getSAC();
                                        Container messageContainer = MessageDecoder.extractContainerFromSAC(sac,
                                                simulatedActor, messageSpecimen, creator, domain);
                                        messageContainer.setWorkOrder(true);
                                        messageContainer.setMainEntity(false);
                                        Container selectedContainer = Container
                                                .getContainerByIdentifierByActorByDomain(
                                                        messageContainer.getContainerIdentifier(), simulatedActor,
                                                        domain, entityManager, true);
                                        if (selectedContainer != null) {
                                            messageContainer = selectedContainer;
                                        }
                                        containers.add(messageContainer);
                                    }
                                    if (messageSpecimen.getContainers() != null) {
                                        for (Container container : containers) {
                                            if (!messageSpecimen.getContainers().contains(container)) {
                                                messageSpecimen.getContainers().add(container);
                                            }
                                        }
                                    } else {
                                        messageSpecimen.setContainers(containers);
                                    }
                                }
                                specimens.add(messageSpecimen);
                            }
                            messageOrder.setSpecimens(specimens);
                        }
                        messageOrder.save(entityManager);
                        continue;
                    } else {
                        entityManager.getTransaction().rollback();
                        return orlBuilder.createNack(incomingMessage, AcknowledgmentCode.AE, Receiver.TABLE_VALUE_NOT_FOUND,
                                Receiver.SEVERITY_ERROR, "Invalid value of ORDER[" + groupIndex + "]/ORC-1", null);
                    }
                }
            }
            return orlBuilder.createAck(incomingMessage, simulatedActor.getKeyword());
        } else {
            return orlBuilder.createNack(incomingMessage, AcknowledgmentCode.AE, Receiver.SEGMENT_SEQUENCE_ERROR,
                    Receiver.SEVERITY_ERROR, "ORDER group should have at least one occurence", null);
        }
    }

    private void cancelOrder(LabOrder workOrder) {
        workOrder.setOrderControlCode("CA");
        workOrder.setLastChanged(new Date());
        workOrder.save(entityManager);
    }

}
