package net.ihe.gazelle.ordermanager.filter;

import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import net.ihe.gazelle.hql.criterion.QueryModifier;
import net.ihe.gazelle.ordermanager.model.ScheduledProcedureStep;
import net.ihe.gazelle.ordermanager.model.ScheduledProcedureStepQuery;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.common.tf.model.Domain;

import java.util.Map;

/**
 * <b>Class Description : </b>AbstractOrderFilter<br>
 * <br>
 *
 * @author Anne-Gaelle Berge / IHE-Europe development Project
 * @version 1.0 - 30/06/16
 *
 *
 *
 */
public class ScheduledProcedureStepFilter implements QueryModifier<ScheduledProcedureStep> {

    private String fillerOrderNumber;
    private String placerOrderNumber;
    private Actor actor;
    private Domain domain;
    private String visitnumber;
    private String patientid;
    private Filter<ScheduledProcedureStep> filter;
    private Boolean worklistCreated;
    private Boolean scheduledProcedure;


    public ScheduledProcedureStepFilter(Domain domain, Actor simulatedActor, Boolean worklistCreated, Boolean scheduledProc) {
        this.domain = domain;
        this.actor = simulatedActor;
        this.worklistCreated = worklistCreated;
        this.scheduledProcedure = scheduledProc;
    }

    public Filter<ScheduledProcedureStep> getFilter() {
        if (filter == null) {
            filter = new Filter<ScheduledProcedureStep>(getHQLCriterionsForFilter());
        }
        return filter;
    }

    protected HQLCriterionsForFilter<ScheduledProcedureStep> getHQLCriterionsForFilter() {
        ScheduledProcedureStepQuery query = new ScheduledProcedureStepQuery();
        HQLCriterionsForFilter<ScheduledProcedureStep> criteria = query.getHQLCriterionsForFilter();
        criteria.addPath("firstname", query.requestedProcedure().order().encounter().patient().firstName());
        criteria.addPath("lastname", query.requestedProcedure().order().encounter().patient().lastName());
        criteria.addPath("actor", query.simulatedActor(), getActor());
        criteria.addPath("domain", query.domain(), getDomain());
        criteria.addPath("creator", query.creator());
        criteria.addPath("creationDate", query.lastChanged());
        if ((scheduledProcedure != null) && scheduledProcedure) {
            criteria.addPath("status", query.requestedProcedure().orderStatus(), "SC");
        }
        criteria.addQueryModifier(this);
        return criteria;
    }

    public FilterDataModel<ScheduledProcedureStep> getProcedureSteps() {
        return new FilterDataModel<ScheduledProcedureStep>(getFilter()) {
            @Override
            protected Object getId(ScheduledProcedureStep t) {
                return t.getId();
            }
        };
    }

    @Override
    public void modifyQuery(HQLQueryBuilder<ScheduledProcedureStep> hqlQueryBuilder, Map<String, Object> map) {
        ScheduledProcedureStepQuery query = new ScheduledProcedureStepQuery();
        if (getPlacerOrderNumber() != null && !getPlacerOrderNumber().isEmpty()) {
            hqlQueryBuilder.addRestriction(query.requestedProcedure().order().placerOrderNumber().likeRestriction(getPlacerOrderNumber()));
        }
        if ((getFillerOrderNumber() != null) && !getFillerOrderNumber().isEmpty()) {
            hqlQueryBuilder.addRestriction(query.requestedProcedure().order().fillerOrderNumber().likeRestriction(getFillerOrderNumber()));
        }
        if (getPatientid() != null && !getPatientid().isEmpty()){
            hqlQueryBuilder.addRestriction(query.requestedProcedure().order().encounter().patient().patientIdentifiers().identifier().likeRestriction(getPatientid()));
        }
        if (getVisitnumber() != null && !getVisitnumber().isEmpty()){
            hqlQueryBuilder.addRestriction(query.requestedProcedure().order().encounter().visitNumber().likeRestriction(getVisitnumber()));
        }
        if (worklistCreated != null) {
            if (worklistCreated) {
                hqlQueryBuilder.addRestriction(query.worklist().isNotEmptyRestriction());
            } else {
                hqlQueryBuilder.addRestriction(query.worklist().isEmptyRestriction());
            }
        }
    }

    public void reset() {
        fillerOrderNumber = null;
        placerOrderNumber = null;
        visitnumber = null;
        patientid = null;
        getFilter().clear();
    }

    public String getFillerOrderNumber() {
        return fillerOrderNumber;
    }

    public void setFillerOrderNumber(String fillerOrderNumber) {
        this.fillerOrderNumber = fillerOrderNumber;
    }

    public String getPlacerOrderNumber() {
        return placerOrderNumber;
    }

    public void setPlacerOrderNumber(String placerOrderNumber) {
        this.placerOrderNumber = placerOrderNumber;
    }

    public String getVisitnumber() {
        return visitnumber;
    }

    public void setVisitnumber(String visitnumber) {
        this.visitnumber = visitnumber;
    }

    public String getPatientid() {
        return patientid;
    }

    public void setPatientid(String patientid) {
        this.patientid = patientid;
    }

    public Actor getActor() {
        return actor;
    }

    public Domain getDomain() {
        return domain;
    }
}
