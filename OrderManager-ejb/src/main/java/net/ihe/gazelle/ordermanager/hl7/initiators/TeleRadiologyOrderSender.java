package net.ihe.gazelle.ordermanager.hl7.initiators;

import net.ihe.gazelle.HL7Common.initiator.Initiator;
import net.ihe.gazelle.HL7Common.model.Charset;
import net.ihe.gazelle.HL7Common.model.HL7Protocol;
import net.ihe.gazelle.HL7Common.model.HL7V2ResponderSUTConfiguration;
import net.ihe.gazelle.hl7.validator.client.MessageEncoding;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.ordermanager.filter.TeleRadiologyOrderFilter;
import net.ihe.gazelle.ordermanager.model.TeleRadiologyOrder;
import net.ihe.gazelle.ordermanager.model.TeleRadiologyProcedure;
import net.ihe.gazelle.ordermanager.utils.TROMessageBuilder;
import net.ihe.gazelle.preferences.PreferenceService;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;
import net.ihe.gazelle.simulator.message.model.TransactionInstance;
import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.Synchronized;
import org.jboss.seam.core.ResourceBundle;
import org.jboss.seam.faces.FacesMessages;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.model.SelectItem;
import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Name("troSender")
@Scope(ScopeType.PAGE)
@Synchronized(timeout = 30000)
public class TeleRadiologyOrderSender extends AbstractOrderSender<TeleRadiologyOrder> {

	private static Logger log = LoggerFactory.getLogger(TeleRadiologyOrderSender.class);

	private HL7V2ResponderSUTConfiguration forwarder;
	private String sutApplication;
	private String sutFacility;
	private TeleRadiologyProcedure currentProcedure;
	private String currentAccessionNumber;

	@Override
	public void createANewOrder() {
		EntityManager entityManager = EntityManagerService.provideEntityManager();
		selectedOrder = new TeleRadiologyOrder(selectedDomain, simulatedActor, getSelectedEncounter(), entityManager);
		if (userPreferences != null) {
			selectedOrder.setCreator(userPreferences.getUsername());
		}
		selectedOrder.setOrderControlCode("NW");
		selectedOrder.setOrderStatus("IP");
	}

	@Override
	public List<SelectItem> listAvailableActions() {
		List<SelectItem> items = new ArrayList<SelectItem>();
		if (simulatedActor.getKeyword().equals(TRO_CREATOR)) {
			items.add(new SelectItem(NEW, ResourceBundle.instance().getString(NEW)));
			items.add(new SelectItem(CANCEL, ResourceBundle.instance().getString(CANCEL)));
		} else if (simulatedActor.getKeyword().equals(TRO_FULFILLER)) {
			items.add(new SelectItem(UPDATE, ResourceBundle.instance().getString(UPDATE)));
			orderControlCode = UPDATE;
			onChangeActionEvent();
		}
		return items;
	}

	@Override
	public List<SelectItem> getAvailableOrderStatuses() {
		List<SelectItem> items = new ArrayList<SelectItem>();
		items.add(new SelectItem(null, ResourceBundle.instance().getString("gazelle.ordermanager.PleaseSelect")));
		if (selectedOrder.getOrderStatus().equals("SC")) {
			items.add(new SelectItem("CM", ResourceBundle.instance().getString("gazelle.ordermanager.OrderCompleted")));
			items.add(new SelectItem("DC", "Order aborted"));
		} else if (selectedOrder.getOrderStatus().equals("IP")) {
			items.add(new SelectItem("SC", ResourceBundle.instance().getString("gazelle.ordermanager.OrderInProgress")));
		}
		return items;
	}

	public List<HL7V2ResponderSUTConfiguration> getAvailableForwarders() {
		return HL7V2ResponderSUTConfiguration.getAllConfigurationsForSelection("TRO_FORWARDER", HL7Protocol.HTTP);
	}

	public boolean isFowarderLocal() {
		return (forwarder != null && forwarder.getName().equals(PreferenceService.getString("local_forwarder_name")));
	}

	@Override
	public void sendMessage() {
		if (orderControlCode.equals(CANCEL)) {
			selectedOrder.setOrderControlCode("OC");
			selectedOrder.setLastChanged(new Date());
		} else if (orderControlCode.equals(UPDATE)) {
			selectedOrder.setOrderControlCode("SC");
			selectedOrder.setOrderStatus(newOrderStatus);
			selectedOrder.setLastChanged(new Date());
		}
		selectedOrder = selectedOrder.save();
		HL7V2ResponderSUTConfiguration sut = null;
		if (isFowarderLocal()) {
			sut = (HL7V2ResponderSUTConfiguration) Component.getInstance("selectedSUT");
		} else {
			sut = new HL7V2ResponderSUTConfiguration();
			sut.setApplication(sutApplication);
			sut.setFacility(sutFacility);
			sut.setUrl(sutApplication);
			sut.setMessageEncoding(MessageEncoding.XML);
			sut.setHl7Protocol(HL7Protocol.HTTP);
			sut.setCharset(Charset.getCharsetByHL7Name("UNICODE UTF-8"));
		}
		TROMessageBuilder builder = new TROMessageBuilder("KSA-TRO", simulatedActor.getKeyword(), selectedOrder, sut,
				sendingApplication, sendingFacility);
		try {
			String messageToSend = builder.generateMessage();
			Initiator initiator = new Initiator(forwarder, sendingFacility, sendingApplication, simulatedActor,
					Transaction.GetTransactionByKeyword("KSA-TRO"), messageToSend, "OMI^O23^OMI_O23 ("
							+ selectedOrder.getOrderControlCode() + ")", "SeHE", Actor.findActorWithKeyword(sutActorKeyword));
			hl7Messages = new ArrayList<TransactionInstance>();
            hl7Messages.add(initiator.sendMessage());
		} catch (Exception e) {
			FacesMessages.instance().add(e.getMessage());
			log.error(e.getMessage(), e);
		}
	}

	@Override
	public void fillOrderRandomly() {
		selectedOrder.fillOrderRandomly(selectedDomain, simulatedActor);
	}

	@Override
	public void onChangeActionEvent() {
		setSelectedEncounter(null);
		setSelectedOrder(null);
		if (orderControlCode.equals(NEW)) {
            setSelectionType(SelectionType.ENCOUNTER);
		} else if (orderControlCode.equals(CANCEL)) {
            setSelectionType(SelectionType.ORDER);
			encounterComesFromDispatch = false;
			orderFilter = new TeleRadiologyOrderFilter(selectedDomain, simulatedActor, "OC");
		} else if (orderControlCode.equals(UPDATE)) {
            setSelectionType(SelectionType.ORDER);
			encounterComesFromDispatch = false;
			orderFilter = new TeleRadiologyOrderFilter(selectedDomain, simulatedActor, "SC");
		}
		// else those cases are not yet processed
		if (!encounterComesFromDispatch) {
			setSelectedEncounter(null);
			setSelectedPatient(null);
		}
	}

	public void createNewProcedure() {
		currentProcedure = new TeleRadiologyProcedure(simulatedActor, username, selectedDomain);
	}

	public void addProcedureToList() {
		currentProcedure.setTeleRadiologyOrder(selectedOrder);
		if (selectedOrder.getTeleRadiologyProcedures() == null) {
			selectedOrder.setTeleRadiologyProcedures(new ArrayList<TeleRadiologyProcedure>());
		}
		selectedOrder.getTeleRadiologyProcedures().add(currentProcedure);
		currentProcedure = null;
	}

	public void addAccessionNumber() {
		if (selectedOrder.getRelatedStudies() == null) {
			selectedOrder.setRelatedStudies(new ArrayList<String>());
		}
		selectedOrder.getRelatedStudies().add(currentAccessionNumber);
		currentAccessionNumber = null;
	}

	public void removeAccessionNumber(String number) {
		if (selectedOrder.getRelatedStudies() != null && !selectedOrder.getRelatedStudies().isEmpty()
				&& selectedOrder.getRelatedStudies().contains(number)) {
			selectedOrder.getRelatedStudies().remove(number);
		}
	}

	public TeleRadiologyOrder getSelectedOrder() {
		return selectedOrder;
	}

	public void setSelectedOrder(TeleRadiologyOrder selectedOrder) {
		if (selectedOrder == null) {
			this.newOrderStatus = null;
		}
		this.selectedOrder = selectedOrder;
	}


	@Override
	public void displayAvailableCountries() {
		super.displayAvailableCountries();
		selectedOrder = null;
	}

	public HL7V2ResponderSUTConfiguration getForwarder() {
		return forwarder;
	}

	public void setForwarder(HL7V2ResponderSUTConfiguration forwarder) {
		this.forwarder = forwarder;
	}

	public String getSutApplication() {
		return sutApplication;
	}

	public void setSutApplication(String sutApplication) {
		this.sutApplication = sutApplication;
	}

	public String getSutFacility() {
		return sutFacility;
	}

	public void setSutFacility(String sutFacility) {
		this.sutFacility = sutFacility;
	}

	public TeleRadiologyProcedure getCurrentProcedure() {
		return currentProcedure;
	}

	public void setCurrentProcedure(TeleRadiologyProcedure currentProcedure) {
		this.currentProcedure = currentProcedure;
	}

	public String getCurrentAccessionNumber() {
		return currentAccessionNumber;
	}

	public void setCurrentAccessionNumber(String currentAccessionNumber) {
		this.currentAccessionNumber = currentAccessionNumber;
	}

}
