package net.ihe.gazelle.ordermanager.databrowser;

import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import net.ihe.gazelle.ordermanager.model.Order;
import net.ihe.gazelle.ordermanager.model.OrderQuery;
import net.ihe.gazelle.ssov7.gum.client.application.UserAttributeCommon;
import net.ihe.gazelle.ssov7.gum.client.application.service.UserService;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

import javax.faces.context.FacesContext;
import java.util.Map;

/**
 * <p>OrderBrowser class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
@Name("orderBrowser")
@Scope(ScopeType.PAGE)
public class OrderBrowser extends DataBrowser<Order> implements UserAttributeCommon {

	private static final long serialVersionUID = 7862691434311754718L;

    /**
     * used for consistency with the orderColumns.xhtml shared page
     */
    private Integer selectedOrderId;

    @In(value="gumUserService")
    private UserService userService;

	/** {@inheritDoc} */
	@Override
	public void modifyQuery(HQLQueryBuilder<Order> queryBuilder, Map<String, Object> map) {
		if ((fillerOrderNumber != null) && !fillerOrderNumber.isEmpty()) {
			queryBuilder.addLike("fillerOrderNumber", fillerOrderNumber);
		}
		if ((placerOrderNumber != null) && !placerOrderNumber.isEmpty()) {
			queryBuilder.addLike("placerOrderNumber", placerOrderNumber);
		}
		if ((patientId != null) && !patientId.isEmpty()) {
			queryBuilder.addLike("encounter.patient.patientIdentifiers.identifier", patientId);
		}
		if ((visitNumber != null) && !visitNumber.isEmpty()) {
			queryBuilder.addLike("encounter.visitNumber", visitNumber);
		}
	}

    /** {@inheritDoc} */
    @Override
    public Filter<Order> getFilter() {
        if (filter == null) {
            filter = new Filter<Order>(getHQLCriterionsForFilter(), FacesContext.getCurrentInstance().getExternalContext()
                    .getRequestParameterMap());
        }
        return filter;
    }

    /** {@inheritDoc} */
    @Override
	protected HQLCriterionsForFilter<Order> getHQLCriterionsForFilter() {
		OrderQuery query = new OrderQuery();
		HQLCriterionsForFilter<Order> criterions = query.getHQLCriterionsForFilter();
		criterions.addPath("actor", query.simulatedActor(), actor);
		criterions.addPath("domain", query.domain(), domain);
		criterions.addPath("lastChanged", query.lastChanged());
		criterions.addPath("creator", query.creator());
		criterions.addQueryModifier(this);
		return criterions;
	}

	/** {@inheritDoc} */
	@Override
	public String getPermanentLink(Integer objectId) {
		return "/browsing/order.seam?domain=" + domain.getKeyword() + "&id=" + objectId;
	}

    /** {@inheritDoc} */
    @Override
    public FilterDataModel<Order> getDataModel() {
        return new FilterDataModel<Order>(getFilter()) {
            @Override
            protected Object getId(Order order) {
                return order.getId();
            }
        };
    }

    /**
     * <p>scheduleProcedureForOrder.</p>
     *
     * @param orderId a {@link java.lang.Integer} object.
     * @return a {@link java.lang.String} object.
     */
    public String scheduleProcedureForOrder(Integer orderId) {
		StringBuilder url = new StringBuilder("/hl7Initiators/procedureSender.seam?actor=OF");
        synchronized (url) {
            url.append("&domain=");
            url.append(domain.getKeyword());
            url.append("&orderId=");
            url.append(orderId);
            return url.toString();
        }
	}

	/**
	 * <p>createWorklistForOrder.</p>
	 *
	 * @param orderId a {@link java.lang.Integer} object.
	 * @return a {@link java.lang.String} object.
	 */
	public String createWorklistForOrder(Integer orderId) {
		StringBuilder url = new StringBuilder("/mwlscp/createWorklistEntry.seam?");
        synchronized (url) {
            url.append("domain=");
            url.append(domain.getKeyword());
            url.append("&orderId=");
            url.append(orderId);
            return url.toString();
        }
	}

    /**
     * <p>Getter for the field <code>selectedOrderId</code>.</p>
     *
     * @return a {@link java.lang.Integer} object.
     */
    public Integer getSelectedOrderId() {
        return selectedOrderId;
    }

    /**
     * <p>Setter for the field <code>selectedOrderId</code>.</p>
     *
     * @param selectedOrderId a {@link java.lang.Integer} object.
     */
    public void setSelectedOrderId(Integer selectedOrderId) {
        this.selectedOrderId = selectedOrderId;
    }

    @Override
    public String getUserName(String userId) {
        return userService.getUserDisplayNameWithoutException(userId);
    }
}
