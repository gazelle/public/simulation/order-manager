package net.ihe.gazelle.ordermanager.datamodel;

import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.hql.criterion.HQLCriterion;
import net.ihe.gazelle.ordermanager.model.ScheduledProcedureStep;

import java.util.ArrayList;
import java.util.List;

/**
 * Class description : <b>RequestedProcedureFilter</b>
 * 
 *
 *
 * @author Anne-Gaëlle Bergé - INRIA Rennes/Bretagne Atlantique
 *
 * @version 1.0 - 2011, November 17th
 * 
 */
public class ScheduledProcedureStepFilter extends Filter<ScheduledProcedureStep> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3500013337306211938L;
	private static List<HQLCriterion<ScheduledProcedureStep, ?>> defaultCriterions;

	static {
		defaultCriterions = new ArrayList<HQLCriterion<ScheduledProcedureStep, ?>>();
	}

	public ScheduledProcedureStepFilter() {
		super(ScheduledProcedureStep.class, defaultCriterions);
	}
}
