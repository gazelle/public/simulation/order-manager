package net.ihe.gazelle.ordermanager.hl7.responders;

import ca.uhn.hl7v2.AcknowledgmentCode;
import ca.uhn.hl7v2.ErrorCode;
import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.model.Message;
import ca.uhn.hl7v2.model.Segment;
import ca.uhn.hl7v2.model.v251.message.ADT_A01;
import ca.uhn.hl7v2.model.v251.message.ADT_A39;
import ca.uhn.hl7v2.model.v231.message.ORM_O01;
import ca.uhn.hl7v2.model.v25.message.ORL_O22;
import ca.uhn.hl7v2.model.v25.message.ORL_O34;
import ca.uhn.hl7v2.model.v25.message.ORL_O36;
import ca.uhn.hl7v2.model.v25.message.OUL_R22;
import ca.uhn.hl7v2.model.v251.message.OMG_O19;
import ca.uhn.hl7v2.parser.PipeParser;
import ca.uhn.hl7v2.util.Terser;
import net.ihe.gazelle.HL7Common.model.Charset;
import net.ihe.gazelle.HL7Common.model.HL7V2ResponderSUTConfiguration;
import net.ihe.gazelle.HL7Common.responder.IHEDefaultHandler;
import net.ihe.gazelle.oml.hl7v2.model.v25.group.OML_O21_ORDER;
import net.ihe.gazelle.oml.hl7v2.model.v25.group.OML_O21_SPECIMEN;
import net.ihe.gazelle.oml.hl7v2.model.v25.group.OML_O33_ORDER;
import net.ihe.gazelle.oml.hl7v2.model.v25.group.OML_O35_ORDER;
import net.ihe.gazelle.oml.hl7v2.model.v25.message.OML_O21;
import net.ihe.gazelle.oml.hl7v2.model.v25.message.OML_O33;
import net.ihe.gazelle.oml.hl7v2.model.v25.message.OML_O35;
import net.ihe.gazelle.ordermanager.model.AbstractOrder;
import net.ihe.gazelle.ordermanager.model.Container;
import net.ihe.gazelle.ordermanager.model.Encounter;
import net.ihe.gazelle.ordermanager.model.LabOrder;
import net.ihe.gazelle.ordermanager.model.NumberGenerator;
import net.ihe.gazelle.ordermanager.model.Observation;
import net.ihe.gazelle.ordermanager.model.Order;
import net.ihe.gazelle.ordermanager.model.Patient;
import net.ihe.gazelle.ordermanager.model.PatientIdentifier;
import net.ihe.gazelle.ordermanager.model.Specimen;
import net.ihe.gazelle.ordermanager.utils.IHERadMessageBuilder;
import net.ihe.gazelle.ordermanager.utils.MessageDecoder;
import net.ihe.gazelle.ordermanager.utils.ORLMessageBuilder;
import net.ihe.gazelle.simulator.common.tf.model.Domain;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Abstract class description : <b>OrderReceiver</b>
 * 
 * This abstract class needs to be extended by actor enables to received HL7 orders. It gathers the methods to parse the received messages and create, update, cancel ... received orders
 * 
 *
 *
 * @author Anne-Gaëlle Bergé - INRIA Rennes/Bretagne Atlantique
 *
 * @version 1.0 - 2011, November 17th
 * 
 */
public abstract class OrderReceiver extends IHEDefaultHandler {

	private static Logger log = LoggerFactory.getLogger(OrderReceiver.class);

	protected static final String ERROR_INCONSISTENT_PID = "The patient identifiers are inconsistent";

	protected String orderControlCode = null;


	/**
	 * 
	 * @param message
	 * @param newControlCode
	 *            - control code expected for a new order
	 * @param cancelControlCode
	 *            - control code expected for the cancellation of an order
	 * @param updateControlCode
	 *            - control code expected for the update of the order status (if supported, null otherwise)
	 * @param discontinueControlCode
	 *            - control code expected for stopping the fulfillment of the order (if supported, null otherwise)
	 * @return the acknowledgement
	 */
	protected Message processRadiologyOrderMessage(Message message, String newControlCode, String cancelControlCode,
			String updateControlCode, String discontinueControlCode) {
		if (message instanceof ORM_O01) {
			ORM_O01 ormMessage = (ORM_O01) message;
			// check the ORC segment is present
			if (MessageDecoder.isSegmentPresent(message, "ORC")) {
				orderControlCode = ormMessage.getORCOBRRQDRQ1ODSODTRXONTEDG1RXRRXCNTEOBXNTECTIBLG().getORC()
						.getOrderControl().getValue();
				if (orderControlCode == null) {
					return Receiver.buildNack(serverApplication, serverFacility, sendingApplication, sendingFacility,
							"O01", AcknowledgmentCode.AE, null, null, Receiver.REQUIRED_FIELD_MISSING, null, messageControlId,
							hl7Charset, hl7Version);
				} else if (orderControlCode.equals(cancelControlCode)) {
					// cancellation of an order
					return processOrderCancellation(ormMessage, cancelControlCode);
				} else if (orderControlCode.equals(newControlCode)) {
					// new order
					return processNewOrder(ormMessage);
				} else if ((updateControlCode != null) && orderControlCode.equals(updateControlCode)) {
					// status update
					return processStatusUpdate(ormMessage);
				} else if ((discontinueControlCode != null) && orderControlCode.equals(discontinueControlCode)) {
					// stop the fulfillment of the order
					return processDiscontinueOrder(ormMessage);
				} else {
					// unrecognized control code
					return Receiver.buildNack(serverApplication, serverFacility, sendingApplication, sendingFacility,
							"O01", AcknowledgmentCode.AE, null, null, Receiver.INTERNAL_ERROR, null, messageControlId, hl7Charset,
							hl7Version);
				}
			} else {
				return Receiver.buildNack(serverApplication, serverFacility, sendingApplication, sendingFacility,
						"O01", AcknowledgmentCode.AE, null, null, Receiver.SEGMENT_SEQUENCE_ERROR, null, messageControlId, hl7Charset,
						hl7Version);
			}
		} else if (message instanceof OMG_O19) {
			OMG_O19 omgMessage = (OMG_O19) message;
			// check the ORC segment is present
			if (omgMessage.getORDERReps() > 0) {
				orderControlCode = omgMessage.getORDER(0).getORC().getOrderControl().getValue();
				if (orderControlCode == null) {
					return Receiver.buildNack(serverApplication, serverFacility, sendingApplication, sendingFacility,
							"O19", AcknowledgmentCode.AE, null, null, Receiver.REQUIRED_FIELD_MISSING, null, messageControlId,
							hl7Charset, hl7Version);
				} else if (orderControlCode.equals(cancelControlCode)) {
					return processOrderCancellation(omgMessage, cancelControlCode);
				} else if (orderControlCode.equals(newControlCode)) {
					return	processNewOrder(omgMessage);
				} else if ((updateControlCode != null) && orderControlCode.equals(updateControlCode)) {
					return processStatusUpdate(omgMessage);
				} else if ((discontinueControlCode != null) && orderControlCode.equals(discontinueControlCode)) {
					return processDiscontinueOrder(omgMessage);
				} else {
					// unrecognized control code
					return Receiver.buildNack(serverApplication, serverFacility, sendingApplication, sendingFacility,
							"O19", AcknowledgmentCode.AE, "unsupported control code", null, Receiver.INTERNAL_ERROR, null, messageControlId, hl7Charset,
							hl7Version);
				}
			} else {
				return Receiver.buildNack(serverApplication, serverFacility, sendingApplication, sendingFacility,
						"O19", AcknowledgmentCode.AE, "ORDER group is missing", null, Receiver.SEGMENT_SEQUENCE_ERROR, null, messageControlId, hl7Charset,
						hl7Version);
			}
		} else {
			try {
				return message.generateACK(AcknowledgmentCode.AR, new HL7Exception(
						"Unexpected message type at this point", ErrorCode.UNSUPPORTED_MESSAGE_TYPE));
			} catch (HL7Exception e) {
				log.error(e.getMessage(), e);
				return null;
			} catch (IOException e) {
				log.error(e.getMessage(), e);
				return null;
			}
		}
	}

	/**
	 * Creation of a new order as described in the received message
	 * 
	 * @param ormMessage
	 * @return the message to send
	 */
	private Message processNewOrder(ORM_O01 ormMessage) {
		Patient selectedPatient = null;
		Order selectedOrder = null;
		Encounter selectedEncounter = null;
		// stored or retrieved the patient contained in the message
		if (MessageDecoder.isSegmentPresent(ormMessage, "PID")) {
			Patient pidPatient = MessageDecoder.extractPatientFromPID(ormMessage.getPIDPD1NTEPV1PV2IN1IN2IN3GT1AL1()
					.getPID());
			List<PatientIdentifier> pidPatientIdentifiers = MessageDecoder
					.extractPatientIdentifiersFromPID251(ormMessage);
			List<PatientIdentifier> identifiersToAdd = new ArrayList<PatientIdentifier>();
			for (PatientIdentifier pid : pidPatientIdentifiers) {
				PatientIdentifier existingPID = pid.getStoredPatientIdentifier(entityManager);
				if (existingPID != null) {
					if (selectedPatient == null) {
						selectedPatient = existingPID.getPatient();
					} else if (selectedPatient.getId().equals(existingPID.getPatient().getId())) {
						// OK: nothing to do
						continue;
					} else {
						// error patients with same identifiers mismatch
						return createResponseForNewOrder(Receiver.DUPLICATE_KEY_IDENTIFIER, null, (ORM_O01) null);
					}
				} else {
					pid = pid.save(entityManager);
					identifiersToAdd.add(pid);
				}
			}
			// if none of the identifiers is already known by the simulator, we save the patient contained in the message
			if (selectedPatient == null) {
				// save the patient
				selectedPatient = pidPatient.save(entityManager);
				selectedPatient.setCreator(sendingApplication + "_" + sendingFacility);
			}
			for (PatientIdentifier pid : identifiersToAdd) {
				pid.setPatient(selectedPatient);
				pid.save(entityManager);
			}
			// check existance of PV1 segment

			if (MessageDecoder.isSegmentPresent(ormMessage, "PV1")) {
				// extract encounter
				Encounter pv1Encounter = MessageDecoder.extractEncounterFromPV1(ormMessage);
				if (pv1Encounter.getVisitNumber() != null) {
					// check the encounter exists
					selectedEncounter = Encounter.getEncounterByVisitNumber(pv1Encounter.getVisitNumber(),
							entityManager);
					if ((selectedEncounter != null)
							&& !selectedEncounter.getPatient().getId().equals(selectedPatient.getId())) {
						return createResponseForNewOrder(Receiver.DUPLICATE_KEY_IDENTIFIER, null, (ORM_O01) null);
					} else if (selectedEncounter == null) {
						selectedEncounter = pv1Encounter;
						selectedEncounter.setPatient(selectedPatient);
						selectedEncounter.setCreator(sendingApplication + "_" + sendingFacility);
						selectedEncounter = selectedEncounter.save(entityManager);
					}
					// else selectedEncounter is the one retrieved from the database
				} else {
					return createResponseForNewOrder(Receiver.REQUIRED_FIELD_MISSING, null, (ORM_O01) null);
				}
			} else {
				return createResponseForNewOrder(Receiver.SEGMENT_SEQUENCE_ERROR, null, (ORM_O01) null);
			}

			// check existance of ORC segment
			if (MessageDecoder.isSegmentPresent(ormMessage, "ORC")) {
				selectedOrder = MessageDecoder.extractNewOrderFromORMWithTerser(ormMessage);
				if (Order.getOrderByNumbersByActor(selectedOrder.getPlacerOrderNumber(),
						selectedOrder.getFillerOrderNumber(), simulatedActor, entityManager) != null) {
					// numbers are already used !!!
					return createResponseForNewOrder(Receiver.DUPLICATE_KEY_IDENTIFIER, null, (ORM_O01) null);
				} else {
					selectedOrder.setCreator(sendingApplication + "_" + sendingFacility);

					if (domain.getKeyword().contains("RAD")) {
						selectedOrder.setDomain(Domain.getDomainByKeyword("RAD", entityManager));
					} else if (domain.getKeyword().contains("EYE")) {
						selectedOrder.setDomain(Domain.getDomainByKeyword("EYE", entityManager));
					} else // cardiology
					{
						selectedOrder.setDomain(Domain.getDomainByKeyword("CARD", entityManager));
					}
					selectedOrder.setSimulatedActor(simulatedActor);
					selectedOrder.setEncounter(selectedEncounter);
					if (simulatedActor.getKeyword().equals("OP")) {
						selectedOrder.setPlacerOrderNumber(NumberGenerator.getOrderNumberForActor(simulatedActor,
								entityManager));
						selectedOrder.setOrderControlCode("NA");
					} else {
						selectedOrder.setFillerOrderNumber(NumberGenerator.getOrderNumberForActor(simulatedActor,
								entityManager));
					}
					selectedOrder = selectedOrder.save(entityManager);
					return createResponseForNewOrder(null, selectedOrder, ormMessage);
				}
			} else {
				return createResponseForNewOrder(Receiver.SEGMENT_SEQUENCE_ERROR, null, (ORM_O01) null);
			}
		} else {
			return createResponseForNewOrder(Receiver.SEGMENT_SEQUENCE_ERROR, null, (ORM_O01) null);
		}
	}

	/**
	 * Creation of a new order as described in the received message
	 * 
	 * @param omgMessage
	 * @return
	 */
	private Message processNewOrder(OMG_O19 omgMessage) {
		Patient selectedPatient = null;
		Order selectedOrder = null;
		Encounter selectedEncounter = null;
		// stored or retrieved the patient contained in the message
		if (MessageDecoder.isSegmentPresent(omgMessage, "PID")) {
			Patient pidPatient = MessageDecoder.extractPatientFromPID(omgMessage.getPATIENT().getPID());
			List<PatientIdentifier> pidPatientIdentifiers = MessageDecoder.extractPatientIdentifiersFromPID251(omgMessage);
			List<PatientIdentifier> identifiersToAdd = new ArrayList<PatientIdentifier>();
			for (PatientIdentifier pid : pidPatientIdentifiers) {
				PatientIdentifier existingPID = pid.getStoredPatientIdentifier(entityManager);
				if (existingPID != null) {
					if (selectedPatient == null) {
						selectedPatient = existingPID.getPatient();
					} else if (selectedPatient.getId().equals(existingPID.getPatient().getId())) {
						// OK: nothing to do
						continue;
					} else {
						// error patients with same identifiers mismatch
						return createResponseForNewOrder(Receiver.DUPLICATE_KEY_IDENTIFIER, null, (OMG_O19) null);
					}
				} else {
					pid = pid.save(entityManager);
					identifiersToAdd.add(pid);
				}
			}
			// if none of the identifiers is already known by the simulator, we save the patient contained in the message
			if (selectedPatient == null) {
				// save the patient
				selectedPatient = pidPatient.save(entityManager);
				selectedPatient.setCreator(sendingApplication + "_" + sendingFacility);
			}
			for (PatientIdentifier pid : identifiersToAdd) {
				pid.setPatient(selectedPatient);
				pid.save(entityManager);
			}
			// check existance of PV1 segment

			if (MessageDecoder.isSegmentPresent(omgMessage, "PV1")) {
				// extract encounter
				Encounter pv1Encounter = MessageDecoder.extractEncounterFromPV1(omgMessage);
				if (pv1Encounter.getVisitNumber() != null) {
					// check the encounter exists
					selectedEncounter = Encounter.getEncounterByVisitNumber(pv1Encounter.getVisitNumber(),
							entityManager);
					if ((selectedEncounter != null)
							&& !selectedEncounter.getPatient().getId().equals(selectedPatient.getId())) {
						return createResponseForNewOrder(Receiver.DUPLICATE_KEY_IDENTIFIER, null, (OMG_O19) null);
					} else if (selectedEncounter == null) {
						selectedEncounter = pv1Encounter;
						selectedEncounter.setPatient(selectedPatient);
						selectedEncounter.setCreator(sendingApplication + "_" + sendingFacility);
						selectedEncounter = selectedEncounter.save(entityManager);
					}
					// else selectedEncounter is the one retrieved from the database
				} else {
					return createResponseForNewOrder(Receiver.REQUIRED_FIELD_MISSING, null, (OMG_O19) null);
				}
			} else {
				return createResponseForNewOrder(Receiver.SEGMENT_SEQUENCE_ERROR, null, (OMG_O19) null);
			}

			// check existance of ORC segment
			if (omgMessage.getORDERReps() > 0) {
				selectedOrder = MessageDecoder.extractNewOrderFromORMWithTerser(omgMessage);
				if (Order.getOrderByNumbersByActor(selectedOrder.getPlacerOrderNumber(),
						selectedOrder.getFillerOrderNumber(), simulatedActor, entityManager) != null) {
					// numbers are already used !!!
					return createResponseForNewOrder(Receiver.DUPLICATE_KEY_IDENTIFIER, null, (OMG_O19) null);
				} else {
					selectedOrder.setCreator(sendingApplication + "_" + sendingFacility);

					if (domain.getKeyword().contains("RAD")) {
						selectedOrder.setDomain(Domain.getDomainByKeyword("RAD", entityManager));
					} else if (domain.getKeyword().contains("EYE")) {
						selectedOrder.setDomain(Domain.getDomainByKeyword("EYE", entityManager));
					} else // cardiology
					{
						selectedOrder.setDomain(Domain.getDomainByKeyword("CARD", entityManager));
					}
					selectedOrder.setSimulatedActor(simulatedActor);
					selectedOrder.setEncounter(selectedEncounter);
					if (simulatedActor.getKeyword().equals("OP")) {
						selectedOrder.setPlacerOrderNumber(NumberGenerator.getOrderNumberForActor(simulatedActor,
								entityManager));
						selectedOrder.setOrderControlCode("NA");
					} else {
						selectedOrder.setFillerOrderNumber(NumberGenerator.getOrderNumberForActor(simulatedActor,
								entityManager));
					}
					selectedOrder = selectedOrder.save(entityManager);
					return createResponseForNewOrder(null, selectedOrder, omgMessage);
				}
			} else {
				return createResponseForNewOrder(Receiver.SEGMENT_SEQUENCE_ERROR, null, (OMG_O19) null);
			}
		} else {
			return createResponseForNewOrder(Receiver.SEGMENT_SEQUENCE_ERROR, null, (OMG_O19) null);
		}
	}

	/**
	 * Update the status of a previous received order
	 * 
	 * @param ormMessage
	 * @return
	 */
	private Message processStatusUpdate(ORM_O01 ormMessage) {
		if (MessageDecoder.isSegmentPresent(ormMessage, "ORC")) {
			try {
				String placerOrderNumber = ormMessage.getORCOBRRQDRQ1ODSODTRXONTEDG1RXRRXCNTEOBXNTECTIBLG().getORC()
						.getPlacerOrderNumber().encode();
				Order selectedOrder = Order.getOrderByNumbersByActor(placerOrderNumber, null, simulatedActor,
						entityManager);
				if (selectedOrder != null) {
					selectedOrder.setLastChanged(new Date());
					selectedOrder.setOrderControlCode("SC");
					Segment orc = ormMessage.getORCOBRRQDRQ1ODSODTRXONTEDG1RXRRXCNTEOBXNTECTIBLG().getORC();
					selectedOrder.setOrderStatus(MessageDecoder.getOrderStatus(orc));
					Segment obr = ormMessage.getORCOBRRQDRQ1ODSODTRXONTEDG1RXRRXCNTEOBXNTECTIBLG()
							.getOBRRQDRQ1ODSODTRXONTEDG1RXRRXCNTEOBXNTE().getOBR();
					selectedOrder.setOrderResultStatus(MessageDecoder.getOrderResultStatus(obr));
					selectedOrder.save(entityManager);
					return Receiver.buildAck(serverApplication, serverFacility, sendingApplication, sendingFacility,
							"O01", AcknowledgmentCode.AA, messageControlId, hl7Charset, hl7Version);
				} else {
					return Receiver.buildNack(serverApplication, serverFacility, sendingApplication, sendingFacility,
							"O01", AcknowledgmentCode.AE, null, null, Receiver.UNKNOWN_KEY_IDENTIFIER, null, messageControlId,
							hl7Charset, hl7Version);
				}
			} catch (HL7Exception e) {
				log.error("Cannot extract placer order number from received message");
				return Receiver.buildNack(serverApplication, serverFacility, sendingApplication, sendingFacility,
						"O01", AcknowledgmentCode.AE, null, null, Receiver.REQUIRED_FIELD_MISSING, null, messageControlId, hl7Charset,
						hl7Version);
			}
		} else {
			return Receiver.buildNack(serverApplication, serverFacility, sendingApplication, sendingFacility, "O01",
					AcknowledgmentCode.AE, null, null, Receiver.SEGMENT_SEQUENCE_ERROR, null, messageControlId, hl7Charset, hl7Version);
		}
	}
	
	/**
	 * Update the status of a previous received order
	 * 
	 * @param omgMessage
	 * @return
	 */
	private Message processStatusUpdate(OMG_O19 omgMessage) {
		if (omgMessage.getORDERReps() > 0) {
			try {
				String placerOrderNumber = omgMessage.getORDER().getORC()
						.getPlacerOrderNumber().encode();
				Order selectedOrder = Order.getOrderByNumbersByActor(placerOrderNumber, null, simulatedActor,
						entityManager);
				if (selectedOrder != null) {
					selectedOrder.setLastChanged(new Date());
					selectedOrder.setOrderControlCode("SC");
					Segment orc = omgMessage.getORDER().getORC();
					selectedOrder.setOrderStatus(MessageDecoder.getOrderStatus(orc));
					Segment obr = omgMessage.getORDER().getOBR();
					selectedOrder.setOrderResultStatus(MessageDecoder.getOrderResultStatus(obr));
					selectedOrder.save(entityManager);
					return Receiver.buildAck(serverApplication, serverFacility, sendingApplication, sendingFacility,
							"O19", AcknowledgmentCode.AA, messageControlId, hl7Charset, hl7Version);
				} else {
					return Receiver.buildNack(serverApplication, serverFacility, sendingApplication, sendingFacility,
							"O19", AcknowledgmentCode.AE, null, null, Receiver.UNKNOWN_KEY_IDENTIFIER, null, messageControlId,
							hl7Charset, hl7Version);
				}
			} catch (HL7Exception e) {
				log.error("Cannot extract placer order number from received message");
				return Receiver.buildNack(serverApplication, serverFacility, sendingApplication, sendingFacility,
						"O19", AcknowledgmentCode.AE, null, null, Receiver.REQUIRED_FIELD_MISSING, null, messageControlId, hl7Charset,
						hl7Version);
			}
		} else {
			return Receiver.buildNack(serverApplication, serverFacility, sendingApplication, sendingFacility, "O19",
					AcknowledgmentCode.AE, null, null, Receiver.SEGMENT_SEQUENCE_ERROR, null, messageControlId, hl7Charset, hl7Version);
		}
	}

	/**
	 * Cancel a previous received order
	 * 
	 * @param ormMessage
	 * @return
	 */
	private Message processOrderCancellation(ORM_O01 ormMessage, String cancellationCode) {
		if (MessageDecoder.isSegmentPresent(ormMessage, "ORC")) {
			try {
				String placerOrderNumber = ormMessage.getORCOBRRQDRQ1ODSODTRXONTEDG1RXRRXCNTEOBXNTECTIBLG().getORC()
						.getPlacerOrderNumber().encode();
				String resultStatus = ormMessage.getORCOBRRQDRQ1ODSODTRXONTEDG1RXRRXCNTEOBXNTECTIBLG()
						.getOBRRQDRQ1ODSODTRXONTEDG1RXRRXCNTEOBXNTE().getOBR().getResultStatus().getValue();
				Order selectedOrder = Order.getOrderByNumbersByActor(placerOrderNumber, null, simulatedActor,
						entityManager);
				if (selectedOrder != null) {
					selectedOrder.setLastChanged(new Date());
					selectedOrder.setOrderControlCode(cancellationCode);
					selectedOrder.setOrderResultStatus(resultStatus);
					// if the simulator is the order placer, we should set the
					// order status to CA (=cancel)
					if (simulatedActor.getKeyword().equals("OP")) {
						selectedOrder.setOrderStatus("CA");
					}
					selectedOrder.save(entityManager);
					return Receiver.buildAck(serverApplication, serverFacility, sendingApplication, sendingFacility,
							"O01", AcknowledgmentCode.AA, messageControlId, hl7Charset, hl7Version);
				} else {
					return Receiver.buildNack(serverApplication, serverFacility, sendingApplication, sendingFacility,
							"O01", AcknowledgmentCode.AE, null, null, Receiver.UNKNOWN_KEY_IDENTIFIER, null, messageControlId,
							hl7Charset, hl7Version);
				}
			} catch (HL7Exception e) {
				log.error("Cannot extract placer order number from received message");
				return Receiver.buildNack(serverApplication, serverFacility, sendingApplication, sendingFacility,
						"O01", AcknowledgmentCode.AE, null, null, Receiver.REQUIRED_FIELD_MISSING, null, messageControlId, hl7Charset,
						hl7Version);
			}
		} else {
			return Receiver.buildNack(serverApplication, serverFacility, sendingApplication, sendingFacility, "O01",
					AcknowledgmentCode.AE, null, null, Receiver.SEGMENT_SEQUENCE_ERROR, null, messageControlId, hl7Charset, hl7Version);
		}
	}

	/**
	 * Cancel a previous received order
	 * 
	 * @param omgMessage
	 * @return
	 */
	private Message processOrderCancellation(OMG_O19 omgMessage, String cancellationCode) {
		if (omgMessage.getORDERReps() > 0) {
			try {
				String placerOrderNumber = omgMessage.getORDER().getORC()
						.getPlacerOrderNumber().encode();
				String resultStatus = omgMessage.getORDER().getOBR().getResultStatus().getValue();
				Order selectedOrder = Order.getOrderByNumbersByActor(placerOrderNumber, null, simulatedActor,
						entityManager);
				if (selectedOrder != null) {
					selectedOrder.setLastChanged(new Date());
					selectedOrder.setOrderControlCode(cancellationCode);
					selectedOrder.setOrderResultStatus(resultStatus);
					// if the simulator is the order placer, we should set the
					// order status to CA (=cancel)
					if (simulatedActor.getKeyword().equals("OP")) {
						selectedOrder.setOrderStatus("CA");
					}
					selectedOrder.save(entityManager);
					return Receiver.buildAck(serverApplication, serverFacility, sendingApplication, sendingFacility,
							"O19", AcknowledgmentCode.AA, messageControlId, hl7Charset, hl7Version);
				} else {
					return Receiver.buildNack(serverApplication, serverFacility, sendingApplication, sendingFacility,
							"O19", AcknowledgmentCode.AE, null, null, Receiver.UNKNOWN_KEY_IDENTIFIER, null, messageControlId,
							hl7Charset, hl7Version);
				}
			} catch (HL7Exception e) {
				log.error("Cannot extract placer order number from received message");
				return Receiver.buildNack(serverApplication, serverFacility, sendingApplication, sendingFacility,
						"O19", AcknowledgmentCode.AE, null, null, Receiver.REQUIRED_FIELD_MISSING, null, messageControlId, hl7Charset,
						hl7Version);
			}
		} else {
			return Receiver.buildNack(serverApplication, serverFacility, sendingApplication, sendingFacility, "O19",
					AcknowledgmentCode.AE, null, null, Receiver.SEGMENT_SEQUENCE_ERROR, null, messageControlId, hl7Charset, hl7Version);
		}
	}

	/**
	 * Stop the fulfillment of a previous received order
	 * 
	 * @param ormMessage
	 * @return
	 */
	private Message processDiscontinueOrder(ORM_O01 ormMessage) {
		if (MessageDecoder.isSegmentPresent(ormMessage, "ORC")) {
			try {
				String placerOrderNumber = ormMessage.getORCOBRRQDRQ1ODSODTRXONTEDG1RXRRXCNTEOBXNTECTIBLG().getORC()
						.getPlacerOrderNumber().encode();
				Order selectedOrder = Order.getOrderByNumbersByActor(placerOrderNumber, null, simulatedActor,
						entityManager);
				if (selectedOrder != null) {
					selectedOrder.setLastChanged(new Date());
					selectedOrder.setOrderControlCode("DC");
					selectedOrder.save(entityManager);
					return Receiver.buildAck(serverApplication, serverFacility, sendingApplication, sendingFacility,
							"O01", AcknowledgmentCode.AA, messageControlId, hl7Charset, hl7Version);
				} else {
					return Receiver.buildNack(serverApplication, serverFacility, sendingApplication, sendingFacility,
							"O01", AcknowledgmentCode.AE, null, null, Receiver.UNKNOWN_KEY_IDENTIFIER, null, messageControlId,
							hl7Charset, hl7Version);
				}
			} catch (HL7Exception e) {
				log.error("Cannot extract placer order number from received message");
				return Receiver.buildNack(serverApplication, serverFacility, sendingApplication, sendingFacility,
						"O01", AcknowledgmentCode.AE, null, null, Receiver.REQUIRED_FIELD_MISSING, null, messageControlId, hl7Charset,
						hl7Version);
			}
		} else {
			return Receiver.buildNack(serverApplication, serverFacility, sendingApplication, sendingFacility, "O01",
					AcknowledgmentCode.AE, null, null, Receiver.SEGMENT_SEQUENCE_ERROR, null, messageControlId, hl7Charset, hl7Version);
		}
	}
	
	/**
	 * Stop the fulfillment of a previous received order
	 * 
	 * @param omgMessage
	 * @return
	 */
	private Message processDiscontinueOrder(OMG_O19 omgMessage) {
		if (omgMessage.getORDERReps() > 0) {
			try {
				String placerOrderNumber = omgMessage.getORDER().getORC()
						.getPlacerOrderNumber().encode();
				Order selectedOrder = Order.getOrderByNumbersByActor(placerOrderNumber, null, simulatedActor,
						entityManager);
				if (selectedOrder != null) {
					selectedOrder.setLastChanged(new Date());
					selectedOrder.setOrderControlCode("DC");
					selectedOrder.save(entityManager);
					return Receiver.buildAck(serverApplication, serverFacility, sendingApplication, sendingFacility,
							"O19", AcknowledgmentCode.AA, messageControlId, hl7Charset, hl7Version);
				} else {
					return Receiver.buildNack(serverApplication, serverFacility, sendingApplication, sendingFacility,
							"O19", AcknowledgmentCode.AE, null, null, Receiver.UNKNOWN_KEY_IDENTIFIER, null, messageControlId,
							hl7Charset, hl7Version);
				}
			} catch (HL7Exception e) {
				log.error("Cannot extract placer order number from received message");
				return Receiver.buildNack(serverApplication, serverFacility, sendingApplication, sendingFacility,
						"O19", AcknowledgmentCode.AE, null, null, Receiver.REQUIRED_FIELD_MISSING, null, messageControlId, hl7Charset,
						hl7Version);
			}
		} else {
			return Receiver.buildNack(serverApplication, serverFacility, sendingApplication, sendingFacility, "O19",
					AcknowledgmentCode.AE, null, null, Receiver.SEGMENT_SEQUENCE_ERROR, null, messageControlId, hl7Charset, hl7Version);
		}
	}

	/**
	 * response to ORM_O01 messages
	 * 
	 * @param errorCode
	 * @param selectedOrder
	 * @param incomingMessage
	 * @return
	 */
	private Message createResponseForNewOrder(String errorCode, Order selectedOrder, ORM_O01 incomingMessage) {
		if (simulatedActor.getKeyword().equals("OF")) {
			// ACK
			if (errorCode == null) {
				return Receiver.buildAck(serverApplication, serverFacility, sendingApplication, sendingFacility, "O01",
						AcknowledgmentCode.AA, messageControlId, hl7Charset, hl7Version);
			}
			// NACK
			else {
				return Receiver.buildNack(serverApplication, serverFacility, sendingApplication, sendingFacility,
						"O01", AcknowledgmentCode.AE, null, null, errorCode, null, messageControlId, hl7Charset, hl7Version);
			}
		} else {
			HL7V2ResponderSUTConfiguration sut = new HL7V2ResponderSUTConfiguration();
			sut.setApplication(sendingApplication);
			sut.setFacility(sendingFacility);
			Charset charset = new Charset();
			charset.setHl7Code(hl7Charset);
			sut.setCharset(charset);
			IHERadMessageBuilder generator = new IHERadMessageBuilder(null, null, selectedOrder, sut, serverApplication,
					serverFacility, false);
			// ORR (OK)
			if (errorCode == null) {
				try {
					return generator.buildORR(false, messageControlId, null, incomingMessage
							.getORCOBRRQDRQ1ODSODTRXONTEDG1RXRRXCNTEOBXNTECTIBLG()
							.getOBRRQDRQ1ODSODTRXONTEDG1RXRRXCNTEOBXNTE().getOBR().encode(), incomingMessage
							.getORCOBRRQDRQ1ODSODTRXONTEDG1RXRRXCNTEOBXNTECTIBLG().getORC().encode());
				} catch (HL7Exception e) {
					log.error("cannot encode received segments", e);
					// Auto-generated catch block
					return generator.buildORR(true, messageControlId, Receiver.INTERNAL_ERROR, null, null);
				}
			}
			// ORR (error)
			else {
				return generator.buildORR(true, messageControlId, errorCode, null, null);
			}
		}
	}

	/**
	 * response to OMG_O19 messages
	 * 
	 * @param errorCode
	 * @param selectedOrder
	 * @param incomingMessage
	 * @return
	 */
	private Message createResponseForNewOrder(String errorCode, Order selectedOrder, OMG_O19 incomingMessage) {
		if (simulatedActor.getKeyword().equals("OF")) {
			// ACK
			if (errorCode == null) {
				return Receiver.buildAck(serverApplication, serverFacility, sendingApplication, sendingFacility, "O19",
						AcknowledgmentCode.AA, messageControlId, hl7Charset, hl7Version);
			}
			// NACK
			else {
				return Receiver.buildNack(serverApplication, serverFacility, sendingApplication, sendingFacility,
						"O19", AcknowledgmentCode.AE, null, null, errorCode, null, messageControlId, hl7Charset, hl7Version);
			}
		} else {
			HL7V2ResponderSUTConfiguration sut = new HL7V2ResponderSUTConfiguration();
			sut.setApplication(sendingApplication);
			sut.setFacility(sendingFacility);
			Charset charset = new Charset();
			charset.setHl7Code(hl7Charset);
			sut.setCharset(charset);
			IHERadMessageBuilder generator = new IHERadMessageBuilder(null, null, selectedOrder, sut, serverApplication,
					serverFacility, false);
			// ORG (OK)
			if (errorCode == null) {
				try {
					return generator.buildORG(false, messageControlId, null, incomingMessage.getORDER().getOBR().encode(), incomingMessage
							.getORDER().getORC().encode());
				} catch (HL7Exception e) {
					log.error("cannot encode received segments", e);
					// Auto-generated catch block
					return generator.buildORG(true, messageControlId, Receiver.INTERNAL_ERROR, null, null);
				}
			}
			// ORR (error)
			else {
				return generator.buildORG(true, messageControlId, errorCode, null, null);
			}
		}
	}

	/**
	 * 
	 * @param inMessage
	 * @param newControlCode
	 * @param cancelControlCode
	 * @param updateControlCode
	 * @return the acknowledgement
	 */
	protected Message processOMLMessage(Message inMessage, String newControlCode, String cancelControlCode,
			String updateControlCode, String discontinueRequestControlCode) {
		ORLMessageBuilder orlBuilder = new ORLMessageBuilder(serverApplication, serverFacility, sendingApplication,
				sendingFacility, messageControlId, hl7Charset);

		// In the OML_O33 for the LAB-28 transaction, the PATIENT group is not Required
		if (!simulatedTransaction.getKeyword().equals("LAB-28") && !MessageDecoder.isSegmentPresent(inMessage, "PID")) {
			return orlBuilder.createNack(inMessage, AcknowledgmentCode.AE, Receiver.SEGMENT_SEQUENCE_ERROR, Receiver.SEVERITY_ERROR,
					"The PID segment is required but missing", simulatedActor.getKeyword());
		}
		String creator = sendingApplication + "_" + sendingFacility;
		Encounter selectedEncounter = null;
		if (MessageDecoder.isSegmentPresent(inMessage, "PID")) {
			Patient pidPatient = null;
			try {
				Terser terser = new Terser(inMessage);
				Segment segment = terser.getSegment("/.PID");
				pidPatient = MessageDecoder.extractPatientFromPID(segment);
			} catch (HL7Exception e) {
				log.error(e.getMessage(), e);
				return orlBuilder.createNack(inMessage, AcknowledgmentCode.AE, Receiver.SEGMENT_SEQUENCE_ERROR,
						Receiver.SEVERITY_ERROR, "PID segment is missing", simulatedActor.getKeyword());
			}
			Patient selectedPatient = null;
			List<PatientIdentifier> pidPatientIdentifiers;
			pidPatientIdentifiers = MessageDecoder.extractPatientIdentifiersFromMessage25(inMessage);
			List<PatientIdentifier> identifiersToAdd = new ArrayList<PatientIdentifier>();
			for (PatientIdentifier pid : pidPatientIdentifiers) {
				PatientIdentifier existingPID = pid.getStoredPatientIdentifier(entityManager);
				if (existingPID != null) {
					if (selectedPatient == null) {
						selectedPatient = existingPID.getPatient();
					} else if (!selectedPatient.getId().equals(existingPID.getPatient().getId())) {
						return orlBuilder.createNack(inMessage, AcknowledgmentCode.AE, Receiver.DUPLICATE_KEY_IDENTIFIER,
								Receiver.SEVERITY_ERROR, ERROR_INCONSISTENT_PID, simulatedActor.getKeyword());
					} else {
						continue;
					}
				} else {
					pid = pid.save(entityManager);
					identifiersToAdd.add(pid);
				}
			}
			// if none of the identifiers is already known by the simulator, we save the patient contained in the message
			if (selectedPatient == null) {
				// save the patient
				pidPatient.setCreator(creator);
				selectedPatient = pidPatient.save(entityManager);
			}
			for (PatientIdentifier pid : identifiersToAdd) {
				pid.setPatient(selectedPatient);
				pid.save(entityManager);
			}
			// check existance of PV1 segment
			Encounter pv1Encounter = null;
			if (MessageDecoder.isSegmentPresent(inMessage, "PV1")) {
				pv1Encounter = MessageDecoder.extractEncounterFromPV1(inMessage);
				if (pv1Encounter.getVisitNumber() != null) {
					// check the encounter exists
					selectedEncounter = Encounter.getEncounterByVisitNumber(pv1Encounter.getVisitNumber(),
							entityManager);
					if ((selectedEncounter != null)
							&& (!selectedEncounter.getPatient().getId().equals(selectedPatient.getId()))) {
						return orlBuilder.createNack(inMessage, AcknowledgmentCode.AE, Receiver.DUPLICATE_KEY_IDENTIFIER,
								Receiver.SEVERITY_ERROR, "This visit number is already used for another patient", simulatedActor.getKeyword());
					} else if (selectedEncounter == null) {
						selectedEncounter = pv1Encounter;
						selectedEncounter.setPatient(selectedPatient);
						selectedEncounter.setCreator(creator);
						selectedEncounter = selectedEncounter.save(entityManager);
					}
					// else selectedEncounter is the one retrieved from the database
				} else {
					return orlBuilder.createNack(inMessage, AcknowledgmentCode.AE, Receiver.REQUIRED_FIELD_MISSING,
							Receiver.SEVERITY_ERROR, "PV1-19 is required but missing", simulatedActor.getKeyword());
				}
			} else {
				log.warn("No PV1 segment found !");
				selectedEncounter = new Encounter();
				selectedEncounter.setPatient(selectedPatient);
				selectedEncounter.setCreator(creator);
				selectedEncounter = selectedEncounter.save(entityManager);
			}
		}

		Domain domain = Domain.getDomainByKeyword("LAB", entityManager);
		if (inMessage instanceof ca.uhn.hl7v2.model.v25.message.OML_O21) {
			return parseOMLO21(inMessage, newControlCode, cancelControlCode, updateControlCode, orlBuilder, domain,
					creator, selectedEncounter);
		} else if (inMessage instanceof ca.uhn.hl7v2.model.v25.message.OML_O33) {
			return parseOMLO33(inMessage, newControlCode, cancelControlCode, updateControlCode,
					discontinueRequestControlCode, orlBuilder, domain, creator, selectedEncounter);
		} else if (inMessage instanceof ca.uhn.hl7v2.model.v25.message.OML_O35) {
			return parseOMLO35(inMessage, newControlCode, cancelControlCode, updateControlCode, orlBuilder, domain,
					creator, selectedEncounter);
		} else {
			return orlBuilder.createNack(inMessage, AcknowledgmentCode.AE, Receiver.INTERNAL_ERROR, Receiver.SEVERITY_ERROR,
					"This message structure is not allowed", simulatedActor.getKeyword());
		}

	}

	/**
	 * Parse the current ORDER group, create the related order and fill the response
	 * @param messageOrder
	 * @param orderGroup
	 * @param creator
	 * @param domain
	 * @param response
	 * @param orlBuilder
	 * @param index
	 * @return
	 */
	private ORL_O22 createNewOrderFromOMLO21(LabOrder messageOrder, OML_O21_ORDER orderGroup, String creator,
			Domain domain, ORL_O22 response, ORLMessageBuilder orlBuilder, int index) {
		// we need to attribute a OP# or OF# to the newly created order
		String orderNumber = NumberGenerator.getOrderNumberForActor(simulatedActor, entityManager);
		if (simulatedActor.getKeyword().equals("OF")) {
			messageOrder.setFillerOrderNumber(orderNumber);
			messageOrder.setOrderStatus("SC");
		} else {
			messageOrder.setPlacerOrderNumber(orderNumber);
			String groupNumber = NumberGenerator.getOrderNumberForActor(simulatedActor, entityManager);
			messageOrder.setPlacerGroupNumber(groupNumber);
		}
		if (messageOrder.getOrderControlCode().equals("SN")) {
			messageOrder.setOrderControlCode("NA");
		} else {
			messageOrder.setOrderControlCode("OK");
		}
		orlBuilder.addOrderGroupAndUpdateControlCode(response.getRESPONSE().getPATIENT().getORDER(index), orderGroup,
				messageOrder.getOrderControlCode(), messageOrder.getPlacerOrderNumber(), messageOrder
						.getFillerOrderNumber(), messageOrder.getPlacerGroupNumber(), simulatedActor.getKeyword()
						.equals("OF"));
		int numberOfSpecimens = orderGroup.getOBSERVATION_REQUEST().getSPECIMENReps();
		if (numberOfSpecimens > 0) {
			List<Specimen> specimens = new ArrayList<Specimen>();
			for (int specimenIndex = 0; specimenIndex < numberOfSpecimens; specimenIndex++) {
				OML_O21_SPECIMEN specimenGroup = orderGroup.getOBSERVATION_REQUEST().getSPECIMEN(specimenIndex);
				Segment spm = specimenGroup.getSPM();
				// specimen contained in the message
				Specimen messageSpecimen = MessageDecoder.extractSpecimenFromSegment(spm, creator, simulatedActor,
						domain);
				messageSpecimen.setMainEntity(false);
				// check if the specimen is already known by the actor
				Specimen selectedSpecimen = Specimen.getSpecimenByAssignedIdentifiersByActorByDomain(
						messageSpecimen.getPlacerAssignedIdentifier(), messageSpecimen.getFillerAssignedIdentifier(),
						simulatedActor, domain, entityManager, false);
				if (selectedSpecimen != null) {
					messageSpecimen = selectedSpecimen;
				} else {
					// assigne identifiers to specimen
					if (simulatedActor.getKeyword().equals("OP")
							&& ((messageSpecimen.getPlacerAssignedIdentifier() == null) || messageSpecimen
									.getPlacerAssignedIdentifier().isEmpty())) {
						messageSpecimen.setPlacerAssignedIdentifier(NumberGenerator.getOrderNumberForActor(
								simulatedActor, entityManager, "&"));
					}
					if (simulatedActor.getKeyword().equals("OF")
							&& ((messageSpecimen.getFillerAssignedIdentifier() == null) || messageSpecimen
									.getFillerAssignedIdentifier().isEmpty())) {
						messageSpecimen.setFillerAssignedIdentifier(NumberGenerator.getOrderNumberForActor(
								simulatedActor, entityManager, "&"));
					}
				}
				orlBuilder.addSpecimen(response.getRESPONSE().getPATIENT().getORDER(index).getOBSERVATION_REQUEST()
						.getSPECIMEN(specimenIndex).getSPM(), specimenGroup.getSPM(),
						messageSpecimen.getPlacerAssignedIdentifier(), messageSpecimen.getFillerAssignedIdentifier());
				int numberOfContainers = specimenGroup.getCONTAINERReps();
				if (numberOfContainers > 0) {
					List<Container> containers = new ArrayList<Container>();
					for (int containerIndex = 0; containerIndex < numberOfContainers; containerIndex++) {
						Container messageContainer = MessageDecoder.extractContainerFromSAC(
								specimenGroup.getCONTAINER(containerIndex).getSAC(), simulatedActor, messageSpecimen,
								creator, domain);
						messageContainer.setMainEntity(false);
						Container selectedContainer = Container
								.getContainerByIdentifierByActorByDomain(messageContainer.getContainerIdentifier(),
										simulatedActor, domain, entityManager, false);
						if (selectedContainer != null) {
							messageContainer = selectedContainer;
						}
						containers.add(messageContainer);
					}
					messageSpecimen.setContainers(containers);
					if (messageSpecimen.getOrders() == null) {
						messageSpecimen.setOrders(new ArrayList<LabOrder>());
					}
					if (!messageSpecimen.getOrders().contains(messageOrder)) {
						messageSpecimen.getOrders().add(messageOrder);
					}
					specimens.add(messageSpecimen);
				}
			}
			if (messageOrder.getSpecimens() == null) {
				messageOrder.setSpecimens(specimens);
			} else {
				messageOrder.getSpecimens().addAll(specimens);
			}
			messageOrder.save(entityManager);
		}
		return response;
	}

	/**
	 * parses the OML^O21^OML_O21 message to extract the list of orders and returns the appropriate ack
	 * 
	 * @param inMessage
	 * @param newControlCode
	 * @param cancelControlCode
	 * @param updateControlCode
	 * @param orlBuilder
	 * @param domain
	 * @param creator
	 * @param selectedEncounter
	 * @return
	 */
	private Message parseOMLO21(Message inMessage, String newControlCode, String cancelControlCode,
			String updateControlCode, ORLMessageBuilder orlBuilder, Domain domain, String creator,
			Encounter selectedEncounter) {
		PipeParser pipeParser = new PipeParser();
		// cast message using the class generated using the IHE HL7 message profile
		OML_O21 o21Message;
		try {
			o21Message = (OML_O21) pipeParser.parseForSpecificPackage(inMessage.encode(),
					"net.ihe.gazelle.oml.hl7v2.model.v25.message");
		} catch (HL7Exception e) {
			return orlBuilder.createNack(inMessage, AcknowledgmentCode.AE, Receiver.INTERNAL_ERROR, Receiver.SEVERITY_ERROR,
					"Cannot properly encode the message for parsing", simulatedActor.getKeyword());
		}

		// prepare response
		ORL_O22 response = orlBuilder.createNewORL_O22(AcknowledgmentCode.AA, o21Message.getPATIENT().getPID());

		// BEGIN PARSING
		int numberOfOrders = o21Message.getORDERReps();

		if (numberOfOrders == 0) {
			return orlBuilder.createNack(inMessage, AcknowledgmentCode.AE, Receiver.SEGMENT_SEQUENCE_ERROR, Receiver.SEVERITY_ERROR,
					"No ORDER group found", simulatedActor.getKeyword());
		}
		for (int index = 0; index < numberOfOrders; index++) {
			// check ORC and OBR segments (required) are present
			Segment orcSegment = o21Message.getORDER(index).getORC();
			if (MessageDecoder.isEmpty(orcSegment)) {
				entityManager.getTransaction().rollback();
				return orlBuilder.createNack(inMessage, AcknowledgmentCode.AE, Receiver.SEGMENT_SEQUENCE_ERROR, Receiver.SEVERITY_ERROR,
						"ORC segment is missing in repetition " + index + " of ORDER group", simulatedActor.getKeyword());
			}
			Segment obrSegment = o21Message.getORDER(index).getOBSERVATION_REQUEST().getOBR();
			if (MessageDecoder.isEmpty(obrSegment)) {
				entityManager.getTransaction().rollback();
				return orlBuilder.createNack(inMessage, AcknowledgmentCode.AE, Receiver.SEGMENT_SEQUENCE_ERROR, Receiver.SEVERITY_ERROR,
						"OBR segment is missing in repetition " + index + " of ORDER group", simulatedActor.getKeyword());
			}
			// the order contained in the current ORDER group of the received message
			LabOrder messageOrder = MessageDecoder.extractLabOrderFromSegments(orcSegment, obrSegment, o21Message
					.getORDER(index).getTIMING().getTQ1(), simulatedActor, domain, creator, selectedEncounter);
			messageOrder.setWorkOrder(false);
			messageOrder.setMainEntity(true);
			// the order retrieved from the database using numbers given in the message
			LabOrder selectedOrder = AbstractOrder.getOrderByNumbersByActor(LabOrder.class,
					messageOrder.getPlacerOrderNumber(), messageOrder.getFillerOrderNumber(), null, simulatedActor,
					entityManager, false);
			if (messageOrder.getOrderControlCode() == null) {
				entityManager.getTransaction().rollback();
				return orlBuilder.createNack(inMessage, AcknowledgmentCode.AE, Receiver.REQUIRED_FIELD_MISSING, Receiver.SEVERITY_ERROR,
						"ORC-1 is required but missing", simulatedActor.getKeyword());
			} else if (messageOrder.getOrderControlCode().equals(newControlCode)) {
				if (simulatedActor.getKeyword().equals("OP")) {
					simulatedTransaction = Transaction.GetTransactionByKeyword("LAB-2", entityManager);
				}
				// the received order is supposed to be new, check that we do not know it
				if (selectedOrder != null) {
					// order already exists with the same placer/filler order number ==> message is discarded and error returns to the sender
					entityManager.getTransaction().rollback();
					if (simulatedActor.getKeyword().equals("OF")) {
						return orlBuilder.createNack(inMessage, AcknowledgmentCode.AE, Receiver.DUPLICATE_KEY_IDENTIFIER,
								Receiver.SEVERITY_ERROR, "An order with this placer order number already exists", simulatedActor.getKeyword());
					} else {
						return orlBuilder.createNack(inMessage, AcknowledgmentCode.AE, Receiver.DUPLICATE_KEY_IDENTIFIER,
								Receiver.SEVERITY_ERROR, "An order with this filler order number already exists", simulatedActor.getKeyword());
					}
				} else {
					response = createNewOrderFromOMLO21(messageOrder, o21Message.getORDER(index), creator, domain,
							response, orlBuilder, index);
				}
			} else if (messageOrder.getOrderControlCode().equals(cancelControlCode)) {
				// unknown order or order already started ==> UA/UC
				if (selectedOrder == null) {
					if (messageOrder.getOrderControlCode().equals("CA")) {
						orlBuilder.addOrderGroupAndUpdateControlCode(response.getRESPONSE().getPATIENT()
								.getORDER(index), o21Message.getORDER(index), "UC", null, null, null, false);
					} else {
						orlBuilder.addOrderGroupAndUpdateControlCode(response.getRESPONSE().getPATIENT()
								.getORDER(index), o21Message.getORDER(index), "UA", null, null, null, false);
					}
				} else if (simulatedActor.getKeyword().equals("OF") && (selectedOrder.getOrderStatus() != null)
						&& !selectedOrder.getOrderStatus().isEmpty()) {
					selectedOrder.setOrderControlCode("UC");
					selectedOrder.save(entityManager);
					orlBuilder.addOrderGroupAndUpdateControlCode(response.getRESPONSE().getPATIENT().getORDER(index),
							o21Message.getORDER(index), "UC", null, null, null, false);
				} else {
					// cancel accepted ==> CR
					String newOrderControlCode = "OK";
					if (messageOrder.getOrderControlCode().equals("CA")) {
						newOrderControlCode = "CR";
					}
					orlBuilder.addOrderGroupAndUpdateControlCode(response.getRESPONSE().getPATIENT().getORDER(index),
							o21Message.getORDER(index), newOrderControlCode, null, null, null, false);
					selectedOrder.setOrderControlCode(newOrderControlCode);
					selectedOrder.setOrderStatus(messageOrder.getOrderStatus());
					Segment obr = o21Message.getORDER(index).getOBSERVATION_REQUEST().getOBR();
					selectedOrder.setOrderResultStatus(MessageDecoder.getOrderResultStatus(obr));
					selectedOrder.save(entityManager);
				}
			} else if ((updateControlCode != null) && messageOrder.getOrderControlCode().equals(updateControlCode)) {
				if (selectedOrder == null) {
					// order not found ==> UA
					orlBuilder.addOrderGroupAndUpdateControlCode(response.getRESPONSE().getPATIENT().getORDER(index),
							o21Message.getORDER(index), "UA", null, null, null, false);
				} else {
					// update accepted
					selectedOrder.setOrderControlCode("OK");
					Segment orc = o21Message.getORDER(index).getORC();
					selectedOrder.setOrderStatus(MessageDecoder.getOrderStatus(orc));
					Segment obr = o21Message.getORDER(index).getOBSERVATION_REQUEST().getOBR();
					selectedOrder.setOrderResultStatus(MessageDecoder.getOrderResultStatus(obr));
					selectedOrder.save(entityManager);
					orlBuilder.addOrderGroupAndUpdateControlCode(response.getRESPONSE().getPATIENT().getORDER(index),
							o21Message.getORDER(index), "OK", null, null, null, false);
				}
			} else {
				entityManager.getTransaction().rollback();
				return orlBuilder.createNack(inMessage, AcknowledgmentCode.AE, Receiver.TABLE_VALUE_NOT_FOUND, Receiver.SEVERITY_ERROR,
						"Unexpected order control code: " + messageOrder.getOrderControlCode(), simulatedActor.getKeyword());
			}
		}
		return response;
	}

	/**
	 * parses the OML^O33^OML_O33 message to extract the list of orders and returns the appropriate ack
	 * 
	 * @param inMessage
	 * @param newControlCode
	 * @param cancelControlCode
	 * @param updateControlCode
	 * @param orlBuilder
	 * @param domain
	 * @param creator
	 * @param selectedEncounter
	 * @return the acknowledgement
	 */
	protected Message parseOMLO33(Message inMessage, String newControlCode, String cancelControlCode,
			String updateControlCode, String discontinueRequestControlCode, ORLMessageBuilder orlBuilder,
			Domain domain, String creator, Encounter selectedEncounter) {
		PipeParser pipeParser = PipeParser.getInstanceWithNoValidation();
		// cast message using the class generated using the IHE HL7 message profile
		OML_O33 o33Message;
		try {
			o33Message = (OML_O33) pipeParser.parseForSpecificPackage(inMessage.encode(),
					"net.ihe.gazelle.oml.hl7v2.model.v25.message");
		} catch (HL7Exception e) {
			return orlBuilder.createNack(inMessage, AcknowledgmentCode.AE, Receiver.INTERNAL_ERROR, Receiver.SEVERITY_ERROR,
					"Cannot properly encode the message for parsing", simulatedActor.getKeyword());
		}
		// prepare response
		ORL_O34 response = null;
		response = orlBuilder.createNewORL_O34(AcknowledgmentCode.AA, o33Message.getPATIENT().getPID());

		// BEGIN PARSING
		int nbOfSpecimens = o33Message.getSPECIMENReps();
		if (nbOfSpecimens == 0) {
			return orlBuilder.createNack(inMessage, AcknowledgmentCode.AE, Receiver.SEGMENT_SEQUENCE_ERROR, Receiver.SEVERITY_ERROR,
					"No SPECIMEN group found", simulatedActor.getKeyword());
		}
		for (int index = 0; index < nbOfSpecimens; index++) {
			Segment spm = o33Message.getSPECIMEN(index).getSPM();
			if (MessageDecoder.isEmpty(spm)) {
				entityManager.getTransaction().rollback();
				return orlBuilder.createNack(inMessage, AcknowledgmentCode.AE, Receiver.SEGMENT_SEQUENCE_ERROR, Receiver.SEVERITY_ERROR,
						"SPM segment is missing in repetition " + index + " of SPECIMEN group", simulatedActor.getKeyword());
			} else {
				// create specimen from data contained in SPM segment
				Specimen messageSpecimen = MessageDecoder.extractSpecimenFromSegment(spm, creator, simulatedActor,
						domain);
				messageSpecimen.setMainEntity(true);
				// check if the specimen is already known by the actor
				Specimen selectedSpecimen = Specimen.getSpecimenByAssignedIdentifiersByActorByDomain(
						messageSpecimen.getPlacerAssignedIdentifier(), messageSpecimen.getFillerAssignedIdentifier(),
						simulatedActor, domain, entityManager, false);
				if (selectedSpecimen != null) {
					messageSpecimen = selectedSpecimen;
				}
				int nbOfContainers = o33Message.getSPECIMEN(index).getSACReps();
				if (nbOfContainers > 0) {
					List<Container> containers = new ArrayList<Container>();
					for (int containerIndex = 0; containerIndex < nbOfContainers; containerIndex++) {
						Container messageContainer = MessageDecoder.extractContainerFromSAC(
								o33Message.getSPECIMEN(index).getSAC(containerIndex), simulatedActor, messageSpecimen,
								creator, domain);
						messageContainer.setMainEntity(false);
						Container selectedContainer = Container.getContainerBasedOnDescription(messageContainer,
								entityManager);
						if (selectedContainer != null) {
							if ((selectedSpecimen != null) && (selectedContainer.getSpecimen() != null)
									&& !selectedContainer.getSpecimen().getId().equals(selectedSpecimen.getId())) {
								entityManager.getTransaction().rollback();
								return orlBuilder.createNack(response, AcknowledgmentCode.AE, Receiver.DUPLICATE_KEY_IDENTIFIER,
										Receiver.SEVERITY_ERROR,
										"Container identifier already used for another specimen", simulatedActor.getKeyword());
							}
							// else, we do not add this container to the list since it is already linked to the specimen
						} else {
							containers.add(messageContainer);
						}
					}
					// link container to the current specimen
					if (messageSpecimen.getContainers() == null) {
						messageSpecimen.setContainers(containers);
					} else {
						messageSpecimen.getContainers().addAll(containers);
					}
				}
				// else, SAC segment is not required ==> not an structure error, continue parsing
				int nbOfOrderGroups = o33Message.getSPECIMEN(index).getORDERReps();
				if (nbOfOrderGroups == 0) {
					entityManager.getTransaction().rollback();
					return orlBuilder.createNack(inMessage, AcknowledgmentCode.AE, Receiver.SEGMENT_SEQUENCE_ERROR,
							Receiver.SEVERITY_ERROR, "ORDER group is missing in repetition " + index
									+ " of SPECIMEN group", simulatedActor.getKeyword());
				} else {
					if (simulatedActor.getKeyword().equals("OP")
							&& ((messageSpecimen.getPlacerAssignedIdentifier() == null) || messageSpecimen
									.getPlacerAssignedIdentifier().isEmpty())) {
						messageSpecimen.setPlacerAssignedIdentifier(NumberGenerator.getOrderNumberForActor(
								simulatedActor, entityManager, "&"));
					}
					if (simulatedActor.getKeyword().equals("OF")
							&& ((messageSpecimen.getFillerAssignedIdentifier() == null) || messageSpecimen
									.getFillerAssignedIdentifier().isEmpty())) {
						messageSpecimen.setFillerAssignedIdentifier(NumberGenerator.getOrderNumberForActor(
								simulatedActor, entityManager, "&"));
					}
					// add segments to response
					orlBuilder.addSpecimenAndContainers(response.getRESPONSE().getPATIENT().getSPECIMEN(index),
							o33Message.getSPECIMEN(index), messageSpecimen.getPlacerAssignedIdentifier(),
							messageSpecimen.getFillerAssignedIdentifier());
					// list orders relative to the current specimen
					List<LabOrder> orders = new ArrayList<LabOrder>();
					for (int orderGroupIndex = 0; orderGroupIndex < nbOfOrderGroups; orderGroupIndex++) {
						OML_O33_ORDER orderGroup = o33Message.getSPECIMEN(index).getORDER(orderGroupIndex);
						Segment orc = orderGroup.getORC();
						if (MessageDecoder.isEmpty(orc)) {
							entityManager.getTransaction().rollback();
							return orlBuilder.createNack(inMessage, AcknowledgmentCode.AE, Receiver.SEGMENT_SEQUENCE_ERROR,
									Receiver.SEVERITY_ERROR, "ORC segment is missing in repetition " + orderGroupIndex
											+ " of ORDER group (SPECIMEN(" + index + "))", simulatedActor.getKeyword());
						}
						Segment obr = orderGroup.getOBSERVATION_REQUEST().getOBR();
						if (MessageDecoder.isEmpty(obr)) {
							entityManager.getTransaction().rollback();
							return orlBuilder.createNack(inMessage, AcknowledgmentCode.AE, Receiver.SEGMENT_SEQUENCE_ERROR,
									Receiver.SEVERITY_ERROR, "OBR segment is missing in repetition " + orderGroupIndex
											+ " of ORDER group (SPECIMEN(" + index + "))", simulatedActor.getKeyword());
						}
						Segment tq1 = orderGroup.getTIMING().getTQ1();
						LabOrder messageOrder = MessageDecoder.extractLabOrderFromSegments(orc, obr, tq1,
								simulatedActor, domain, creator, selectedEncounter);
						messageOrder.setWorkOrder(false);
						messageOrder.setMainEntity(false);
						// the order retrieved from the database using numbers given in the message
						LabOrder selectedOrder = AbstractOrder.getOrderByNumbersByActor(LabOrder.class,
								messageOrder.getPlacerOrderNumber(), messageOrder.getFillerOrderNumber(),
								messageOrder.getPlacerGroupNumber(), simulatedActor, entityManager, false);
						if (messageOrder.getOrderControlCode() == null) {
							entityManager.getTransaction().rollback();
							return orlBuilder.createNack(inMessage, AcknowledgmentCode.AE, Receiver.REQUIRED_FIELD_MISSING,
									Receiver.SEVERITY_ERROR, "ORC-1 is required but missing", simulatedActor.getKeyword());
						} else if (messageOrder.getOrderControlCode().equals(newControlCode)) {
							if (simulatedActor.getKeyword().equals("OP")) {
								simulatedTransaction = Transaction.GetTransactionByKeyword("LAB-2", entityManager);
							}
							// the received order is supposed to be new, check that we do not know it
							if (selectedOrder != null) {
								// order already exists with the same placer/filler order number ==> message is discarded and error returns to the sender
								entityManager.getTransaction().rollback();
								if (simulatedActor.getKeyword().equals("OF")) {
									return orlBuilder.createNack(inMessage, AcknowledgmentCode.AE, Receiver.DUPLICATE_KEY_IDENTIFIER,
											Receiver.SEVERITY_ERROR,
											"An order with this placer order number already exists", simulatedActor.getKeyword());
								} else if (simulatedActor.getKeyword().equals("ANALYZER")) {
									return orlBuilder.createNack(inMessage, AcknowledgmentCode.AE, Receiver.DUPLICATE_KEY_IDENTIFIER,
											Receiver.SEVERITY_ERROR, "A awos with this WOS ID already exists", simulatedActor.getKeyword());
								} else {
									return orlBuilder.createNack(inMessage, AcknowledgmentCode.AE, Receiver.DUPLICATE_KEY_IDENTIFIER,
											Receiver.SEVERITY_ERROR,
											"An order with this filler order number already exists", simulatedActor.getKeyword());
								}
							} else {
								if (simulatedActor.getKeyword().equals("OP")) {
									messageOrder.setPlacerOrderNumber(NumberGenerator.getOrderNumberForActor(
											simulatedActor, entityManager));
									messageOrder.setPlacerGroupNumber(NumberGenerator.getOrderNumberForActor(
											simulatedActor, entityManager));
									messageOrder.setOrderControlCode("NA");
								} else {
									if (simulatedActor.getKeyword().equals("OF")) {
										messageOrder.setFillerOrderNumber(NumberGenerator.getOrderNumberForActor(
												simulatedActor, entityManager));
									}
									messageOrder.setOrderControlCode("OK");
									messageOrder.setOrderStatus("SC");
								}
								orders.add(messageOrder);
								// add segments in response
								orlBuilder.addOrderGroupAndUpdateControlCode(response.getRESPONSE().getPATIENT()
										.getSPECIMEN(index).getORDER(orderGroupIndex), o33Message.getSPECIMEN(index)
										.getORDER(orderGroupIndex), messageOrder.getOrderControlCode(), messageOrder
										.getPlacerOrderNumber(), messageOrder.getFillerOrderNumber(), messageOrder
										.getPlacerGroupNumber(), simulatedActor.getKeyword().equals("OF"));
							}
						} else if (messageOrder.getOrderControlCode().equals(cancelControlCode)) {
							// unknown order or order already started ==> UA/UC
							if (selectedOrder == null) {
								if (messageOrder.getOrderControlCode().equals("CA")) {
									orlBuilder.addOrderGroupAndUpdateControlCode(response.getRESPONSE().getPATIENT()
											.getSPECIMEN(index).getORDER(orderGroupIndex), o33Message
											.getSPECIMEN(index).getORDER(orderGroupIndex), "UC", null, null, null,
											false);
								} else {
									orlBuilder.addOrderGroupAndUpdateControlCode(response.getRESPONSE().getPATIENT()
											.getSPECIMEN(index).getORDER(orderGroupIndex), o33Message
											.getSPECIMEN(index).getORDER(orderGroupIndex), "UA", null, null, null,
											false);
								}
							} else if ((selectedOrder.getOrderStatus() != null)
									&& !selectedOrder.getOrderStatus().isEmpty()) {
								selectedOrder.setOrderControlCode("UC");
								selectedOrder.save(entityManager);
								orlBuilder.addOrderGroupAndUpdateControlCode(response.getRESPONSE().getPATIENT()
										.getSPECIMEN(index).getORDER(orderGroupIndex), o33Message.getSPECIMEN(index)
										.getORDER(orderGroupIndex), "UC", null, null, null, false);
							} else {
								// cancel accepted ==> CR
								String newOrderControlCode = "OK";
								if (messageOrder.getOrderControlCode().equals("CA")) {

									newOrderControlCode = "CR";
								}
								orlBuilder.addOrderGroupAndUpdateControlCode(response.getRESPONSE().getPATIENT()
										.getSPECIMEN(index).getORDER(orderGroupIndex), o33Message.getSPECIMEN(index)
										.getORDER(orderGroupIndex), newOrderControlCode, null, null, null, false);
								selectedOrder.setOrderStatus(messageOrder.getOrderStatus());
								selectedOrder.setOrderControlCode(newOrderControlCode);
								selectedOrder.setOrderResultStatus(MessageDecoder.getOrderResultStatus(obr));
								selectedOrder.save(entityManager);
							}
						} else if ((updateControlCode != null)
								&& messageOrder.getOrderControlCode().equals(updateControlCode)) {
							if (selectedOrder == null) {
								// order not found ==> UA
								orlBuilder.addOrderGroupAndUpdateControlCode(response.getRESPONSE().getPATIENT()
										.getSPECIMEN(index).getORDER(orderGroupIndex), o33Message.getSPECIMEN(index)
										.getORDER(orderGroupIndex), "UA", null, null, null, false);
							} else {
								// update accepted
								selectedOrder.setOrderControlCode("OK");
								selectedOrder.setOrderStatus(MessageDecoder.getOrderStatus(orc));
								if (!simulatedActor.getKeyword().equals("ANALYZER")) {
									selectedOrder.setOrderResultStatus(MessageDecoder.getOrderResultStatus(obr));
								}
								selectedOrder.save(entityManager);
								orlBuilder.addOrderGroupAndUpdateControlCode(response.getRESPONSE().getPATIENT()
										.getSPECIMEN(index).getORDER(orderGroupIndex), o33Message.getSPECIMEN(index)
										.getORDER(orderGroupIndex), "OK", null, null, null, false);
							}
						} else {
							entityManager.getTransaction().rollback();
							return orlBuilder.createNack(inMessage, AcknowledgmentCode.AE, Receiver.TABLE_VALUE_NOT_FOUND,
									Receiver.SEVERITY_ERROR,
									"Unexpected order control code: " + messageOrder.getOrderControlCode(), simulatedActor.getKeyword());
						}
					}
					if (!orders.isEmpty()) {
						if (messageSpecimen.getOrders() == null) {
							messageSpecimen.setOrders(orders);
						} else {
							messageSpecimen.getOrders().addAll(orders);
						}
					}
					messageSpecimen.save(entityManager);
				}
			}
		}
		return response;
	}

	/**
	 * 
	 * @param inMessage
	 * @param newControlCode
	 * @param cancelControlCode
	 * @param updateControlCode
	 * @param orlBuilder
	 * @param domain
	 * @param creator
	 * @param selectedEncounter
	 * @return
	 */
	private Message parseOMLO35(Message inMessage, String newControlCode, String cancelControlCode,
			String updateControlCode, ORLMessageBuilder orlBuilder, Domain domain, String creator,
			Encounter selectedEncounter) {
		PipeParser pipeParser = new PipeParser();
		// cast message using the class generated using the IHE HL7 message profile
		OML_O35 o35Message;
		try {
			o35Message = (OML_O35) pipeParser.parseForSpecificPackage(inMessage.encode(),
					"net.ihe.gazelle.oml.hl7v2.model.v25.message");
		} catch (HL7Exception e) {
			return orlBuilder.createNack(inMessage, AcknowledgmentCode.AE, Receiver.INTERNAL_ERROR, Receiver.SEVERITY_ERROR,
					"Cannot properly encode the message for parsing", simulatedActor.getKeyword());
		}

		// prepare response
		ORL_O36 response = orlBuilder.createNewORL_O36(AcknowledgmentCode.AA, o35Message.getPATIENT().getPID());

		// BEGIN PARSING
		int nbOfSpecimens = o35Message.getSPECIMENReps();
		if (nbOfSpecimens == 0) {
			return orlBuilder.createNack(inMessage, AcknowledgmentCode.AE, Receiver.SEGMENT_SEQUENCE_ERROR, Receiver.SEVERITY_ERROR,
					"No SPECIMEN group found", simulatedActor.getKeyword());
		}
		for (int index = 0; index < nbOfSpecimens; index++) {
			Segment spm = o35Message.getSPECIMEN(index).getSPM();
			if (MessageDecoder.isEmpty(spm)) {
				entityManager.getTransaction().rollback();
				return orlBuilder.createNack(inMessage, AcknowledgmentCode.AE, Receiver.SEGMENT_SEQUENCE_ERROR, Receiver.SEVERITY_ERROR,
						"SPM segment is missing in repetition " + index + " of SPECIMEN group", simulatedActor.getKeyword());
			} else {
				// create specimen from data contained in SPM segment
				Specimen messageSpecimen = MessageDecoder.extractSpecimenFromSegment(spm, creator, simulatedActor,
						domain);
				messageSpecimen.setMainEntity(true);
				// check if the specimen is already known by the actor
				Specimen selectedSpecimen = Specimen.getSpecimenByAssignedIdentifiersByActorByDomain(
						messageSpecimen.getPlacerAssignedIdentifier(), messageSpecimen.getFillerAssignedIdentifier(),
						simulatedActor, domain, entityManager, false);
				if (selectedSpecimen != null) {
					messageSpecimen = selectedSpecimen;
				}
				int nbOfContainers = o35Message.getSPECIMEN(index).getSPECIMEN_CONTAINERReps();
				if (nbOfContainers == 0) {
					entityManager.getTransaction().rollback();
					return orlBuilder.createNack(response, AcknowledgmentCode.AE, Receiver.SEGMENT_SEQUENCE_ERROR,
							Receiver.SEVERITY_ERROR, "CONTAINER group is missing in repetition " + index
									+ "of SPECIMEN group", simulatedActor.getKeyword());
				}
				// else continue parsing
				// add SPM segment to response
				if ((messageSpecimen.getPlacerAssignedIdentifier() == null)
						|| messageSpecimen.getPlacerAssignedIdentifier().isEmpty()) {
					messageSpecimen.setPlacerAssignedIdentifier(NumberGenerator.getOrderNumberForActor(simulatedActor,
							entityManager, "&"));
				}
				if ((messageSpecimen.getFillerAssignedIdentifier() == null)
						|| messageSpecimen.getFillerAssignedIdentifier().isEmpty()) {
					messageSpecimen.setFillerAssignedIdentifier(NumberGenerator.getOrderNumberForActor(simulatedActor,
							entityManager, "&"));
				}
				orlBuilder.addSpecimen(response.getRESPONSE().getPATIENT().getSPECIMEN(index).getSPM(), o35Message
						.getSPECIMEN(index).getSPM(), messageSpecimen.getPlacerAssignedIdentifier(), messageSpecimen
						.getFillerAssignedIdentifier());
				List<Container> containers = new ArrayList<Container>();
				for (int containerIndex = 0; containerIndex < nbOfContainers; containerIndex++) {
					Container messageContainer = MessageDecoder.extractContainerFromSAC(o35Message.getSPECIMEN(index)
							.getSPECIMEN_CONTAINER(containerIndex).getSAC(), simulatedActor, messageSpecimen, creator,
							domain);
					messageContainer.setMainEntity(true);
					Container selectedContainer = Container.getContainerByIdentifierByActorByDomain(
							messageContainer.getContainerIdentifier(), simulatedActor, domain, entityManager, false);
					if (selectedContainer != null) {
						if (!((selectedSpecimen != null) && (selectedContainer.getSpecimen() != null) && !selectedContainer
								.getSpecimen().getId().equals(selectedSpecimen.getId()))) {
							messageContainer = selectedContainer;
						}
					}
					// else messageContainer is used
					// PARSE ORDERS related to the current container
					int nbOfOrderGroups = o35Message.getSPECIMEN(index).getSPECIMEN_CONTAINER(containerIndex)
							.getORDERReps();
					if (nbOfOrderGroups == 0) {
						entityManager.getTransaction().rollback();
						return orlBuilder.createNack(inMessage, AcknowledgmentCode.AE, Receiver.SEGMENT_SEQUENCE_ERROR,
								Receiver.SEVERITY_ERROR, "ORDER group is missing in repetition " + index
										+ " of SPECIMEN group", simulatedActor.getKeyword());
					} else {
						// add SAC segment to response
						orlBuilder.addContainer(response.getRESPONSE().getPATIENT().getSPECIMEN(index)
								.getSPECIMEN_CONTAINER(containerIndex).getSAC(), o35Message.getSPECIMEN(index)
								.getSPECIMEN_CONTAINER(containerIndex).getSAC());

						// list orders relative to the current specimen/container
						List<LabOrder> orders = new ArrayList<LabOrder>();
						for (int orderGroupIndex = 0; orderGroupIndex < nbOfOrderGroups; orderGroupIndex++) {
							OML_O35_ORDER orderGroup = o35Message.getSPECIMEN(index)
									.getSPECIMEN_CONTAINER(containerIndex).getORDER(orderGroupIndex);
							Segment orc = orderGroup.getORC();
							if (MessageDecoder.isEmpty(orc)) {
								entityManager.getTransaction().rollback();
								return orlBuilder.createNack(inMessage, AcknowledgmentCode.AE, Receiver.SEGMENT_SEQUENCE_ERROR,
										Receiver.SEVERITY_ERROR, "ORC segment is missing in repetition "
												+ orderGroupIndex + " of ORDER group (SPECIMEN(" + index + "))", simulatedActor.getKeyword());
							}
							Segment obr = orderGroup.getOBSERVATION_REQUEST().getOBR();
							if (MessageDecoder.isEmpty(obr)) {
								entityManager.getTransaction().rollback();
								return orlBuilder.createNack(inMessage, AcknowledgmentCode.AE, Receiver.SEGMENT_SEQUENCE_ERROR,
										Receiver.SEVERITY_ERROR, "OBR segment is missing in repetition "
												+ orderGroupIndex + " of ORDER group (SPECIMEN(" + index + "))", simulatedActor.getKeyword());
							}
							Segment tq1 = orderGroup.getTIMING().getTQ1();
							LabOrder messageOrder = MessageDecoder.extractLabOrderFromSegments(orc, obr, tq1,
									simulatedActor, domain, creator, selectedEncounter);
							messageOrder.setWorkOrder(false);
							messageOrder.setMainEntity(false);
							// the order retrieved from the database using numbers given in the message
							LabOrder selectedOrder = AbstractOrder.getOrderByNumbersByActor(LabOrder.class,
									messageOrder.getPlacerOrderNumber(), messageOrder.getFillerOrderNumber(), null,
									simulatedActor, entityManager, false);
							if (messageOrder.getOrderControlCode() == null) {
								entityManager.getTransaction().rollback();
								return orlBuilder.createNack(inMessage, AcknowledgmentCode.AE, Receiver.REQUIRED_FIELD_MISSING,
										Receiver.SEVERITY_ERROR, "ORC-1 is required but missing", simulatedActor.getKeyword());
							} else if (messageOrder.getOrderControlCode().equals(newControlCode)) {
								if (simulatedActor.getKeyword().equals("OP")) {
									simulatedTransaction = Transaction.GetTransactionByKeyword("LAB-2", entityManager);
								}
								// the received order is supposed to be new, check that we do not know it
								if (selectedOrder != null) {
									// order already exists with the same placer/filler order number ==> message is discarded and error returns to the sender
									entityManager.getTransaction().rollback();
									if (simulatedActor.getKeyword().equals("OF")) {
										return orlBuilder.createNack(inMessage, AcknowledgmentCode.AE,
												Receiver.DUPLICATE_KEY_IDENTIFIER, Receiver.SEVERITY_ERROR,
												"An order with this placer order number already exists", simulatedActor.getKeyword());
									} else {
										return orlBuilder.createNack(inMessage, AcknowledgmentCode.AE,
												Receiver.DUPLICATE_KEY_IDENTIFIER, Receiver.SEVERITY_ERROR,
												"An order with this filler order number already exists", simulatedActor.getKeyword());
									}
								} else {
									if (simulatedActor.getKeyword().equals("OP")) {
										messageOrder.setPlacerOrderNumber(NumberGenerator.getOrderNumberForActor(
												simulatedActor, entityManager));
										messageOrder.setPlacerGroupNumber(NumberGenerator.getOrderNumberForActor(
												simulatedActor, entityManager));
										messageOrder.setOrderControlCode("NA");
									} else if (simulatedActor.getKeyword().equals("OF")) {
										messageOrder.setFillerOrderNumber(NumberGenerator.getOrderNumberForActor(
												simulatedActor, entityManager));
										messageOrder.setOrderControlCode("OK");
										messageOrder.setOrderStatus("SC");
									}
									log.info("add order to list");
									messageOrder.setContainer(messageContainer);
									orders.add(messageOrder);
									// add segments in response
									orlBuilder.addOrderGroupAndUpdateControlCode(
											response.getRESPONSE().getPATIENT().getSPECIMEN(index)
													.getSPECIMEN_CONTAINER(containerIndex).getORDER(orderGroupIndex),
											o35Message.getSPECIMEN(index).getSPECIMEN_CONTAINER(containerIndex)
													.getORDER(orderGroupIndex), messageOrder.getOrderControlCode(),
											messageOrder.getPlacerOrderNumber(), messageOrder.getFillerOrderNumber(),
											messageOrder.getPlacerGroupNumber(),
											simulatedActor.getKeyword().equals("OF"));
								}
							} else if (messageOrder.getOrderControlCode().equals(cancelControlCode)) {
								// unknown order or order already started ==> UA/UC
								if (selectedOrder == null) {
									if (messageOrder.getOrderControlCode().equals("CA")) {
										orlBuilder.addOrderGroupAndUpdateControlCode(response.getRESPONSE()
												.getPATIENT().getSPECIMEN(index).getSPECIMEN_CONTAINER(containerIndex)
												.getORDER(orderGroupIndex), o35Message.getSPECIMEN(index)
												.getSPECIMEN_CONTAINER(containerIndex).getORDER(orderGroupIndex), "UC",
												null, null, null, false);
									} else {
										orlBuilder.addOrderGroupAndUpdateControlCode(response.getRESPONSE()
												.getPATIENT().getSPECIMEN(index).getSPECIMEN_CONTAINER(containerIndex)
												.getORDER(orderGroupIndex), o35Message.getSPECIMEN(index)
												.getSPECIMEN_CONTAINER(containerIndex).getORDER(orderGroupIndex), "UA",
												null, null, null, false);
									}
								} else if (simulatedActor.getKeyword().equals("OF")
										&& (selectedOrder.getOrderStatus() != null)
										&& !selectedOrder.getOrderStatus().isEmpty()) {
									selectedOrder.setOrderControlCode("UC");
									selectedOrder.save(entityManager);
									orlBuilder.addOrderGroupAndUpdateControlCode(
											response.getRESPONSE().getPATIENT().getSPECIMEN(index)
													.getSPECIMEN_CONTAINER(containerIndex).getORDER(orderGroupIndex),
											o35Message.getSPECIMEN(index).getSPECIMEN_CONTAINER(containerIndex)
													.getORDER(orderGroupIndex), "UC", null, null, null, false);
								} else {
									// cancel accepted ==> CR
									String newOrderControlCode = "OK";
									if (messageOrder.getOrderControlCode().equals("CA")) {

										newOrderControlCode = "CR";
									}
									orlBuilder.addOrderGroupAndUpdateControlCode(
											response.getRESPONSE().getPATIENT().getSPECIMEN(index)
													.getSPECIMEN_CONTAINER(containerIndex).getORDER(orderGroupIndex),
											o35Message.getSPECIMEN(index).getSPECIMEN_CONTAINER(containerIndex)
													.getORDER(orderGroupIndex), newOrderControlCode, null, null, null,
											false);
									selectedOrder.setOrderControlCode(newOrderControlCode);
									selectedOrder.setOrderStatus(messageOrder.getOrderStatus());
									selectedOrder.setOrderResultStatus(MessageDecoder.getOrderResultStatus(obr));
									selectedOrder.save(entityManager);
								}
							} else if ((updateControlCode != null)
									&& messageOrder.getOrderControlCode().equals(updateControlCode)) {
								if (selectedOrder == null) {
									// order not found ==> UA
									orlBuilder.addOrderGroupAndUpdateControlCode(
											response.getRESPONSE().getPATIENT().getSPECIMEN(index)
													.getSPECIMEN_CONTAINER(containerIndex).getORDER(orderGroupIndex),
											o35Message.getSPECIMEN(index).getSPECIMEN_CONTAINER(containerIndex)
													.getORDER(orderGroupIndex), "UA", null, null, null, false);
								} else {
									// update accepted
									selectedOrder.setOrderControlCode("OK");
									selectedOrder.setOrderStatus(MessageDecoder.getOrderStatus(orc));
									selectedOrder.setOrderResultStatus(MessageDecoder.getOrderResultStatus(obr));
									selectedOrder.save(entityManager);
									orlBuilder.addOrderGroupAndUpdateControlCode(
											response.getRESPONSE().getPATIENT().getSPECIMEN(index)
													.getSPECIMEN_CONTAINER(containerIndex).getORDER(orderGroupIndex),
											o35Message.getSPECIMEN(index).getSPECIMEN_CONTAINER(containerIndex)
													.getORDER(orderGroupIndex), "OK", null, null, null, false);
								}
							} else {
								entityManager.getTransaction().rollback();
								return orlBuilder.createNack(inMessage, AcknowledgmentCode.AE, Receiver.TABLE_VALUE_NOT_FOUND,
										Receiver.SEVERITY_ERROR,
										"Unexpected order control code: " + messageOrder.getOrderControlCode(), simulatedActor.getKeyword());
							}
						}
						log.info("add orders to container: " + orders.size());
						if (!orders.isEmpty()) {
							if (messageContainer.getOrders() == null) {
								messageContainer.setOrders(orders);
							} else {
								messageContainer.getOrders().addAll(orders);
							}
						}
						containers.add(messageContainer);
					}
				}
				if (!containers.isEmpty()) {
					if (messageSpecimen.getContainers() == null) {
						messageSpecimen.setContainers(containers);
					} else {
						messageSpecimen.getContainers().addAll(containers);
					}
				}
				messageSpecimen.save(entityManager);
			}
		}
		return response;
	}

	/**
	 * Decodes OUL^R22^OUL_R22 messages received by OF or ORT
	 * 
	 * @param oulMessage
	 * @return the acknowledgement
	 */
	protected Message processOULR22Message(OUL_R22 oulMessage) {
		log.debug("processing OUL^R22 message by " + simulatedActor.getKeyword());
		Patient selectedPatient = null;
		String creator = sendingApplication + "_" + sendingFacility;
		Domain domain = Domain.getDomainByKeyword("LAB", entityManager);
		Encounter selectedEncounter = null;
		// extract patient
		if (!MessageDecoder.isEmpty(oulMessage.getPATIENT().getPID())) {
			Patient pidPatient = MessageDecoder.extractPatientFromPID(oulMessage.getPATIENT().getPID());
			List<PatientIdentifier> pidPatientIdentifiers;
			pidPatientIdentifiers = MessageDecoder.extractPatientIdentifiersFromMessage25(oulMessage);
			List<PatientIdentifier> identifiersToAdd = new ArrayList<PatientIdentifier>();
			for (PatientIdentifier pid : pidPatientIdentifiers) {
				PatientIdentifier existingPID = pid.getStoredPatientIdentifier(entityManager);
				if (existingPID != null) {
					if (selectedPatient == null) {
						selectedPatient = existingPID.getPatient();
					} else if (!selectedPatient.getId().equals(existingPID.getPatient().getId())) {
						// error patients with same identifiers mismatch
						// create a good ACK
						return Receiver.buildNack(sendingApplication, sendingFacility, serverApplication,
								serverFacility, "R22", AcknowledgmentCode.AE, ERROR_INCONSISTENT_PID, null,
								Receiver.DUPLICATE_KEY_IDENTIFIER, Receiver.SEVERITY_ERROR, messageControlId,
								hl7Charset, hl7Version);
					}
				} else {
					pid = pid.save(entityManager);
					identifiersToAdd.add(pid);
				}
			}
			// if none of the identifiers is already known by the simulator, we save the patient contained in the message
			if (selectedPatient == null) {
				// save the patient
				pidPatient.setCreator(creator);
				selectedPatient = pidPatient.save(entityManager);
			}
			for (PatientIdentifier pid : identifiersToAdd) {
				pid.setPatient(selectedPatient);
				pid.save(entityManager);
			}
			// then, extract encounter
			if ((selectedPatient != null) && !MessageDecoder.isEmpty(oulMessage.getVISIT().getPV1())) {
				// check existance of PV1 segment
				Encounter pv1Encounter = null;
				pv1Encounter = MessageDecoder.extractEncounterFromPV1(oulMessage);
				if (pv1Encounter.getVisitNumber() != null) {
					// check the encounter exists
					selectedEncounter = Encounter.getEncounterByVisitNumber(pv1Encounter.getVisitNumber(),
							entityManager);
					if ((selectedEncounter != null)
							&& (!selectedEncounter.getPatient().getId().equals(selectedPatient.getId()))) {
						return Receiver.buildNack(sendingApplication, sendingFacility, serverApplication,
								serverFacility, "R22", AcknowledgmentCode.AE, "This visit number is already used for another patient",
								null, Receiver.DUPLICATE_KEY_IDENTIFIER, Receiver.SEVERITY_ERROR, messageControlId,
								hl7Charset, hl7Version);
					}
					// if not rattach the encounter to the selected patient
					else if (selectedEncounter == null) {
						selectedEncounter = pv1Encounter;
						selectedEncounter.setPatient(selectedPatient);
						selectedEncounter.setCreator(creator);
						selectedEncounter = selectedEncounter.save(entityManager);
					}
					// else selectedEncounter is the one retrieved from the database
				} else {
					return Receiver.buildNack(sendingApplication, sendingFacility, serverApplication, serverFacility,
							"R22", AcknowledgmentCode.AE, "PV1-19 is required but empty", null, Receiver.REQUIRED_FIELD_MISSING,
							Receiver.SEVERITY_ERROR, messageControlId, hl7Charset, hl7Version);
				}
			} else {
				selectedEncounter = new Encounter();
				selectedEncounter.setPatient(selectedPatient);
				selectedEncounter.setCreator(creator);
				selectedEncounter = selectedEncounter.save(entityManager);
			}
		}
		// extract specimens
		int nbOfSpecimens = oulMessage.getSPECIMENReps();
		if (nbOfSpecimens > 0) {
			for (int specimenIndex = 0; specimenIndex < nbOfSpecimens; specimenIndex++) {
				// SPECIMEN begin
				Segment spm = oulMessage.getSPECIMEN(specimenIndex).getSPM();
				if (MessageDecoder.isEmpty(spm)) {
					return Receiver.buildNack(sendingApplication, sendingFacility, serverApplication, serverFacility,
							"R22", AcknowledgmentCode.AE, "SPM segment is required but missing in rep " + specimenIndex, null,
							Receiver.SEGMENT_SEQUENCE_ERROR, Receiver.SEVERITY_ERROR, messageControlId, hl7Charset,
							hl7Version);
				}
				Specimen messageSpecimen = MessageDecoder.extractSpecimenFromSegment(spm, creator, simulatedActor,
						domain);
				messageSpecimen.setWorkOrder(simulatedActor.getKeyword().equals("OF"));
				messageSpecimen.setMainEntity(true);
				Specimen selectedSpecimen = Specimen.getSpecimenByAssignedIdentifiersByActorByDomain(
						messageSpecimen.getPlacerAssignedIdentifier(), messageSpecimen.getFillerAssignedIdentifier(),
						simulatedActor, domain, entityManager, simulatedActor.getKeyword().equals("OF"));
				if (selectedSpecimen == null) {
					selectedSpecimen = messageSpecimen;
				}
				// Specimen observations
				int nbOfObservations = oulMessage.getSPECIMEN(specimenIndex).getOBXReps();
				if (nbOfObservations > 0) {
					for (int obxIndex = 0; obxIndex < nbOfObservations; obxIndex++) {
						// OBSERVATION begin
						Segment obx = oulMessage.getSPECIMEN(specimenIndex).getOBX(obxIndex);
						boolean obxIsEmpty = true;
						try {
							obxIsEmpty = obx.isEmpty();
						} catch (HL7Exception e) {
							log.info(e.getMessage(), e);
						}
						if (!obxIsEmpty) {
							Observation messageObservation = MessageDecoder.extractObservationFromOBX(obx,
									simulatedActor, domain, creator);
							if (selectedSpecimen.getId() != null) {
								List<Observation> observations = Observation.getObservationFiltered(entityManager,
										simulatedActor, domain, null, selectedSpecimen, null);
								if (observations != null) {
									selectedSpecimen.getObservations().remove(observations.get(0));
									messageObservation.setId(observations.get(0).getId());
								}
							}
							messageObservation.setSpecimen(selectedSpecimen);
							if (selectedSpecimen.getObservations() == null) {
								selectedSpecimen.setObservations(new ArrayList<Observation>());
							}
							selectedSpecimen.getObservations().add(messageObservation);
							// OBSERVATION end
						}
					}
				}
				// containers
				int nbOfContainers = oulMessage.getSPECIMEN(specimenIndex).getCONTAINERReps();
				if (nbOfContainers > 0) {
					for (int sacIndex = 0; sacIndex < nbOfContainers; sacIndex++) {
						// CONTAINER begin
						Segment sac = oulMessage.getSPECIMEN(specimenIndex).getCONTAINER(sacIndex).getSAC();
						Container messageContainer = MessageDecoder.extractContainerFromSAC(sac, simulatedActor,
								selectedSpecimen, creator, domain);
						messageContainer.setSpecimen(selectedSpecimen);
						messageContainer.setMainEntity(false);
						messageContainer.setWorkOrder(simulatedActor.getKeyword().equals("OF"));
						Container selectedContainer = null;
						if (selectedSpecimen.getId() != null) {
							selectedContainer = Container.getContainerByIdentifierByActorByDomain(
									messageContainer.getContainerIdentifier(), simulatedActor, domain, entityManager,
									simulatedActor.getKeyword().equals("OF"));
							if ((selectedContainer != null) && (selectedContainer.getSpecimen() != null)
									&& !selectedContainer.getSpecimen().equals(selectedSpecimen)) {
								return Receiver.buildNack(sendingApplication, sendingFacility, serverApplication,
										serverFacility, "R22", AcknowledgmentCode.AE,
										"This container identifier is already used with a different specimen", null,
										Receiver.INTERNAL_ERROR, Receiver.SEVERITY_ERROR, messageControlId, hl7Charset,
										hl7Version);
							}
						}
						if (selectedContainer == null) {
							selectedContainer = messageContainer;
						}
						// CONTAINER end
						if ((selectedSpecimen.getContainers() != null)
								&& !selectedSpecimen.getContainers().contains(selectedContainer)) {
							selectedSpecimen.getContainers().add(selectedContainer);
						} else {
							selectedSpecimen.setContainers(new ArrayList<Container>());
						}
					}
				}
				// related orders
				int nbOfOrders = oulMessage.getSPECIMEN(specimenIndex).getORDERReps();
				if (nbOfOrders > 0) {
					for (int orderIndex = 0; orderIndex < nbOfOrders; orderIndex++) {
						// ORDER begin
						Segment obr = oulMessage.getSPECIMEN(specimenIndex).getORDER(orderIndex).getOBR();
						Segment orc = oulMessage.getSPECIMEN(specimenIndex).getORDER(orderIndex).getORC();
						Segment tq1 = oulMessage.getSPECIMEN(specimenIndex).getORDER(orderIndex).getTIMING_QTY()
								.getTQ1();
						if (MessageDecoder.isEmpty(obr)) {
							return Receiver.buildNack(sendingApplication, sendingFacility, serverApplication,
									serverFacility, "R22", AcknowledgmentCode.AE, "OBR segment is required but missing in SPECIMEN["
											+ specimenIndex + "]/ORDER[" + orderIndex + "]", null,
									Receiver.SEGMENT_SEQUENCE_ERROR, Receiver.SEVERITY_ERROR, messageControlId,
									hl7Charset, hl7Version);
						} else if (simulatedActor.getKeyword().equals("ORT") && MessageDecoder.isEmpty(orc)) {
							return Receiver.buildNack(sendingApplication, sendingFacility, serverApplication,
									serverFacility, "R22", AcknowledgmentCode.AE, "ORC segment is required but missing in SPECIMEN["
											+ specimenIndex + "]/ORDER[" + orderIndex + "]", null,
									Receiver.SEGMENT_SEQUENCE_ERROR, Receiver.SEVERITY_ERROR, messageControlId,
									hl7Charset, hl7Version);
						} else {
							LabOrder messageOrder = MessageDecoder.extractLabOrderFromSegments(orc, obr, tq1,
									simulatedActor, domain, creator, selectedEncounter);
							messageOrder.setMainEntity(false);
							messageOrder.setWorkOrder(simulatedActor.getKeyword().equals("OF"));
							LabOrder selectedOrder = AbstractOrder.getOrderByNumbersByActor(LabOrder.class,
									messageOrder.getPlacerOrderNumber(), messageOrder.getFillerOrderNumber(), null,
									simulatedActor, entityManager, simulatedActor.getKeyword().equals("OF"));
							if (selectedOrder == null) {
								selectedOrder = messageOrder;
							} else {
								// update order status and result status
								selectedOrder.setOrderStatus(messageOrder.getOrderStatus());
								selectedOrder.setOrderResultStatus(messageOrder.getOrderResultStatus());
							}
							// observations
							nbOfObservations = oulMessage.getSPECIMEN(specimenIndex).getORDER(orderIndex)
									.getRESULTReps();
							if (nbOfObservations > 0) {
								for (int obxIndex = 0; obxIndex < nbOfObservations; obxIndex++) {
									// OBSERVATION begin
									Segment obx = oulMessage.getSPECIMEN(specimenIndex).getORDER(orderIndex)
											.getRESULT(obxIndex).getOBX();
									Observation messageObservation = MessageDecoder.extractObservationFromOBX(obx,
											simulatedActor, domain, creator);
									if (selectedOrder.getId() != null) {
										// this assumes that observations are always transmitted with the same setId
										List<Observation> observations = Observation.getObservationFiltered(
												entityManager, simulatedActor, domain, selectedOrder, null,
												messageObservation.getSetId());
										if (observations != null) {
											messageObservation.setId(observations.get(0).getId());
											selectedOrder.getObservations().remove(observations.get(0));
										}
									}
									messageObservation.setOrder(selectedOrder);
									if (selectedOrder.getObservations() == null) {
										selectedOrder.setObservations(new ArrayList<Observation>());
									}
									selectedOrder.getObservations().add(messageObservation);
									// OBSERVATION end
								}
							}
							if ((selectedOrder.getId() != null) && (selectedSpecimen.getId() != null)
									&& (selectedSpecimen.getOrders() != null)) {
								for (int i = 0; i < selectedSpecimen.getOrders().size(); i++) {
									if (selectedSpecimen.getOrders().get(i).getId().equals(selectedOrder.getId())) {
										selectedSpecimen.getOrders().set(i, selectedOrder.save(entityManager));
										break;
									} else {
										continue;
									}
								}
							} else if (selectedSpecimen.getOrders() != null) {
								selectedSpecimen.getOrders().add(selectedOrder);
							} else {
								selectedSpecimen.setOrders(new ArrayList<LabOrder>());
								selectedSpecimen.getOrders().add(selectedOrder);
							}
						}
						// ORDER end
					}
				}

				// SPECIMEN end
				selectedSpecimen.save(entityManager);
			}
		} else {
			return Receiver.buildNack(sendingApplication, sendingFacility, serverApplication, serverFacility, "R22",
					AcknowledgmentCode.AE, "SPECIMEN group is required but missing", null, Receiver.SEGMENT_SEQUENCE_ERROR,
					Receiver.SEVERITY_ERROR, messageControlId, hl7Charset, hl7Version);
		}
		return Receiver.buildAck(sendingApplication, sendingFacility, serverApplication, serverFacility, "R22", AcknowledgmentCode.AA,
				messageControlId, hl7Charset, hl7Version);
	}

	protected Message processMessageFromADT(Message incomingMessage) {
		Terser terser = new Terser(incomingMessage);
		Message response = null;
		try {
			String event = terser.get("/.MSH-9-2");
			String transactionKeyword = null;
			if (event.equals("A01") || event.equals("A04")) {
				transactionKeyword = "RAD-1";
				response = processADT_A01Message(incomingMessage, event);
			} else if (event.equals("A08")) {
				transactionKeyword = "RAD-12";
				response = processA08Message(incomingMessage);
			} else if (event.equals("A40")) {
				transactionKeyword = "RAD-12";
				response = processA40Message(incomingMessage);
			}
			simulatedTransaction = Transaction.GetTransactionByKeyword(transactionKeyword, entityManager);
		} catch (HL7Exception e) {
			log.info(e.getMessage(), e);
		}
		return response;
	}

	protected Message processADT_A01Message(Message incomingMessage, String event) {
		if (incomingMessage instanceof ADT_A01) {
			ADT_A01 message = (ADT_A01) incomingMessage;
			Patient selectedPatient = null;
			String creator = sendingApplication + "_" + sendingFacility;
			Patient pidPatient = MessageDecoder.extractPatientFromPID(message.getPID());
			List<PatientIdentifier> pidPatientIdentifiers = MessageDecoder.extractPatientIdentifiersFromPID251(message);
			if (pidPatientIdentifiers == null) {
				return Receiver.buildNack(sendingApplication, sendingFacility, serverApplication, serverFacility,
						event, AcknowledgmentCode.AE, "PID-3 field is required but missing", null, Receiver.REQUIRED_FIELD_MISSING,
						Receiver.SEVERITY_ERROR, messageControlId, hl7Charset, hl7Version);
			}
			List<PatientIdentifier> identifiersToAdd = new ArrayList<PatientIdentifier>();
			for (PatientIdentifier pid : pidPatientIdentifiers) {
				PatientIdentifier existingPID = pid.getStoredPatientIdentifier(entityManager);
				if (existingPID != null) {
					if (selectedPatient == null) {
						selectedPatient = existingPID.getPatient();
					} else if (selectedPatient.getId().equals(existingPID.getPatient().getId())) {
						// OK: nothing to do
						continue;
					} else {
						// error patients with same identifiers mismatch
						// create a good ACK
						return Receiver.buildNack(sendingApplication, sendingFacility, serverApplication,
								serverFacility, event, AcknowledgmentCode.AE, ERROR_INCONSISTENT_PID, null,
								Receiver.DUPLICATE_KEY_IDENTIFIER, Receiver.SEVERITY_ERROR, messageControlId,
								hl7Charset, hl7Version);
					}
				} else {
					pid = pid.save(entityManager);
					identifiersToAdd.add(pid);
				}
			}
			// if none of the identifiers is already known by the simulator, we save the patient contained in the message
			if (selectedPatient == null) {
				// save the patient
				pidPatient.setCreator(creator);
				selectedPatient = pidPatient.save(entityManager);
			}
			for (PatientIdentifier pid : identifiersToAdd) {
				pid.setPatient(selectedPatient);
				pid.save(entityManager);
			}
			// then, extract encounter
			Encounter selectedEncounter = null;
			if ((selectedPatient != null) && !MessageDecoder.isEmpty(message.getPV1())) {
				// check existance of PV1 segment
				Encounter pv1Encounter = null;
				pv1Encounter = MessageDecoder.extractEncounterFromPV1(message);
				if (pv1Encounter.getVisitNumber() != null) {
					// check the encounter exists
					selectedEncounter = Encounter.getEncounterByVisitNumber(pv1Encounter.getVisitNumber(),
							entityManager);
					if (selectedEncounter != null) {
						return Receiver.buildNack(sendingApplication, sendingFacility, serverApplication,
								serverFacility, event, AcknowledgmentCode.AE, "This visit number is already used", null,
								Receiver.DUPLICATE_KEY_IDENTIFIER, Receiver.SEVERITY_ERROR, messageControlId,
								hl7Charset, hl7Version);
					}
					// if not rattach the encounter to the selected patient
					else {
						selectedEncounter = pv1Encounter;
						selectedEncounter.setPatient(selectedPatient);
						selectedEncounter.setCreator(creator);
						selectedEncounter = selectedEncounter.save(entityManager);
					}
					// else selectedEncounter is the one retrieved from the database
				} else {
					return Receiver.buildNack(sendingApplication, sendingFacility, serverApplication, serverFacility,
							event, AcknowledgmentCode.AE, "PV1-19 is required but empty", null, Receiver.REQUIRED_FIELD_MISSING,
							Receiver.SEVERITY_ERROR, messageControlId, hl7Charset, hl7Version);
				}
				return Receiver.buildAck(sendingApplication, sendingFacility, serverApplication, serverFacility, event,
						AcknowledgmentCode.AA, messageControlId, hl7Charset, hl7Version);
			} else {
				return Receiver.buildNack(sendingApplication, sendingFacility, serverApplication, serverFacility,
						event, AcknowledgmentCode.AE, "PV1 segment is required but missing", null, Receiver.SEGMENT_SEQUENCE_ERROR,
						Receiver.SEVERITY_ERROR, messageControlId, hl7Charset, hl7Version);
			}
		} else {
			return Receiver.buildNack(sendingApplication, sendingFacility, serverApplication, serverFacility, event,
					AcknowledgmentCode.AR, "Message structure is not allowed for this event", null, Receiver.INTERNAL_ERROR,
					Receiver.SEVERITY_ERROR, messageControlId, hl7Charset, "2.3.1");
		}
	}

	protected Message processA08Message(Message incomingMessage) {
		if (incomingMessage instanceof ADT_A01) {
			ADT_A01 message = (ADT_A01) incomingMessage;
			Patient selectedPatient = null;
			String creator = sendingApplication + "_" + sendingFacility;
			Patient pidPatient = MessageDecoder.extractPatientFromPID(message.getPID());
			List<PatientIdentifier> pidPatientIdentifiers;
			pidPatientIdentifiers = MessageDecoder.extractPatientIdentifiersFromPID251(message);
			for (PatientIdentifier pid : pidPatientIdentifiers) {
				PatientIdentifier existingPID = pid.getStoredPatientIdentifier(entityManager);
				if (existingPID != null) {
					if (selectedPatient == null) {
						selectedPatient = existingPID.getPatient();
					} else if (selectedPatient.getId().equals(existingPID.getPatient().getId())) {
						// OK: nothing to do
						continue;
					} else {
						// error patients with same identifiers mismatch
						// create a good ACK
						return Receiver.buildNack(sendingApplication, sendingFacility, serverApplication,
								serverFacility, "A08", AcknowledgmentCode.AE, ERROR_INCONSISTENT_PID, null,
								Receiver.DUPLICATE_KEY_IDENTIFIER, Receiver.SEVERITY_ERROR, messageControlId,
								hl7Charset, hl7Version);
					}
				}
			}
			if (selectedPatient != null) {
				pidPatient.setId(selectedPatient.getId());
				pidPatient.setPatientIdentifiers(selectedPatient.getPatientIdentifiers());
				pidPatient.setEncounters(selectedPatient.getEncounters());
				pidPatient.setWeight(selectedPatient.getWeight());
				pidPatient.setCreator(creator);
				pidPatient.save(entityManager);
				return Receiver.buildAck(sendingApplication, sendingFacility, serverApplication, serverFacility, "A08",
						AcknowledgmentCode.AA, messageControlId, hl7Charset, hl7Version);
			} else {
				return Receiver.buildNack(sendingApplication, sendingFacility, serverApplication, serverFacility,
						"A08", AcknowledgmentCode.AE, "No patient found with such identifiers", null, Receiver.UNKNOWN_KEY_IDENTIFIER,
						Receiver.SEVERITY_ERROR, messageControlId, hl7Charset, hl7Version);
			}
		} else {
			return Receiver.buildNack(sendingApplication, sendingFacility, serverApplication, serverFacility, "A08",
					AcknowledgmentCode.AR, "Message structure is not allowed for this event", null, Receiver.INTERNAL_ERROR,
					Receiver.SEVERITY_ERROR, messageControlId, hl7Charset, "2.3.1");
		}

	}

	protected Message processA40Message(Message incomingMessage) {
		// check the message structure is the one expected by IHE
		if (incomingMessage instanceof ADT_A39) {
			ADT_A39 adtMessage = (ADT_A39) incomingMessage;
			String creator = sendingApplication + "_" + sendingFacility;
			PatientIdentifier incorrectPatientIdentifier = MessageDecoder.buildPatientIdentifier(adtMessage
					.getPATIENT().getMRG().getPriorPatientIdentifierList()[0]);
			List<PatientIdentifier> correctPatientIdentifiers = MessageDecoder
					.extractPatientIdentifiersFromPID251(adtMessage);
			Patient incorrectPatient;
			Patient correctPatient;

			PatientIdentifier existingIncorrectPID = incorrectPatientIdentifier
					.getStoredPatientIdentifier(entityManager);
			if ((existingIncorrectPID == null) || (existingIncorrectPID.getPatient() == null)) {
				return Receiver.buildNack(sendingApplication, sendingFacility, serverApplication, serverFacility,
						"A40", AcknowledgmentCode.AR, "Prior patient identifier is unknown", null, Receiver.UNKNOWN_KEY_IDENTIFIER,
						Receiver.SEVERITY_ERROR, messageControlId, hl7Charset, "2.3.1");
			} else {
				incorrectPatient = existingIncorrectPID.getPatient();
			}
			PatientIdentifier existingCorrectPID = null;

			for (PatientIdentifier pid : correctPatientIdentifiers) {
				existingCorrectPID = pid.getStoredPatientIdentifier(entityManager);
				if (existingCorrectPID != null) {
					break;
				}
			}

			if ((existingCorrectPID == null) || (existingCorrectPID.getPatient() == null)) {
				correctPatient = MessageDecoder.extractPatientFromPID(adtMessage.getPATIENT().getPID());
				correctPatient.setPatientIdentifiers(correctPatientIdentifiers);
				correctPatient.setCreator(creator);
			} else {
				correctPatient = existingCorrectPID.getPatient();
			}
			correctPatient.save(entityManager);
			if ((incorrectPatient.getEncounters() != null) && (incorrectPatient.getEncounters().size() > 0)) {
				for (Encounter encounter : incorrectPatient.getEncounters()) {
					encounter.setPatient(correctPatient);
					encounter.save(entityManager);
				}
			}
			for (PatientIdentifier pid : incorrectPatient.getPatientIdentifiers()) {
				entityManager.remove(pid);
			}
			entityManager.flush();
			entityManager.remove(incorrectPatient);
			entityManager.flush();
			return Receiver.buildAck(sendingApplication, sendingFacility, serverApplication, serverFacility, "A40",
					AcknowledgmentCode.AA, messageControlId, hl7Charset, hl7Version);
		} else {
			// message structure is not the one expected (ADT_A39)
			return Receiver.buildNack(serverApplication, serverFacility, sendingApplication, sendingFacility, "A40",
					null, "Message structure is not the one expected, event A40 should use ADT_A39", null,
					Receiver.INTERNAL_ERROR, Receiver.SEVERITY_ERROR, messageControlId, hl7Charset, "2.3.1");
		}
	}
}
