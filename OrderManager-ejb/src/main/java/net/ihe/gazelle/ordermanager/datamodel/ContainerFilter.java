package net.ihe.gazelle.ordermanager.datamodel;

import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.hql.criterion.HQLCriterion;
import net.ihe.gazelle.ordermanager.model.Container;

import java.util.ArrayList;
import java.util.List;

/**
 * Class description : <b>ContainerFilter</b>
 * 
 *
 *
 * @author Anne-Gaëlle Bergé - INRIA Rennes/Bretagne Atlantique
 *
 * @version 1.0 - 2011, December 6th
 * 
 */
public class ContainerFilter extends Filter<Container> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6012379698476422758L;
	private static List<HQLCriterion<Container, ?>> defaultCriterions;

	static {
		defaultCriterions = new ArrayList<HQLCriterion<Container, ?>>();
	}

	public ContainerFilter() {
		super(Container.class, defaultCriterions);
	}
}
