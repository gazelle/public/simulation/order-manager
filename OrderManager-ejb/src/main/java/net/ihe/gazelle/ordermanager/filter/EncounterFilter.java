package net.ihe.gazelle.ordermanager.filter;

import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import net.ihe.gazelle.hql.criterion.QueryModifier;
import net.ihe.gazelle.ordermanager.model.Encounter;
import net.ihe.gazelle.ordermanager.model.EncounterQuery;

import java.util.Map;

/**
 * <b>Class Description : </b>EncounterFilter<br>
 * <br>
 *
 * @author Anne-Gaelle Berge / IHE-Europe development Project
 * @version 1.0 - 30/06/16
 *
 *
 *
 */
public class EncounterFilter implements QueryModifier<Encounter> {

    private String visitnumber;
    private String patientid;
    private Filter<Encounter> filter;



    @Override
    public void modifyQuery(HQLQueryBuilder<Encounter> hqlQueryBuilder, Map<String, Object> map) {
        EncounterQuery query = new EncounterQuery();
        if (visitnumber != null && !visitnumber.isEmpty()){
            hqlQueryBuilder.addRestriction(query.visitNumber().likeRestriction(visitnumber));
        }
        if (patientid != null && !patientid.isEmpty()){
            hqlQueryBuilder.addRestriction(query.patient().patientIdentifiers().identifier().likeRestriction(patientid));
        }
    }

    public FilterDataModel<Encounter> getEncounterFiltered(){
        return new FilterDataModel<Encounter>(getFilter()) {
            @Override
            protected Object getId(Encounter encounter) {
                return encounter.getId();
            }
        };
    }

    public void reset(){
        visitnumber = null;
        patientid = null;
        getFilter().clear();
    }

    public String getVisitnumber() {
        return visitnumber;
    }

    public void setVisitnumber(String visitnumber) {
        this.visitnumber = visitnumber;
    }

    public String getPatientid() {
        return patientid;
    }

    public void setPatientid(String patientid) {
        this.patientid = patientid;
    }

    public Filter<Encounter> getFilter() {
        if (filter == null){
            filter = new Filter<Encounter>(getHQLCriterionsForFilter());
        }
        return filter;
    }

    private HQLCriterionsForFilter<Encounter> getHQLCriterionsForFilter() {
        EncounterQuery query = new EncounterQuery();
        HQLCriterionsForFilter<Encounter> criteria = query.getHQLCriterionsForFilter();
        criteria.addPath("firstname", query.patient().firstName());
        criteria.addPath("lastname", query.patient().lastName());
        criteria.addPath("creator", query.creator());
        criteria.addPath("creationDate", query.creationDate());
        criteria.addQueryModifier(this);
        return criteria;
    }
}
