package net.ihe.gazelle.ordermanager.datamodel;

import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.restrictions.HQLRestrictionLikeMatchMode;
import net.ihe.gazelle.hql.restrictions.HQLRestrictions;
import net.ihe.gazelle.ordermanager.model.Container;
import net.ihe.gazelle.ordermanager.model.LabOrder;
import net.ihe.gazelle.ordermanager.model.Specimen;
import net.ihe.gazelle.ordermanager.model.UserPreferences;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.common.tf.model.Domain;
import org.jboss.seam.Component;
import org.jboss.seam.security.Identity;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * Class description : <b>LabOrderDataModel</b>
 * 
 * This class is a data model for properly and quickly displaying the lab order objects in the GUI
 * 
 *
 *
 * @author Anne-Gaëlle Bergé - INRIA Rennes/Bretagne Atlantique
 *
 * @version 1.0 - 2011, December 6th
 * 
 */

public class LabOrderDataModel extends FilterDataModel<LabOrder> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3330545600085622006L;
	private String creator;
	private Date startDate;
	private Date endDate;
	private String placerNumber;
	private String fillerNumber;
	private Domain domain;
	private Actor simulatedActor;
	private String newOrderControlCode;
	private String searchedVisitNumber;
	private String searchedPatientId;
	private Boolean isMainEntity;
	private boolean workOrder;
	private Specimen relatedSpecimen;
	private Container relatedContainer;

	public LabOrderDataModel() {
		super(new LabOrderFilter());
	}

	public LabOrderDataModel(Specimen relatedSpecimen) {
		super(new LabOrderFilter());
		this.relatedSpecimen = relatedSpecimen;
		this.creator = null;
		this.startDate = null;
		this.endDate = null;
		this.placerNumber = null;
		this.domain = null;
		this.simulatedActor = null;
		this.newOrderControlCode = null;
		this.searchedPatientId = null;
		this.searchedVisitNumber = null;
		this.isMainEntity = false;
		if (relatedSpecimen != null) {
			this.workOrder = relatedSpecimen.isWorkOrder();
		}
		this.relatedContainer = null;
	}

	public LabOrderDataModel(Container relatedContainer) {
		super(new LabOrderFilter());
		this.relatedContainer = relatedContainer;
		this.creator = null;
		this.startDate = null;
		this.endDate = null;
		this.placerNumber = null;
		this.domain = null;
		this.simulatedActor = null;
		this.newOrderControlCode = null;
		this.searchedPatientId = null;
		this.searchedVisitNumber = null;
		this.isMainEntity = false;
		if (relatedContainer != null) {
			this.workOrder = relatedContainer.isWorkOrder();
		}
		this.relatedSpecimen = null;
	}

	public LabOrderDataModel(Date startDate, Date endDate, String searchedOrderPlacerNumber,
			String searchedOrderFillerNumber, Domain selectedDomain, Actor simulatedActor, String searchedPatientId,
			String searchedVisitNumber, String newOrderControlCode, Boolean isMainEntity, boolean isWorkOrder) {
		super(new LabOrderFilter());
		this.creator = null;
		if (Identity.instance().isLoggedIn()) {
			UserPreferences preferences = (UserPreferences) Component.getInstance("userPreferences");
			if (preferences.isViewOnlyMyObjects()) {
				this.creator = preferences.getUsername();
			}
		}
		if (startDate != null) {
            this.startDate = (Date) startDate.clone();
        }
        if (endDate != null) {
            this.endDate = (Date) endDate.clone();
        }
		this.placerNumber = searchedOrderPlacerNumber;
		this.fillerNumber = searchedOrderFillerNumber;
		this.domain = selectedDomain;
		this.simulatedActor = simulatedActor;
		this.searchedVisitNumber = searchedVisitNumber;
		this.searchedPatientId = searchedPatientId;
		this.newOrderControlCode = newOrderControlCode;
		this.isMainEntity = isMainEntity;
		this.workOrder = isWorkOrder;
		this.relatedSpecimen = null;
		this.relatedContainer = null;
	}

	@Override
	public void appendFiltersFields(HQLQueryBuilder<LabOrder> hqlBuilder) {
		hqlBuilder.addEq("workOrder", workOrder);
		if (relatedSpecimen != null) {
			hqlBuilder.addEq("specimens.id", relatedSpecimen.getId());
		}
		if (relatedContainer != null) {
			hqlBuilder.addEq("container", relatedContainer);
		}
		if ((creator != null) && !creator.isEmpty()) {
			hqlBuilder.addEq("creator", creator);
		}
		if (startDate != null) {
			hqlBuilder.addRestriction(HQLRestrictions.gt("lastChanged", startDate));
		}
		if (endDate != null) {
			hqlBuilder.addRestriction(HQLRestrictions.lt("lastChanged", endDate));
		}
		if ((placerNumber != null) && !placerNumber.isEmpty()) {
			hqlBuilder.addRestriction(HQLRestrictions.like("placerOrderNumber", placerNumber,
					HQLRestrictionLikeMatchMode.ANYWHERE));
		}
		if ((fillerNumber != null) && !fillerNumber.isEmpty()) {
			hqlBuilder.addRestriction(HQLRestrictions.like("fillerOrderNumber", fillerNumber,
					HQLRestrictionLikeMatchMode.ANYWHERE));
		}
		if (domain != null) {
			hqlBuilder.addEq("domain", domain);
		}
		if (simulatedActor != null) {
			hqlBuilder.addEq("simulatedActor", simulatedActor);
		}
		if ((newOrderControlCode != null) && !newOrderControlCode.isEmpty()) {
			if (newOrderControlCode.equals("DC")) {
				hqlBuilder.addEq("orderStatus", "IP");
				hqlBuilder.addRestriction(HQLRestrictions.neq("orderControlCode", "DC"));
			} else if (newOrderControlCode.equals("CA") && !workOrder) {
				hqlBuilder.addEq("orderStatus", null);
				hqlBuilder.addRestriction(HQLRestrictions.or(HQLRestrictions.eq("orderControlCode", "OK"),
						HQLRestrictions.eq("orderControlCode", "NA")));
			} else if (newOrderControlCode.equals("CA") && workOrder) {
				hqlBuilder.addRestriction(HQLRestrictions.or(HQLRestrictions.eq("orderControlCode", "OK"),
						HQLRestrictions.eq("orderControlCode", "UC")));
			} else if (newOrderControlCode.equals("OC") || newOrderControlCode.equals("SC")) {
				hqlBuilder.addRestriction(HQLRestrictions.or(HQLRestrictions.eq("orderStatus", null),
						HQLRestrictions.neq("orderStatus", "CA")));
				List<String> controlCodeIn = Arrays.asList("OK", "NA", "UC");
				hqlBuilder.addRestriction(HQLRestrictions.in("orderControlCode", controlCodeIn));
			}
		}
		if ((searchedVisitNumber != null) && !searchedVisitNumber.isEmpty()) {
			hqlBuilder.addRestriction(HQLRestrictions.like("encounter.visitNumber", searchedVisitNumber,
					HQLRestrictionLikeMatchMode.ANYWHERE));
		}
		if ((searchedPatientId != null) && !searchedPatientId.isEmpty()) {
			hqlBuilder.addRestriction(HQLRestrictions.like("encounter.patient.patientIdentifiers.identifier",
					searchedPatientId, HQLRestrictionLikeMatchMode.ANYWHERE));
		}
		if (isMainEntity != null) {
			hqlBuilder.addEq("mainEntity", isMainEntity);
		}
	}
@Override
        protected Object getId(LabOrder t) {
        // TODO Auto-generated method stub
        return t.getId();
        }
}
