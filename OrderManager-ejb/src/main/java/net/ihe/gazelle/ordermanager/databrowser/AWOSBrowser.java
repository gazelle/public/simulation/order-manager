package net.ihe.gazelle.ordermanager.databrowser;

import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import net.ihe.gazelle.ordermanager.model.Container;
import net.ihe.gazelle.ordermanager.model.ContainerQuery;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

import javax.faces.context.FacesContext;
import java.util.Map;

@Name("awosBrowser")
@Scope(ScopeType.PAGE)
public class AWOSBrowser extends DataBrowser<Container> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8131309896637229416L;

	private String containerIdentifier;

	@Override
	public void modifyQuery(HQLQueryBuilder<Container> queryBuilder, Map<String, Object> filterValuesApplied) {
		if (containerIdentifier != null) {
            ContainerQuery query = new ContainerQuery();
			queryBuilder.addRestriction(query.containerIdentifier().likeRestriction(containerIdentifier));
		}
	}

    @Override
    public Filter<Container> getFilter() {
        if (filter == null){
            filter = new Filter<Container>(getHQLCriterionsForFilter(), FacesContext.getCurrentInstance().getExternalContext()
                    .getRequestParameterMap());
        }
        return filter;
    }

    @Override
	protected HQLCriterionsForFilter<Container> getHQLCriterionsForFilter() {
		ContainerQuery query = new ContainerQuery();
		HQLCriterionsForFilter<Container> criterions = query.getHQLCriterionsForFilter();
		criterions.addPath("actor", query.simulatedActor(), actor);
		criterions.addPath("domain", query.domain(), domain);
		criterions.addPath("lastChanged", query.lastChanged());
		criterions.addPath("creator", query.creator());
        criterions.addQueryModifier(this);
		return criterions;
	}

	@Override
	public String getPermanentLink(Integer objectId) {
		ContainerQuery query = new ContainerQuery();
		query.id().eq(objectId);
		Container container = query.getUniqueResult();
		if (container != null && container.getSpecimen() != null) {
			return "/browsing/specimen.seam?domain=LAB&id=" + container.getSpecimen().getId();
		} else {
			return null;
		}
	}

    @Override
    public FilterDataModel<Container> getDataModel() {
        return new FilterDataModel<Container>(getFilter()) {
            @Override
            protected Object getId(Container container) {
                return container.getId();
            }
        };
    }

    public String getSendResultTitle() {
		return "Send AWOS status change to Analyzer Manager";
	}

	public String sendResults(Integer awosId) {
		ContainerQuery query = new ContainerQuery();
		query.id().eq(awosId);
		Container container = query.getUniqueResult();
		if (container != null && container.getSpecimen() != null) {
			return "/hl7Initiators/orderResultSender.seam?domain=LAB&actor=ANALYZER&specimenId=" + container.getSpecimen().getId();
		} else {
			return null;
		}
	}

	public String getContainerIdentifier() {
		return containerIdentifier;
	}

	public void setContainerIdentifier(String containerIdentifier) {
		this.containerIdentifier = containerIdentifier;
	}
}
