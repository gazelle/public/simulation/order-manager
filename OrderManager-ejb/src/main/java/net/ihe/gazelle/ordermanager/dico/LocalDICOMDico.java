package net.ihe.gazelle.ordermanager.dico;

/**
 * Created by aberge on 22/03/17.
 *
 * @author aberge
 * @version $Id: $Id
 */
public enum  LocalDICOMDico {

    CODE_VALUE("0008,0100", "LO", "CodeValue"),
    CODING_SCHEME("0008,0102", "SH", "CodingSchemeDesignator"),
    CODE_MEANING("0008,0104", "LO", "CodeMeaning"),
    TRANSFER_SYNTAX_UID("0002,0010", "UI", "TransferSyntaxUID"),
    SPECIFIC_CHARACTER_SET("0008,0005", "CS", "SpecificCharacterSet"),
    PATIENT_NAME("0010,0010", "PN", "PatientName"),
    PATIENT_ID("0010,0020", "LO", "PatientID"),
    ISSUER_OF_PATIENT_ID("0010,0021", "LO", "IssuerOfPatientID"),
    OTHER_PID_SEQUENCE("0010,1002", "SQ", "OtherPatientIDSequence"),
    PATIENT_BIRTH_DATE("0010,0030", "DA", "PatientBirthDate"),
    PATIENT_BIRTH_TIME("0010,0032", "TM", "PatientBirthTime"),
    PATIENT_AGE("0010,1010", "AS", "PatientAge"),
    PATIENT_SEX("0010,0040", "CS", "PatientSex"),
    PATIENT_WEIGHT("0010,1030", "DS", "PatientWeight"),
    PATIENT_SIZE("0010,1020", "DS", "PatientSize"),
    ALLERGIES("0010,2110", "LO", "Allergies"),
    STUDY_INSTANCE_UID("0020,000d", "UI", "StudyInstanceUID"),
    PATIENT_STATE("0038,0500", "LO", "PatientState"),
    MEDICAL_ALERTS("0010,2000", "LO", "MedicalAlerts"),
    CURRENT_PATIENT_LOCATION("0038,0300", "LO", "CurrentPatientLocation"),
    ADMISSION_ID("0038,0010", "LO", "AdmissionID"),
    ISSUER_OF_ADMISSION_ID("0038,0011", "LO", "IssuerOfAdmissionID"),
    REFERENCED_PATIENT_SEQUENCE("0008,1120", "SQ", "ReferencedPatientSequence"),
    ACCESSION_NUMBER("0008,0050", "SH", "AccessionNumber"),
    REQUESTING_PHYSICIAN("0032,1032", "PN", "RequestingPhysician"),
    REFERRING_PHYSICIAN_NAME("0008,0090", "PN", "ReferringPhysicianName"),
    PLACER_ORDER_NUMBER_IMAGING_SERVICE_REQUEST("0040,2016", "LO", "PlacerOrderNumberOfImagingServiceRequest"),
    FILLER_ORDER_NUMBER_IMAGING_SERVICE_REQUEST("0040,2017", "LO", "FillerOrderNumberOfImagingServiceRequest"),
    ORDER_ENTERED_BY("0040,2008", "PN", "OrderEnteredBy"),
    ORDER_ENTERER_ORGANIZATION("0040,2009", "SH", "OrderEntererLocation"),
    ORDER_CALLBACK_PHONE_NUMBER("0040,2010", "SH", "OrderCallbackPhoneNumber"),
    SCHEDULED_PROCEDURE_STEP_SEQUENCE("0040,0100", "SQ", "ScheduledProcedureStepSequence"),
    SCHEDULED_STATION_AE_TITLE("0040,0001", "AE", "ScheduledStationAETitle"),
    SCHEDULED_PROCEDURE_STEP_START_DATE("0040,0002", "DA", "ScheduledProcedureStepStartDate"),
    SCHEDULED_PROCEDURE_STEP_START_TIME("0040,0003", "TM", "ScheduledProcedureStepStartTime"),
    MODALITY("0008,0060", "CS", "Modality"),
    SCHEDULED_PERFORMING_PHYSICIAN_NAME("0040,0006", "PN", "ScheduledPerformingPhysicianName"),
    SCHEDULED_PROCEDURE_STEP_DESCRIPTION("0040,0007", "LO", "ScheduledProcedureStepDescription"),
    SCHEDULED_PROTOCOL_CODE_SEQUENCE("0040,0008", "SQ", "ScheduledProtocolCodeSequence"),
    SCHEDULED_PROCEDURE_STEP_ID("0040,0009", "SH", "ScheduledProcedureStepID"),
    REQUESTED_PROCEDURE_ID("0040,1001", "SH", "RequestedProcedureID"),
    REASON_FOR_REQUESTING_PROCEDURE("0040,1002", "LO", "ReasonForRequestedProcedure"),
    REQUESTED_PROCEDURE_CODE_SEQUENCE("0032,1064", "SQ", "RequestedProcedureCodeSequence"),
    REQUESTED_PROCEDURE_DESCRIPTION("0032,1060", "LO", "RequestedProcedureDescription"),
    REFERENCED_STUDY_SEQUENCE("0008,1110", "SQ", "ReferencedStudySequence"),
    PATIENT_TRANSPORT_ARRANGEMENTS("0040,1004", "LO", "PatientTransportArrangements"),
    REQUESTED_PROCEDURE_PRIORITY("0040,1003", "SH", "RequestedProcedurePriority"),
    ADMITTING_DIAGNOSES_DESCRIPTION("0008,1080", "LO", "AdmittingDiagnosesDescription"),
    ADMITTING_DIAGNOSES_CODE_SEQUENCE("0008,1084", "SQ", "AdmittingDiagnosesCodeSequence"),
    REFERENCED_SOP_CLASS_UID("0008,1150", "UI", "ReferencedSOPClassUID"),
    REFERENCED_SOP_INSTANCE_UID("0008,1155", "UI", "ReferencedSOPInstanceUID");

    private String key;
    private String valueRepresentation;
    private String keyword;

    LocalDICOMDico(String tag, String valueRepresentation, String keyword){
        this.key = tag;
        this.valueRepresentation = valueRepresentation;
        this.keyword = keyword;
    }

    /**
     * <p>Getter for the field <code>key</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getKey() {
        return key;
    }

    /**
     * <p>Getter for the field <code>valueRepresentation</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getValueRepresentation() {
        return valueRepresentation;
    }

    /**
     * <p>Getter for the field <code>keyword</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getKeyword() {
        return keyword;
    }
}
