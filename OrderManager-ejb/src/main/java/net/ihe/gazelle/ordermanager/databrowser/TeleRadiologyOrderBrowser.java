package net.ihe.gazelle.ordermanager.databrowser;

import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import net.ihe.gazelle.ordermanager.model.TeleRadiologyOrder;
import net.ihe.gazelle.ordermanager.model.TeleRadiologyOrderQuery;
import net.ihe.gazelle.ssov7.gum.client.application.UserAttributeCommon;
import net.ihe.gazelle.ssov7.gum.client.application.service.UserService;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

import javax.faces.context.FacesContext;
import java.util.Map;


@Name("teleRadiologyOrderBrowser")
@Scope(ScopeType.PAGE)
public class TeleRadiologyOrderBrowser extends DataBrowser<TeleRadiologyOrder> implements UserAttributeCommon {

	private static final long serialVersionUID = -7167130407208833592L;

	@In(value="gumUserService")
	private UserService userService;

	@Override
	public void modifyQuery(HQLQueryBuilder<TeleRadiologyOrder> queryBuilder, Map<String, Object> filterValuesApplied) {
		if ((fillerOrderNumber != null) && !fillerOrderNumber.isEmpty()) {
			queryBuilder.addLike("fillerOrderNumber", fillerOrderNumber);
		}
		if ((placerOrderNumber != null) && !placerOrderNumber.isEmpty()) {
			queryBuilder.addLike("placerOrderNumber", placerOrderNumber);
		}
		if ((patientId != null) && !patientId.isEmpty()) {
			queryBuilder.addLike("encounter.patient.patientIdentifiers.identifier", patientId);
		}
		if ((visitNumber != null) && !visitNumber.isEmpty()) {
			queryBuilder.addLike("encounter.visitNumber", visitNumber);
		}
	}

    @Override
    public Filter<TeleRadiologyOrder> getFilter() {
        if (filter == null){
            filter = new Filter<TeleRadiologyOrder>(getHQLCriterionsForFilter(), FacesContext.getCurrentInstance().getExternalContext()
                    .getRequestParameterMap());
        }
        return filter;
    }

    @Override
	protected HQLCriterionsForFilter<TeleRadiologyOrder> getHQLCriterionsForFilter() {
		TeleRadiologyOrderQuery query = new TeleRadiologyOrderQuery();
		HQLCriterionsForFilter<TeleRadiologyOrder> criteria = query.getHQLCriterionsForFilter();
		criteria.addPath("actor", query.simulatedActor(), actor);
		criteria.addPath("domain", query.domain(), domain);
		criteria.addPath("lastChanged", query.lastChanged());
		criteria.addPath("creator", query.creator());
		criteria.addQueryModifier(this);
		return criteria;
	}

	@Override
	public String getPermanentLink(Integer objectId) {
		return "/browsing/teleRadOrder.seam?domain=TRO&id=" + objectId;
	}

    @Override
    public FilterDataModel<TeleRadiologyOrder> getDataModel() {
        return new FilterDataModel<TeleRadiologyOrder>(getFilter()) {
            @Override
            protected Object getId(TeleRadiologyOrder teleRadiologyOrder) {
                return teleRadiologyOrder.getId();
            }
        };
    }

	@Override
	public String getUserName(String userId) {
		return userService.getUserDisplayNameWithoutException(userId);
	}

}
