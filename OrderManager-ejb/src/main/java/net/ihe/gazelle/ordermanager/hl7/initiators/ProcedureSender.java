package net.ihe.gazelle.ordermanager.hl7.initiators;

import net.ihe.gazelle.HL7Common.initiator.Initiator;
import net.ihe.gazelle.HL7Common.model.HL7V2ResponderSUTConfiguration;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.ordermanager.admin.UserPreferencesManager;
import net.ihe.gazelle.ordermanager.filter.OrderFilter;
import net.ihe.gazelle.ordermanager.filter.ProcedureFilter;
import net.ihe.gazelle.ordermanager.model.AccessionNumberGenerator;
import net.ihe.gazelle.ordermanager.model.NumberGenerator;
import net.ihe.gazelle.ordermanager.model.Order;
import net.ihe.gazelle.ordermanager.model.Patient;
import net.ihe.gazelle.ordermanager.model.RequestedProcedure;
import net.ihe.gazelle.ordermanager.utils.IHERadMessageBuilder;
import net.ihe.gazelle.ordermanager.utils.RequestedProcedureCreator;
import net.ihe.gazelle.simulator.common.model.ApplicationConfiguration;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;
import net.ihe.gazelle.simulator.message.model.TransactionInstance;
import net.ihe.gazelle.ssov7.gum.client.application.UserAttributeCommon;
import net.ihe.gazelle.ssov7.gum.client.application.service.UserService;
import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.core.ResourceBundle;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.model.SelectItem;
import javax.persistence.EntityManager;
import java.io.FileNotFoundException;
import java.io.Serializable;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;

@Name("procedureSender")
@Scope(ScopeType.PAGE)
public class ProcedureSender extends AbstractOrderSender<Order> implements Serializable, UserAttributeCommon {

    private static Logger log = LoggerFactory.getLogger(ProcedureSender.class);
    private static final long serialVersionUID = 3922943622688548600L;

    public static final String SCHEDULE = "NW";
    public static final String CANCEL_OCC = "CA";
    public static final String DISCONTINUE_OCC = "DC";
    public static final String UPDATE_OCC = "SC";
    public static final String COMPLETE = "CM";
    public static final String IMAGE_MANAGER = "IM";

    private RequestedProcedure selectedRequestedProcedure;
    private ProcedureFilter procedureFilter;
    private List<HL7V2ResponderSUTConfiguration> availableSUTs;

    @In(value="gumUserService")
    private UserService userService;

    @Override
    @Create
    public void initializePage() {
        super.initializePage();
        sutActorKeyword = IMAGE_MANAGER;
        orderControlCode = SCHEDULE;
        setSelectionType(SelectionType.ORDER);
        boolean executeOnChangeActionEvent = true;
        if (urlParams.containsKey("orderId")) {
            EntityManager entityManager = EntityManagerService.provideEntityManager();
            Integer orderId = Integer.valueOf(urlParams.get("orderId"));
            Order orderToSchedule = entityManager.find(Order.class, orderId);
            if ((orderToSchedule != null)
                    && ((orderToSchedule.getRequestedProcedures() == null) || orderToSchedule.getRequestedProcedures()
                    .isEmpty())) {
                scheduleProcedureForSelectedOrder(orderToSchedule);
                executeOnChangeActionEvent = false;
                log.info("order selected from URL");
            } else {
                log.error("Cannot schedule procedure for this order. Order does not exist or procedures have already been scheduled");
                FacesMessages
                        .instance()
                        .add("Cannot schedule procedure for this order. Order does not exist or procedures have already been scheduled");
            }
        }
        if (executeOnChangeActionEvent) {
            onChangeActionEvent();
        }
    }

    public List<HL7V2ResponderSUTConfiguration> getAvailableSUTs() {
        if (availableSUTs == null) {
            String usage = selectedDomain.getKeyword() + "_RAD4";
            availableSUTs = HL7V2ResponderSUTConfiguration.getAllConfigurationsForUsage(usage);
        }
        return availableSUTs;
    }

    public void scheduleProcedureForSelectedOrder(Order order) {
        if (order == null) {
            return;
        } else {
            EntityManager entityManager = EntityManagerService.provideEntityManager();
            selectedOrder = order;
            setSelectedEncounter(selectedOrder.getEncounter());
            if (getSelectedEncounter() == null) {
                FacesMessages.instance().add(StatusMessage.Severity.WARN, "Cannot scheduled this order, no encounter data found");
            } else {
                setSelectedPatient(entityManager.find(Patient.class, getSelectedEncounter().getPatient().getId()));
                if (getSelectedPatient() == null) {
                    FacesMessages.instance().add(StatusMessage.Severity.WARN, "Cannot scheduled this order, no patient data found");
                } else {
                    scheduleProcedure();
                }
            }
        }
    }

    public void scheduleProcedure() {
        String creator = null;
        if (userPreferences != null) {
            creator = userPreferences.getUsername();
        }
        if ((selectedOrder.getAccessionNumber() == null) || selectedOrder.getAccessionNumber().isEmpty()) {
            selectedOrder.setAccessionNumber(AccessionNumberGenerator.generate());
        }
        selectedOrder = selectedOrder.save(EntityManagerService.provideEntityManager());
        RequestedProcedureCreator procedureCreator = new RequestedProcedureCreator(
                selectedOrder.getUniversalServiceId(), simulatedActor, selectedDomain, creator);
        String orderHierarchyLocation;
        if (selectedDomain.getKeyword().equals("EYE")) {
            orderHierarchyLocation = ApplicationConfiguration.getValueOfVariable("eye_order_hierarchy_location");
        } else // domain is Radiology or Cardiology
        {
            orderHierarchyLocation = ApplicationConfiguration.getValueOfVariable("order_hierarchy_location");
        }
        try {
            selectedRequestedProcedure = procedureCreator.createProcedure(orderHierarchyLocation);
            if (selectedRequestedProcedure == null) {
                FacesMessages.instance().add(StatusMessage.Severity.ERROR,
                        "The tool has not been able to create the requested procedure associated to this order");
                return;
            } else {
                selectedRequestedProcedure.setOrder(selectedOrder);
                selectedRequestedProcedure = selectedRequestedProcedure.save(null);
            }
        } catch (FileNotFoundException e) {
            FacesMessages.instance()
                    .add(StatusMessage.Severity.ERROR,"The order hierarchy file is not available or its location has not be defined");
        } catch (MalformedURLException e) {
            FacesMessages.instance()
                    .add(StatusMessage.Severity.ERROR,"The order hierarchy file is not available or its location has not be defined");
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            FacesMessages.instance().add(StatusMessage.Severity.ERROR,
                    "An internal error has prevented the tool from creating the requested procedure");
        }
    }

    @Override
    public void createANewOrder() {
        selectedOrder = new Order(selectedDomain, simulatedActor, getSelectedEncounter(),
                EntityManagerService.provideEntityManager());
        if (userPreferences != null) {
            selectedOrder.setCreator(userPreferences.getUsername());
        }
        selectedOrder.setPlacerOrderNumber(NumberGenerator.getOrderNumberForActor(Actor.findActorWithKeyword("OP"),
                EntityManagerService.provideEntityManager()));
        selectedOrder.setOrderControlCode("NA");

    }

    /**
     * This method is not useful in this context
     */
    @Override
    public List<SelectItem> getAvailableOrderStatuses() {
        return null;
    }

    @Override
    public List<SelectItem> listAvailableActions() {
        List<SelectItem> actions = new ArrayList<SelectItem>();
        actions.add(new SelectItem(SCHEDULE, ResourceBundle.instance().getString(
                "gazelle.ordermanager.ProcedureScheduled")));
        actions.add(new SelectItem(CANCEL_OCC, ResourceBundle.instance().getString(
                "gazelle.ordermanager.ProcedureUpdateCancel")));
        actions.add(new SelectItem(DISCONTINUE_OCC, ResourceBundle.instance().getString(
                "gazelle.ordermanager.ProcedureUpdateDiscontinue")));
        actions.add(new SelectItem(UPDATE_OCC, ResourceBundle.instance().getString("gazelle.ordermanager.ProcedureUpdate")));
        actions.add(new SelectItem(COMPLETE, ResourceBundle.instance().getString(
                "gazelle.ordermanager.ProcedureUpdateComplete")));
        return actions;
    }

    @Override
    public void fillOrderRandomly() {
        selectedOrder.fillOrderRandomly(selectedDomain, simulatedActor);
    }

    @Override
    public void onChangeActionEvent() {
        selectedRequestedProcedure = null;
        selectedOrder = null;
        setSelectedEncounter(null);
        if (orderControlCode.equals(SCHEDULE)) {
            selectedOrder = null;
            selectedRequestedProcedure = null;
            setSelectionType(SelectionType.ORDER);
            orderFilter = new OrderFilter(selectedDomain, simulatedActor, "SC", false);
        } else {
            selectedOrder = null;
            selectedRequestedProcedure = null;
            setSelectionType(SelectionType.PROCEDURE);
            procedureFilter = new ProcedureFilter(selectedDomain, simulatedActor, orderControlCode);
        }
    }

    @Override
    public void sendMessage() {
        HL7V2ResponderSUTConfiguration sut = (HL7V2ResponderSUTConfiguration) Component.getInstance("selectedSUT");
        if (sut == null) {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "No SUT selected ! please select the system to test and retry");
            return;
        }
        String transaction;
        if (orderControlCode.equals(SCHEDULE)) {
            transaction = "RAD-4";
        } else if (orderControlCode.equals(CANCEL_OCC)) {
            transaction = "RAD-13";
            selectedRequestedProcedure.setOrderControlCode(CANCEL_OCC);
            selectedRequestedProcedure.setOrderStatus(CANCEL_OCC);
        } else if (orderControlCode.equals(DISCONTINUE_OCC)) {
            transaction = "RAD-13";
            selectedRequestedProcedure.setOrderControlCode(DISCONTINUE_OCC);
            selectedRequestedProcedure.setOrderStatus(CANCEL_OCC);
        } else {
            transaction = "RAD-13";
            selectedRequestedProcedure.setOrderControlCode("XO");
            selectedRequestedProcedure.setOrderStatus(orderControlCode);
        }
        selectedRequestedProcedure = selectedRequestedProcedure.save(null);
        String messageType;
        if (UserPreferencesManager.instance().isHL7v251OptionActivated()) {
            messageType = "OMI^O23^OMI_O23";
        } else {
            messageType = "ORM^O01^ORM_O01";
        }
        IHERadMessageBuilder builder = new IHERadMessageBuilder(transaction, "OF", selectedRequestedProcedure,
                selectedOrder, sut, sendingApplication, sendingFacility,
                UserPreferencesManager.instance().isHL7v251OptionActivated());
        try {
            String messageStringToSend = builder.generateMessage();
            Initiator hl7Initiator = new Initiator(sut, sendingFacility, sendingApplication, simulatedActor,
                    Transaction.GetTransactionByKeyword(transaction), messageStringToSend, messageType, getSelectedDomain().getKeyword(),
                    Actor.findActorWithKeyword(sutActorKeyword));

            TransactionInstance hl7Message = hl7Initiator.sendMessageAndGetTheHL7Message();
            hl7Messages = new ArrayList<TransactionInstance>();
            hl7Messages.add(hl7Message);
        } catch (Exception e) {
            FacesMessages.instance().add(e.getMessage());
            log.error(e.getMessage(), e);
            return;
        }
        selectedOrder = null;
        selectedRequestedProcedure = null;
        setSelectedEncounter(null);
        setSelectedPatient(null);
    }

    @Override
    public String getUserName(String userId) {
        return userService.getUserDisplayNameWithoutException(userId);
    }


    public Order getSelectedOrder() {
        return selectedOrder;
    }

    public void setSelectedOrder(Order selectedOrder) {
        this.selectedOrder = selectedOrder;
    }

    public RequestedProcedure getSelectedRequestedProcedure() {
        return selectedRequestedProcedure;
    }

    public void setSelectedRequestedProcedure(RequestedProcedure selectedRequestedProcedure) {
        log.info("setSelectedRequestedProcedure");
        if (selectedRequestedProcedure != null) {
            EntityManager entityManager = EntityManagerService.provideEntityManager();
            this.selectedRequestedProcedure = entityManager.find(RequestedProcedure.class,
                    selectedRequestedProcedure.getId());
            this.selectedOrder = this.selectedRequestedProcedure.getOrder();
        } else {
            this.selectedRequestedProcedure = null;
            this.selectedOrder = null;
        }
    }


    public String getSCHEDULE() {
        return SCHEDULE;
    }

    public String getUPDATE_OCC() {
        return UPDATE_OCC;
    }

    public String getCOMPLETE() {
        return COMPLETE;
    }

    public String getCANCEL_OCC() {
        return CANCEL_OCC;
    }

    public String getDISCONTINUE_OCC() {
        return DISCONTINUE_OCC;
    }

    public ProcedureFilter getProcedureFilter() {
        return procedureFilter;
    }

    public void displayEncounterList(){
        setSelectionType(SelectionType.ENCOUNTER);
    }

    public void displayOrderList(){
        setSelectionType(SelectionType.ORDER);
    }
}
