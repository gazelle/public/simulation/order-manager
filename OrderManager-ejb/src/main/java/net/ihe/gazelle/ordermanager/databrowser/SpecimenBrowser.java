package net.ihe.gazelle.ordermanager.databrowser;

import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import net.ihe.gazelle.ordermanager.model.Specimen;
import net.ihe.gazelle.ordermanager.model.SpecimenQuery;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

import javax.faces.context.FacesContext;
import java.util.Map;

@Name("specimenBrowser")
@Scope(ScopeType.PAGE)
public class SpecimenBrowser extends DataBrowser<Specimen> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4332704629203079598L;

	@Override
	public void modifyQuery(HQLQueryBuilder<Specimen> queryBuilder, Map<String, Object> map) {
		if ((fillerOrderNumber != null) && !fillerOrderNumber.isEmpty()) {
			queryBuilder.addLike("fillerAssignedIdentifier", fillerOrderNumber);
		}
		if ((placerOrderNumber != null) && !placerOrderNumber.isEmpty()) {
			queryBuilder.addLike("placerAssignedIdentifier", placerOrderNumber);
		}
		if ((patientId != null) && !patientId.isEmpty()) {
			queryBuilder.addLike("orders.encounter.patient.patientIdentifiers.identifier", patientId);
		}
		if ((visitNumber != null) && !visitNumber.isEmpty()) {
			queryBuilder.addLike("orders.encounter.visitNumber", visitNumber);
		}
		if (containerId != null) {
			queryBuilder.addEq("containers.id", containerId);
		}
	}

    @Override
    public Filter<Specimen> getFilter() {
        if (filter == null){
            filter = new Filter<Specimen>(getHQLCriterionsForFilter(), FacesContext.getCurrentInstance().getExternalContext()
                    .getRequestParameterMap());
        }
        return filter;
    }

    @Override
	protected HQLCriterionsForFilter<Specimen> getHQLCriterionsForFilter() {
		SpecimenQuery query = new SpecimenQuery();
		HQLCriterionsForFilter<Specimen> criterions = query.getHQLCriterionsForFilter();
		criterions.addPath("actor", query.simulatedActor(), actor);
		criterions.addPath("domain", query.domain(), domain);
		criterions.addPath("lastChanged", query.lastChanged());
		criterions.addPath("creator", query.creator());
		criterions.addPath("mainEntity", query.mainEntity(), true);
		if (objectType.equals(ObjectType.SPECIMEN)) {
			criterions.addPath("isWorkOrder", query.workOrder(), false);
		} else if (objectType.equals(ObjectType.WORKSPECIMEN)) {
			criterions.addPath("isWorkOrder", query.workOrder(), true);
		}
		criterions.addQueryModifier(this);
		return criterions;
	}

	@Override
	public String getPermanentLink(Integer objectId) {
		return "/browsing/specimen.seam?domain=LAB&id=" + objectId;
	}

    @Override
    public FilterDataModel<Specimen> getDataModel() {
        return new FilterDataModel<Specimen>(getFilter()) {
            @Override
            protected Object getId(Specimen specimen) {
                return specimen.getId();
            }
        };
    }

    public String getSendResultTitle() {
		if (actor.getKeyword().equals("OF")) {
			return "Send results to ORT";
		} else if (actor.getKeyword().equals("AM")) {
			return "Send results to OF";
		} else {
			return null;
		}
	}

	public String sendResults(Integer specimenId) {
		if (objectType.equals(ObjectType.WORKSPECIMEN)) {
			return "/hl7Initiators/orderResultSender.seam?domain=LAB&actor=AM&specimenId=" + specimenId;
		} else {
			return "/hl7Initiators/orderResultSender.seam?domain=LAB&actor=OF&specimenId=" + specimenId;
		}
	}

	public String createWorkOrder(Integer specimenId) {
		return "/hl7Initiators/workOrderSender.seam?domain=LAB&specimenId=" + specimenId;
	}

}
