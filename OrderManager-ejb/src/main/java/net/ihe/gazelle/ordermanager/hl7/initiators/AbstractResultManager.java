package net.ihe.gazelle.ordermanager.hl7.initiators;

import net.ihe.gazelle.HL7Common.model.HL7V2ResponderSUTConfiguration;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.ordermanager.filter.AbstractOrderFilter;
import net.ihe.gazelle.ordermanager.filter.ContainerFilter;
import net.ihe.gazelle.ordermanager.filter.SpecimenFilter;
import net.ihe.gazelle.ordermanager.model.AbstractOrder;
import net.ihe.gazelle.ordermanager.model.Container;
import net.ihe.gazelle.ordermanager.model.Encounter;
import net.ihe.gazelle.ordermanager.model.NoteAndComment;
import net.ihe.gazelle.ordermanager.model.Observation;
import net.ihe.gazelle.ordermanager.model.Patient;
import net.ihe.gazelle.ordermanager.model.Specimen;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * <b>Class description</b>: AbstractResultManager This is the super class for the managed-bean used to send result management messages. That means, this is the super class for initiator part of
 * LAB-3, LAB-5 and LAB-29 transactions
 *
 * @author Anne-Gaëlle Bergé / IHE-Europe
 * @version 1.0 - February, 28th 2012
 *
 *
 *
 */
public abstract class AbstractResultManager<T extends AbstractOrder> extends AbstractSender {

    public static final String BATTERYCENTRIC = "BATTERY-CENTRIC";
    public static final String SPECIMENCENTRIC = "SPECIMEN-CENTRIC";
    public static final String CONTAINERCENTRIC = "CONTAINER-CENTRIC";
    public static final String PARENTORDER = "order";
    public static final String PARENTSPECIMEN = "specimen";

    protected String messageStructure;
    protected boolean displayEncounterList;
    protected Observation selectedObservation;
    protected Specimen selectedSpecimen;
    protected NoteAndComment selectedComment;
    protected SpecimenFilter specimenFilter;
    protected AbstractOrderFilter<T> orderFilter;
    protected String observationParent;
    protected Integer selectedOrderId;
    protected String newResultStatus;
    protected String newOrderStatus;
    /**
     * used for creation from scratch
     */
    protected Integer numberOfContainers;
    protected boolean editOrder;
    protected boolean editSpecimen;
    protected boolean expendPanel;
    protected Container selectedContainer;
    protected ContainerFilter containerFilter;
    protected boolean newObject = false;

    protected List<HL7V2ResponderSUTConfiguration> availableSuts;

    public abstract List<HL7V2ResponderSUTConfiguration> getAvailableSuts();


    /**
     * not used in JSF
     */
    protected Transaction transaction;

    /**
     * This 'selectedOrder' attribute stands for the currently selected order (or work order, or wos)
     */
    protected T selectedOrder;

    /**
     * A list of the objects (order/specimen/container) the user does not want to include in the message is maintained. Objects are referenced by their ids.
     */
    protected List<Integer> objectToNotIncludeInMessage;

    /**
     * Sets the common parameters and retrieves some information in the URL
     */
    @Override
    public void initializePage() {
        super.initializePage();
        messageStructure = SPECIMENCENTRIC;
        if (simulatedActor.getKeyword().equals("OF")) {
            sutActorKeyword = "ORT";
            transaction = Transaction.GetTransactionByKeyword("LAB-3");
        } else if (simulatedActor.getKeyword().equals("AM")) {
            sutActorKeyword = "OF";
            transaction = Transaction.GetTransactionByKeyword("LAB-5");
        } else if (simulatedActor.getKeyword().equals("ANALYZER")) {
            sutActorKeyword = "ANALYZER_MGR";
            transaction = Transaction.GetTransactionByKeyword("LAB-29");
        } else {
            sutActorKeyword = null;
        }
    }

    @Override
    public void createANewEncounterForPatient(Patient inPatient){
        super.createANewEncounterForPatient(inPatient);
        selectPatient = false;
        newObject = true;
        createNewResult();
    }

    /**
     * Instanciates a new result object
     */
    public abstract void createNewResult();

    /**
     * Methods used to create a new order from scratch
     */
    public abstract void createSpecimenForOrder();

    public abstract void addSpecimenToOrder();

    public abstract void removeSpecimenFromOrder(Specimen inSpecimen);

    public abstract void removeOrderFromSpecimen(T inOrder);

    public abstract void createOrderForSpecimen();

    public abstract void addOrderToSpecimen();

    public abstract void createContainerForSpecimen();

    public abstract void removeContainerFromSpecimen(Container inContainer);

    public abstract void createOrderForContainer(Container inContainer);

    public abstract void removeOrderFromContainer(Container currentContainer, T currentOrder);

    public abstract void addOrderToContainer();

    /**
     * END
     */

    /**
     * This method is called when selecting the specimen for which sending results
     *
     * @param inSpecimen : the specimen to retrieve
     */
    public void selectSpecimen(Specimen inSpecimen) {
        selectedSpecimen = inSpecimen;
        selectSpecimen();
    }

    @Override
    public void skipPatientSelection() {
        selectEncounter(null);
        displayEncounterList = false;
        selectPatient = false;
    }

    /**
     * This method is called when the user selects the specimen for which sending results This method should set the related encounter and patient (retrieve from one of the related order)
     */
    public abstract void selectSpecimen();

    /**
     * Adds the current observation to the list of observations related to the specimen The currentObservation is then set to null in order to close the fillingObservation panel
     */
    public void addObservationToSpecimen() {
        if ((selectedSpecimen != null) && (selectedObservation != null)) {
            if (selectedSpecimen.getObservations() == null) {
                selectedSpecimen.setObservations(new ArrayList<Observation>(Arrays.asList(selectedObservation)));
            } else {
                selectedSpecimen.getObservations().add(selectedObservation);
            }
            selectedObservation = null;
            selectedSpecimen = selectedSpecimen.save(EntityManagerService.provideEntityManager());
            // reset the selectedSpecimen to null if the main entity is the order
            if (messageStructure.equals(BATTERYCENTRIC)) {
                selectedSpecimen = null;
            }
        } else {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Both specimen and observation should not be null");
        }
    }

    /**
     * Creates a new observation for selected specimen
     */
    public void createNewObservationForSpecimen(Specimen specimen) {
        selectedSpecimen = specimen;
        createNewObservationForSpecimen();
    }

    public void createNewObservationForSpecimen() {
        selectedObservation = new Observation(selectedDomain, simulatedActor, username);
        selectedObservation.setSpecimen(selectedSpecimen);
        if (selectedSpecimen.getObservations() != null) {
            selectedObservation.setSetId(selectedSpecimen.getObservations().size() + 1);
        } else {
            selectedObservation.setSetId(1);
        }
        selectedObservation.setIncludeInMessage(true);
        observationParent = PARENTSPECIMEN;
    }

    /**
     * Add the just created observation to currently selected order (lab order, work order, awos...) Save the selectedOrder set the selectedObservation to null then
     */
    public abstract void addObservationToOrder();

    /**
     * This method is called when the user click on the "Add an observation" button for a given order
     *
     * @param inOrder
     */
    public void createNewObservationForOrder(T inOrder) {
        selectedOrder = inOrder;
        createNewObservationForOrder();
    }

    /**
     * This method is called when the user click on the "Add an observation" button for the current order TODO : do not forget to set setID attribute
     */
    public abstract void createNewObservationForOrder();

    /**
     * This method is called when the user does not want a given object to appear in the message
     *
     * @param objectId
     */
    public void removeObjectFromList(Integer objectId) {
        if (objectToNotIncludeInMessage == null) {
            objectToNotIncludeInMessage = new ArrayList<Integer>(Arrays.asList(objectId));
        } else {
            objectToNotIncludeInMessage.add(objectId);
        }
    }

    /**
     * Fills the currentSpecimen object with random values
     */
    public void fillSpecimenRandomly() {
        selectedSpecimen.fillRandomly();
    }

    /**
     * When the encounter is selected, it means a new result needs to be created from scratch
     *
     * @param inEncounter : the encounter to use for result creation
     */
    @Override
    public void selectEncounter(Encounter inEncounter) {
        if (inEncounter != null) {
            super.selectEncounter(inEncounter);
        } else {
            super.setSelectedEncounter(null);
        }
        newObject = true;
        createNewResult();
    }

    /**
     * Instanciate a new NoteAndComment object for the selected observation
     *
     * @param observation
     */
    public void createNewCommentForObservation(Observation observation) {
        selectedObservation = observation;
        createNewCommentForObservation();
    }

    /**
     * Instanciate a new NoteAndComment object for the current observation
     */
    public void createNewCommentForObservation() {
        selectedComment = new NoteAndComment(simulatedActor, username, selectedDomain);
        if (selectedObservation.getComments() != null) {
            selectedComment.setId(selectedObservation.getComments().size() + 1);
        } else {
            selectedComment.setId(1);
        }
    }

    /**
     * Add the current NoteAndComment object to the current observation currentComment is then set to null to enable the fillingComment panel to be closed
     */
    public void addCommentToObservation() {
        if (selectedObservation.getComments() == null) {
            selectedObservation.setComments(new ArrayList<NoteAndComment>(Arrays.asList(selectedComment)));
        } else {
            selectedObservation.getComments().add(selectedComment);
        }
        selectedComment = null;
    }

    @Override
    public void onChangeActionEvent() {
        setSelectedEncounter(null);
        setSelectedPatient(null);
        orderFilter = null;
        specimenFilter = null;
        // TODO mandatory ??
        selectedSpecimen = null;
        selectedOrder = null;
        selectedObservation = null;
        if (displayEncounterList) {
            setSelectionType(SelectionType.ENCOUNTER);
        } else {
            if (messageStructure.equals(SPECIMENCENTRIC) && !simulatedActor.getKeyword().equals("ANALYZER")) {
                onChangeActionToSpecimenCentric();
            } else if (messageStructure.equals(CONTAINERCENTRIC) || simulatedActor.getKeyword().equals("ANALYZER")) {
                onChangeActionToContainerCentric();
            } else {
                onChangeActionToBatteryCentric();
            }
        }
    }

    /**
     * Updates the orders attribute This method is called when building the page the first time and each time the selected message structure changes If your transaction does not support
     * BATTERY-CENTRIC case, let this method empty
     */
    public abstract void onChangeActionToBatteryCentric();

    /**
     * Updates the specimens (SpecimenDataModel) attribute This method is called when building the page the first time and each time the selected message structure changes If your transaction does not
     * support SPECIMEN-CENTRIC case, let this method empty
     */
    public abstract void onChangeActionToSpecimenCentric();

    /**
     * Updates the specimens (SpecimenDataModel) attribute This method is called when building the page the first time and each time the selected message structure changes If your transaction does not
     * support SPECIMEN-CENTRIC case, let this method empty
     */
    public abstract void onChangeActionToContainerCentric();

    /**
     * create new patient, new encounter and instanciate new result
     */
    @Override
    public void createNewPatientAndEncounter() {
        super.createNewPatientAndEncounter();
        createNewResult();
        newObject = true;
        selectPatient = false;
    }

    public void cancelObservationCreation() {
        selectedObservation = null;
    }

    /**
     * Save the AbstractOrder/Specimen and set the newObject attribute to false;
     */
    public abstract void saveObjectAndGetItReadyForObservations();

    /**
     * Save the newly updated observation, retrieve the linked entity (AbstractOrder or Specimen) and reload this object
     *
     * @param inOrder
     */
    public abstract void updateOrder(T inOrder);

    /**
     * @param inObservation
     */
    public abstract void selectObservationForEdit(Observation inObservation);

    /**
     *
     */
    public abstract void updateObservation();

    /***************************************
     * GETTERS and SETTERS
     ***************************************/

    public String getMessageStructure() {
        return messageStructure;
    }

    public void setMessageStructure(String messageStructure) {
        this.messageStructure = messageStructure;
    }

    public boolean isDisplayEncounterList() {
        return displayEncounterList;
    }

    public void setDisplayEncounterList(boolean displayEncounterList) {
        this.displayEncounterList = displayEncounterList;
        if (displayEncounterList) {
            setSelectionType(SelectionType.ENCOUNTER);
        } else {
            onChangeActionEvent();
        }
    }

    public Observation getSelectedObservation() {
        return selectedObservation;
    }

    public void setSelectedObservation(Observation currentObservation) {
        this.selectedObservation = currentObservation;
    }

    public Specimen getSelectedSpecimen() {
        return selectedSpecimen;
    }

    public void setSelectedSpecimen(Specimen currentSpecimen) {
        this.selectedSpecimen = currentSpecimen;
    }

    public List<Integer> getObjectToNotIncludeInMessage() {
        return objectToNotIncludeInMessage;
    }

    public void setObjectToNotIncludeInMessage(List<Integer> objectToNotIncludeInMessage) {
        this.objectToNotIncludeInMessage = objectToNotIncludeInMessage;
    }

    public String getBatteryCentric() {
        return BATTERYCENTRIC;
    }

    public String getSpecimenCentric() {
        return SPECIMENCENTRIC;
    }

    public String getContainerCentric() {
        return CONTAINERCENTRIC;
    }

    public String getParentOrder() {
        return PARENTORDER;
    }

    public String getParentSpecimen() {
        return PARENTSPECIMEN;
    }

    public T getSelectedOrder() {
        return selectedOrder;
    }

    public void setSelectedOrder(T currentOrder) {
        this.selectedOrder = currentOrder;
        if (currentOrder != null) {
            setSelectedEncounter(this.selectedOrder.getEncounter());
            if (getSelectedEncounter() != null) {
                setSelectedPatient(getSelectedEncounter().getPatient());
            }
        } else {
            setSelectedEncounter(null);
            setSelectedPatient(null);
        }
    }

    public NoteAndComment getSelectedComment() {
        return selectedComment;
    }

    public void setSelectedComment(NoteAndComment currentComment) {
        this.selectedComment = currentComment;
    }

    public SpecimenFilter getSpecimenFilter() {
        return specimenFilter;
    }

    public AbstractOrderFilter<T> getOrderFilter() {
        return orderFilter;
    }

    public void setOrderFilter(AbstractOrderFilter<T> orderFilter) {
        this.orderFilter = orderFilter;
    }

    public String getObservationParent() {
        return observationParent;
    }

    public void setObservationParent(String observationParent) {
        this.observationParent = observationParent;
    }

    public Integer getNumberOfContainers() {
        return numberOfContainers;
    }

    public void setNumberOfContainers(Integer numberOfContainers) {
        this.numberOfContainers = numberOfContainers;
    }

    public boolean isEditOrder() {
        return editOrder;
    }

    public void setEditOrder(boolean editOrder) {
        this.editOrder = editOrder;
    }

    public boolean isEditSpecimen() {
        return editSpecimen;
    }

    public void setEditSpecimen(boolean editSpecimen) {
        this.editSpecimen = editSpecimen;
    }

    public boolean isExpendPanel() {
        return expendPanel;
    }

    public void setExpendPanel(boolean expendPanel) {
        this.expendPanel = expendPanel;
    }

    public Container getSelectedContainer() {
        return selectedContainer;
    }

    public void setSelectedContainer(Container selectedContainer) {
        this.selectedContainer = selectedContainer;
    }

    public ContainerFilter getContainerFilter() {
        return containerFilter;
    }

    public boolean isNewObject() {
        return newObject;
    }

    public void setNewObject(boolean newObject) {
        this.newObject = newObject;
    }

    public Integer getSelectedOrderId() {
        return selectedOrderId;
    }

    public void setSelectedOrderId(Integer selectedOrderId) {
        this.selectedOrderId = selectedOrderId;
    }

    public String getNewResultStatus() {
        return newResultStatus;
    }

    public void setNewResultStatus(String newResultStatus) {
        this.newResultStatus = newResultStatus;
    }

    public String getNewOrderStatus() {
        return newOrderStatus;
    }

    public void setNewOrderStatus(String newOrderStatus) {
        this.newOrderStatus = newOrderStatus;
    }
}
