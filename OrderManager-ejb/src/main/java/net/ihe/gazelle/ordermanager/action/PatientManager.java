package net.ihe.gazelle.ordermanager.action;

import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.ordermanager.datamodel.EncounterDataModel;
import net.ihe.gazelle.ordermanager.datamodel.OrderDataModel;
import net.ihe.gazelle.ordermanager.model.Encounter;
import net.ihe.gazelle.ordermanager.model.Patient;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;

import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import java.io.Serializable;
import java.util.Map;

@Name("patientManager")
@Scope(ScopeType.PAGE)
public class PatientManager implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Patient patient;

    @Create
	public void selectPatientById() {
		Map<String, String> urlParams = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		if ((urlParams != null) && urlParams.containsKey("id")) {
			try {
				Integer patientId = Integer.valueOf(urlParams.get("id"));
				EntityManager entityManager = EntityManagerService.provideEntityManager();
				patient = entityManager.find(Patient.class, patientId);
			} catch (NumberFormatException e) {
				FacesMessages.instance().add(urlParams.get("id") + " is not a valid identifier");
				patient = null;
			}
		}
	}

	public EncounterDataModel getEncountersForSelectedPatient() {
		return new EncounterDataModel(patient);
	}

	public Patient getPatient() {
		return patient;
	}

	public void setPatient(Patient patient) {
		this.patient = patient;
	}

	public OrderDataModel getOrdersForEncounter(Encounter encounter) {
		return new OrderDataModel(encounter);
	}
}
