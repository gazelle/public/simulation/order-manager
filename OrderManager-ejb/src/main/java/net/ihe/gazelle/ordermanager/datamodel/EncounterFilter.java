package net.ihe.gazelle.ordermanager.datamodel;

import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.hql.criterion.HQLCriterion;
import net.ihe.gazelle.ordermanager.model.Encounter;

import java.util.ArrayList;
import java.util.List;

/**
 * Class description : <b>EncounterFilter</b>
 * 
 *
 *
 * @author Anne-Gaëlle Bergé - INRIA Rennes/Bretagne Atlantique
 *
 * @version 1.0 - 2011, November 17th
 * 
 */
public class EncounterFilter extends Filter<Encounter> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3254728188512120092L;
	private static List<HQLCriterion<Encounter, ?>> defaultCriterions;

	static {
		defaultCriterions = new ArrayList<HQLCriterion<Encounter, ?>>();
	}

	public EncounterFilter() {
		super(Encounter.class, defaultCriterions);
	}
}
