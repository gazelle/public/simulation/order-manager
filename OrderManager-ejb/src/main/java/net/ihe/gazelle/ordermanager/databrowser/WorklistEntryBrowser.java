package net.ihe.gazelle.ordermanager.databrowser;

import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import net.ihe.gazelle.ordermanager.model.WorklistEntry;
import net.ihe.gazelle.ordermanager.model.WorklistEntryQuery;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

import javax.faces.context.FacesContext;
import java.util.Map;

@Name("worklistEntryBrowser")
@Scope(ScopeType.PAGE)
public class WorklistEntryBrowser extends DataBrowser<WorklistEntry> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1506411688221688199L;

	@Override
	public void modifyQuery(HQLQueryBuilder<WorklistEntry> queryBuilder, Map<String, Object> map) {
		if ((fillerOrderNumber != null) && !fillerOrderNumber.isEmpty()) {
			queryBuilder
					.addLike("scheduledProcedureStep.requestedProcedure.order.fillerOrderNumber", fillerOrderNumber);
		}
		if ((placerOrderNumber != null) && !placerOrderNumber.isEmpty()) {
			queryBuilder
					.addLike("scheduledProcedureStep.requestedProcedure.order.placerOrderNumber", placerOrderNumber);
		}
		if ((patientId != null) && !patientId.isEmpty()) {
			queryBuilder.addLike(
					"scheduledProcedureStep.requestedProcedure.order.encounter.patient.patientIdentifiers.identifier",
					patientId);
		}
		if ((visitNumber != null) && !visitNumber.isEmpty()) {
			queryBuilder.addLike("scheduledProcedureStep.requestedProcedure.order.encounter.visitNumber", visitNumber);
		}
	}

    @Override
    public Filter<WorklistEntry> getFilter() {
            if (filter == null) {
                filter = new Filter<WorklistEntry>(getHQLCriterionsForFilter(), FacesContext.getCurrentInstance().getExternalContext()
                        .getRequestParameterMap());
            }
            return filter;
    }

    @Override
	protected HQLCriterionsForFilter<WorklistEntry> getHQLCriterionsForFilter() {
		WorklistEntryQuery query = new WorklistEntryQuery();
		HQLCriterionsForFilter<WorklistEntry> criterions = query.getHQLCriterionsForFilter();
		criterions.addPath("aeTitle", query.stationAETitle());
		criterions.addPath("modality", query.scheduledProcedureStep().modality());
		criterions.addPath("actor", query.simulatedActor(), actor);
		criterions.addPath("domain", query.domain(), domain);
		criterions.addPath("lastChanged", query.lastChanged());
		criterions.addPath("creator", query.creator());
		criterions.addPath("deleted", query.deleted(), null, WorklistDeletedFixer.INSTANCE);
		criterions.addQueryModifier(this);
		return criterions;
	}

	@Override
	public String getPermanentLink(Integer objectId) {
		return "/browsing/worklist.seam?id=" + objectId;
	}

    @Override
    public FilterDataModel<WorklistEntry> getDataModel() {
        return new FilterDataModel<WorklistEntry>(getFilter()) {

            @Override
            protected Object getId(WorklistEntry worklistEntry) {
                return worklistEntry.getId();
            }
        };
    }

    public void deleteWorklist(WorklistEntry inWorklist) {
		inWorklist.delete();
        reset();
	}

	public void restoreWorklist(WorklistEntry inWorklist) {
		inWorklist.restore();
        reset();
	}

}
