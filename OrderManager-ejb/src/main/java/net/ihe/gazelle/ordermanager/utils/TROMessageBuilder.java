package net.ihe.gazelle.ordermanager.utils;

import ca.uhn.hl7v2.AcknowledgmentCode;
import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.model.DataTypeException;
import ca.uhn.hl7v2.model.Message;
import ca.uhn.hl7v2.model.v251.message.ACK;
import ca.uhn.hl7v2.model.v251.segment.IPC;
import ca.uhn.hl7v2.model.v251.segment.MSH;
import ca.uhn.hl7v2.model.v251.segment.NTE;
import ca.uhn.hl7v2.parser.DefaultXMLParser;
import ca.uhn.hl7v2.parser.Parser;
import net.ihe.gazelle.HL7Common.model.HL7V2ResponderSUTConfiguration;
import net.ihe.gazelle.ordermanager.model.TeleRadiologyOrder;
import net.ihe.gazelle.ordermanager.model.TeleRadiologyProcedure;
import net.ihe.gazelle.simulator.common.action.ValueSetProvider;
import net.ihe.gazelle.tro.hl7v2.model.v251.message.OMI_O23;
import net.ihe.gazelle.tro.hl7v2.model.v251.segment.ZPS;

import java.util.Date;

/**
 * <p>TROMessageBuilder class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
public class TROMessageBuilder extends RadMessageBuilder<TeleRadiologyOrder> {

	/**
	 * <p>Constructor for TROMessageBuilder.</p>
	 *
	 * @param inTransactionKeyword a {@link java.lang.String} object.
	 * @param inSendingActorKeyword a {@link java.lang.String} object.
	 * @param inOrder a {@link net.ihe.gazelle.ordermanager.model.TeleRadiologyOrder} object.
	 * @param sut a {@link net.ihe.gazelle.HL7Common.model.HL7V2ResponderSUTConfiguration} object.
	 * @param sendingApplication a {@link java.lang.String} object.
	 * @param sendingFacility a {@link java.lang.String} object.
	 */
	public TROMessageBuilder(String inTransactionKeyword, String inSendingActorKeyword, TeleRadiologyOrder inOrder,
			HL7V2ResponderSUTConfiguration sut, String sendingApplication, String sendingFacility) {
		super(inTransactionKeyword, inSendingActorKeyword, inOrder, sut, sendingApplication, sendingFacility);
	}

	/**
	 * <p>Constructor for TROMessageBuilder.</p>
	 */
	public TROMessageBuilder() {
		super();
	}

	/** {@inheritDoc} */
	@Override
	public String generateMessage() throws HL7Exception {
		OMI_O23 omiMessage = new OMI_O23();
		populateMSHv251(omiMessage.getMSH(), "OMI^O23^OMI_O23");
		populatePIDv251(omiMessage.getPATIENT().getPID());
		populatePV1v251(omiMessage.getPATIENT().getPATIENT_VISIT().getPV1());
		populateORCv251(omiMessage.getORDER().getORC(), order.getOrderControlCode().equals("NW"), true, true);
		populateTQ1v251(omiMessage.getORDER().getTIMING().getTQ1(), order);
		populateOBRv251(omiMessage.getORDER().getOBR());
		populateNTESegment(omiMessage.getORDER().getNTE());
		if (order.getOrderControlCode().equals("NW")){
			populateZPSSegment(omiMessage.getZPS());
		}
		int procedureIndex = 0;
		for (TeleRadiologyProcedure procedure : order.getTeleRadiologyProcedures()) {
			populateIPCSegment(omiMessage.getORDER().getIPC(procedureIndex), procedure);
			procedureIndex++;
		}
		Parser parser = DefaultXMLParser.getInstanceWithNoValidation();
		return parser.encode(omiMessage);
	}

	/** {@inheritDoc} */
	@Override
	protected void populateMSHv251(MSH mshSegment, String messageType) throws HL7Exception {
		super.populateMSHv251(mshSegment, messageType);
		mshSegment.getReceivingApplication().parse(sut.getUrl());
		mshSegment.getCountryCode().setValue("SAU");
		mshSegment.getPrincipalLanguageOfMessage().getIdentifier().setValue("en");
	}

	private void populateIPCSegment(IPC ipc, TeleRadiologyProcedure procedure) throws HL7Exception {
		ipc.getAccessionIdentifier().parse(procedure.getAccessionIdentifier());
		ipc.getRequestedProcedureID().parse(procedure.getRequestedProcedureId());
		ipc.getStudyInstanceUID().parse(procedure.getStudyInstanceUid());
		ipc.getScheduledProcedureStepID().getEntityIdentifier().setValue("1");
		if (ipc.getModality() != null) {
			ipc.getModality().parse(
					ValueSetProvider.getInstance().buildCEElementFromCode(procedure.getModality(), "troModality"));
		}
		ipc.getProtocolCode(0).parse(procedure.getProtocolCode());
	}

	/** {@inheritDoc} */
	@Override
	protected void populateOBRv251(ca.uhn.hl7v2.model.v251.segment.OBR obrSegment) throws HL7Exception {
		super.populateOBRv251(obrSegment);
		obrSegment.getSetIDOBR().setValue("1");
		obrSegment.getPlacerField2().setValue(order.getImagingStudySource());
		if (order.getProcedureCode() != null) {
			obrSegment.getProcedureCode().parse(
					ValueSetProvider.getInstance().buildCEElementFromCode(order.getProcedureCode(), "troProcedure"));
		}
		if (order.getProcedureDescription() != null) {
			obrSegment.getProcedureCode().getAlternateText().setValue(order.getProcedureDescription());
		}
		if (order.getOrderStatus().equals("DC") && order.getAbortReason() != null){
			obrSegment.getFillerField1().setValue(order.getAbortReason());
		} else if (order.getOrderControlCode().equals("NW")){
			obrSegment.getFillerField1().setValue(order.getViews());
		}
	}

	private void populateNTESegment(NTE nte) throws HL7Exception {
		nte.getComment(0).setValue(order.getComment());
	}

	private void populateZPSSegment(ZPS zpsSegment) throws HL7Exception {
		if (order.getRelatedStudies() != null && !order.getRelatedStudies().isEmpty()) {
			int repIndex = 0;
			for (String accessionNumber : order.getRelatedStudies()) {
				zpsSegment.getRelatedSudiesAccessIdentifier(repIndex).parse(accessionNumber);
			}
		}
	}

	/**
	 * <p>buildAcknowledgement.</p>
	 *
	 * @param incomingMessageHeader a {@link MSH} object.
	 * @param ackCode a {@link String} object.
	 * @param userMessage a {@link String} object.
	 * @param errorType a {@link String} object.
	 * @param hl7ErrorCode a {@link String} object.
	 * @param severity a {@link String} object.
	 * @param messageControlId a {@link String} object.
	 * @param hl7Version a {@link String} object.
	 * @param isNACK a boolean.
	 * @param errorLocation a {@link String} object.
	 * @return a {@link ca.uhn.hl7v2.model.Message} object.
	 * @throws ca.uhn.hl7v2.HL7Exception if any.
	 */
	public Message buildAcknowledgement(MSH incomingMessageHeader, AcknowledgmentCode ackCode, String userMessage,
										String errorType, String hl7ErrorCode, String severity, String messageControlId, String hl7Version,
										boolean isNACK, String errorLocation) throws HL7Exception {
		ACK ack = new ACK();
		try {
			ack.getMSH().getFieldSeparator().setValue("|");
			ack.getMSH().getEncodingCharacters().setValue("^~\\&");
			ack.getMSH().getSendingApplication().parse(incomingMessageHeader.getReceivingApplication().encode());
			ack.getMSH().getSendingFacility().parse(incomingMessageHeader.getReceivingFacility().encode());
			ack.getMSH().getReceivingApplication().parse(incomingMessageHeader.getSendingApplication().encode());
			ack.getMSH().getReceivingFacility().getNamespaceID()
					.setValue(incomingMessageHeader.getSendingFacility().encode());
			ack.getMSH().getDateTimeOfMessage().getTime().setValue(sdf.format(new Date()));
			ack.getMSH().getMessageType().getMessageCode().setValue("ACK");
			ack.getMSH().getMessageType().getTriggerEvent()
					.setValue(incomingMessageHeader.getMessageType().getTriggerEvent().getValue());
			ack.getMSH().getMessageType().getMessageStructure().setValue("ACK");
			ack.getMSH().getProcessingID().getProcessingID().setValue("P");
			ack.getMSH().getMessageControlID().setValue(sdf.format(new Date()));
			ack.getMSH().getVersionID().getVersionID().setValue("2.5.1");
			ack.getMSH().getCountryCode().setValue("SAU");
			ack.getMSH().getPrincipalLanguageOfMessage()
					.parse(incomingMessageHeader.getPrincipalLanguageOfMessage().encode());
			ack.getMSH().getCharacterSet(0).parse(incomingMessageHeader.getCharacterSet(0).encode());
			fillMSASegment(ack.getMSA(), messageControlId, ackCode);
			if (isNACK) {
				this.populateERRv251(ack.getERR(), hl7ErrorCode, severity, userMessage, errorLocation);
			}
		} catch (DataTypeException e) {
			throw new HL7Exception(e);
		}
		return ack;
	}
	
	/** {@inheritDoc} */
	protected void populateERRv251(ca.uhn.hl7v2.model.v251.segment.ERR err, String hl7ErrorCode, String severity,
			String userMessage, String errorLocation) {
		try {
			err.getHL7ErrorCode().getIdentifier().setValue("207");
			err.getSeverity().setValue(severity);
			err.getApplicationErrorCode().getIdentifier().setValue("KSAContentValidation");
			StringBuilder diagnostic = new StringBuilder(hl7ErrorCode);
			diagnostic.append("_");
			diagnostic.append(errorLocation);
			diagnostic.append("_");
			diagnostic.append(userMessage);
			err.getDiagnosticInformation().setValue(diagnostic.toString());
		} catch (HL7Exception e) {
			log.error("Cannot populate ERR segment: " + e.getMessage());
		}

	}
	
}
