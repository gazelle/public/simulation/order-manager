package net.ihe.gazelle.ordermanager.model;

import org.jboss.seam.annotations.Name;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

/**
 * Class description : <b>AppointmentResource</b>
 * 
 * This class describes the appointment resource object which represents the resource required by a given appointment
 * 
 * This class defines the following attributes
 * <ul>
 * <li><b>id</b> unique identifier in the database</li>
 * <li><b>appointment</b> associated Appointment object</li>
 * <li><b>rgsId</b></li>
 * <li><b>status</b> action code (A -add-, U -update-, D -delete-)</li>
 * <li><b>aisId</b></li>
 * <li><b>universalServiceIdentifier</b></li>
 * <li><b>startDateTime</b> date/time of the appointment</li>
 * <ul>
 * 
 *
 *
 * @author Anne-Gaëlle Bergé - INRIA Rennes/Bretagne Atlantique
 *
 * @version 1.0 - 2011, November 17th
 * 
 */

@Entity
@Name("appointmentResource")
@Table(name = "om_appointment_resource", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = "id"))
@SequenceGenerator(name = "om_appointment_resource_sequence", sequenceName = "om_appointment_resource_id_seq", allocationSize = 1)
public class AppointmentResource implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@NotNull
	@GeneratedValue(generator = "om_appointment_resource_sequence", strategy = GenerationType.SEQUENCE)
	@Column(name = "id", nullable = false, unique = true)
	private Integer id;

	@ManyToOne
	@JoinColumn(name = "appointment_id")
	private Appointment appointment;

	/**
	 * RGS-1
	 */
	@Column(name = "rgs_id")
	private String rgsId;

	/**
	 * RGS-2
	 */
	@Column(name = "status")
	private String status;

	/**
	 * AIS-1
	 */
	@Column(name = "ais_id")
	private String aisId;

	/**
	 * AIS-3
	 */
	@Column(name = "universal_service_identifier")
	private String universalServiceIdentifier;

	/**
	 * AIS-4
	 */
	@Column(name = "start_date_time")
	@Temporal(TemporalType.TIMESTAMP)
	private Date startDateTime;

	public AppointmentResource() {

	}

	public Appointment getAppointment() {
		return appointment;
	}

	public void setAppointment(Appointment appointment) {
		this.appointment = appointment;
	}

	public String getRgsId() {
		return rgsId;
	}

	public void setRgsId(String rgsId) {
		this.rgsId = rgsId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getAisId() {
		return aisId;
	}

	public void setAisId(String aisId) {
		this.aisId = aisId;
	}

	public String getUniversalServiceIdentifier() {
		return universalServiceIdentifier;
	}

	public void setUniversalServiceIdentifier(String universalServiceIdentifier) {
		this.universalServiceIdentifier = universalServiceIdentifier;
	}

    public void setStartDateTime(Date startDateTime) {
        if (startDateTime != null) {
            this.startDateTime = (Date) startDateTime.clone();
        } else {
            this.startDateTime = null;
        }
    }

    /**
     * @return the startDateTime
     */
    public Date getStartDateTime() {
        if (startDateTime != null) {
            return (Date) startDateTime.clone();
        } else {
            return null;
        }
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = (prime * result) + ((aisId == null) ? 0 : aisId.hashCode());
		result = (prime * result) + ((rgsId == null) ? 0 : rgsId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		AppointmentResource other = (AppointmentResource) obj;
		if (aisId == null) {
			if (other.aisId != null) {
				return false;
			}
		} else if (!aisId.equals(other.aisId)) {
			return false;
		}
		if (rgsId == null) {
			if (other.rgsId != null) {
				return false;
			}
		} else if (!rgsId.equals(other.rgsId)) {
			return false;
		}
		return true;
	}

}
