package net.ihe.gazelle.ordermanager.hl7.initiators;

import net.ihe.gazelle.HL7Common.initiator.Initiator;
import net.ihe.gazelle.HL7Common.model.HL7V2ResponderSUTConfiguration;
import net.ihe.gazelle.HL7Common.model.HL7V2ResponderSUTConfigurationQuery;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.hql.restrictions.HQLRestrictions;
import net.ihe.gazelle.ordermanager.filter.LabOrderFilter;
import net.ihe.gazelle.ordermanager.model.Container;
import net.ihe.gazelle.ordermanager.model.LabOrder;
import net.ihe.gazelle.ordermanager.model.Specimen;
import net.ihe.gazelle.ordermanager.utils.LabMessageBuilder;
import net.ihe.gazelle.ordermanager.utils.MessageDecoder;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;
import net.ihe.gazelle.simulator.message.model.TransactionInstance;
import net.ihe.gazelle.ssov7.gum.client.application.UserAttributeCommon;
import net.ihe.gazelle.ssov7.gum.client.application.service.UserService;
import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.*;
import org.jboss.seam.core.ResourceBundle;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.model.SelectItem;
import javax.persistence.EntityManager;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Class description : <b>LabOrderSender</b>
 * 
 * This class is the backing bean for the page /hl7Intiators/labOrderSender.xhtml, the latter enables the user to create a new order, cancel, replace, update or discontinue it and sends the message to
 * his/her system under test
 * 
 *
 *
 * @author Anne-Gaëlle Bergé - INRIA Rennes/Bretagne Atlantique
 *
 * @version 1.0 - 2011, December 6th
 * 
 */

@Name("labOrderSender")
@Scope(ScopeType.PAGE)
@Synchronized(timeout = 30000)
public class LabOrderSender extends AbstractOrderSender<LabOrder> implements Serializable, UserAttributeCommon {

	private static final long serialVersionUID = -3833081015406182145L;

	private static Logger log = LoggerFactory.getLogger(LabOrderSender.class);

	public static final String BATTERY_CENTRIC = "OML^O21^OML_O21";
	public static final String SPECIMEN_CENTRIC = "OML^O33^OML_O33";
	public static final String CONTAINER_CENTRIC = "OML^O35^OML_O35";

	private Specimen selectedSpecimen;
	private Container selectedContainer;
	private String selectedMessageStructure = BATTERY_CENTRIC;
	private Integer numberOfContainers;
	private boolean editOrder;
	private boolean editSpecimen;
	private boolean expendPanel;
    private List<HL7V2ResponderSUTConfiguration> availableSuts;

	@In(value="gumUserService")
	private UserService userService;

    @Create
    public void initialize(){
        super.initializePage();
    }

	/**
	 * This class is also used to create a new specimen or container depending on the context
	 */
	@Override
	public void createANewOrder() {
		EntityManager entityManager = EntityManagerService.provideEntityManager();
		if (selectedMessageStructure.equals(BATTERY_CENTRIC)) {
			// one order - several specimens - several containers for each specimen
			selectedOrder = new LabOrder(selectedDomain, simulatedActor, getSelectedEncounter(), entityManager, true,
					workOrder);
			selectedOrder.setCreator(username);
			selectedOrder.setContainer(null);
			if (simulatedActor.getKeyword().equals(OP_KEYWORD) || workOrder) {
				selectedOrder.setOrderControlCode("NW");
			} else {
				selectedOrder.setOrderControlCode("SN");
			}
			selectedSpecimen = null;
			selectedContainer = null;
			editOrder = true;
			editSpecimen = false;
		} else if (selectedMessageStructure.equals(SPECIMEN_CENTRIC)) {
			// one specimen - several containers - several orders
			selectedSpecimen = new Specimen(true, selectedDomain, simulatedActor, entityManager, workOrder);
			selectedSpecimen.setCreator(username);
			numberOfContainers = 1;
			editOrder = false;
			editSpecimen = true;
			selectedOrder = null;
			selectedContainer = null;
		} else {
			// one specimen - several containers - several orders by container
			selectedSpecimen = new Specimen(true, selectedDomain, simulatedActor, entityManager, workOrder);
			selectedSpecimen.setCreator(username);
			editOrder = false;
			editSpecimen = true;
			selectedContainer = null;
			selectedOrder = null;
		}
	}

	@Override
	public void onChangeActionEvent() {
		if (orderControlCode.equals(NEW)) {
			setSelectionType(SelectionType.ENCOUNTER);
		} else if (orderControlCode.equals(CANCEL)) {
			encounterComesFromDispatch = false;
			setSelectionType(SelectionType.ORDER);
			if (simulatedActor.getKeyword().equals(OP_KEYWORD)) {
				orderFilter = new LabOrderFilter(selectedDomain, simulatedActor, "CA", null, false);
			}
			// retrieve the list of work orders which can be cancelled
			else if (workOrder) {
                orderFilter = new LabOrderFilter(selectedDomain, simulatedActor, "CA", null, true);
			} else {
                orderFilter = new LabOrderFilter(selectedDomain, simulatedActor, "OC", null, false);
			}
		} else if (orderControlCode.equals(UPDATE)) {
			setSelectionType(SelectionType.ORDER);
            orderFilter = new LabOrderFilter(selectedDomain, simulatedActor, "SC", null, false);
		} else {
			// those cases are not yet processed
			log.info("This cas is not yet handled by the simulator");
		}
		if (!encounterComesFromDispatch) {
			setSelectedEncounter(null);
			setSelectedPatient(null);
			selectedOrder = null;
			selectedSpecimen = null;
			selectedContainer = null;
		}

	}

	@Override
	public void sendMessage() {
		encounterComesFromDispatch = false;
		Transaction transaction;
		HL7V2ResponderSUTConfiguration sut = (HL7V2ResponderSUTConfiguration) Component.getInstance("selectedSUT");
		EntityManager entityManager = EntityManagerService.provideEntityManager();
		if (sut == null) {
			FacesMessages.instance().add(StatusMessage.Severity.ERROR, "No SUT selected ! please select the system to test and retry");
			return;
		}
		if (orderControlCode.equals(NEW)) {
			if (selectedMessageStructure.equals(BATTERY_CENTRIC)) {
				selectedOrder = selectedOrder.save(entityManager);
			} else {
				selectedSpecimen = selectedSpecimen.save(entityManager);
			}
			if (simulatedActor.getKeyword().equals(OP_KEYWORD)) {
				transaction = Transaction.GetTransactionByKeyword("LAB-1");
			} else if (workOrder) {
				transaction = Transaction.GetTransactionByKeyword("LAB-4");
			} else {
				transaction = Transaction.GetTransactionByKeyword("LAB-2");
			}
		} else if (orderControlCode.equals(CANCEL)) {
			if (workOrder) {
				selectedOrder.setOrderControlCode("CA");
				selectedOrder.setOrderStatus("CA");
				transaction = Transaction.GetTransactionByKeyword("LAB-4");
			} else if (simulatedActor.getKeyword().equals(OP_KEYWORD)) {
				selectedOrder.setOrderControlCode("CA");
				transaction = Transaction.GetTransactionByKeyword("LAB-1");
			} else {
				selectedOrder.setOrderControlCode("OC");
				selectedOrder.setOrderStatus("CA");
				transaction = Transaction.GetTransactionByKeyword("LAB-1");
			}
			selectedOrder.setOrderResultStatus("X");
			selectedOrder = selectedOrder.save(entityManager);
			setSelectedEncounter(selectedOrder.getEncounter());
		} else // UPDATE case (not available for LAB-4)
		{
			selectedOrder.setOrderControlCode("SC");
			selectedOrder.setOrderStatus(newOrderStatus);
			selectedOrder.setOrderResultStatus(newResultStatus);
			transaction = Transaction.GetTransactionByKeyword("LAB-1");
			selectedOrder = selectedOrder.save(entityManager);
			setSelectedEncounter(selectedOrder.getEncounter());
			newOrderStatus = null;
			newResultStatus = null;
		}
		// build message
		LabMessageBuilder messageGenerator = new LabMessageBuilder(transaction.getKeyword(),
				simulatedActor.getKeyword(), getSelectedEncounter(), selectedOrder, selectedSpecimen, sut,
				sendingApplication, sendingFacility, selectedMessageStructure, null);
		try {
			String messageToSend = messageGenerator.generateMessage();
			if (messageToSend != null) {
                // TODO sut actor is currently set to null !!!
				Initiator hl7Initiator = new Initiator(sut, sendingFacility, sendingApplication, simulatedActor,
						transaction, messageToSend, selectedMessageStructure, selectedDomain.getKeyword(), Actor.findActorWithKeyword(sutActorKeyword));
				TransactionInstance message = hl7Initiator.sendMessageAndGetTheHL7Message();
				if (message == null) {
					hl7Messages = null;
				} else {
					hl7Messages = new ArrayList<TransactionInstance>();
                    hl7Messages.add(message);

					// examine response: check OP/OF number and order control code
					MessageDecoder.decodeAcknowledgement(message.getReceivedMessageContent(), simulatedActor);
				}
			} else {
				FacesMessages.instance().add(StatusMessage.Severity.ERROR, "The message has not been built !");
			}
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Unable to build the message for sending: " + e.getMessage());
		}
		selectedOrder = null;
		editOrder = false;
		editSpecimen = false;
		selectedSpecimen = null;
	}

	public List<SelectItem> getAvailableMessageStructures() {
		List<SelectItem> items = new ArrayList<SelectItem>();
		items.add(new SelectItem(BATTERY_CENTRIC, ResourceBundle.instance().getString(
				"gazelle.ordermanager.batteryCentric") + " (" + BATTERY_CENTRIC + ")"));
		items.add(new SelectItem(SPECIMEN_CENTRIC, ResourceBundle.instance().getString(
				"gazelle.ordermanager.specimenCentric") + " (" + SPECIMEN_CENTRIC + ")"));
		items.add(new SelectItem(CONTAINER_CENTRIC, ResourceBundle.instance().getString(
				"gazelle.ordermanager.containerCentric") + " (" + CONTAINER_CENTRIC + ")"));
		return items;
	}

	public void createSpecimenForOrder() {
		editOrder = false;
		selectedSpecimen = new Specimen(false, selectedDomain, simulatedActor,
				EntityManagerService.provideEntityManager(), workOrder);
		selectedSpecimen.setCreator(username);
		selectedSpecimen.setOrders(new ArrayList<LabOrder>());
		selectedSpecimen.getOrders().add(selectedOrder);
		numberOfContainers = 1;
		expendPanel = false;
		editSpecimen = true;
	}

	public void fillSpecimenRandomly() {
		selectedSpecimen.fillRandomly();
	}

	public void addSpecimenToOrder() {
		if (selectedOrder.getSpecimens() == null) {
			selectedOrder.setSpecimens(new ArrayList<Specimen>());
		}
		createContainersForSpecimen();
		selectedOrder.getSpecimens().add(selectedSpecimen);
		createSpecimenForOrder();
		expendPanel = true;
		selectedSpecimen = null;
		editSpecimen = false;
	}

	private void createContainersForSpecimen() {
		if ((numberOfContainers != null) && (numberOfContainers > 0)) {
			selectedSpecimen.setContainers(new ArrayList<Container>());
			for (int index = 1; index <= numberOfContainers; index++) {
				Container container = new Container(false, selectedDomain, simulatedActor);
				container.setSpecimen(selectedSpecimen);
				container.setWorkOrder(workOrder);
				selectedSpecimen.getContainers().add(container);
			}
		}
	}

	public void addSpecimenToOrderAndSendMessage() {
		addSpecimenToOrder();
		sendMessage();
	}

	public void removeSpecimenFromOrder(Specimen inSpecimen) {
		selectedOrder.getSpecimens().remove(inSpecimen);
	}

	public void removeOrderFromSpecimen(LabOrder inOrder) {
		selectedSpecimen.getOrders().remove(inOrder);
	}

	public void createOrderForSpecimen() {
		editSpecimen = false;
		createContainersForSpecimen();
		selectedOrder = new LabOrder(selectedDomain, simulatedActor, getSelectedEncounter(),
				EntityManagerService.provideEntityManager(), false, workOrder);
		selectedOrder.setCreator(username);
		selectedOrder.setSpecimens(new ArrayList<Specimen>());
		selectedOrder.getSpecimens().add(selectedSpecimen);
		if (simulatedActor.getKeyword().equals(OP_KEYWORD) || workOrder) {
			selectedOrder.setOrderControlCode("NW");
		} else {
			selectedOrder.setOrderControlCode("SN");
		}
		expendPanel = false;
		editOrder = true;
	}

	public void addOrderToSpecimen() {
		if (selectedSpecimen.getOrders() == null) {
			selectedSpecimen.setOrders(new ArrayList<LabOrder>());
		}
		selectedSpecimen.getOrders().add(selectedOrder);
		createOrderForSpecimen();
		expendPanel = true;
		editOrder = false;
		selectedOrder = null;
	}

	public void addOrderToSpecimenAndSendMessage() {
		addOrderToSpecimen();
		sendMessage();
	}

	public void createContainerForSpecimen() {
		if (selectedSpecimen.getContainers() == null) {
			selectedSpecimen.setContainers(new ArrayList<Container>());
		}
		Container container = new Container(true, selectedDomain, simulatedActor);
		container.setWorkOrder(workOrder);
		container.setSpecimen(selectedSpecimen);
		selectedSpecimen.getContainers().add(container);
		expendPanel = true;
		editSpecimen = false;
	}

	public void removeContainerFromSpecimen(Container inContainer) {
		selectedSpecimen.getContainers().remove(inContainer);
	}

	public void createOrderForContainer(Container inContainer) {
		selectedContainer = inContainer;
		selectedOrder = new LabOrder(selectedDomain, simulatedActor, getSelectedEncounter(),
				EntityManagerService.provideEntityManager(), false, workOrder);
		if (simulatedActor.getKeyword().equals(OP_KEYWORD) || workOrder) {
			selectedOrder.setOrderControlCode("NW");
		} else {
			selectedOrder.setOrderControlCode("SN");
		}
		selectedOrder.setCreator(username);
		expendPanel = false;
	}

	public void removeOrderFromContainer(Container currentContainer, LabOrder currentOrder) {
		int currentContainerIndex = selectedSpecimen.getContainers().indexOf(currentContainer);
		selectedContainer = selectedSpecimen.getContainers().get(currentContainerIndex);
		selectedContainer.getOrders().remove(currentOrder);
		selectedSpecimen.getContainers().set(currentContainerIndex, selectedContainer);
	}

	public void addOrderToContainer() {
		selectedOrder.setContainer(selectedContainer);
		int currentContainerIndex = selectedSpecimen.getContainers().indexOf(selectedContainer);
		selectedContainer = selectedSpecimen.getContainers().get(currentContainerIndex);
		if (selectedContainer.getOrders() == null) {
			selectedContainer.setOrders(new ArrayList<LabOrder>());
		}
		selectedContainer.getOrders().add(selectedOrder);
		selectedSpecimen.getContainers().set(currentContainerIndex, selectedContainer);
		selectedOrder = null;
		expendPanel = true;
	}

	public void addOrderToContainerAndSendMessage() {
		addOrderToContainer();
		sendMessage();
	}

	@Override
	public void fillOrderRandomly() {
		selectedOrder.fillOrderRandomly(selectedDomain, simulatedActor);
	}

	@Override
	public List<SelectItem> getAvailableOrderStatuses() {
		List<SelectItem> items = new ArrayList<SelectItem>();
		items.add(new SelectItem(null, ResourceBundle.instance().getString("gazelle.ordermanager.PleaseSelect")));
		items.add(new SelectItem("A", ResourceBundle.instance().getString("gazelle.ordermanager.SomeResultsAvailable")));
		items.add(new SelectItem("CA", ResourceBundle.instance().getString("gazelle.ordermanager.OrderCancelled")));
		items.add(new SelectItem("CM", ResourceBundle.instance().getString("gazelle.ordermanager.OrderCompleted")));
		items.add(new SelectItem("IP", ResourceBundle.instance().getString("gazelle.ordermanager.OrderInProgressUnsp")));
		items.add(new SelectItem("SC", ResourceBundle.instance().getString("gazelle.ordermanager.OrderInProgressSch")));
		return items;
	}

	@Override
	public List<SelectItem> listAvailableActions() {
		List<SelectItem> items = new ArrayList<SelectItem>();
		items.add(new SelectItem(NEW, ResourceBundle.instance().getString(NEW)));
		items.add(new SelectItem(CANCEL, ResourceBundle.instance().getString(CANCEL)));
		if (simulatedActor.getKeyword().equals(OF_KEYWORD) && !workOrder) {
			items.add(new SelectItem(UPDATE, ResourceBundle.instance().getString(UPDATE)));
		}
		return items;
	}

	@Override
	public String getUserName(String userId) {
		return userService.getUserDisplayNameWithoutException(userId);
	}

	/**
	 * GETTERS and SETTERS
	 */

	public Specimen getSelectedSpecimen() {
		return selectedSpecimen;
	}

	public void setSelectedSpecimen(Specimen selectedSpecimen) {
		this.selectedSpecimen = selectedSpecimen;
	}

	public Container getSelectedContainer() {
		return selectedContainer;
	}

	public void setSelectedContainer(Container selectedContainer) {
		this.selectedContainer = selectedContainer;
	}

	public String getSelectedMessageStructure() {
		return selectedMessageStructure;
	}

	public void setSelectedMessageStructure(String selectedMessageStructure) {
		this.selectedMessageStructure = selectedMessageStructure;
	}

	public Integer getNumberOfContainers() {
		return numberOfContainers;
	}

	public void setNumberOfContainers(Integer numberOfContainers) {
		this.numberOfContainers = numberOfContainers;
	}

	public String getBatteryCentric() {
		return BATTERY_CENTRIC;
	}

	public String getSpecimenCentric() {
		return SPECIMEN_CENTRIC;
	}

	public String getContainerCentric() {
		return CONTAINER_CENTRIC;
	}

	public boolean isEditOrder() {
		return editOrder;
	}

	public void setEditOrder(boolean editOrder) {
		this.editOrder = editOrder;
	}

	public boolean isEditSpecimen() {
		return editSpecimen;
	}

	public void setEditSpecimen(boolean editSpecimen) {
		this.editSpecimen = editSpecimen;
	}

	public void setExpendPanel(boolean expendPanel) {
		this.expendPanel = expendPanel;
	}

	public boolean isExpendPanel() {
		return expendPanel;
	}

    public List<HL7V2ResponderSUTConfiguration> getAvailableSuts() {
        if (availableSuts == null){
            HL7V2ResponderSUTConfigurationQuery query = new HL7V2ResponderSUTConfigurationQuery();
            if (workOrder){
                query.listUsages().transaction().keyword().eq("LAB-4");
            } else if (sutActorKeyword.equals("OF")){
                query.addRestriction(HQLRestrictions.or(query.listUsages().keyword().eqRestriction("LAB_OF_LAB1"),
                        query.listUsages().keyword().eqRestriction("LAB_OF_LAB2")));
            } else if (sutActorKeyword.equals("OP")){
                query.addRestriction(HQLRestrictions.or(query.listUsages().keyword().eqRestriction("LAB_OP_LAB1"),
                        query.listUsages().keyword().eqRestriction("LAB_OP_LAB2")));
            }
            query.name().order(true);
            availableSuts = query.getList();
        }
        return availableSuts;
    }
}
