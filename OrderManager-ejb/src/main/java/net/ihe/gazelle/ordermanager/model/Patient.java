/*
 * Copyright 2011 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.ihe.gazelle.ordermanager.model;

import net.ihe.gazelle.patient.AbstractPatient;
import org.jboss.seam.annotations.Name;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Random;

/**
 * <b>Class Description : </b>Patient<br>
 * <br>
 *
 * The patient class is used to store the patient for which an order is created. This patient extends the AbstractPatient defined in PatientGeneration module
 *
 * Patient possesses the following attributes :
 * <ul>
 * <li><b>id</b> : id in the database</li>
 * <li><b>weight</b> : patient's weight</li>
 * <li><b>encounters</b>: lists of encounters involving this patient</li>
 * <li><b>patientIdentifiers</b> the identifiers of the patient in the different affinity domains</li>
 * </ul>
 *
 * @author Anne-Gaelle Berge / INRIA Rennes IHE development Project
 * @version 1.0 - 2011, March 21st
 */
@Entity
@Name("patient")
@DiscriminatorValue("om_patient")
public class Patient extends AbstractPatient implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3331490707205944204L;

	@Column(name = "creator")
	private String creator;

	@Column(name = "contrast_allergies")
	private String contrastAllergies;

	@OneToMany(mappedBy = "patient")
	private List<Encounter> encounters;

	@OneToMany(mappedBy = "patient")
	private List<PatientIdentifier> patientIdentifiers;

	public Patient(AbstractPatient abPatient) {
		super();
		this.firstName = abPatient.getFirstName();
		this.lastName = abPatient.getLastName();
		this.countryCode = abPatient.getCountryCode();
		this.genderCode = abPatient.getGenderCode();
		this.religionCode = abPatient.getReligionCode();
		this.raceCode = abPatient.getRaceCode();
		this.dateOfBirth = abPatient.getDateOfBirth();
		this.creationDate = abPatient.getCreationDate();
		this.nationalPatientIdentifier = abPatient.getNationalPatientIdentifier();
		this.characterSet = abPatient.getCharacterSet();
		this.ddsIdentifier = abPatient.getDdsIdentifier();
		this.alternateFirstName = abPatient.getAlternateFirstName();
		this.alternateLastName = abPatient.getAlternateLastName();
		this.secondName = abPatient.getSecondName();
		this.alternateSecondName = abPatient.getAlternateSecondName();
		this.thirdName = abPatient.getThirdName();
		this.alternateThirdName = abPatient.getAlternateThirdName();
		copyAddressList(abPatient);
		this.motherMaidenName = abPatient.getMotherMaidenName();
	}

	public Patient(){

	}

    /**
	 * <p>save.</p>
	 *
	 * @param entityManager a {@link javax.persistence.EntityManager} object.
	 * @return a {@link net.ihe.gazelle.ordermanager.model.Patient} object.
	 */
	public Patient save(EntityManager entityManager) {
		Patient patient = entityManager.merge(this);
		entityManager.flush();
		return patient;
	}


	/** {@inheritDoc} */
	@Override
	public void setCreationDate(Date creationDate) {
		if (creationDate != null) {
			this.creationDate = (Date) creationDate.clone();
		} else {
            this.creationDate = null;
        }
	}

	/** {@inheritDoc} */
	@Override
	public Date getCreationDate() {
        if (creationDate != null) {
            return (Date) creationDate.clone();
        } else {
            return null;
        }
	}

	/**
	 * <p>Setter for the field <code>encounters</code>.</p>
	 *
	 * @param encounters
	 *            the encounters to set
	 */
	public void setEncounters(List<Encounter> encounters) {
		this.encounters = encounters;
	}

	/**
	 * <p>Getter for the field <code>encounters</code>.</p>
	 *
	 * @return the encounters
	 */
	public List<Encounter> getEncounters() {
		return encounters;
	}

	/**
	 * <p>Setter for the field <code>patientIdentifiers</code>.</p>
	 *
	 * @param patientIdentifiers
	 *            the patientIdentifiers to set
	 */
	public void setPatientIdentifiers(List<PatientIdentifier> patientIdentifiers) {
		this.patientIdentifiers = patientIdentifiers;
	}

	/**
	 * <p>Getter for the field <code>patientIdentifiers</code>.</p>
	 *
	 * @return the patientIdentifiers
	 */
	public List<PatientIdentifier> getPatientIdentifiers() {
		return this.patientIdentifiers;
	}

	/**
	 * <p>Setter for the field <code>creator</code>.</p>
	 *
	 * @param creator
	 *            the creator to set
	 */
	public void setCreator(String creator) {
		this.creator = creator;
	}

	/**
	 * <p>Getter for the field <code>creator</code>.</p>
	 *
	 * @return the creator
	 */
	public String getCreator() {
		return creator;
	}

	/**
	 * <p>Setter for the field <code>contrastAllergies</code>.</p>
	 *
	 * @param contrastAllergies a {@link java.lang.String} object.
	 */
	public void setContrastAllergies(String contrastAllergies) {
		this.contrastAllergies = contrastAllergies;
	}

	/**
	 * <p>Getter for the field <code>contrastAllergies</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getContrastAllergies() {
		return contrastAllergies;
	}

    /** {@inheritDoc} */
    @Override
    public boolean equals(Object o) {
        return super.equals(o);
    }

    /** {@inheritDoc} */
    @Override
    public int hashCode() {
        return super.hashCode();
    }
}
