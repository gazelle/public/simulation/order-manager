package net.ihe.gazelle.ordermanager.databrowser;

public enum ObjectType {
	PATIENT("patient", "/browsing/allPatients.seam", null),
	ORDER("order", "/browsing/allOrders.seam", "OF/RAD, OF/EYE, OP/RAD, OP/EYE"),
	WORKLIST("worklist", "/browsing/allWorklists.seam", "OF/RAD"),
	APPOINTMENT("appointment", "/browsing/allAppointments.seam", "OF/RAD, OF/EYE, OP/RAD, OP/EYE"),
	DICOM("dicomMessage", "/browsing/allDicomMessages.seam?objectType=dicomMessage", null),
	LOG("scpLog", "/browsing/allSCPLogs.seam", null),
	LABORDER("labOrder", "/browsing/allLabOrders.seam", "OF/LAB, OP/LAB, ORT/LAB"),
	SPECIMEN("specimen", "/browsing/allSpecimens.seam", "OF/LAB, OP/LAB, ORT/LAB"),
	WORKORDER("workOrder", "/browsing/allLabOrders.seam", "AM/LAB, OF/LAB"),
	WORKSPECIMEN("workspecimen", "/browsing/allSpecimens.seam", "AM/LAB, OF/LAB"),
	AWOS("awos", "/browsing/allAWOS.seam", "ANALYZER/LAB, ANALYZER_MGR/LAB"),
	PROCEDURE("procedure", "/browsing/allProcedures.seam", "OF/RAD, OF/EYE, IM/RAD, IM/EYE"),
	TRO("teleradorder", "/browsing/allTeleRadiologyOrders.seam", "TRO/TRO_CREATOR, TRO/TRO_FULFILLER");

	ObjectType(String keyword, String url, String domainActor) {
		this.keyword = keyword;
		this.url = url;
		this.domainActor = domainActor;
	}

	String keyword;
	String url;
	String domainActor;

	public String getKeyword() {
		return this.keyword;
	}

	public static ObjectType getObjectTypeByKeyword(String string) {
		for (ObjectType ot : ObjectType.values()) {
			if (ot.getKeyword().equalsIgnoreCase(string)) {
				return ot;
			}
		}
		return null;
	}

	public String getUrl() {
		return url;
	}

	public String getDomainActor() {
		return domainActor;
	}
}
