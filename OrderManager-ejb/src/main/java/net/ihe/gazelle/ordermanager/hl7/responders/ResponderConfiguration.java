package net.ihe.gazelle.ordermanager.hl7.responders;

import net.ihe.gazelle.HL7Common.action.SimulatorResponderConfigurationDisplay;
import net.ihe.gazelle.HL7Common.model.SimulatorResponderConfiguration;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.common.tf.model.Domain;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;

import javax.faces.context.FacesContext;
import java.util.Map;

/**
 * Class description : <b>ResponderConfiguration</b>
 * 
 * This class is backing bean for page /hl7Responders/configurationAndMessages.xhtml. For each actor, in a particular domain, acting as receiver for a given transaction, this page is used to display
 * the simulator configuration and the messages received in the defined context
 * 
 *
 *
 * @author Anne-Gaëlle Bergé - INRIA Rennes/Bretagne Atlantique
 *
 * @version 1.0 - 2011, November 17th
 * 
 */

@Name("hl7ResponderConfiguration")
@Scope(ScopeType.PAGE)
public class ResponderConfiguration extends SimulatorResponderConfigurationDisplay {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String actorKeyword;
	private String domainKeyword;
	private Actor actor;
	private Domain selectedDomain;

	@Create
	public void initializeBean() {
		Map<String, String> urlParams = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		if ((urlParams != null) && !urlParams.isEmpty()) {
			if (urlParams.get("actor") != null) {
				actorKeyword = urlParams.get("actor");
				actor = Actor.findActorWithKeyword(actorKeyword);
			} else {
				FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Cannot create the page because actor parameter is missing in the URL");
			}
			if (urlParams.get("domain") != null) {
				domainKeyword = urlParams.get("domain");
				if (domainKeyword != null) {
					selectedDomain = Domain.getDomainByKeyword(domainKeyword);
				}
			} else {
				domainKeyword = null;
			}
			if (actorKeyword == null || (domainKeyword == null)) {
				FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Cannot create the page because domain parameter is missing in the URL");
			} else {
				getSimulatorResponderConfiguration();
			}
		}
	}


    public Actor getActor() {
		return actor;
	}

	public Domain getSelectedDomain() {
		return selectedDomain;
	}


    /**
     * returns the URL to the list of messages exchanged with this part of the tool
     *
     * @param conf
     * @return the url
     */
    @Override
    public String getUrlForHL7Messages(SimulatorResponderConfiguration conf) {
        return "/messages/browser.seam?simulatedActor=" + conf.getSimulatedActor().getId()
                + "&domain=" + conf.getDomain().getId();
    }

    @Override
    public void getSimulatorResponderConfiguration() {
        getSimulatorResponderConfigurations(actor, null, selectedDomain);
    }

    public String linkToPatients(){
        return "/browsing/allPatients.seam?actorKey=" + actorKeyword + "&domainKey=" + domainKeyword + "&objectType=patient";
    }

}
