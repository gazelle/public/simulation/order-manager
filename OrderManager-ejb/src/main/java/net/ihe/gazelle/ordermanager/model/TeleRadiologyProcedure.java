package net.ihe.gazelle.ordermanager.model;

import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.common.tf.model.Domain;
import org.jboss.seam.annotations.Name;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Entity
@Name("teleRadiologyProcedure")
@Table(name="om_tele_radiology_procedure", schema = "public", uniqueConstraints=@UniqueConstraint(columnNames="id"))
@SequenceGenerator(name="om_tele_radiology_procedure_sequence", sequenceName="om_tele_radiology_procedure_id_seq", allocationSize = 1)
public class TeleRadiologyProcedure extends CommonColumns implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@NotNull
	@GeneratedValue(generator="om_tele_radiology_procedure_sequence", strategy=GenerationType.SEQUENCE)
	@Column(name="id", nullable = false, unique = true)
	private Integer id;
	
	@Column(name="accession_identifier")
	private String accessionIdentifier;
	
	@Column(name="requested_procedure_id")
	private String requestedProcedureId;
	
	@Column(name="study_instance_uid")
	private String studyInstanceUid;
	
	@Column(name="modality")
	private String modality;
	
	@Column(name="protocol_code")
	private String protocolCode;
	
	@ManyToOne
	@JoinColumn(name="tele_radiology_order_id")
	private TeleRadiologyOrder teleRadiologyOrder;
	
	public TeleRadiologyProcedure(){
		super();
	}
	
	public TeleRadiologyProcedure(Actor simulatedActor, String username, Domain domain){
		super(simulatedActor, username, domain);
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getAccessionIdentifier() {
		return accessionIdentifier;
	}

	public void setAccessionIdentifier(String accessionIdentifier) {
		this.accessionIdentifier = accessionIdentifier;
	}

	public String getRequestedProcedureId() {
		return requestedProcedureId;
	}

	public void setRequestedProcedureId(String requestedProcedureId) {
		this.requestedProcedureId = requestedProcedureId;
	}

	public String getStudyInstanceUid() {
		return studyInstanceUid;
	}

	public void setStudyInstanceUid(String studyInstanceUid) {
		this.studyInstanceUid = studyInstanceUid;
	}

	public String getModality() {
		return modality;
	}

	public void setModality(String modality) {
		this.modality = modality;
	}

	public String getProtocolCode() {
		return protocolCode;
	}

	public void setProtocolCode(String protocolCode) {
		this.protocolCode = protocolCode;
	}

	public TeleRadiologyOrder getTeleRadiologyOrder() {
		return teleRadiologyOrder;
	}

	public void setTeleRadiologyOrder(TeleRadiologyOrder teleRadiologyOrder) {
		this.teleRadiologyOrder = teleRadiologyOrder;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((accessionIdentifier == null) ? 0 : accessionIdentifier.hashCode());
		result = prime * result + ((requestedProcedureId == null) ? 0 : requestedProcedureId.hashCode());
		result = prime * result + ((studyInstanceUid == null) ? 0 : studyInstanceUid.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		TeleRadiologyProcedure other = (TeleRadiologyProcedure) obj;
		if (accessionIdentifier == null) {
			if (other.accessionIdentifier != null)
				return false;
		} else if (!accessionIdentifier.equals(other.accessionIdentifier))
			return false;
		if (requestedProcedureId == null) {
			if (other.requestedProcedureId != null)
				return false;
		} else if (!requestedProcedureId.equals(other.requestedProcedureId))
			return false;
		if (studyInstanceUid == null) {
			if (other.studyInstanceUid != null)
				return false;
		} else if (!studyInstanceUid.equals(other.studyInstanceUid))
			return false;
		return true;
	}

	public TeleRadiologyProcedure save(EntityManager entityManager) {
		TeleRadiologyProcedure procedure = entityManager.merge(this);
		entityManager.flush();
		return procedure;
	}
}
