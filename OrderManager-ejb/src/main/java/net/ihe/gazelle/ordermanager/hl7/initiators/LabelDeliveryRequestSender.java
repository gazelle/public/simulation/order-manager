package net.ihe.gazelle.ordermanager.hl7.initiators;

import ca.uhn.hl7v2.HL7Exception;
import net.ihe.gazelle.HL7Common.initiator.Initiator;
import net.ihe.gazelle.HL7Common.model.HL7Protocol;
import net.ihe.gazelle.HL7Common.model.HL7V2ResponderSUTConfiguration;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.ordermanager.admin.UserPreferencesManager;
import net.ihe.gazelle.ordermanager.model.Container;
import net.ihe.gazelle.ordermanager.model.Encounter;
import net.ihe.gazelle.ordermanager.model.LabOrder;
import net.ihe.gazelle.ordermanager.model.Patient;
import net.ihe.gazelle.ordermanager.model.Specimen;
import net.ihe.gazelle.ordermanager.utils.LabMessageBuilder;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;
import net.ihe.gazelle.simulator.message.model.TransactionInstance;
import net.ihe.gazelle.ssov7.gum.client.application.UserAttributeCommon;
import net.ihe.gazelle.ssov7.gum.client.application.service.UserService;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.model.SelectItem;
import javax.persistence.EntityManager;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * <b>Class Description : </b>LabelDeliveryRequestSender<br>
 * <br>
 *
 * @author Anne-Gaelle Berge / IHE-Europe development Project
 * @version 1.0 - 17/08/16
 *
 *
 *
 */

@Name("labelDeliveryRequestSender")
@Scope(ScopeType.PAGE)
public class LabelDeliveryRequestSender extends AbstractSender implements Serializable, UserAttributeCommon {

    private static Logger log = LoggerFactory.getLogger(LabelDeliveryRequestSender.class);

    private Specimen selectedSpecimen;
    private LabOrder currentOrder;
    private Container currentContainer;
    private Integer labelCount = 1;
    private boolean editSpecimen = true;
    private final static String MESSAGE_TYPE = "OML^O33^OML_O33";

    @In(value="gumUserService")
    private UserService userService;

    @Create
    public void init() {
        super.initializePage();
        sutActorKeyword = "LB";
    }

    /**
     * this method is used to send the message to the SUT
     */
    @Override
    public void sendMessage() {
        HL7V2ResponderSUTConfiguration sut = UserPreferencesManager.instance().getSelectedSUT();
        if (sut == null){
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "Please first select a SUT and retry");
        } else {
            selectedSpecimen = selectedSpecimen.save(EntityManagerService.provideEntityManager());
            Transaction transaction = Transaction.GetTransactionByKeyword("LAB-61");
            LabMessageBuilder builder = new LabMessageBuilder("LAB-61", "LIP", getSelectedEncounter(), null, selectedSpecimen,
                    sut, getSendingApplication(), getSendingFacility(), MESSAGE_TYPE, null);
            String oml = null;
            try {
                oml = builder.generateMessage();
            } catch (Exception e) {
                log.error(e.getMessage(), e);
                FacesMessages.instance().add(StatusMessage.Severity.ERROR, "The simulator was not able to build the message: " + e.getMessage());
                return;
            }
            if (oml != null){
                Initiator initiator = new Initiator(sut, getSendingFacility(), getSendingApplication(), simulatedActor,
                        transaction,  oml, MESSAGE_TYPE, "LAB", Actor.findActorWithKeyword(sutActorKeyword));
                try {
                    TransactionInstance instance = initiator.sendMessageAndGetTheHL7Message();
                    if (instance == null) {
                        hl7Messages = null;
                    } else {
                        hl7Messages = new ArrayList<TransactionInstance>();
                        hl7Messages.add(instance);
                    }
                } catch (HL7Exception e) {
                    FacesMessages.instance().add(StatusMessage.Severity.ERROR, e.getMessage());
                }

            }
        }
    }

    /**
     * This method is called when the user hit the button "randomly fill order"
     */
    @Override
    public void fillOrderRandomly() {
        currentOrder.fillOrderRandomly(selectedDomain, simulatedActor);
    }

    /**
     * methods called when the selected message structure changes, or when the selected action changes, also called after reseting filters...
     * This methods must update the content of the display
     * datamodel
     */
    @Override
    public void onChangeActionEvent() {
        setSelectedEncounter(null);
        setSelectedPatient(null);
        displayPatientsList();
        selectedSpecimen = null;
        labelCount = 1;
        hl7Messages = null;
    }

    /**
     * @return list actions
     */
    @Override
    public List<SelectItem> listAvailableActions() {
        return null;
    }

    @Override
    public void skipPatientSelection() {
        // not allowed for this transaction
    }

    @Override
    public void createNewPatientAndEncounter() {
        super.createNewPatientAndEncounter();
        createANewSpecimen();
    }

    @Override
    public void selectEncounter(Encounter encounter) {
        super.selectEncounter(encounter);
        createANewSpecimen();
    }

    @Override
    public void createANewEncounterForPatient(Patient pat) {
        super.createANewEncounterForPatient(pat);
        createANewSpecimen();
    }

    @Override
    public String getUserName(String userId) {
        return userService.getUserDisplayNameWithoutException(userId);
    }

    private void createANewSpecimen() {
        selectedSpecimen = new Specimen(true, selectedDomain, simulatedActor, EntityManagerService.provideEntityManager(), false);
    }

    public void createNewOrderForSpecimen() {
        EntityManager entityManager = EntityManagerService.provideEntityManager();
        editSpecimen = false;
        currentOrder = new LabOrder(selectedDomain, simulatedActor, getSelectedEncounter(), entityManager, false, false);
        currentOrder.setOrderControlCode("NW");
        currentOrder.setSpecimens(new ArrayList<Specimen>());
        currentOrder.getSpecimens().add(selectedSpecimen);
    }

    public void fillSpecimenRandomly() {
        selectedSpecimen.fillRandomly();
    }

    public void addOrderToSpecimen() {
        if (selectedSpecimen.getOrders() == null) {
            selectedSpecimen.setOrders(new ArrayList<LabOrder>());
        }
        selectedSpecimen.getOrders().add(currentOrder);
        currentOrder = null;
        FacesMessages.instance().add(StatusMessage.Severity.INFO, "Order successfully added to the specimen");
    }

    public void removeOrderFromSpecimen(LabOrder order) {
        selectedSpecimen.getOrders().remove(order);
    }

    public void addALabel() {
        currentContainer = new Container(false, selectedDomain, simulatedActor);
        currentContainer.setSpecimen(selectedSpecimen);
    }

    public void confirmAddLabel(){
        if (selectedSpecimen.getContainers() == null) {
            selectedSpecimen.setContainers(new ArrayList<Container>());
        }
        selectedSpecimen.getContainers().add(currentContainer);
        labelCount++;
        FacesMessages.instance().add(StatusMessage.Severity.INFO, "A label has been added to the list");
    }

    public void removeLabel(Container container) {
        selectedSpecimen.getContainers().remove(container);
        labelCount--;
        FacesMessages.instance().add(StatusMessage.Severity.INFO, "The last entered label has been removed from list");
    }

    public Specimen getSelectedSpecimen() {
        return selectedSpecimen;
    }

    public List<HL7V2ResponderSUTConfiguration> getAvailableSuts() {
        return HL7V2ResponderSUTConfiguration.getAllConfigurationsForSelection("LAB-61", HL7Protocol.MLLP);
    }

    public void setSelectedSpecimen(Specimen selectedSpecimen) {
        this.selectedSpecimen = selectedSpecimen;
    }

    public LabOrder getCurrentOrder() {
        return currentOrder;
    }

    public void setCurrentOrder(LabOrder currentOrder) {
        this.currentOrder = currentOrder;
    }

    public Container getCurrentContainer() {
        return currentContainer;
    }

    public void setCurrentContainer(Container currentContainer) {
        this.currentContainer = currentContainer;
    }

    public Integer getLabelCount() {
        return labelCount;
    }

    public void setLabelCount(Integer labelCount) {
        this.labelCount = labelCount;
    }

    public boolean isEditSpecimen() {
        return editSpecimen;
    }
}
