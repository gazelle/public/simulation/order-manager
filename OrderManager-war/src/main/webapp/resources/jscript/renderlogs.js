/**
 * Created by aberge on 06/10/16.
 */
var LogBox = React.createClass({displayName: 'LogBox',
    loadCommentsFromServer: function() {
        $.ajax({
            url: this.props.url,
            dataType: 'json',
            cache: false,
            success: function(data) {
                this.setState({data: data});
            }.bind(this),
            error: function(xhr, status, err) {
                console.error(this.props.url, status, err.toString());
            }.bind(this)
        });
    },
    getInitialState: function() {
        return {data: []};
    },
    componentDidMount: function() {
        this.loadCommentsFromServer();
        setInterval(this.loadCommentsFromServer, this.props.pollInterval);
    },
    render: function() {
        return (
            <code className="logBox">
            {this.state.data.logs}
        </code>
        );
    }
});
ReactDOM.render(
<LogBox url="/OrderManager/rest/worklist/logs" pollInterval={3000}/>,
    document.getElementById('content')
);
