# Updating database for Order Manager

Database management is set to validate. That means that Jboss is not going to create the missing tables, columns ... when you deploy your EAR.
That means that when the changes you brought to the code imply changes in the database, you need to update the schema.sql file and create the update-gss-XXX.sql file.

1. Make sure you already run all the update-*.sql scripts so that you are inline with the last version of the DB schema
1. First, update the OrderManger-ear/pom.xml file to change the hibernate.hbm2ddl.auto property to "update".
1. Package and deploy your application locally, the database will be updated
1. Do not forget to change back the property hibernate.hbm2ddl.auto to "validate"
1. Execute pg_dump -s order-manager --username=gazelle > schema.sql, it will update the schema.sql
1. Perform a diff with the previous version of the file and extract your changes in a file named update-XXX.sql
1. Finally, commit both schema.sql and the update-XXX.sql files
