INSERT INTO om_value_set (id, value_set_name, value_set_oid, usage, value_set_keyword) VALUES (35,'TRO - Abort reason','2.16.840.1.113883.3.3731.1.204.18','OBR-20','troAbortReason');
SELECT pg_catalog.setval('om_value_set_id_seq', 35, true);
