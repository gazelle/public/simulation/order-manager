INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'), 'application_issue_tracker_url', 'https://gazelle.ihe.net/jira/projects/OM');
INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'), 'application_release_notes_url', 'https://gazelle.ihe.net/jira/projects/OM?selectedItem=com.atlassian.jira.jira-projects-plugin:release-page&status=released');
INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'), 'svs_repository_url', 'https://gazelle.ihe.net');
INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'), 'hl7v2_xsl_location', 'https://gazelle.ihe.net/xsl/hl7Validation/resultStylesheet.xsl');
INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'), 'url_EVCS_ws', 'https://gazelle.ihe.net/GazelleHL7v2Validator-ejb/gazelleHL7v2ValidationWSService/gazelleHL7v2ValidationWS?wsdl');
INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'), 'timeout_for_receiving_messages', '30000');
INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'), 'documentation_url', 'https://gazelle.ihe.net/content/order-manager-user-manual');
INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'), 'application_url', 'http://localhost:8080/OrderManager');
INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'), 'dicom_proxy_port', '10112');
INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'), 'wlmscpfs_port', '12345');
INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'), 'wlmscpfs_host', 'localhost');
INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'), 'dcm_data_set_basedir', '/opt/worklists/exchange');
INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'), 'number_of_segments_to_display', '40');
INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'), 'worklists_basedir', '/opt/worklists');
INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'), 'pam_encounter_generation_url', 'https://gazelle.ihe.net/PatientManager/rest/GenerateRandomData');
INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'), 'dds_ws_endpoint', 'https://gazelle.ihe.net/DemographicDataServer-ejb/DemographicDataServerService/DemographicDataServer?wsdl');
INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'), 'dds_mode', 'minimal');
INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'), 'time_zone', 'CET');
INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'), 'order_hierarchy_location', 'https://gazelle.ihe.net/examples/Bern2012-orderHierarchy.xml');
INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'), 'dicom_proxy_ip', '127.0.0.1');
INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'), 'gazelle_hl7v2_validator_url', 'https://gazelle.ihe.net/GazelleHL7Validator');
INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'), 'eye_order_hierarchy_location', 'https://gazelle.ihe.net/examples/orderHierarchy-EYE2012.xml');
INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'), 'proxy_port_low_limit', '10001');
INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'), 'analyzer_serial_number', 'to be uniquely defined');
INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'), 'cas_enabled', 'true');
INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'), 'ip_login', 'false');
INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'), 'ip_login_admin', '.*');
INSERT INTO app_configuration (id, variable, value) values (nextval('app_configuration_id_seq'), 'radiology_enabled', 'true');
INSERT INTO app_configuration (id, variable, value) values (nextval('app_configuration_id_seq'), 'laboratory_enabled', 'true');
INSERT INTO app_configuration (id, variable, value) values (nextval('app_configuration_id_seq'), 'eyecare_enabled', 'true');
INSERT INTO app_configuration (id, variable, value) values (nextval('app_configuration_id_seq'), 'hl7v3_validation_xsl_location', 'http://localhost/xsl/commonValidatorDetailedResult.xsl');
INSERT INTO app_configuration (id, variable, value) values (nextval('app_configuration_id_seq'), 'message_permanent_link', 'http://localhost:8380/OrderManager/messages/messageDisplay.seam?id=');
INSERT INTO app_configuration (id, variable, value) values (nextval('app_configuration_id_seq'), 'NUMBER_OF_ITEMS_PER_PAGE', '20');
insert into app_configuration (id, variable, value) values (nextval('app_configuration_id_seq'), 'contact_email', 'anne-gaelle.berge@ihe-europe.net');
insert into app_configuration (id, variable, value) values (nextval('app_configuration_id_seq'), 'contact_name', 'Anne-Gaëlle Bergé');
insert into app_configuration (id, variable, value) values (nextval('app_configuration_id_seq'), 'contact_title', 'Application administrator');
insert into app_configuration (id, variable, value) values (nextval('app_configuration_id_seq'), 'SeHE_mode_enabled', 'false');
INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'), 'restrict_access_to_messages', false);
INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'), 'evs_client_url', 'https://gazelle.ihe.net/EVSClient');
INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'), 'tool_instance_oid', 'oid for this tool');


-- transaction definition
INSERT INTO tf_transaction(id, description, keyword, name) VALUES (nextval('tf_transaction_id_seq'), 'Lab Placer Order Management', 'LAB-1', 'LAB-1');
INSERT INTO tf_transaction(id, description, keyword, name) VALUES (nextval('tf_transaction_id_seq'), 'Placer Order Management', 'RAD-2', 'RAD-2');
INSERT INTO tf_transaction(id, description, keyword, name) VALUES (nextval('tf_transaction_id_seq'), 'Lab Filler Order Management', 'LAB-2', 'LAB-2');
INSERT INTO tf_transaction(id, description, keyword, name) VALUES (nextval('tf_transaction_id_seq'), 'Filler Order Management', 'RAD-3', 'RAD-3');
INSERT INTO tf_transaction(id, description, keyword, name) VALUES (nextval('tf_transaction_id_seq'), 'Appointment Notification', 'RAD-48', 'RAD-48');
INSERT INTO tf_transaction(id, description, keyword, name) VALUES (nextval('tf_transaction_id_seq'), 'Order Results Management', 'LAB-3', 'LAB-3');
INSERT INTO tf_transaction(id, description, keyword, name) VALUES (nextval('tf_transaction_id_seq'), 'Work Order Management', 'LAB-4', 'LAB-4');
INSERT INTO tf_transaction(id, description, keyword, name) VALUES (nextval('tf_transaction_id_seq'), 'Test Result Management', 'LAB-5', 'LAB-5');
INSERT INTO tf_transaction(id, description, keyword, name) VALUES (nextval('tf_transaction_id_seq'), 'Query for AWOS', 'LAB-27', 'LAB-27');
INSERT INTO tf_transaction(id, description, keyword, name) VALUES (nextval('tf_transaction_id_seq'), 'AWOS Broadcast', 'LAB-28', 'LAB-28');
INSERT INTO tf_transaction(id, description, keyword, name) VALUES (nextval('tf_transaction_id_seq'), 'AWOS Change status', 'LAB-29', 'LAB-29');
INSERT INTO tf_transaction(id, description, keyword, name) VALUES (nextval('tf_transaction_id_seq'), 'Procedure Scheduled', 'RAD-4', 'RAD-4');
INSERT INTO tf_transaction(id, description, keyword, name) VALUES (nextval('tf_transaction_id_seq'), 'Procedure Update', 'RAD-13', 'RAD-13');
INSERT INTO tf_transaction(id, description, keyword, name) VALUES (nextval('tf_transaction_id_seq'), 'Patient Registration', 'RAD-1', 'RAD-1');
INSERT INTO tf_transaction(id, description, keyword, name) VALUES (nextval('tf_transaction_id_seq'), 'Patient Update', 'RAD-12', 'RAD-12');
INSERT INTO tf_transaction(id, description, keyword, name) VALUES (nextval('tf_transaction_id_seq'), 'Unknown transaction', 'UNKNOWN', 'UNKNOWN');
INSERT INTO tf_transaction(id, description, keyword, name) values (nextval('tf_transaction_id_seq'), 'Query Modality Worklist', 'RAD-5', 'Query Modality Worklist');
INSERT INTO tf_transaction(id, description, keyword, name) values (nextval('tf_transaction_id_seq'), 'Tele-Radiology Orders', 'KSA-TRO', 'Tele-Radiology Orders');
INSERT INTO tf_transaction(id, description, keyword, name) values (nextval('tf_transaction_id_seq'), 'Label Delivery Request', 'LAB-61', 'Label Delivery Request');
INSERT INTO tf_transaction(id, description, keyword, name) values (nextval('tf_transaction_id_seq'), 'Query for Label Delivery', 'LAB-62', 'Query for Label Delivery');
INSERT INTO tf_transaction(id, description, keyword, name) values (nextval('tf_transaction_id_seq'), 'Labels & Containers delivered', 'LAB-63', 'Labels & Containers delivered');


-- actor definition
INSERT INTO tf_actor(id, description, keyword, name, can_act_as_responder) VALUES (nextval('tf_actor_id_seq'), 'Order Placer', 'OP', 'Order Placer', true);
INSERT INTO tf_actor(id, description, keyword, name, can_act_as_responder) VALUES (nextval('tf_actor_id_seq'), 'Order Filler', 'OF', 'Order Filler', true);
INSERT INTO tf_actor(id, description, keyword, name, can_act_as_responder) VALUES (nextval('tf_actor_id_seq'), 'Automation Manager', 'AM', 'Automation Manager', true);
INSERT INTO tf_actor(id, description, keyword, name, can_act_as_responder) VALUES (nextval('tf_actor_id_seq'), 'Order Result Tracker', 'ORT', 'Order Result Tracker', true);
INSERT INTO tf_actor(id, description, keyword, name, can_act_as_responder) VALUES (nextval('tf_actor_id_seq'), 'Analyzer', 'ANALYZER', 'Analyzer', true);
INSERT INTO tf_actor(id, description, keyword, name, can_act_as_responder) VALUES (nextval('tf_actor_id_seq'), 'Analyzer Manager', 'ANALYZER_MGR', 'Analyzer Manager', true);
INSERT INTO tf_actor(id, description, keyword, name, can_act_as_responder) VALUES (nextval('tf_actor_id_seq'), 'Image/Report Manager', 'IM', 'Image/Report Manager', true);
INSERT INTO tf_actor(id, description, keyword, name, can_act_as_responder) VALUES (nextval('tf_actor_id_seq'), 'ADT Patient Registration', 'ADT', 'ADT Patient Registration', false);
INSERT INTO tf_actor(id, description, keyword, name, can_act_as_responder) VALUES (nextval('tf_actor_id_seq'), 'Aquisition Modality', 'MOD', 'Aquisition Modality', false);
INSERT INTO tf_actor(id, description, keyword, name, can_act_as_responder) VALUES (nextval('tf_actor_id_seq'), 'Unknown actor', 'UNKNOWN', 'UNKNOWN', false);
INSERT INTO tf_actor(id, description, keyword, name, can_act_as_responder) values (nextval('tf_actor_id_seq'), 'Creator', 'TRO_CREATOR', 'TRO Creator', true);
INSERT INTO tf_actor(id, description, keyword, name, can_act_as_responder) values (nextval('tf_actor_id_seq'), 'Forwarder', 'TRO_FORWARDER', 'TRO Forwarder', true);
INSERT INTO tf_actor(id, description, keyword, name, can_act_as_responder) values (nextval('tf_actor_id_seq'), 'Fulfiller', 'TRO_FULFILLER', 'TRO Fulfiller', true);
INSERT INTO tf_actor(id, description, keyword, name, can_act_as_responder) values (nextval('tf_actor_id_seq'), 'Label Broker', 'LB', 'Label Broker', true);
INSERT INTO tf_actor(id, description, keyword, name, can_act_as_responder) values (nextval('tf_actor_id_seq'), 'Label Information Provider', 'LIP', 'Label Information Provider', true);


-- domain definition
INSERT INTO tf_domain (id, description, keyword, name) VALUES (nextval('tf_domain_id_seq'), 'Radiology', 'RAD', 'Radiology');
INSERT INTO tf_domain (id, description, keyword, name) VALUES (nextval('tf_domain_id_seq'), 'Laboratory', 'LAB', 'Laboratory');
INSERT INTO tf_domain (id, description, keyword, name) VALUES (nextval('tf_domain_id_seq'), 'Eyecare', 'EYE', 'Eyecare');
INSERT INTO tf_domain (id, description, keyword, name) VALUES (nextval('tf_domain_id_seq'), 'SeHE', 'SeHE', 'Saoudi eHealth Exchange');

-- set there the charset used by your simulator
INSERT INTO hl7_charset (id, display_name, hl7_code, java_code) VALUES (nextval('hl7_charset_id_seq'),  'ASCII','ASCII', 'US-ASCII' );
INSERT INTO hl7_charset (id, display_name, hl7_code, java_code) VALUES (nextval('hl7_charset_id_seq'),  'ISO8859-1',	'8859/1'	,'ISO8859_1');
INSERT INTO hl7_charset (id, display_name, hl7_code, java_code) VALUES (nextval('hl7_charset_id_seq'),  'ISO8859-2', 	'8859/2'	, 'ISO8859_2');
INSERT INTO hl7_charset (id, display_name, hl7_code, java_code) VALUES (nextval('hl7_charset_id_seq'),  'ISO8859-3', 	'8859/3'	, 'ISO8859_3');
INSERT INTO hl7_charset (id, display_name, hl7_code, java_code) VALUES (nextval('hl7_charset_id_seq'),  'ISO8859-4', 	'8859/4'	, 'ISO8859_4');
INSERT INTO hl7_charset (id, display_name, hl7_code, java_code) VALUES (nextval('hl7_charset_id_seq'),  'ISO8859-5', 	'8859/5'	, 'ISO8859_5');
INSERT INTO hl7_charset (id, display_name, hl7_code, java_code) VALUES (nextval('hl7_charset_id_seq'),  'ISO8859-6', 	'8859/6'	, 'ISO8859_6');
INSERT INTO hl7_charset (id, display_name, hl7_code, java_code) VALUES (nextval('hl7_charset_id_seq'),  'ISO8859-7', 	'8859/7'	, 'ISO8859_7');
INSERT INTO hl7_charset (id, display_name, hl7_code, java_code) VALUES (nextval('hl7_charset_id_seq'),  'ISO8859-8', 	'8859/8'	, 'ISO8859_8');
INSERT INTO hl7_charset (id, display_name, hl7_code, java_code) VALUES (nextval('hl7_charset_id_seq'), 'ISO8859-9', 	'8859/9'	, 'ISO8859_9');
INSERT INTO hl7_charset (id, display_name, hl7_code, java_code) VALUES (nextval('hl7_charset_id_seq'), 'Japanese (JIS X 0201)', 	'ISO IR14', 'JIS0201' );
INSERT INTO hl7_charset (id, display_name, hl7_code, java_code) VALUES (nextval('hl7_charset_id_seq'), 'Japanese (JIS X 0208)', 	'ISO IR87', 'JIS0208' );
INSERT INTO hl7_charset (id, display_name, hl7_code, java_code) VALUES (nextval('hl7_charset_id_seq'), 'Japanese (JIS X 0212)', 	'ISO IR159',  'JIS0212');
INSERT INTO hl7_charset (id, display_name, hl7_code, java_code) VALUES (nextval('hl7_charset_id_seq'), 'Chinese', 	'GB 18030-2000'	, 'EUC_CN');
INSERT INTO hl7_charset (id, display_name, hl7_code, java_code) VALUES (nextval('hl7_charset_id_seq'), 'Korean', 	'KS X 1001'	, 'EUC_KR');
INSERT INTO hl7_charset (id, display_name, hl7_code, java_code) VALUES (nextval('hl7_charset_id_seq'), 'Taiwanese', 	'CNS 11643-1992', 'EUC_TW');
INSERT INTO hl7_charset (id, display_name, hl7_code, java_code) VALUES (nextval('hl7_charset_id_seq'), 'Taiwanese (BIG 5)', 'BIG-5', 'Big5');
INSERT INTO hl7_charset (id, display_name, hl7_code, java_code) VALUES (nextval('hl7_charset_id_seq'), 'UTF-8', 		'UNICODE UTF-8' , 'UTF8'    );
INSERT INTO hl7_charset (id, display_name, hl7_code, java_code) VALUES (nextval('hl7_charset_id_seq'), 'UTF-16', 		'UNICODE UTF-16', 'UTF-16');
INSERT INTO hl7_charset (id, display_name, hl7_code, java_code) VALUES (nextval('hl7_charset_id_seq'), 'UTF-32', 		'UNICODE UTF-32', 'UTF-32');
INSERT INTO hl7_charset (id, display_name, hl7_code, java_code) VALUES (nextval('hl7_charset_id_seq'), 'ISO8859-15', '8859/15', 'ISO8859_15');

INSERT INTO affinity_domain (id, keyword, label_to_display, profile) VALUES (nextval('affinity_domain_id_seq'), 'IHE_RAD', 'IHE Radiology', 'All');
INSERT INTO affinity_domain (id, keyword, label_to_display, profile) VALUES (nextval('affinity_domain_id_seq'), 'IHE_EYE', 'IHE Eyecare', 'All');
INSERT INTO affinity_domain (id, keyword, label_to_display, profile) VALUES (nextval('affinity_domain_id_seq'), 'IHE_LAB', 'IHE Laboratory', 'All');
INSERT INTO affinity_domain (id, keyword, label_to_display, profile) VALUES (nextval('tf_domain_id_seq'), 'SeHE', 'SeHE', 'All');

INSERT INTO usage_metadata (id, transaction_id, affinity_id, action, keyword)
VALUES (nextval('usage_id_seq'), (SELECT id
                                  FROM tf_transaction
                                  WHERE keyword = 'RAD-2'), (SELECT id
                                                             FROM affinity_domain
                                                             WHERE keyword = 'IHE_EYE'), 'Order Filler', 'EYE_RAD2');
INSERT INTO usage_metadata (id, transaction_id, affinity_id, action, keyword)
VALUES (nextval('usage_id_seq'), (SELECT id
                                  FROM tf_transaction
                                  WHERE keyword = 'RAD-2'), (SELECT id
                                                             FROM affinity_domain
                                                             WHERE keyword = 'IHE_RAD'), 'Order Filler', 'RAD_RAD2');

INSERT INTO usage_metadata (id, transaction_id, affinity_id, action, keyword)
VALUES (nextval('usage_id_seq'), (SELECT id
                                  FROM tf_transaction
                                  WHERE keyword = 'RAD-3'), (SELECT id
                                                             FROM affinity_domain
                                                             WHERE keyword = 'IHE_EYE'), 'Order Placer', 'EYE_RAD3');

INSERT INTO usage_metadata (id, transaction_id, affinity_id, action, keyword)
VALUES (nextval('usage_id_seq'), (SELECT id
                                  FROM tf_transaction
                                  WHERE keyword = 'RAD-3'), (SELECT id
                                                             FROM affinity_domain
                                                             WHERE keyword = 'IHE_RAD'), 'Order Placer', 'RAD_RAD3');

INSERT INTO usage_metadata (id, transaction_id, affinity_id, action, keyword)
VALUES (nextval('usage_id_seq'), (SELECT id
                                  FROM tf_transaction
                                  WHERE keyword = 'RAD-4'), (SELECT id
                                                             FROM affinity_domain
                                                             WHERE keyword = 'IHE_EYE'), 'Image/Report Manager', 'EYE_RAD4');

INSERT INTO usage_metadata (id, transaction_id, affinity_id, action, keyword)
VALUES (nextval('usage_id_seq'), (SELECT id
                                  FROM tf_transaction
                                  WHERE keyword = 'RAD-4'), (SELECT id
                                                             FROM affinity_domain
                                                             WHERE keyword = 'IHE_RAD'), 'Image/Report Manager', 'RAD_RAD4');

INSERT INTO usage_metadata (id, transaction_id, affinity_id, action, keyword)
VALUES (nextval('usage_id_seq'), (SELECT id
                                  FROM tf_transaction
                                  WHERE keyword = 'RAD-13'), (SELECT id
                                                              FROM affinity_domain
                                                              WHERE keyword = 'IHE_EYE'), 'Image/Report Manager', 'EYE_RAD13');

INSERT INTO usage_metadata (id, transaction_id, affinity_id, action, keyword)
VALUES (nextval('usage_id_seq'), (SELECT id
                                  FROM tf_transaction
                                  WHERE keyword = 'RAD-13'), (SELECT id
                                                              FROM affinity_domain
                                                              WHERE keyword = 'IHE_RAD'), 'Image/Report Manager', 'RAD_RAD13');

INSERT INTO usage_metadata (id, transaction_id, affinity_id, action, keyword)
VALUES (nextval('usage_id_seq'), (SELECT id
                                  FROM tf_transaction
                                  WHERE keyword = 'LAB-1'), (SELECT id
                                                             FROM affinity_domain
                                                             WHERE keyword = 'IHE_LAB'), 'Order Filler', 'LAB_OF_LAB1');
INSERT INTO usage_metadata (id, transaction_id, affinity_id, action, keyword)
VALUES (nextval('usage_id_seq'), (SELECT id
                                  FROM tf_transaction
                                  WHERE keyword = 'LAB-2'), (SELECT id
                                                             FROM affinity_domain
                                                             WHERE keyword = 'IHE_LAB'), 'Order Filler', 'LAB_OF_LAB2');
INSERT INTO usage_metadata (id, transaction_id, affinity_id, action, keyword)
VALUES (nextval('usage_id_seq'), (SELECT id
                                  FROM tf_transaction
                                  WHERE keyword = 'LAB-1'), (SELECT id
                                                             FROM affinity_domain
                                                             WHERE keyword = 'IHE_LAB'), 'Order Placer', 'LAB_OP_LAB1');
INSERT INTO usage_metadata (id, transaction_id, affinity_id, action, keyword)
VALUES (nextval('usage_id_seq'), (SELECT id
                                  FROM tf_transaction
                                  WHERE keyword = 'LAB-2'), (SELECT id
                                                             FROM affinity_domain
                                                             WHERE keyword = 'IHE_LAB'), 'Order Placer', 'LAB_OP_LAB2');
INSERT INTO usage_metadata (id, transaction_id, affinity_id, action, keyword)
VALUES (nextval('usage_id_seq'), (SELECT id
                                  FROM tf_transaction
                                  WHERE keyword = 'LAB-3'), (SELECT id
                                                             FROM affinity_domain
                                                             WHERE keyword = 'IHE_LAB'), 'Order Result Tracker', 'LAB_ORT');

INSERT INTO usage_metadata (id, transaction_id, affinity_id, action, keyword)
VALUES (nextval('usage_id_seq'), (SELECT id
                                  FROM tf_transaction
                                  WHERE keyword = 'LAB-4'), (SELECT id
                                                             FROM affinity_domain
                                                             WHERE keyword = 'IHE_LAB'), 'Automation Manager', 'LAB_AM');

INSERT INTO usage_metadata (id, transaction_id, affinity_id, action, keyword)
VALUES (nextval('usage_id_seq'), (SELECT id
                                  FROM tf_transaction
                                  WHERE keyword = 'LAB-5'), (SELECT id
                                                             FROM affinity_domain
                                                             WHERE keyword = 'IHE_LAB'), 'Order Filler', 'LAB_OF_LAB5');

INSERT INTO usage_metadata (id, transaction_id, affinity_id, action, keyword)
VALUES (nextval('usage_id_seq'), (SELECT id
                                  FROM tf_transaction
                                  WHERE keyword = 'LAB-27'), (SELECT id
                                                              FROM affinity_domain
                                                              WHERE keyword = 'IHE_LAB'), 'Analyzer Manager', 'LAB_AN_MGR_LAB27');

INSERT INTO usage_metadata (id, transaction_id, affinity_id, action, keyword)
VALUES (nextval('usage_id_seq'), (SELECT id
                                  FROM tf_transaction
                                  WHERE keyword = 'LAB-28'), (SELECT id
                                                              FROM affinity_domain
                                                              WHERE keyword = 'IHE_LAB'), 'Analyzer', 'LAB_ANALYZER');

INSERT INTO usage_metadata (id, transaction_id, affinity_id, action, keyword)
VALUES (nextval('usage_id_seq'), (SELECT id
                                  FROM tf_transaction
                                  WHERE keyword = 'LAB-29'), (SELECT id
                                                              FROM affinity_domain
                                                              WHERE keyword = 'IHE_LAB'), 'Analyzer Manager', 'LAB_AN_MGR_LAB29');
INSERT INTO usage_metadata (id, transaction_id, affinity_id, action, keyword)
VALUES (nextval('usage_id_seq'), (SELECT id
                                  FROM tf_transaction
                                  WHERE keyword = 'KSA-TRO'), (SELECT id
                                                               FROM affinity_domain
                                                               WHERE keyword = 'SeHE'), 'TRO Creator', 'KSA_TRO_CREATOR');
INSERT INTO usage_metadata (id, transaction_id, affinity_id, action, keyword)
VALUES (nextval('usage_id_seq'), (SELECT id
                                  FROM tf_transaction
                                  WHERE keyword = 'KSA-TRO'), (SELECT id
                                                               FROM affinity_domain
                                                               WHERE keyword = 'SeHE'), 'TRO Fulfiller', 'KSA_TRO_FULFILLER');
INSERT INTO usage_metadata (id, transaction_id, affinity_id, action, keyword)
VALUES (nextval('usage_id_seq'), (SELECT id
                                  FROM tf_transaction
                                  WHERE keyword = 'KSA-TRO'), (SELECT id
                                                               FROM affinity_domain
                                                               WHERE keyword = 'SeHE'), 'TRO Forwarder', 'KSA_TRO_FORWARDER');

INSERT INTO usage_metadata (id, transaction_id, affinity_id, action, keyword)
VALUES (nextval('usage_id_seq'), (SELECT id
                                  FROM tf_transaction
                                  WHERE keyword = 'RAD-5'), (SELECT id
                                                             FROM affinity_domain
                                                             WHERE keyword = 'IHE_EYE'), 'Order Filler', 'EYE_RAD5');

INSERT INTO usage_metadata (id, transaction_id, affinity_id, action, keyword)
VALUES (nextval('usage_id_seq'), (SELECT id
                                  FROM tf_transaction
                                  WHERE keyword = 'RAD-5'), (SELECT id
                                                             FROM affinity_domain
                                                             WHERE keyword = 'IHE_RAD'), 'Order Filler', 'RAD_RAD5');
INSERT INTO usage_metadata (id, transaction_id, affinity_id, action, keyword)
VALUES (nextval('usage_id_seq'), (SELECT id
                                  FROM tf_transaction
                                  WHERE keyword = 'LAB-61'), (SELECT id
                                                              FROM affinity_domain
                                                              WHERE keyword = 'IHE_LAB'), 'Label Broker', 'LAB_LB');
INSERT INTO usage_metadata (id, transaction_id, affinity_id, action, keyword)
VALUES (nextval('usage_id_seq'), (SELECT id
                                  FROM tf_transaction
                                  WHERE keyword = 'LAB-62'), (SELECT id
                                                              FROM affinity_domain
                                                              WHERE keyword = 'IHE_LAB'), 'Label Information Provider', 'LAB_LAB62');
INSERT INTO usage_metadata (id, transaction_id, affinity_id, action, keyword)
VALUES (nextval('usage_id_seq'), (SELECT id
                                  FROM tf_transaction
                                  WHERE keyword = 'LAB-63'), (SELECT id
                                                              FROM affinity_domain
                                                              WHERE keyword = 'IHE_LAB'), 'Label Information Provider', 'LAB_LAB63');


INSERT INTO hl7_simulator_responder_configuration (id, name, listening_port, simulated_actor_id, transaction_id, domain_id, accepted_message_type, accepted_trigger_event, server_bean_name, ip_address, receiving_application, receiving_facility, is_running) VALUES (nextval('hl7_simulator_responder_configuration_id_seq'), 'IHE RAD Order Placer', 10120, (select id from tf_actor where keyword = 'OP'), (select id from tf_transaction where keyword = 'RAD-3'), (select id from tf_domain where keyword = 'RAD'), '*', '*', 'orderPlacerServer', '127.0.0.1', 'OM_RAD_OP', 'IHE', false);
INSERT INTO hl7_simulator_responder_configuration (id, name, listening_port, simulated_actor_id, transaction_id, domain_id, accepted_message_type, accepted_trigger_event, server_bean_name, ip_address, receiving_application, receiving_facility, is_running) VALUES (nextval('hl7_simulator_responder_configuration_id_seq'), 'IHE RAD Order Filler', 10121, (select id from tf_actor where keyword = 'OF'), (select id from tf_transaction where keyword = 'RAD-2'), (select id from tf_domain where keyword = 'RAD'), '*', '*', 'orderFillerServer', '127.0.0.1', 'OM_RAD_OF', 'IHE', false);
INSERT INTO hl7_simulator_responder_configuration (id, name, listening_port, simulated_actor_id, transaction_id, domain_id, accepted_message_type, accepted_trigger_event, server_bean_name, ip_address, receiving_application, receiving_facility, is_running) VALUES (nextval('hl7_simulator_responder_configuration_id_seq'), 'IHE LAB Order Filler', 10125, (select id from tf_actor where keyword = 'OF'), null, (select id from tf_domain where keyword = 'LAB'), '*', '*', 'orderFillerServer', '127.0.0.1', 'OM_LAB_OF', 'IHE', false);
INSERT INTO hl7_simulator_responder_configuration (id, name, listening_port, simulated_actor_id, transaction_id, domain_id, accepted_message_type, accepted_trigger_event, server_bean_name, ip_address, receiving_application, receiving_facility, is_running) VALUES (nextval('hl7_simulator_responder_configuration_id_seq'), 'IHE LAB Order Placer', 10126, (select id from tf_actor where keyword = 'OP'), null, (select id from tf_domain where keyword = 'LAB'), '*', '*', 'orderPlacerServer', '127.0.0.1', 'OM_LAB_OP', 'IHE', false);
INSERT INTO hl7_simulator_responder_configuration (id, name, listening_port, simulated_actor_id, transaction_id, domain_id, accepted_message_type, accepted_trigger_event, server_bean_name, ip_address, receiving_application, receiving_facility, is_running) VALUES (nextval('hl7_simulator_responder_configuration_id_seq'), 'IHE LAB Automation Manager', 10127, (select id from tf_actor where keyword = 'AM'), (select id from tf_transaction where keyword = 'LAB-4'), (select id from tf_domain where keyword = 'LAB'), '*', '*', 'automationManagerServer', '127.0.0.1', 'OM_LAB_AM', 'IHE', false);
INSERT INTO hl7_simulator_responder_configuration (id, name, listening_port, simulated_actor_id, transaction_id, domain_id, accepted_message_type, accepted_trigger_event, server_bean_name, ip_address, receiving_application, receiving_facility, is_running) VALUES (nextval('hl7_simulator_responder_configuration_id_seq'), 'IHE LAB Order Result Tracker', 10128, (select id from tf_actor where keyword = 'ORT'), (select id from tf_transaction where keyword = 'LAB-3'), (select id from tf_domain where keyword = 'LAB'), '*', '*', 'orderResultTrackerServer', '127.0.0.1', 'OM_LAB_ORT', 'IHE', false);
INSERT INTO hl7_simulator_responder_configuration (id, name, listening_port, simulated_actor_id, transaction_id, domain_id, accepted_message_type, accepted_trigger_event, server_bean_name, ip_address, receiving_application, receiving_facility, is_running) VALUES (nextval('hl7_simulator_responder_configuration_id_seq'), 'IHE LAB Analyzer', 10122, (select id from tf_actor where keyword = 'ANALYZER'), (select id from tf_transaction where keyword = 'LAB-28'), (select id from tf_domain where keyword = 'LAB'), '*', '*', 'analyzerServer', '127.0.0.1', 'OM_LAB_ANALYZER', 'IHE', false);
INSERT INTO hl7_simulator_responder_configuration (id, name, listening_port, simulated_actor_id, transaction_id, domain_id, accepted_message_type, accepted_trigger_event, server_bean_name, ip_address, receiving_application, receiving_facility, is_running) VALUES (nextval('hl7_simulator_responder_configuration_id_seq'), 'IHE LAB Analyzer Manager', 10129, (select id from tf_actor where keyword = 'ANALYZER_MGR'), null, (select id from tf_domain where keyword = 'LAB'), '*', '*', 'analyzerManagerServer', '127.0.0.1', 'OM_LAB_ANALYZER_MGR', 'IHE', false);
INSERT INTO hl7_simulator_responder_configuration (id, name, listening_port, simulated_actor_id, transaction_id, domain_id, accepted_message_type, accepted_trigger_event, server_bean_name, ip_address, receiving_application, receiving_facility, is_running) VALUES (nextval('hl7_simulator_responder_configuration_id_seq'), 'IHE RAD Image/Report Manager', 10130, (select id from tf_actor where keyword = 'IM'), null, (select id from tf_domain where keyword = 'RAD'), 'ORM', 'O01', 'imageReportManagerServer', '127.0.0.1', 'OM_RAD_IM', 'IHE', false);
INSERT INTO hl7_simulator_responder_configuration (id, name, listening_port, simulated_actor_id, transaction_id, domain_id, accepted_message_type, accepted_trigger_event, server_bean_name, ip_address, receiving_application, receiving_facility, is_running) VALUES (nextval('hl7_simulator_responder_configuration_id_seq'),  'IHE EYE Order Placer', 10131, (select id from tf_actor where keyword = 'OP'), (select id from tf_transaction where keyword = 'RAD-3'), (select id from tf_domain where keyword = 'EYE'), '*', '*', 'orderPlacerServer', '127.0.0.1', 'OM_EYE_OP', 'IHE', false);
INSERT INTO hl7_simulator_responder_configuration (id, name, listening_port, simulated_actor_id, transaction_id, domain_id, accepted_message_type, accepted_trigger_event, server_bean_name, ip_address, receiving_application, receiving_facility, is_running) VALUES (nextval('hl7_simulator_responder_configuration_id_seq'),  'IHE EYE Order Filler', 10132, (select id from tf_actor where keyword = 'OF'), (select id from tf_transaction where keyword = 'RAD-2'), (select id from tf_domain where keyword = 'EYE'), '*', '*', 'orderFillerServer', '127.0.0.1', 'OM_EYE_OF', 'IHE', false);
INSERT INTO hl7_simulator_responder_configuration (id, name, listening_port, simulated_actor_id, transaction_id, domain_id, accepted_message_type, accepted_trigger_event, server_bean_name, ip_address, receiving_application, receiving_facility, is_running) VALUES (nextval('hl7_simulator_responder_configuration_id_seq'),  'IHE EYE Image/Report Manager', 10133, (select id from tf_actor where keyword = 'IM'), null, (select id from tf_domain where keyword = 'EYE'), 'ORM', 'O01', 'imageReportManagerServer', '127.0.0.1', 'OM_EYE_IM', 'IHE', false);
INSERT INTO hl7_simulator_responder_configuration (id, name, listening_port, simulated_actor_id, transaction_id, domain_id, accepted_message_type, accepted_trigger_event, server_bean_name, ip_address, receiving_application, receiving_facility, is_running) VALUES (nextval('hl7_simulator_responder_configuration_id_seq'),  'IHE LAB Label Broker', 10134, (select id from tf_actor where keyword = 'LB'), null, (select id from tf_domain where keyword = 'LAB'), 'OML', 'O33', 'labelBrokerServer', '127.0.0.1', 'OM_LAB_LB', 'IHE', false);
INSERT INTO hl7_simulator_responder_configuration (id, name, listening_port, simulated_actor_id, transaction_id, domain_id, accepted_message_type, accepted_trigger_event, server_bean_name, ip_address, receiving_application, receiving_facility, is_running) VALUES (nextval('hl7_simulator_responder_configuration_id_seq'),  'IHE LAB Label Information Provider', 10135, (select id from tf_actor where keyword = 'LIP'), null, (select id from tf_domain where keyword = 'LAB'), 'OML, QBP', 'O33, SLI', 'labelInformationProviderServer', '127.0.0.1', 'OM_LAB_LIP', 'IHE', false);
UPDATE hl7_simulator_responder_configuration set hl7_protocol = 1;
UPDATE hl7_simulator_responder_configuration set message_encoding = 1;


INSERT INTO system_configuration (id, dtype, is_available, application, facility, ip_address, name, system_name, hl7_protocol, message_encoding, port, is_public, owner,  charset_id) VALUES (nextval('system_configuration_id_seq'), 'hl7v2_responder', true, 'OM_RAD_OF',            'IHE', '127.0.0.1', 'Radiology Order Filler',          'Gazelle Order Manager',1, 1 , 10121, true, 'admin', 19);
INSERT INTO system_configuration (id, dtype, is_available, application, facility, ip_address, name, system_name, hl7_protocol, message_encoding, port, is_public, owner,  charset_id) VALUES (nextval('system_configuration_id_seq'), 'hl7v2_responder', true, 'OM_RAD_OP',            'IHE', '127.0.0.1', 'Radiology Order Placer',          'Gazelle Order Manager',1, 1 , 10120, true, 'admin', 19);
INSERT INTO system_configuration (id, dtype, is_available, application, facility, ip_address, name, system_name, hl7_protocol, message_encoding, port, is_public, owner,  charset_id) VALUES (nextval('system_configuration_id_seq'), 'hl7v2_responder', true, 'OM_LAB_OF',            'IHE', '127.0.0.1', 'Laboratory Order Filler',         'Gazelle Order Manager',1, 1 , 10125, true, 'admin', 19);
INSERT INTO system_configuration (id, dtype, is_available, application, facility, ip_address, name, system_name, hl7_protocol, message_encoding, port, is_public, owner,  charset_id) VALUES (nextval('system_configuration_id_seq'), 'hl7v2_responder', true, 'OM_LAB_OP',            'IHE', '127.0.0.1', 'Laboratory Order Placer',         'Gazelle Order Manager',1, 1 , 10126, true, 'admin', 19);
INSERT INTO system_configuration (id, dtype, is_available, application, facility, ip_address, name, system_name, hl7_protocol, message_encoding, port, is_public, owner,  charset_id) VALUES (nextval('system_configuration_id_seq'), 'hl7v2_responder', true, 'OM_LAB_AM',            'IHE', '127.0.0.1', 'Laboratory Automation Manager',   'Gazelle Order Manager',1, 1 , 10127, true, 'admin', 19);
INSERT INTO system_configuration (id, dtype, is_available, application, facility, ip_address, name, system_name, hl7_protocol, message_encoding, port, is_public, owner,  charset_id) VALUES (nextval('system_configuration_id_seq'), 'hl7v2_responder', true, 'OM_LAB_ORT',           'IHE', '127.0.0.1', 'Laboratory Order Result Tracker', 'Gazelle Order Manager',1, 1 , 10128, true, 'admin', 19);
INSERT INTO system_configuration (id, dtype, is_available, application, facility, ip_address, name, system_name, hl7_protocol, message_encoding, port, is_public, owner,  charset_id) VALUES (nextval('system_configuration_id_seq'), 'hl7v2_responder', true, 'OM_RAD_IM',            'IHE', '127.0.0.1', 'Radiology Image/Report Manager',  'Gazelle Order Manager',1, 1 , 10130, true, 'admin', 19);
INSERT INTO system_configuration (id, dtype, is_available, application, facility, ip_address, name, system_name, hl7_protocol, message_encoding, port, is_public, owner,  charset_id) VALUES (nextval('system_configuration_id_seq'), 'hl7v2_responder', true, 'OM_EYE_OF',            'IHE', '127.0.0.1', 'Eye care Order Filler',           'Gazelle Order Manager',1, 1 , 10132, true, 'admin', 19);
INSERT INTO system_configuration (id, dtype, is_available, application, facility, ip_address, name, system_name, hl7_protocol, message_encoding, port, is_public, owner,  charset_id) VALUES (nextval('system_configuration_id_seq'), 'hl7v2_responder', true, 'OM_EYE_OP',            'IHE', '127.0.0.1', 'Eye care Order Placer',           'Gazelle Order Manager',1, 1 , 10131, true, 'admin', 19);
INSERT INTO system_configuration (id, dtype, is_available, application, facility, ip_address, name, system_name, hl7_protocol, message_encoding, port, is_public, owner,  charset_id) VALUES (nextval('system_configuration_id_seq'), 'hl7v2_responder', true, 'OM_EYE_IM',           'IHE', '127.0.0.1', 'Eye care Image/Report Manager',   'Gazelle Order Manager',1, 1 , 10133, true, 'admin', 19);
INSERT INTO system_configuration (id, dtype, is_available, application, facility, ip_address, name, system_name, hl7_protocol, message_encoding, port, is_public, owner,  charset_id) VALUES (nextval('system_configuration_id_seq'), 'hl7v2_responder', true, 'OM_LAB_ANALYZER',     'IHE', '127.0.0.1', 'Laboratory Analyzer',             'Gazelle Order Manager',1, 1 , 10122, true, 'admin', 19);
INSERT INTO system_configuration (id, dtype, is_available, application, facility, ip_address, name, system_name, hl7_protocol, message_encoding, port, is_public, owner,  charset_id) VALUES (nextval('system_configuration_id_seq'), 'hl7v2_responder', true, 'OM_LAB_ANALYZER_MGR', 'IHE', '127.0.0.1', 'Laboratory Analyzer Manager',     'Gazelle Order Manager',1, 1 , 10129, true, 'admin', 19);
INSERT INTO system_configuration (id, dtype, is_available, application, facility, ip_address, name, system_name, hl7_protocol, message_encoding, port, is_public, owner,  charset_id) VALUES (nextval('system_configuration_id_seq'), 'hl7v2_responder', true, 'OM_LAB_LB',           'IHE', '127.0.0.1', 'Laboratory Label Broker',         'Gazelle Order Manager',1, 1 , 10134, true, 'admin', 19);
INSERT INTO system_configuration (id, dtype, is_available, application, facility, ip_address, name, system_name, hl7_protocol, message_encoding, port, is_public, owner,  charset_id) VALUES (nextval('system_configuration_id_seq'), 'hl7v2_responder', true, 'OM_LAB_LIP',          'IHE', '127.0.0.1', 'Laboratory Label Information Provider',     'Gazelle Order Manager',1, 1 , 10135, true, 'admin', 19);

INSERT INTO sys_conf_type_usages(system_configuration_id, listusages_id) values ((select id from system_configuration where application = 'OM_RAD_OF'), (select id from usage_metadata where keyword = 'RAD_RAD2'));
INSERT INTO sys_conf_type_usages(system_configuration_id, listusages_id) values ((select id from system_configuration where application = 'OM_RAD_OP'), (select id from usage_metadata where keyword = 'RAD_RAD3'));
INSERT INTO sys_conf_type_usages(system_configuration_id, listusages_id) values ((select id from system_configuration where application = 'OM_RAD_IM'), (select id from usage_metadata where keyword = 'RAD_RAD4'));
INSERT INTO sys_conf_type_usages(system_configuration_id, listusages_id) values ((select id from system_configuration where application = 'OM_RAD_IM'), (select id from usage_metadata where keyword = 'RAD_RAD13'));
INSERT INTO sys_conf_type_usages(system_configuration_id, listusages_id) values ((select id from system_configuration where application = 'OM_EYE_OF'), (select id from usage_metadata where keyword = 'EYE_RAD2'));
INSERT INTO sys_conf_type_usages(system_configuration_id, listusages_id) values ((select id from system_configuration where application = 'OM_EYE_OP'), (select id from usage_metadata where keyword = 'EYE_RAD3'));
INSERT INTO sys_conf_type_usages(system_configuration_id, listusages_id) values ((select id from system_configuration where application = 'OM_LAB_OF'), (select id from usage_metadata where keyword = 'LAB_OF_LAB1'));
INSERT INTO sys_conf_type_usages(system_configuration_id, listusages_id) values ((select id from system_configuration where application = 'OM_LAB_OF'), (select id from usage_metadata where keyword = 'LAB_OF_LAB1'));
INSERT INTO sys_conf_type_usages(system_configuration_id, listusages_id) values ((select id from system_configuration where application = 'OM_LAB_OF'), (select id from usage_metadata where keyword = 'LAB_OF_LAB5'));
INSERT INTO sys_conf_type_usages(system_configuration_id, listusages_id) values ((select id from system_configuration where application = 'OM_EYE_IM'), (select id from usage_metadata where keyword = 'EYE_RAD4'));
INSERT INTO sys_conf_type_usages(system_configuration_id, listusages_id) values ((select id from system_configuration where application = 'OM_EYE_IM'), (select id from usage_metadata where keyword = 'EYE_RAD13'));
INSERT INTO sys_conf_type_usages(system_configuration_id, listusages_id) values ((select id from system_configuration where application = 'OM_LAB_OP'), (select id from usage_metadata where keyword = 'LAB_OP_LAB2'));
INSERT INTO sys_conf_type_usages(system_configuration_id, listusages_id) values ((select id from system_configuration where application = 'OM_LAB_OP'), (select id from usage_metadata where keyword = 'LAB_OP_LAB1'));
INSERT INTO sys_conf_type_usages(system_configuration_id, listusages_id) values ((select id from system_configuration where application = 'OM_LAB_ORT'), (select id from usage_metadata where keyword = 'LAB_ORT'));
INSERT INTO sys_conf_type_usages(system_configuration_id, listusages_id) values ((select id from system_configuration where application = 'OM_LAB_AM'), (select id from usage_metadata where keyword = 'LAB_AM'));
INSERT INTO sys_conf_type_usages(system_configuration_id, listusages_id) values ((select id from system_configuration where application = 'OM_LAB_ANALYZER'), (select id from usage_metadata where keyword = 'LAB_ANALYZER'));
INSERT INTO sys_conf_type_usages(system_configuration_id, listusages_id) values ((select id from system_configuration where application = 'OM_LAB_ANALYZER_MGR'), (select id from usage_metadata where keyword = 'LAB_AN_MGR_LAB27'));
INSERT INTO sys_conf_type_usages(system_configuration_id, listusages_id) values ((select id from system_configuration where application = 'OM_LAB_ANALYZER_MGR'), (select id from usage_metadata where keyword = 'LAB_AN_MGR_LAB29'));
INSERT INTO sys_conf_type_usages(system_configuration_id, listusages_id) values ((select id from system_configuration where application = 'OM_LAB_LIP'), (select id from usage_metadata where keyword = 'LAB_LAB62'));
INSERT INTO sys_conf_type_usages(system_configuration_id, listusages_id) values ((select id from system_configuration where application = 'OM_LAB_LIP'), (select id from usage_metadata where keyword = 'LAB_LAB63'));
INSERT INTO sys_conf_type_usages(system_configuration_id, listusages_id) values ((select id from system_configuration where application = 'OM_LAB_LB'), (select id from usage_metadata where keyword = 'LAB_LB'));

INSERT INTO om_number_generator (id, namespace_id, universal_id, next_entity_identifier, universal_id_type, actor_id) VALUES (nextval('om_number_generator_id_seq'), 'IHE_OM_OP', '%ROOT_OID%.2', 1, 'ISO', 1);
INSERT INTO om_number_generator (id, namespace_id, universal_id, next_entity_identifier, universal_id_type, actor_id) VALUES (nextval('om_number_generator_id_seq'), 'IHE_OM_OF', '%ROOT_OID%.3', 1, 'ISO', 2);
INSERT INTO om_number_generator (id, namespace_id, universal_id, next_entity_identifier, universal_id_type, actor_id) VALUES (nextval('om_number_generator_id_seq'), 'IHE_OM_AM', '%ROOT_OID%.6', 1, 'ISO', 3);
INSERT INTO om_number_generator (id, namespace_id, universal_id, next_entity_identifier, universal_id_type, actor_id) VALUES (nextval('om_number_generator_id_seq'), 'IHE_OM_ANALYZER_MGR', '%ROOT_OID%.4', 1, 'ISO', 6);
INSERT INTO om_number_generator (id, namespace_id, universal_id, next_entity_identifier, universal_id_type, actor_id) VALUES (nextval('om_number_generator_id_seq'), 'IHE_OM_ANALYZER', '%ROOT_OID%.5', 1, 'ISO', 4);
insert into om_number_generator (id, actor_id, namespace_id, next_entity_identifier, universal_id, universal_id_type) values (nextval('om_number_generator_id_seq'), (select id from tf_actor where keyword = 'LB'), 'IHE_OM_LB', 1, '%ROOT_OID%.7', 'ISO');
insert into om_number_generator (id, actor_id, namespace_id, next_entity_identifier, universal_id, universal_id_type) values (nextval('om_number_generator_id_seq'), (select id from tf_actor where keyword = 'LIP'), 'IHE_OM_LIP', 1,  '%ROOT_OID%.8', 'ISO');

INSERT INTO cmn_value_set (id, value_set_name, value_set_oid, usage, value_set_keyword) VALUES (nextval('cmn_value_set_id_seq'),'Patient class - HL7 Table 0004','1.3.6.1.4.1.21367.101.104','PV1-2','patientClass');
INSERT INTO cmn_value_set (id, value_set_name, value_set_oid, usage, value_set_keyword) VALUES (nextval('cmn_value_set_id_seq'),'Physician ID - HL7 Table 0001','1.3.6.1.4.1.21367.101.110','HL7: PV1-8 ORC-10 ORC-11 ORC-12 OBR-16 OBR-28 OBR-34 OBX-16 -- DICOM: (0032,1032)  (0008,0090)  (0040,2008) (0040, 0006)','doctor');
INSERT INTO cmn_value_set (id, value_set_name, value_set_oid, usage, value_set_keyword) VALUES (nextval('cmn_value_set_id_seq'),'Priority','1.3.6.1.4.1.21367.101.116','ORC-7-6 (RAD/EYE), OBR-27-6 (RAD/EYE), TQ1-9 (LAB)','quantityTiming');
INSERT INTO cmn_value_set (id, value_set_name, value_set_oid, usage, value_set_keyword) VALUES (nextval('cmn_value_set_id_seq'),'Entering Organization - HL7 Table IHE005','1.3.6.1.4.1.21367.101.117','HL7: ORC-17 OBX-23 -- DICOM: (0040,2009)','enteringOrganization');
INSERT INTO cmn_value_set (id, value_set_name, value_set_oid, usage, value_set_keyword) VALUES (nextval('cmn_value_set_id_seq'),'Ordering Codes - Universal Service ID (RAD) ','1.3.6.1.4.1.21367.101.118','OBR-4','radUniversalServiceId');
INSERT INTO cmn_value_set (id, value_set_name, value_set_oid, usage, value_set_keyword) VALUES (nextval('cmn_value_set_id_seq'),'Danger Code - HL7 table IHE007','1.3.6.1.4.1.21367.101.119','HL7: OBR-12 -- DICOM: (0038,0500)','danger');
INSERT INTO cmn_value_set (id, value_set_name, value_set_oid, usage, value_set_keyword) VALUES (nextval('cmn_value_set_id_seq'),'Transportation mode code - HL7 Table 0124','1.3.6.1.4.1.21367.101.120','OBR-30','transportationMode');
INSERT INTO cmn_value_set (id, value_set_name, value_set_oid, usage, value_set_keyword) VALUES (nextval('cmn_value_set_id_seq'),'Transport arranged - HL7 Table 0224','1.3.6.1.4.1.21367.101.121','HL7: OBR-41 -- DICOM: (0040,1004)','transportArranged');
INSERT INTO cmn_value_set (id, value_set_name, value_set_oid, usage, value_set_keyword) VALUES (nextval('cmn_value_set_id_seq'),'Ordering Codes - Universal Service ID (CARD) ','1.3.6.1.4.1.21367.101.118','OBR-4','cardUniversalServiceId');
INSERT INTO cmn_value_set (id, value_set_name, value_set_oid, usage, value_set_keyword) VALUES (nextval('cmn_value_set_id_seq'),'Ordering Codes - Universal Service ID (EYE) ','1.3.6.1.4.1.12559.11.4.2.26','OBR-4','eyeUniversalServiceId');
INSERT INTO cmn_value_set (id, value_set_name, value_set_oid, usage, value_set_keyword) VALUES (nextval('cmn_value_set_id_seq'),'patUniversalServiceId','1.3.6.1.4.1.21367.101.118','PAT domain is not yet implemented','patUniversalServiceId');
INSERT INTO cmn_value_set (id, value_set_name, value_set_oid, usage, value_set_keyword) VALUES (nextval('cmn_value_set_id_seq'),'Ordering Codes - Universal Service ID (LAB) ','1.3.6.1.4.1.12559.11.4.2.15','OBR-4','labUniversalServiceId');
INSERT INTO cmn_value_set (id, value_set_name, value_set_oid, usage, value_set_keyword) VALUES (nextval('cmn_value_set_id_seq'),'Specimen Source/Specimen type','2.16.840.1.114222.4.11.3020','OBR-15 SPM-4','specimenSource');
INSERT INTO cmn_value_set (id, value_set_name, value_set_oid, usage, value_set_keyword) VALUES (nextval('cmn_value_set_id_seq'),'Acquisition Modality Codes','1.3.6.1.4.1.21367.101.9','HL7: OBR-24 (RAD/EYE) -- DICOM: (0008,0060) ','modality');
INSERT INTO cmn_value_set (id, value_set_name, value_set_oid, usage, value_set_keyword) VALUES (nextval('cmn_value_set_id_seq'),'Specimen Source/Specimen type','2.16.840.1.114222.4.11.3020','OBR-15 SPM-4','specimenType');
INSERT INTO cmn_value_set (id, value_set_name, value_set_oid, usage, value_set_keyword) VALUES (nextval('cmn_value_set_id_seq'),'Specimen Collection Method - HL7 Table 0488','2.16.840.1.113883.12.488','SPM-7','collectionMethod');
INSERT INTO cmn_value_set (id, value_set_name, value_set_oid, usage, value_set_keyword) VALUES (nextval('cmn_value_set_id_seq'),'Specimen Role - HL7 Table 0369','1.3.6.1.4.1.12559.11.4.2.14','SPM-11','specimenRole');
INSERT INTO cmn_value_set (id, value_set_name, value_set_oid, usage, value_set_keyword) VALUES (nextval('cmn_value_set_id_seq'),'Risk Code - HL7 Table 0489','2.16.840.1.113883.12.489','SPM-16','riskCode');
INSERT INTO cmn_value_set (id, value_set_name, value_set_oid, usage, value_set_keyword) VALUES (nextval('cmn_value_set_id_seq'),'Diagnostic Section Service ID (subset for LAB)','1.3.6.1.4.1.12559.11.4.2.16','OBR-24','diagnosticService');
INSERT INTO cmn_value_set (id, value_set_name, value_set_oid, usage, value_set_keyword) VALUES (nextval('cmn_value_set_id_seq'),'Value type - HL7 Table 0125','2.16.840.1.113883.12.125','OBX-2','valueType');
INSERT INTO cmn_value_set (id, value_set_name, value_set_oid, usage, value_set_keyword) VALUES (nextval('cmn_value_set_id_seq'),'Abnormal flags - HL7 Table 0078','2.16.840.1.113883.12.78','OBX-8','abnormalFlag');
INSERT INTO cmn_value_set (id, value_set_name, value_set_oid, usage, value_set_keyword) VALUES (nextval('cmn_value_set_id_seq'),'Observation result status code interpretation - HL7 Table 0125','2.16.840.1.113883.12.85','OBX-11','resultStatus');
INSERT INTO cmn_value_set (id, value_set_name, value_set_oid, usage, value_set_keyword) VALUES (nextval('cmn_value_set_id_seq'),'Source of comment (defined in LAB TF)','1.3.6.1.4.1.12559.11.4.2.19','NTE-2','sourceOfComment');
INSERT INTO cmn_value_set (id, value_set_name, value_set_oid, usage, value_set_keyword) VALUES (nextval('cmn_value_set_id_seq'),'Comment Type (defined in LAB TF)','1.3.6.1.4.1.12559.11.4.2.20','NTE-4','commentType');
INSERT INTO cmn_value_set (id, value_set_name, value_set_oid, usage, value_set_keyword) VALUES (nextval('cmn_value_set_id_seq'),'access check','1.3.6.1.4.1.12559.11.4.2.20','OBX-13','accessCheck');
INSERT INTO cmn_value_set (id, value_set_name, value_set_oid, usage, value_set_keyword) VALUES (nextval('cmn_value_set_id_seq'),'Observation identifier (related to order) ','1.3.6.1.4.1.12559.11.4.2.21','OBX-3 (in OBX segment related to an OBR segment) ','orderObservationIdentifier');
INSERT INTO cmn_value_set (id, value_set_name, value_set_oid, usage, value_set_keyword) VALUES (nextval('cmn_value_set_id_seq'),'Observation identifier (related to specimen) ','1.3.6.1.4.1.12559.11.4.2.22','OBX-3 (in OBX segment related to a SPM segment)','specimenObservationIdentifier');
INSERT INTO cmn_value_set (id, value_set_name, value_set_oid, usage, value_set_keyword) VALUES (nextval('cmn_value_set_id_seq'),'Result status (sub set of HL7 Table 0123)','1.3.6.1.4.1.12559.11.4.2.23','OBR-25','orderResultStatus');
INSERT INTO cmn_value_set (id, value_set_name, value_set_oid, usage, value_set_keyword) VALUES (nextval('cmn_value_set_id_seq'),'Order status (sub set of HL7 Table 0038)','1.3.6.1.4.1.12559.11.4.2.24','ORC-5','orderStatus');
INSERT INTO cmn_value_set (id, value_set_name, value_set_oid, usage, value_set_keyword) VALUES (nextval('cmn_value_set_id_seq'),'UCUM - Unified Code for Units of Measure','2.16.840.1.114222.4.11.838','OBX','units');
INSERT INTO cmn_value_set (id, usage, value_set_keyword, value_set_name, value_set_oid) VALUES (nextval('cmn_value_set_id_seq'), 'PID', 'RACE', 'Race', '1.3.6.1.4.1.21367.101.102');
INSERT INTO cmn_value_set (id, usage, value_set_keyword, value_set_name, value_set_oid) VALUES (nextval('cmn_value_set_id_seq'), 'PID', 'RELIGION', 'Religion', '1.3.6.1.4.1.21367.101.122');
INSERT INTO cmn_value_set (id, usage, value_set_keyword, value_set_name, value_set_oid) VALUES (nextval('cmn_value_set_id_seq'), 'PID-8', 'HL70001', 'Administrative Sex', '1.3.6.1.4.1.21367.101.101');
insert into cmn_value_set (id, value_set_name, value_set_keyword, usage, value_set_oid) VALUES (nextval('cmn_value_set_id_seq'), 'Observation type', 'OBS_TYPE', 'OBX-29 (LAW)', '1.3.6.1.4.1.12559.11.4.2.28');
insert into cmn_value_set (id, value_set_name, value_set_keyword, usage, value_set_oid) VALUES (nextval('cmn_value_set_id_seq'), 'Quantity/Timing (LAW)', 'quantityTiming4law', 'TQ1-9', '1.3.6.1.4.1.12559.11.4.2.29');
INSERT INTO cmn_value_set (id, value_set_keyword, value_set_name, value_set_oid, usage) VALUES (nextval('cmn_value_set_id_seq'), 'ADMITTING_DIAG', 'Admitting Diagnosis', '1.3.6.1.4.1.213376.101.210', '0008,1084');

INSERT INTO validation_parameters (id, validator_type, domain_id, message_type, profile_oid, actor_id, transaction_id) VALUES (nextval('ort_validation_parameters_id_seq'), 0, (select id from tf_domain where keyword = 'RAD'), 'ORM^O01^ORM_O01 (NW)', '1.3.6.1.4.12559.11.1.1.103', (select id from tf_actor where keyword = 'OP'),           (select id from tf_transaction where keyword = 'RAD-2'));
INSERT INTO validation_parameters (id, validator_type, domain_id, message_type, profile_oid, actor_id, transaction_id) VALUES (nextval('ort_validation_parameters_id_seq'), 0, (select id from tf_domain where keyword = 'RAD'), 'ORM^O01^ORM_O01 (CA)', '1.3.6.1.4.12559.11.1.1.104', (select id from tf_actor where keyword = 'OP'),           (select id from tf_transaction where keyword = 'RAD-2'));
INSERT INTO validation_parameters (id, validator_type, domain_id, message_type, profile_oid, actor_id, transaction_id) VALUES (nextval('ort_validation_parameters_id_seq'), 0, (select id from tf_domain where keyword = 'RAD'), 'ORR^O02^ORR_O02', '1.3.6.1.4.12559.11.1.1.105',      (select id from tf_actor where keyword = 'OP'),           (select id from tf_transaction where keyword = 'RAD-3'));
INSERT INTO validation_parameters (id, validator_type, domain_id, message_type, profile_oid, actor_id, transaction_id) VALUES (nextval('ort_validation_parameters_id_seq'), 0, (select id from tf_domain where keyword = 'RAD'), 'ORM^O01^ORM_O01 (SN)', '1.3.6.1.4.12559.11.1.1.9',   (select id from tf_actor where keyword = 'OF'),           (select id from tf_transaction where keyword = 'RAD-3'));
INSERT INTO validation_parameters (id, validator_type, domain_id, message_type, profile_oid, actor_id, transaction_id) VALUES (nextval('ort_validation_parameters_id_seq'), 0, (select id from tf_domain where keyword = 'RAD'), 'ORM^O01^ORM_O01 (SC)', '1.3.6.1.4.12559.11.1.1.8',   (select id from tf_actor where keyword = 'OF'),           (select id from tf_transaction where keyword = 'RAD-3'));
INSERT INTO validation_parameters (id, validator_type, domain_id, message_type, profile_oid, actor_id, transaction_id) VALUES (nextval('ort_validation_parameters_id_seq'), 0, (select id from tf_domain where keyword = 'RAD'), 'ORM^O01^ORM_O01 (OC)', '1.3.6.1.4.12559.11.1.1.7',   (select id from tf_actor where keyword = 'OF'),           (select id from tf_transaction where keyword = 'RAD-3'));
INSERT INTO validation_parameters (id, validator_type, domain_id, message_type, profile_oid, actor_id, transaction_id) VALUES (nextval('ort_validation_parameters_id_seq'), 0, (select id from tf_domain where keyword = 'RAD'), 'ACK^ALL^ACK', '1.3.6.1.4.12559.11.1.1.21',           (select id from tf_actor where keyword = 'OF'),           (select id from tf_transaction where keyword = 'RAD-2'));
INSERT INTO validation_parameters (id, validator_type, domain_id, message_type, profile_oid, actor_id, transaction_id) VALUES (nextval('ort_validation_parameters_id_seq'), 0, (select id from tf_domain where keyword = 'RAD'), 'ACK^ALL^ACK', '1.3.6.1.4.12559.11.1.1.21',           (select id from tf_actor where keyword = 'OP'),           (select id from tf_transaction where keyword = 'RAD-3'));
INSERT INTO validation_parameters (id, validator_type, domain_id, message_type, profile_oid, actor_id, transaction_id) VALUES (nextval('ort_validation_parameters_id_seq'), 0, (select id from tf_domain where keyword = 'LAB'), 'OML^O21^OML_O21', '1.3.6.1.4.12559.11.1.1.106',      (select id from tf_actor where keyword = 'OP'),           (select id from tf_transaction where keyword = 'LAB-1'));
INSERT INTO validation_parameters (id, validator_type, domain_id, message_type, profile_oid, actor_id, transaction_id) VALUES (nextval('ort_validation_parameters_id_seq'), 0, (select id from tf_domain where keyword = 'LAB'), 'OML^O33^OML_O33', '1.3.6.1.4.12559.11.1.1.107',      (select id from tf_actor where keyword = 'OP'),           (select id from tf_transaction where keyword = 'LAB-1'));
INSERT INTO validation_parameters (id, validator_type, domain_id, message_type, profile_oid, actor_id, transaction_id) VALUES (nextval('ort_validation_parameters_id_seq'), 0, (select id from tf_domain where keyword = 'LAB'), 'OML^O35^OML_O35', '1.3.6.1.4.12559.11.1.1.108',      (select id from tf_actor where keyword = 'OP'),           (select id from tf_transaction where keyword = 'LAB-1'));
INSERT INTO validation_parameters (id, validator_type, domain_id, message_type, profile_oid, actor_id, transaction_id) VALUES (nextval('ort_validation_parameters_id_seq'), 0, (select id from tf_domain where keyword = 'LAB'), 'OML^O21^OML_O21', '1.3.6.1.4.12559.11.1.1.15',       (select id from tf_actor where keyword = 'OF'),           (select id from tf_transaction where keyword = 'LAB-2'));
INSERT INTO validation_parameters (id, validator_type, domain_id, message_type, profile_oid, actor_id, transaction_id) VALUES (nextval('ort_validation_parameters_id_seq'), 0, (select id from tf_domain where keyword = 'LAB'), 'OML^O33^OML_O33', '1.3.6.1.4.12559.11.1.1.16',       (select id from tf_actor where keyword = 'OF'),           (select id from tf_transaction where keyword = 'LAB-2'));
INSERT INTO validation_parameters (id, validator_type, domain_id, message_type, profile_oid, actor_id, transaction_id) VALUES (nextval('ort_validation_parameters_id_seq'), 0, (select id from tf_domain where keyword = 'LAB'), 'OML^O35^OML_O35', '1.3.6.1.4.12559.11.1.1.17',       (select id from tf_actor where keyword = 'OF'),           (select id from tf_transaction where keyword = 'LAB-2'));
INSERT INTO validation_parameters (id, validator_type, domain_id, message_type, profile_oid, actor_id, transaction_id) VALUES (nextval('ort_validation_parameters_id_seq'), 0, (select id from tf_domain where keyword = 'LAB'), 'ORL^O22^ORL_O22', '1.3.6.1.4.12559.11.1.1.14',       (select id from tf_actor where keyword = 'OF'),           (select id from tf_transaction where keyword = 'LAB-1'));
INSERT INTO validation_parameters (id, validator_type, domain_id, message_type, profile_oid, actor_id, transaction_id) VALUES (nextval('ort_validation_parameters_id_seq'), 0, (select id from tf_domain where keyword = 'LAB'), 'ORL^O34^ORL_O34', '1.3.6.1.4.12559.11.1.1.12',       (select id from tf_actor where keyword = 'OF'),           (select id from tf_transaction where keyword = 'LAB-1'));
INSERT INTO validation_parameters (id, validator_type, domain_id, message_type, profile_oid, actor_id, transaction_id) VALUES (nextval('ort_validation_parameters_id_seq'), 0, (select id from tf_domain where keyword = 'LAB'), 'ORL^O36^ORL_O36', '1.3.6.1.4.12559.11.1.1.13',       (select id from tf_actor where keyword = 'OF'),           (select id from tf_transaction where keyword = 'LAB-1'));
INSERT INTO validation_parameters (id, validator_type, domain_id, message_type, profile_oid, actor_id, transaction_id) VALUES (nextval('ort_validation_parameters_id_seq'), 0, (select id from tf_domain where keyword = 'LAB'), 'ORL^O22^ORL_O22', '1.3.6.1.4.12559.11.1.1.111',      (select id from tf_actor where keyword = 'OP'),           (select id from tf_transaction where keyword = 'LAB-2'));
INSERT INTO validation_parameters (id, validator_type, domain_id, message_type, profile_oid, actor_id, transaction_id) VALUES (nextval('ort_validation_parameters_id_seq'), 0, (select id from tf_domain where keyword = 'LAB'), 'ORL^O34^ORL_O34', '1.3.6.1.4.12559.11.1.1.109',      (select id from tf_actor where keyword = 'OP'),           (select id from tf_transaction where keyword = 'LAB-2'));
INSERT INTO validation_parameters (id, validator_type, domain_id, message_type, profile_oid, actor_id, transaction_id) VALUES (nextval('ort_validation_parameters_id_seq'), 0, (select id from tf_domain where keyword = 'LAB'), 'ORL^O36^ORL_O36', '1.3.6.1.4.12559.11.1.1.110',      (select id from tf_actor where keyword = 'OP'),           (select id from tf_transaction where keyword = 'LAB-2'));
INSERT INTO validation_parameters (id, validator_type, domain_id, message_type, profile_oid, actor_id, transaction_id) VALUES (nextval('ort_validation_parameters_id_seq'), 0, (select id from tf_domain where keyword = 'LAB'), 'OML^O21^OML_O21', '1.3.6.1.4.12559.11.1.1.1',        (select id from tf_actor where keyword = 'OF'),           (select id from tf_transaction where keyword = 'LAB-4'));
INSERT INTO validation_parameters (id, validator_type, domain_id, message_type, profile_oid, actor_id, transaction_id) VALUES (nextval('ort_validation_parameters_id_seq'), 0, (select id from tf_domain where keyword = 'LAB'), 'OML^O33^OML_O33', '1.3.6.1.4.12559.11.1.1.2',        (select id from tf_actor where keyword = 'OF'),           (select id from tf_transaction where keyword = 'LAB-4'));
INSERT INTO validation_parameters (id, validator_type, domain_id, message_type, profile_oid, actor_id, transaction_id) VALUES (nextval('ort_validation_parameters_id_seq'), 0, (select id from tf_domain where keyword = 'LAB'), 'OML^O35^OML_O35', '1.3.6.1.4.12559.11.1.1.3',        (select id from tf_actor where keyword = 'OF'),           (select id from tf_transaction where keyword = 'LAB-4'));
INSERT INTO validation_parameters (id, validator_type, domain_id, message_type, profile_oid, actor_id, transaction_id) VALUES (nextval('ort_validation_parameters_id_seq'), 0, (select id from tf_domain where keyword = 'LAB'), 'ORL^O22^ORL_O22', '1.3.6.1.4.12559.11.1.1.95',       (select id from tf_actor where keyword = 'AM'),           (select id from tf_transaction where keyword = 'LAB-4'));
INSERT INTO validation_parameters (id, validator_type, domain_id, message_type, profile_oid, actor_id, transaction_id) VALUES (nextval('ort_validation_parameters_id_seq'), 0, (select id from tf_domain where keyword = 'LAB'), 'ORL^O34^ORL_O34', '1.3.6.1.4.12559.11.1.1.93',       (select id from tf_actor where keyword = 'AM'),           (select id from tf_transaction where keyword = 'LAB-4'));
INSERT INTO validation_parameters (id, validator_type, domain_id, message_type, profile_oid, actor_id, transaction_id) VALUES (nextval('ort_validation_parameters_id_seq'), 0, (select id from tf_domain where keyword = 'LAB'), 'ORL^O36^ORL_O36', '1.3.6.1.4.12559.11.1.1.94',       (select id from tf_actor where keyword = 'AM'),           (select id from tf_transaction where keyword = 'LAB-4'));
INSERT INTO validation_parameters (id, validator_type, domain_id, message_type, profile_oid, actor_id, transaction_id) VALUES (nextval('ort_validation_parameters_id_seq'), 0, (select id from tf_domain where keyword = 'LAB'), 'ACK^ALL^ACK', '1.3.6.1.4.12559.11.1.1.22',           (select id from tf_actor where keyword = 'ORT'),          (select id from tf_transaction where keyword = 'LAB-3'));
INSERT INTO validation_parameters (id, validator_type, domain_id, message_type, profile_oid, actor_id, transaction_id) VALUES (nextval('ort_validation_parameters_id_seq'), 0, (select id from tf_domain where keyword = 'LAB'), 'OUL^R22^OUL_R22', '1.3.6.1.4.12559.11.1.1.18',       (select id from tf_actor where keyword = 'OF'),           (select id from tf_transaction where keyword = 'LAB-3'));
INSERT INTO validation_parameters (id, validator_type, domain_id, message_type, profile_oid, actor_id, transaction_id) VALUES (nextval('ort_validation_parameters_id_seq'), 0, (select id from tf_domain where keyword = 'LAB'), 'ORU^R01^ORU_R01', '1.3.6.1.4.12559.11.1.1.20',       (select id from tf_actor where keyword = 'OF'),           (select id from tf_transaction where keyword = 'LAB-3'));
INSERT INTO validation_parameters (id, validator_type, domain_id, message_type, profile_oid, actor_id, transaction_id) VALUES (nextval('ort_validation_parameters_id_seq'), 0, (select id from tf_domain where keyword = 'LAB'), 'OUL^R22^OUL_R22', '1.3.6.1.4.12559.11.1.1.96',       (select id from tf_actor where keyword = 'AM'),           (select id from tf_transaction where keyword = 'LAB-5'));
INSERT INTO validation_parameters (id, validator_type, domain_id, message_type, profile_oid, actor_id, transaction_id) VALUES (nextval('ort_validation_parameters_id_seq'), 0, (select id from tf_domain where keyword = 'LAB'), 'OUL^R23^OUL_R23', '1.3.6.1.4.12559.11.1.1.97',       (select id from tf_actor where keyword = 'AM'),           (select id from tf_transaction where keyword = 'LAB-5'));
INSERT INTO validation_parameters (id, validator_type, domain_id, message_type, profile_oid, actor_id, transaction_id) VALUES (nextval('ort_validation_parameters_id_seq'), 0, (select id from tf_domain where keyword = 'LAB'), 'ACK^ALL^ACK', '1.3.6.1.4.12559.11.1.1.6',            (select id from tf_actor where keyword = 'OF'),           (select id from tf_transaction where keyword = 'LAB-5'));
INSERT INTO validation_parameters (id, validator_type, domain_id, message_type, profile_oid, actor_id, transaction_id) VALUES (nextval('ort_validation_parameters_id_seq'), 0, (select id from tf_domain where keyword = 'LAB'), 'QBP^Q11^QBP_Q11', '1.3.6.1.4.12559.11.1.1.180',      (select id from tf_actor where keyword = 'ANALYZER'),     (select id from tf_transaction where keyword = 'LAB-27'));
INSERT INTO validation_parameters (id, validator_type, domain_id, message_type, profile_oid, actor_id, transaction_id) VALUES (nextval('ort_validation_parameters_id_seq'), 0, (select id from tf_domain where keyword = 'LAB'), 'ORL^O34^ORL_O34', '1.3.6.1.4.12559.11.1.1.181',      (select id from tf_actor where keyword = 'ANALYZER'),     (select id from tf_transaction where keyword = 'LAB-28'));
INSERT INTO validation_parameters (id, validator_type, domain_id, message_type, profile_oid, actor_id, transaction_id) VALUES (nextval('ort_validation_parameters_id_seq'), 0, (select id from tf_domain where keyword = 'LAB'), 'OUL^R22^OUL_R22', '1.3.6.1.4.12559.11.1.1.182',      (select id from tf_actor where keyword = 'ANALYZER'),     (select id from tf_transaction where keyword = 'LAB-29'));
INSERT INTO validation_parameters (id, validator_type, domain_id, message_type, profile_oid, actor_id, transaction_id) VALUES (nextval('ort_validation_parameters_id_seq'), 0, (select id from tf_domain where keyword = 'LAB'), 'RSP^K11^RSP_K11', '1.3.6.1.4.12559.11.1.1.177',      (select id from tf_actor where keyword = 'ANALYZER_MGR'), (select id from tf_transaction where keyword = 'LAB-27'));
INSERT INTO validation_parameters (id, validator_type, domain_id, message_type, profile_oid, actor_id, transaction_id) VALUES (nextval('ort_validation_parameters_id_seq'), 0, (select id from tf_domain where keyword = 'LAB'), 'OML^O33^OML_O33', '1.3.6.1.4.12559.11.1.1.178',      (select id from tf_actor where keyword = 'ANALYZER_MGR'), (select id from tf_transaction where keyword = 'LAB-28'));
INSERT INTO validation_parameters (id, validator_type, domain_id, message_type, profile_oid, actor_id, transaction_id) VALUES (nextval('ort_validation_parameters_id_seq'), 0, (select id from tf_domain where keyword = 'LAB'), 'ACK^R22^ACK', '1.3.6.1.4.12559.11.1.1.179',          (select id from tf_actor where keyword = 'ANALYZER_MGR'), (select id from tf_transaction where keyword = 'LAB-29'));
INSERT INTO validation_parameters (id, validator_type, domain_id, message_type, profile_oid, actor_id, transaction_id) VALUES (nextval('ort_validation_parameters_id_seq'), 0, (select id from tf_domain where keyword = 'LAB'), 'ORM^O01^ORM_O01', '1.3.6.1.4.12559.11.1.1.10',       (select id from tf_actor where keyword = 'OF'),           (select id from tf_transaction where keyword = 'RAD-4'));
INSERT INTO validation_parameters (id, validator_type, domain_id, message_type, profile_oid, actor_id, transaction_id) VALUES (nextval('ort_validation_parameters_id_seq'), 0, (select id from tf_domain where keyword = 'LAB'), 'ORM^O01^ORM_O01', '1.3.6.1.4.12559.11.1.1.185',      (select id from tf_actor where keyword = 'OF'),           (select id from tf_transaction where keyword = 'RAD-13'));
INSERT INTO validation_parameters (id, validator_type, domain_id, message_type, profile_oid, actor_id, transaction_id) VALUES (nextval('ort_validation_parameters_id_seq'), 0, (select id from tf_domain where keyword = 'RAD'), 'ACK^O01^ACK', '1.3.6.1.4.12559.11.1.1.183',          (select id from tf_actor where keyword = 'IM'),           (select id from tf_transaction where keyword = 'RAD-4'));
INSERT INTO validation_parameters (id, validator_type, domain_id, message_type, profile_oid, actor_id, transaction_id) VALUES (nextval('ort_validation_parameters_id_seq'), 0, (select id from tf_domain where keyword = 'RAD'), 'ACK^O01^ACK', '1.3.6.1.4.12559.11.1.1.184',          (select id from tf_actor where keyword = 'IM'),           (select id from tf_transaction where keyword = 'RAD-13'));
INSERT INTO validation_parameters (id, validator_type, domain_id, message_type, profile_oid, actor_id, transaction_id) VALUES (nextval('ort_validation_parameters_id_seq'), 0, (select id from tf_domain where keyword = 'LAB'), 'OML^O21^OML_O21', '1.3.6.1.4.12559.11.1.1.186',      (select id from tf_actor where keyword = 'OF'),           (select id from tf_transaction where keyword = 'LAB-1'));
INSERT INTO validation_parameters (id, validator_type, domain_id, message_type, profile_oid, actor_id, transaction_id) VALUES (nextval('ort_validation_parameters_id_seq'), 0, (select id from tf_domain where keyword = 'LAB'), 'OML^O33^OML_O33', '1.3.6.1.4.12559.11.1.1.187',      (select id from tf_actor where keyword = 'OF'),           (select id from tf_transaction where keyword = 'LAB-1'));
INSERT INTO validation_parameters (id, validator_type, domain_id, message_type, profile_oid, actor_id, transaction_id) VALUES (nextval('ort_validation_parameters_id_seq'), 0, (select id from tf_domain where keyword = 'LAB'), 'OML^O35^OML_O35', '1.3.6.1.4.12559.11.1.1.188',      (select id from tf_actor where keyword = 'OF'),           (select id from tf_transaction where keyword = 'LAB-1'));
INSERT INTO validation_parameters (id, validator_type, domain_id, message_type, profile_oid, actor_id, transaction_id) VALUES (nextval('ort_validation_parameters_id_seq'), 0, (select id from tf_domain where keyword = 'LAB'), 'ORL^O22^ORL_O22', '1.3.6.1.4.12559.11.1.1.189',      (select id from tf_actor where keyword = 'OP'),           (select id from tf_transaction where keyword = 'LAB-1'));
INSERT INTO validation_parameters (id, validator_type, domain_id, message_type, profile_oid, actor_id, transaction_id) VALUES (nextval('ort_validation_parameters_id_seq'), 0, (select id from tf_domain where keyword = 'LAB'), 'ORL^O34^ORL_O34', '1.3.6.1.4.12559.11.1.1.190',      (select id from tf_actor where keyword = 'OP'),           (select id from tf_transaction where keyword = 'LAB-1'));
INSERT INTO validation_parameters (id, validator_type, domain_id, message_type, profile_oid, actor_id, transaction_id) VALUES (nextval('ort_validation_parameters_id_seq'), 0, (select id from tf_domain where keyword = 'LAB'), 'ORL^O36^ORL_O36', '1.3.6.1.4.12559.11.1.1.191',      (select id from tf_actor where keyword = 'OP'),           (select id from tf_transaction where keyword = 'LAB-2'));
INSERT INTO validation_parameters (id, validator_type, domain_id, message_type, profile_oid, actor_id, transaction_id) VALUES (nextval('ort_validation_parameters_id_seq'), 0, (select id from tf_domain where keyword = 'EYE'), 'ORM^O01^ORM_O01 (NW)', '1.3.6.1.4.12559.11.1.1.194', (select id from tf_actor where keyword = 'OP'),           (select id from tf_transaction where keyword = 'RAD-2'));
INSERT INTO validation_parameters (id, validator_type, domain_id, message_type, profile_oid, actor_id, transaction_id) VALUES (nextval('ort_validation_parameters_id_seq'), 0, (select id from tf_domain where keyword = 'EYE'), 'ORM^O01^ORM_O01 (CA)', '1.3.6.1.4.12559.11.1.1.195', (select id from tf_actor where keyword = 'OP'),           (select id from tf_transaction where keyword = 'RAD-2'));
INSERT INTO validation_parameters (id, validator_type, domain_id, message_type, profile_oid, actor_id, transaction_id) VALUES (nextval('ort_validation_parameters_id_seq'), 0, (select id from tf_domain where keyword = 'EYE'), 'ORR^O02^ORR_O02', '1.3.6.1.4.12559.11.1.1.196',      (select id from tf_actor where keyword = 'OP'),           (select id from tf_transaction where keyword = 'RAD-3'));
INSERT INTO validation_parameters (id, validator_type, domain_id, message_type, profile_oid, actor_id, transaction_id) VALUES (nextval('ort_validation_parameters_id_seq'), 0, (select id from tf_domain where keyword = 'EYE'), 'ORM^O01^ORM_O01 (SN)', '1.3.6.1.4.12559.11.1.1.197', (select id from tf_actor where keyword = 'OF'),           (select id from tf_transaction where keyword = 'RAD-3'));
INSERT INTO validation_parameters (id, validator_type, domain_id, message_type, profile_oid, actor_id, transaction_id) VALUES (nextval('ort_validation_parameters_id_seq'), 0, (select id from tf_domain where keyword = 'EYE'), 'ORM^O01^ORM_O01 (SC)', '1.3.6.1.4.12559.11.1.1.198', (select id from tf_actor where keyword = 'OF'),           (select id from tf_transaction where keyword = 'RAD-3'));
INSERT INTO validation_parameters (id, validator_type, domain_id, message_type, profile_oid, actor_id, transaction_id) VALUES (nextval('ort_validation_parameters_id_seq'), 0, (select id from tf_domain where keyword = 'EYE'), 'ORM^O01^ORM_O01 (OC)', '1.3.6.1.4.12559.11.1.1.199', (select id from tf_actor where keyword = 'OF'),           (select id from tf_transaction where keyword = 'RAD-3'));
INSERT INTO validation_parameters (id, validator_type, domain_id, message_type, profile_oid, actor_id, transaction_id) VALUES (nextval('ort_validation_parameters_id_seq'), 0, (select id from tf_domain where keyword = 'EYE'), 'ACK^ALL^ACK', '1.3.6.1.4.12559.11.1.1.200',          (select id from tf_actor where keyword = 'OF'),           (select id from tf_transaction where keyword = 'RAD-2'));
INSERT INTO validation_parameters (id, validator_type, domain_id, message_type, profile_oid, actor_id, transaction_id) VALUES (nextval('ort_validation_parameters_id_seq'), 0, (select id from tf_domain where keyword = 'EYE'), 'ACK^ALL^ACK', '1.3.6.1.4.12559.11.1.1.200',          (select id from tf_actor where keyword = 'OP'),           (select id from tf_transaction where keyword = 'RAD-3'));
INSERT INTO validation_parameters (id, validator_type, domain_id, message_type, profile_oid, actor_id, transaction_id) VALUES (nextval('ort_validation_parameters_id_seq'), 0, (select id from tf_domain where keyword = 'RAD'), 'ADT^A01^ADT_A01', '1.3.6.1.4.12559.11.1.1.77',       (select id from tf_actor where keyword = 'ADT'),          (select id from tf_transaction where keyword = 'RAD-1'));
INSERT INTO validation_parameters (id, validator_type, domain_id, message_type, profile_oid, actor_id, transaction_id) VALUES (nextval('ort_validation_parameters_id_seq'), 0, (select id from tf_domain where keyword = 'RAD'), 'ADT^A04^ADT_A01', '1.3.6.1.4.12559.11.1.1.79',       (select id from tf_actor where keyword = 'ADT'),          (select id from tf_transaction where keyword = 'RAD-1'));
INSERT INTO validation_parameters (id, validator_type, domain_id, message_type, profile_oid, actor_id, transaction_id) VALUES (nextval('ort_validation_parameters_id_seq'), 0, (select id from tf_domain where keyword = 'RAD'), 'ADT^A40^ADT_A39', '1.3.6.1.4.12559.11.1.1.87',       (select id from tf_actor where keyword = 'ADT'),          (select id from tf_transaction where keyword = 'RAD-12'));
INSERT INTO validation_parameters (id, validator_type, domain_id, message_type, profile_oid, actor_id, transaction_id) VALUES (nextval('ort_validation_parameters_id_seq'), 0, (select id from tf_domain where keyword = 'RAD'), 'ADT^A08^ADT_A01', '1.3.6.1.4.12559.11.1.1.82',       (select id from tf_actor where keyword = 'ADT'),          (select id from tf_transaction where keyword = 'RAD-12'));
INSERT INTO validation_parameters (id, validator_type, domain_id, message_type, profile_oid, actor_id, transaction_id) VALUES (nextval('ort_validation_parameters_id_seq'), 0, (select id from tf_domain where keyword = 'RAD'), 'ACK^ALL^ACK', '1.3.6.1.4.12559.11.1.1.148',          (select id from tf_actor where keyword = 'OF'),           (select id from tf_transaction where keyword = 'RAD-1'));
INSERT INTO validation_parameters (id, validator_type, domain_id, message_type, profile_oid, actor_id, transaction_id) VALUES (nextval('ort_validation_parameters_id_seq'), 0, (select id from tf_domain where keyword = 'RAD'), 'ACK^ALL^ACK', '1.3.6.1.4.12559.11.1.1.148',          (select id from tf_actor where keyword = 'OP'),           (select id from tf_transaction where keyword = 'RAD-1'));
INSERT INTO validation_parameters (id, validator_type, domain_id, message_type, profile_oid, actor_id, transaction_id) VALUES (nextval('ort_validation_parameters_id_seq'), 0, (select id from tf_domain where keyword = 'RAD'), 'ACK^ALL^ACK', '1.3.6.1.4.12559.11.1.1.150',          (select id from tf_actor where keyword = 'OF'),           (select id from tf_transaction where keyword = 'RAD-12'));
INSERT INTO validation_parameters (id, validator_type, domain_id, message_type, profile_oid, actor_id, transaction_id) VALUES (nextval('ort_validation_parameters_id_seq'), 0, (select id from tf_domain where keyword = 'RAD'), 'ACK^ALL^ACK', '1.3.6.1.4.12559.11.1.1.150',          (select id from tf_actor where keyword = 'OP'),           (select id from tf_transaction where keyword = 'RAD-12'));
INSERT INTO validation_parameters (id, validator_type, domain_id, message_type, profile_oid, actor_id, transaction_id) VALUES (nextval('ort_validation_parameters_id_seq'), 0, (select id from tf_domain where keyword = 'RAD'), 'ACK^ALL^ACK', '1.3.6.1.4.12559.11.1.1.150',          (select id from tf_actor where keyword = 'IM'),           (select id from tf_transaction where keyword = 'RAD-12'));
INSERT INTO validation_parameters (id, validator_type, domain_id, message_type, profile_oid, actor_id, transaction_id) VALUES (nextval('ort_validation_parameters_id_seq'), 0, (select id from tf_domain where keyword = 'LAB'), 'OML^O33^OML_O33 (DC)', '1.3.6.1.4.12559.11.1.1.215', (select id from tf_actor where keyword = 'ANALYZER_MGR'), (select id from tf_transaction where keyword = 'LAB-28'));
INSERT INTO validation_parameters (id, validator_type, domain_id, message_type, profile_oid, actor_id, transaction_id) VALUES (nextval('ort_validation_parameters_id_seq'), 0, (select id from tf_domain where keyword = 'RAD'), 'OMG^O19^OMG_O19 (NW)', '1.3.6.1.4.12559.11.1.1.205', (select id from tf_actor where keyword = 'OP'),           (select id from tf_transaction where keyword = 'RAD-2'));
INSERT INTO validation_parameters (id, validator_type, domain_id, message_type, profile_oid, actor_id, transaction_id) VALUES (nextval('ort_validation_parameters_id_seq'), 0, (select id from tf_domain where keyword = 'RAD'), 'OMG^O19^OMG_O19 (CA)', '1.3.6.1.4.12559.11.1.1.206', (select id from tf_actor where keyword = 'OP'),           (select id from tf_transaction where keyword = 'RAD-2'));
INSERT INTO validation_parameters (id, validator_type, domain_id, message_type, profile_oid, actor_id, transaction_id) VALUES (nextval('ort_validation_parameters_id_seq'), 0, (select id from tf_domain where keyword = 'RAD'), 'ORG^O20^ORG_O20', '1.3.6.1.4.12559.11.1.1.207',      (select id from tf_actor where keyword = 'OP'),           (select id from tf_transaction where keyword = 'RAD-3'));
INSERT INTO validation_parameters (id, validator_type, domain_id, message_type, profile_oid, actor_id, transaction_id) VALUES (nextval('ort_validation_parameters_id_seq'), 0, (select id from tf_domain where keyword = 'RAD'), 'OMG^O19^OMG_O19 (SN)', '1.3.6.1.4.12559.11.1.1.201', (select id from tf_actor where keyword = 'OF'),           (select id from tf_transaction where keyword = 'RAD-3'));
INSERT INTO validation_parameters (id, validator_type, domain_id, message_type, profile_oid, actor_id, transaction_id) VALUES (nextval('ort_validation_parameters_id_seq'), 0, (select id from tf_domain where keyword = 'RAD'), 'OMG^O19^OMG_O19 (SC)', '1.3.6.1.4.12559.11.1.1.202', (select id from tf_actor where keyword = 'OF'),           (select id from tf_transaction where keyword = 'RAD-3'));
INSERT INTO validation_parameters (id, validator_type, domain_id, message_type, profile_oid, actor_id, transaction_id) VALUES (nextval('ort_validation_parameters_id_seq'), 0, (select id from tf_domain where keyword = 'RAD'), 'OMG^O19^OMG_O19 (OC)', '1.3.6.1.4.12559.11.1.1.203', (select id from tf_actor where keyword = 'OF'),           (select id from tf_transaction where keyword = 'RAD-3'));
INSERT INTO validation_parameters (id, validator_type, domain_id, message_type, profile_oid, actor_id, transaction_id) VALUES (nextval('ort_validation_parameters_id_seq'), 0, (select id from tf_domain where keyword = 'RAD'), 'ACK^O19^ACK', '1.3.6.1.4.12559.11.1.1.204',          (select id from tf_actor where keyword = 'OF'),           (select id from tf_transaction where keyword = 'RAD-2'));
INSERT INTO validation_parameters (id, validator_type, domain_id, message_type, profile_oid, actor_id, transaction_id) VALUES (nextval('ort_validation_parameters_id_seq'), 0, (select id from tf_domain where keyword = 'RAD'), 'ACK^O19^ACK', '1.3.6.1.4.12559.11.1.1.208',          (select id from tf_actor where keyword = 'OP'),           (select id from tf_transaction where keyword = 'RAD-3'));
INSERT INTO validation_parameters (id, validator_type, domain_id, message_type, profile_oid, actor_id, transaction_id) VALUES (nextval('ort_validation_parameters_id_seq'), 0, (select id from tf_domain where keyword = 'RAD'), 'OMI^O23^OMI_O23', '1.3.6.1.4.12559.11.1.1.209',      (select id from tf_actor where keyword = 'OF'),           (select id from tf_transaction where keyword = 'RAD-4'));
INSERT INTO validation_parameters (id, validator_type, domain_id, message_type, profile_oid, actor_id, transaction_id) VALUES (nextval('ort_validation_parameters_id_seq'), 0, (select id from tf_domain where keyword = 'RAD'), 'OMI^O23^OMI_O23', '1.3.6.1.4.12559.11.1.1.210',      (select id from tf_actor where keyword = 'OF'),           (select id from tf_transaction where keyword = 'RAD-13'));
INSERT INTO validation_parameters (id, validator_type, domain_id, message_type, profile_oid, actor_id, transaction_id) VALUES (nextval('ort_validation_parameters_id_seq'), 0, (select id from tf_domain where keyword = 'RAD'), 'ACK^O23^ACK', '1.3.6.1.4.12559.11.1.1.211',          (select id from tf_actor where keyword = 'IM'),           (select id from tf_transaction where keyword = 'RAD-4'));
INSERT INTO validation_parameters (id, validator_type, domain_id, message_type, profile_oid, actor_id, transaction_id) VALUES (nextval('ort_validation_parameters_id_seq'), 0, (select id from tf_domain where keyword = 'RAD'), 'ACK^O23^ACK', '1.3.6.1.4.12559.11.1.1.212',          (select id from tf_actor where keyword = 'IM'),           (select id from tf_transaction where keyword = 'RAD-13'));
INSERT INTO validation_parameters (id, validator_type, domain_id, message_type, profile_oid, actor_id, transaction_id) VALUES (nextval('ort_validation_parameters_id_seq'), 0, (select id from tf_domain where keyword = 'RAD'), 'OMG^O19^OMG_O19 (DC)', '1.3.6.1.4.12559.11.1.1.206', (select id from tf_actor where keyword = 'OP'),           (select id from tf_transaction where keyword = 'RAD-2'));
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id, domain_id, validator_type) VALUES (nextval('ort_validation_parameters_id_seq'), 'OML^O33^OML_O33', '1.3.6.1.4.12559.11.1.1.136', (select id from tf_actor where keyword = 'LIP'), (select id from tf_transaction where keyword = 'LAB-61'),(select id from tf_domain where keyword = 'LAB'), 0);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id, domain_id, validator_type) VALUES (nextval('ort_validation_parameters_id_seq'), 'ORL^O34^ORL_O34', '1.3.6.1.4.12559.11.1.1.134', (select id from tf_actor where keyword = 'LB'), (select id from tf_transaction where keyword = 'LAB-61'),(select id from tf_domain where keyword = 'LAB'), 0);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id, domain_id, validator_type) VALUES (nextval('ort_validation_parameters_id_seq'), 'QBP^SLI^QBP_Q11', '1.3.6.1.4.12559.11.1.1.135', (select id from tf_actor where keyword = 'LB'), (select id from tf_transaction where keyword = 'LAB-62'),(select id from tf_domain where keyword = 'LAB'), 0);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id, domain_id, validator_type) VALUES (nextval('ort_validation_parameters_id_seq'), 'RSP^SLI^RSP_K11', '1.3.6.1.4.12559.11.1.1.137', (select id from tf_actor where keyword = 'LIP'), (select id from tf_transaction where keyword = 'LAB-62'),(select id from tf_domain where keyword = 'LAB'), 0);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id, domain_id, validator_type) VALUES (nextval('ort_validation_parameters_id_seq'), 'OML^O33^OML_O33', '1.3.6.1.4.12559.11.1.1.171', (select id from tf_actor where keyword = 'LB'), (select id from tf_transaction where keyword = 'LAB-63'),(select id from tf_domain where keyword = 'LAB'), 0);
INSERT INTO validation_parameters (id, message_type, profile_oid, actor_id, transaction_id, domain_id, validator_type) VALUES (nextval('ort_validation_parameters_id_seq'), 'ORL^O34^ORL_O34', '1.3.6.1.4.12559.11.1.1.172', (select id from tf_actor where keyword = 'LIP'), (select id from tf_transaction where keyword = 'LAB-63'),(select id from tf_domain where keyword = 'LAB'), 0);

INSERT INTO uid_generator (id, tag, root, last_index) VALUES (nextval('uid_generator_id_seq'), '(0020,000d)', '%ROOT_OID%.1.1.', 0);
INSERT INTO uid_generator (id, tag, root, last_index) VALUES (nextval('uid_generator_id_seq'), '(0040,1001)', 'OMRP', 0);
INSERT INTO uid_generator (id, tag, root, last_index) VALUES (nextval('uid_generator_id_seq'), '(0040,0009)', 'OMSPS', 0);
INSERT INTO uid_generator (id, tag, root, last_index) VALUES (nextval('uid_generator_id_seq'), '(0008,1155)', '%ROOT_OID%.1.2.', 0);

INSERT INTO cmn_home (id, iso3_language, home_title, main_content) VALUES (nextval('cmn_home_id_seq'), 'eng', 'Order Manager (configuration in progress)', '<p>This is a new instance of Order Manager. Log into and go to Administration &gt; Configuration to finalize the installation.</p>');
INSERT INTO cmn_home (id, iso3_language, home_title, main_content) VALUES (nextval('cmn_home_id_seq'), 'fra', 'Order Manager (configuration en cours)', '<p>Ceci est une nouvelle instance de l&apos;outil Order Manager. Connectez-vous et allez dans le menu Administation &gt; Configuration pour finaliser l&apos;installation</p>');

-- HLSEVENCMN-91
INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'), 'hl7v2_length_fault_level', 'WARNING');
INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'), 'hl7v2_structure_fault_level', 'ERROR');
INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'), 'hl7v2_datatype_fault_level', 'ERROR');
INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'), 'hl7v2_value_fault_level', 'WARNING');
