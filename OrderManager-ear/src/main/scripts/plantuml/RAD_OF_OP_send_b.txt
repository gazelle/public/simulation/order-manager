@startuml
hide footbox
title Order Filler Management (RAD-3)
participant OF as "OF (Order Manager)" #99FF99
participant OP as "OP (SUT)"
OF -> OP : OMG^O19^OMG_O19 (New)
activate OP
OP --> OF : ORG^O20^ORG_O20
deactivate OP
OF -> OP : OMG^O19^OMG_O19 \n(Cancel or Status update)
activate OP
OP --> OF : ACK^O19^ACK
deactivate OP
@enduml
