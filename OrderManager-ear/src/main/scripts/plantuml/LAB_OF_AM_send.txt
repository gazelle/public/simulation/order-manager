@startuml
hide footbox
title Work Order Management (LAB-4)
participant OF as "OF (Order Manager)" #99FF99
participant AM as "AM (SUT)"
OF -> AM : OML (021, O33, O35)
activate AM
AM --> OF : ORL (O22, O34, O35)
deactivate AM
@enduml
