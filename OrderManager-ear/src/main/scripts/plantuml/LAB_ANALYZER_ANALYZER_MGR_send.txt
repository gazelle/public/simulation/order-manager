@startuml
hide footbox
title AWOS Status Change (LAB-29)
participant A as "ANALYZER\n(Order Manager)" #99FF99
participant AM as "ANALYZER_MGR\n(SUT)"
A -> AM : OUL^R22^OUL_R22
activate AM
AM --> A : ACK^R22^ACK
deactivate AM
@enduml
