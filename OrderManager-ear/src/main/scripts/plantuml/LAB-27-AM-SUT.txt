@startuml
hide footbox
title Query for AWOS (LAB-27)
participant A as "ANALYZER\n(Order Manager)" #99FF99
participant AM as "ANALYZER_MGR\n(SUT)"
A -> AM : QBP^Q11^QBP_Q11
activate AM
AM --> A : RSP^K11^RSP_K11
deactivate AM
@enduml
