@startuml
hide footbox
title AWOS Broadcast (LAB-29)
participant AM as "ANALYZER_MGR\n(Order Manager)" #99FF99
participant A as "ANALYZER\n(SUT)"
AM -> A : OML^O33^OML_O33
activate A
A --> AM : ORL^O34^ORL_O34
deactivate A
@enduml
