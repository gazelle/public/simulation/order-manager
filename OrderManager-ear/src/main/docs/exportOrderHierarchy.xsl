<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
    xmlns:office="urn:oasis:names:tc:opendocument:xmlns:office:1.0"
    xmlns:table="urn:oasis:names:tc:opendocument:xmlns:table:1.0"
    xmlns:text="urn:oasis:names:tc:opendocument:xmlns:text:1.0"
    exclude-result-prefixes="office table text xd"
    version="1.0">
    <xd:doc scope="stylesheet">
        <xd:desc>
            <xd:p><xd:b>Created on:</xd:b> Oct 2, 2012</xd:p>
            <xd:p><xd:b>Author:</xd:b> aberge</xd:p>
            <xd:p>This stylesheet is used to convert openoffice spreadsheet into an XML file valid for OrderHierarchy.xsd</xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:output method = "xml" indent = "yes" encoding = "UTF-8" omit-xml-declaration = "no"/>
        
    <xsl:template match="/">
        <?xml-stylesheet type="text/xsl" href="./orderHierarchy.xsl"?>
        <OrderHierarchy>
            <!-- Process all tables -->
            <xsl:apply-templates select="//table:table[2]"/>
        </OrderHierarchy>
    </xsl:template>
    
    <xsl:template match="table:table">
        <xsl:for-each select="table:table-row">
            <xsl:if test="position()>2">
                <xsl:element name="Order">
                    <xsl:attribute name="universalServiceID"><xsl:value-of select="table:table-cell[1]"/></xsl:attribute>
                    <xsl:element name="RequestedProcedure">
                        <xsl:attribute name="codingSchemeDesignator"><xsl:value-of select="table:table-cell[6]"/></xsl:attribute>
                        <xsl:attribute name="code"><xsl:value-of select="table:table-cell[4]"/></xsl:attribute>
                        <xsl:element name="ScheduledProcedureStep">
                            <xsl:attribute name="description"><xsl:value-of select="table:table-cell[7]"/></xsl:attribute>
                            <xsl:attribute name="modality"><xsl:value-of select="table:table-cell[8]"/></xsl:attribute>
                            <xsl:element name="ProtocolItem">
                                <xsl:attribute name="code"><xsl:value-of select="table:table-cell[9]"/></xsl:attribute>
                                <xsl:attribute name="codeMeaning"><xsl:value-of select="table:table-cell[10]"/></xsl:attribute>
                                <xsl:attribute name="codingSchemeDesignator"><xsl:value-of select="table:table-cell[11]"/></xsl:attribute>
                            </xsl:element>
                        </xsl:element>
                    </xsl:element>
                </xsl:element>
            </xsl:if>
        </xsl:for-each>
    </xsl:template>
</xsl:stylesheet>