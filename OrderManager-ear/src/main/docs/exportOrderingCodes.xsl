<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
    xmlns:office="urn:oasis:names:tc:opendocument:xmlns:office:1.0"
    xmlns:table="urn:oasis:names:tc:opendocument:xmlns:table:1.0"
    xmlns:text="urn:oasis:names:tc:opendocument:xmlns:text:1.0"
    exclude-result-prefixes="office table text xd xmlns"
    version="1.0">
    <xd:doc scope="stylesheet">
        <xd:desc>
            <xd:p><xd:b>Created on:</xd:b> Oct 2, 2012</xd:p>
            <xd:p><xd:b>Author:</xd:b> aberge</xd:p>
            <xd:p>This stylesheet is used to convert openoffice spreadsheet into an XML file valid for SVS</xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:output method = "xml" indent = "yes" encoding = "UTF-8" omit-xml-declaration = "no"/>
        
    <xsl:template match="/">
        <xsl:element name="RetrieveValueSetResponse">
            <xsl:attribute name="xsi" namespace="xmlns">
                <xsl:text>http://www.w3.org/2001/XMLSchema-instance</xsl:text>
            </xsl:attribute>
           <xsl:attribute name="schemaLocation" namespace="xsi">
               <xsl:text>urn:ihe:iti:svs:2008 ../../SVS.Support.Materials.v2/schema/IHE/SVS.xsd</xsl:text>
           </xsl:attribute>
            <xsl:element name="ValueSet">
                <xsl:attribute name="id">
                    <xsl:text>put value set id here</xsl:text>
                </xsl:attribute>
                <xsl:attribute name="displayName">
                    <xsl:text>put value set display name here</xsl:text>
                </xsl:attribute>
                <xsl:attribute name="version">
                    <xsl:text>put value set version here</xsl:text>
                </xsl:attribute>
                <xsl:element name="ConceptList">
                    <xsl:attribute name="lang" namespace="xml">
                        <xsl:text>en-US</xsl:text>
                    </xsl:attribute>
                    <xsl:apply-templates select="//table:table[2]"/>
                </xsl:element>
            </xsl:element>
        </xsl:element>
    </xsl:template>
    
    <xsl:template match="table:table">
        <xsl:for-each select="table:table-row">
            <xsl:if test="position()>2">
                <xsl:element name="Concept">
                    <xsl:attribute name="code"><xsl:value-of select="table:table-cell[1]"/></xsl:attribute>
                    <xsl:attribute name="displayName"><xsl:value-of select="table:table-cell[2]"/></xsl:attribute>
                    <xsl:attribute name="codeSystem"><xsl:value-of select="table:table-cell[3]"/></xsl:attribute>
                </xsl:element>
            </xsl:if>
        </xsl:for-each>
    </xsl:template>
</xsl:stylesheet>